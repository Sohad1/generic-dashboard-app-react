import {
  useClearStatisticsMutation,
  useLogoutMutation,
} from "@/api/auth/useAuth";
import { useTranslation } from "react-i18next";
import GenericDeleteModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
type Props = {
  open: boolean;
  handleClose: () => void;
};

function ClearStatisticsModal({ open, handleClose }: Props) {
  const { t } = useTranslation();
  const { mutate: handleSubmit, isLoading: isSubmitting } =
    useClearStatisticsMutation(handleClose);

  return (
    <GenericDeleteModal
      title="Are you sure you want to Clear Statistics?"
      open={open}
      handleClose={handleClose}
      handleSubmit={handleSubmit}
      isSubmittingLoading={isSubmitting}
      submitButtonText={"clear"}
    />
  );
}

export default ClearStatisticsModal;
