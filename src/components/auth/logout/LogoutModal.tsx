import { useLogoutMutation } from "@/api/auth/useAuth";
import { useTranslation } from "react-i18next";
import GenericDeleteModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
type Props = {
  open: boolean;
  handleClose: () => void;
};

function LogoutModal({ open, handleClose }: Props) {
  const { t } = useTranslation();
  const { mutate: handleSubmit, isLoading: isSubmitting } = useLogoutMutation();

  return (
    <GenericDeleteModal
      title="Are you sure you want to log out?"
      open={open}
      handleClose={handleClose}
      handleSubmit={handleSubmit}
      isSubmittingLoading={isSubmitting}
      submitButtonText={t("logout")}
    />
  );
}

export default LogoutModal;
