import React from "react";
import ChangePasswordForm from "./ChangePasswordForm";
import { useTranslation } from "react-i18next";
import useChangePasswordLogic from "./hooks/useChangePasswordLogic";
import GenericModal from "@/components/items/modal/GenericModal/GenericModal";

function ChangePasswordModal({
  open,
  handleClose,
  handleCloseModel,
}: {
  open: boolean;
  handleClose: () => void;
  handleCloseModel: () => void;
}) {
  const { t } = useTranslation();
  const {
    initialValues,
    isSubmitting,
    handleSubmit,
    // openModal: open,
    // handleCloseModal,
  } = useChangePasswordLogic();
  return (
    <GenericModal title={t("change")} childrenHaveActions={true} open={open}>
      <ChangePasswordForm
        {...{
          initialValues,
          handleCancel: handleClose,
          handleClose: handleCloseModel,
          isSubmitting,
          handleSubmit,
        }}
      />
    </GenericModal>
  );
}

export default ChangePasswordModal;
