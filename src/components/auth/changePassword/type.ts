export type TChangePasswordInitialValues = {
  oldPassword: string
  newPassword: string
  confirmPassword: string
}
export type TChangePasswordValues = {
  oldPassword: string
  newPassword: string
}
