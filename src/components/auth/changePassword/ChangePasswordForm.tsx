import React from "react";
import { Form, Formik } from "formik";
import { Grid } from "@mui/material";
import PasswordField from "@/components/items/inputField/PasswordField/PasswordField";
import useChangePasswordValidationSchema from "./hooks/validationSchema";
import FormActions from "@/components/items/form/FormActions/FormActions";
import { TChangePasswordInitialValues } from "./type";
import { Box, Button, Stack, TableCell } from "@mui/material";
import { useTranslation } from "react-i18next";
type Props = {
  initialValues: TChangePasswordInitialValues;
  isSubmitting: boolean;
  handleClose: () => void;
  handleCancel: () => void;
  handleSubmit: (values: TChangePasswordInitialValues) => void;
};

function ChangePasswordForm({
  initialValues,
  isSubmitting,
  handleCancel,
  handleClose,
  handleSubmit,
}: Props) {
  const validationSchema = useChangePasswordValidationSchema();
  const { t } = useTranslation();
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, touched, errors, handleChange }) => (
        <Form>
          <Grid
            container
            justifyContent={"center"}
            alignItems={"center"}
            spacing={2}
          >
            {/* {Inputs.map((input) => (
              <Grid item xs={12} md={10}>
                <PasswordField
                  name={input.name}
                  fullWidth
                  label={input.label}
                  value={values[input.name]}
                  onChange={handleChange}
                  error={touched[input.name] && !!errors[input.name]}
                  helperText={touched[input.name] && errors[input.name]}
                />
              </Grid>
            ))} */}
            <Grid item xs={12} md={10}>
              <PasswordField
                name="oldPassword"
                fullWidth
                label="oldPassword"
                value={values.oldPassword}
                onChange={handleChange}
                error={touched.oldPassword && !!errors.oldPassword}
                helperText={touched.oldPassword && errors.oldPassword}
              />
            </Grid>
            <Grid item xs={12} md={10}>
              <PasswordField
                name="newPassword"
                fullWidth
                label="newPassword"
                value={values.newPassword}
                onChange={handleChange}
                error={touched.newPassword && !!errors.newPassword}
                helperText={touched.newPassword && errors.newPassword}
              />
            </Grid>
            <Grid item xs={12} md={10}>
              <PasswordField
                name="confirmPassword"
                fullWidth
                label="confirmPassword"
                value={values.confirmPassword}
                onChange={handleChange}
                error={touched.confirmPassword && !!errors.confirmPassword}
                helperText={touched.confirmPassword && errors.confirmPassword}
              />
            </Grid>
            <FormActions
              isSubmitting={isSubmitting}
              submitButtonText={"change"}
              handleCancel={handleCancel}
            />
            <Button
              // variant="contained"
              // color={'secondary'}
              onClick={handleClose}
            >
              {t("common.cancel")}
            </Button>
          </Grid>
        </Form>
      )}
    </Formik>
  );
}

export default ChangePasswordForm;
