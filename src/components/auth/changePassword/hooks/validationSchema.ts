import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

const useChangePasswordValidationSchema = () => {
  const { t } = useTranslation()

  const ValidationSchema = Yup.object({
    oldPassword: Yup.string()
      .min(6, `${t('form.minChar')}`)
      .required(`${t('form.required')}`),
    newPassword: Yup.string()
      .min(6, `${t('form.minChar')}`)
      .required(`${t('form.required')}`),
    confirmPassword: Yup.string()
      .required(`${t('form.required')}`)
      .oneOf([Yup.ref('newPassword')], `${t('form.matchPassword')}`),
  })

  return ValidationSchema
}

export default useChangePasswordValidationSchema
