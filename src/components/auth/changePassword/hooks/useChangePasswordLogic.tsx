import { useChangePasswordMutation } from '@/api/auth/useAuth'
import { TChangePasswordInitialValues } from '../type'

const useChangePasswordLogic = () => {
  const initialValues: TChangePasswordInitialValues = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  }

  const { mutate: handleSubmit, isLoading: isSubmitting } =
    useChangePasswordMutation()

  return {
    initialValues,
    isSubmitting,
    handleSubmit,
  }
}

export default useChangePasswordLogic
