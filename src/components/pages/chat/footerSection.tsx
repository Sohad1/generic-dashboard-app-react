import React from 'react'
import SendIcon from '@mui/icons-material/Send'
import MenuMessage from './menuMessage'
import { Box, Button } from '@mui/material'
import { TMessage_templates } from '@/api/message_templates/type'
import SendImageMessageModal from './SendImageMessageModal'
import { useTranslation } from 'react-i18next'

function FooterSection({
  addMessage,
  MessagesList,
  message,
  setMessage,
  isLoadingSendMessage,
  userId,
}: {
  userId: string
  addMessage: (m: string) => void
  MessagesList: TMessage_templates[]
  message: string
  setMessage: any
  isLoadingSendMessage: boolean
}) {
  const { t } = useTranslation()
  return (
    <Box sx={BOX_STYLES}>
      <MenuMessage setMessage={setMessage} MessagesList={MessagesList} />
      <Box sx={TEXTAREA_STYLES}>
        <textarea
          placeholder={t('pages.chat.writeHere')}
          autoFocus
          value={message}
          onKeyUp={(e: any) => {
            if (e.which == 13 && e.shiftKey) {
              setMessage(e.target.value)
            } else if (e.which == 13) {
              addMessage(message)
            } else setMessage(e.target.value)
          }}
          onChange={(e: any) => {
            setMessage(e.target.value)
          }}
        />
      </Box>
      <SendImageMessageModal userId={userId} />
      <Button
        sx={SEND_BUTTON_STYLES}
        onClick={() => addMessage(message)}
        disabled={isLoadingSendMessage}
      >
        <SendIcon sx={{ transform: 'scaleX(1)' }} />
      </Button>
    </Box>
  )
}

export default FooterSection

const BOX_STYLES = {
  position: 'absolute',
  width: 'calc(100% - 30px)',
  background: '#f8f8f8',
  height: '60px',
  bottom: '5px',
  borderRadius: '15px',
  display: 'flex',
  alignItems: 'center',
  padding: '5px',
  boxShadow: '0px 0px 5px #ccc',
  gap: '5px',
  margin: '0px 15px',
}

const TEXTAREA_STYLES = {
  flex: 1,
  textarea: {
    height: '45px',
    width: '100%',
    border: 'none !important',
    padding: '10px 5px',
    background: 'none',
    fontSize: '16px',
    resize: 'none',
    '&:focus': {
      border: 'none !important',
      outline: 'none !important',
      // background: '#f1f1f1',
    },
    '&::-webkit-scrollbar': {
      width: '0px',
    },
  },
}

const SEND_BUTTON_STYLES = {
  color: 'rgb(243 198 45)',
  background: '#1e1740',
  minWidth: '40px',
  height: '40px',
  boxShadow: '0px 0px 10px #aaa',
  borderRadius: '10px',
  margin: '0 5px',
  '&:hover': {
    background: '#3a3657',
  },
}
