import React, { useState, useRef } from 'react'
import AttachFileIcon from '@mui/icons-material/AttachFile'
import { Box, Button, IconButton } from '@mui/material'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'
import RepeatIcon from '@mui/icons-material/Repeat'
import { useSendMessageMutation } from '@/api/chats/useChatQueries'
import { useTranslation } from 'react-i18next'
type Props = {
  userId: string
}

function SendImageMessageModal({ userId }: Props) {
  const [image, setImage] = useState(null)
  const [file, setFile] = useState(null)
  const [openModal, setOpenModal] = useState(false)
  const inputRef = useRef(null)

  const handleChangeImage = (e: any) => {
    e.preventDefault()
    if (e.target.files && e.target.files[0]) {
      let x = URL.createObjectURL(e.target.files[0])
      setFile(e.target.files[0])
      setImage(x)
      setOpenModal(true)
    }
  }
  const onButtonClick = (e: any) => {
    e.preventDefault()
    inputRef.current.click()
  }

  const { mutate: sendMessage, isLoading: isLoadingSendMessage } =
    useSendMessageMutation(userId!, () => setOpenModal(false))

  const addMessage = () => {
    if (image) {
      sendMessage({ to_user: userId, image: file })
    }
  }
  const { t } = useTranslation()
  return (
    <Box>
      <IconButton onClick={onButtonClick}>
        <AttachFileIcon />
      </IconButton>
      <input
        ref={inputRef}
        type="file"
        id="image"
        accept="image/*"
        onChange={handleChangeImage}
        style={{ display: 'none' }}
      />
      <GenericModal
        open={openModal}
        handleClose={() => setOpenModal(false)}
        childrenHaveActions={true}
      >
        <Box
          sx={{
            img: { maxHeight: '400px', objectFit: 'contain', maxWidth: '100%' },
            width: '400px',
            textAlign: 'center',
          }}
        >
          <img src={image} alt="send-message" />
        </Box>
        <Box
          sx={{
            position: 'relative',
            top: '-25px',
            width: '100%',
            textAlign: 'center',
          }}
        >
          <IconButton onClick={onButtonClick} sx={SEND_BUTTON_STYLES}>
            <RepeatIcon />
          </IconButton>
        </Box>
        <Box sx={BUTTONS_STYLES}>
          <Button onClick={() => setOpenModal(false)} sx={{ color: '#444' }}>
            {t('common.cancel')}
          </Button>

          <Button
            onClick={addMessage}
            sx={SEND_BUTTON_STYLES}
            disabled={isLoadingSendMessage}
          >
            {t('pages.chat.send')}
          </Button>
        </Box>
      </GenericModal>
    </Box>
  )
}

export default SendImageMessageModal

const BUTTONS_STYLES = {
  display: 'flex',
  justifyContent: 'center',
  gap: '20px',
}

const SEND_BUTTON_STYLES = {
  color: 'rgb(243 198 45)',
  background: '#1e1740',
  '&:hover': {
    background: '#3a3657',
  },
}
