import React from 'react'
import { Box, Button, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

function ChatCard({ item }: { item: any }) {
  const navigate = useNavigate()
  return (
    <Button
      sx={{
        width: '100%',
        justifyContent: 'start',
        textTransform: 'capitalize',
        color: '#999',
      }}
      onClick={() => navigate(`/chat/${item.id}`)}
    >
      <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
        <Box
          sx={{
            borderRadius: '50%',
            background: '#f4f4f4',
            width: '60px',
            height: '60px',
          }}
          component="img"
          src={item.img}
        />
        <Box sx={{ textAlign: 'left' }}>
          <Typography
            sx={{
              color: '#333',
              fontWeight: 600,
              fontSize: '16px',
              marginBottom: '5px',
            }}
          >
            {item.message.name}
          </Typography>
          <Typography
            sx={{
              width: '100%',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
              whiteSpace: 'nowrap',
              color: '#999',
              fontSize: '12px',
            }}
          >
            {item.message.message}
          </Typography>
        </Box>
      </Box>
    </Button>
  )
}

export default ChatCard
