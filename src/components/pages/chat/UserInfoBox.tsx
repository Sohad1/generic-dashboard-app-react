import { TUser } from '@/api/auth/type'
import React from 'react'
import MailOutlineIcon from '@mui/icons-material/MailOutline'
import PhoneIcon from '@mui/icons-material/Phone'
import PublicIcon from '@mui/icons-material/Public'
import { Box, Typography } from '@mui/material'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { usePutUserSupportFilesMutation } from '@/api/chats/useChatQueries'
import { useTranslation } from 'react-i18next'

type Props = {
  user: TUser & { files_support: 0 | 1 }
  userId: string
}

function UserInfoBox({ user, userId }: Props) {
  const { mutate: putSupportFile, isLoading: isLoadingPutSupportFile } =
    usePutUserSupportFilesMutation(userId!)
  const { t } = useTranslation()
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '70px 20px 20px',
      }}
    >
      <Box
        sx={{
          borderRadius: '50%',
          background: '#f4f4f4',
          width: '160px',
          height: '160px',
        }}
        component="img"
        src={user.img}
      />
      <Typography sx={{ fontWeight: 700, fontSize: '25px', margin: '10px 0' }}>
        {user?.name}
      </Typography>
      <Box
        sx={{
          display: 'flex',
          gap: '25px',
          textAlign: 'center',
          margin: '20px 0 20px',
        }}
      >
        <Box
          sx={{
            background: '#eee',
            padding: '5px 15px',
            borderRadius: '10px',
          }}
        >
          {user?.referral_id}

          {/* <Typography>Referred ID</Typography> */}
        </Box>
        <Box
          sx={{
            background: '#eee',
            padding: '5px 15px',
            borderRadius: '10px',
          }}
        >
          {user?.status
            ? t('pages.chat.status-active')
            : t('pages.chat.status-pause')}
        </Box>
      </Box>
      <Box
        sx={{ marginBottom: '20px', color: '#333' }}
        onClick={() =>
          putSupportFile({
            user_id: userId,
            files_support: user.files_support ? 0 : 1,
          })
        }
      >
        <LoadingButton
          buttonText={
            !user.files_support
              ? t('pages.chat.supportFile')
              : t('pages.chat.unSupportFile')
          }
          isSubmitting={isLoadingPutSupportFile}
          color="inherit"
        />
      </Box>

      <Box
        sx={{
          // width: '100%',
          display: 'flex',
          flexDirection: 'column',
          gap: '15px',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            gap: '10px',
          }}
        >
          <MailOutlineIcon />
          <Typography> {user?.email}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            gap: '10px',
          }}
        >
          <PhoneIcon />
          <Typography> {user?.phone}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            gap: '10px',
          }}
        >
          <PublicIcon />
          <Typography>{user?.country?.country_enName}</Typography>
        </Box>
      </Box>
    </Box>
  )
}

export default UserInfoBox
