import {
  useGetChatPageQuery,
  useSendMessageMutation,
} from '@/api/chats/useChatQueries'
import React, { useState } from 'react'
import SingleChatBox from '@/components/pages/chat/SingleChatBox'
import FooterSection from '@/components/pages/chat/footerSection'
import InfoIcon from '@mui/icons-material/Info'
import UserInfoBox from '@/components/pages/chat/UserInfoBox'
import { Grid, Box, Typography, IconButton } from '@mui/material'
import { useTranslation } from 'react-i18next'
import Loading from '@/components/others/loading'

type Props = {
  userId: string
}

function SingleChatSections({ userId }: Props) {
  const { t } = useTranslation()
  const [showInfo, setShowInfo] = useState(false)

  const { data, isLoading, isError } = useGetChatPageQuery(userId)
  const { mutate: sendMessage, isLoading: isLoadingSendMessage } =
    useSendMessageMutation(userId)

  const [message, setMessage] = useState('')

  const addMessage = (m: string) => {
    if (m.length > 0) {
      sendMessage({ to_user: userId, message: m })
      setMessage('')
    }
  }
  if (isLoading)
    return (
      <Grid
        item
        md={9}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Loading />
      </Grid>
    )
  if (isError) return <></>
  return (
    <>
      <Grid item md={showInfo ? 6 : 9} sx={{ transition: '.5s' }}>
        <Box
          sx={{
            width: '100%',
            height: '100%',
            padding: '10px',
          }}
        >
          <Box
            sx={{
              height: '60px',
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              gap: '5px',
              padding: '0px 15px',
              justifyContent: 'space-between',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: '5px',
              }}
            >
              <Box
                sx={{
                  borderRadius: '50%',
                  background: '#f4f4f4',
                  width: '40px',
                  height: '40px',
                }}
              ></Box>
              <Typography sx={{ opacity: 0.8 }}>
                {t('pages.chat.conversationWith')}
                {/* Conversation with */}{' '}
              </Typography>
              <Box sx={{ fontWeight: 600 }}>{data?.user?.name}</Box>
            </Box>
            <IconButton onClick={() => setShowInfo(!showInfo)}>
              <InfoIcon />
            </IconButton>
          </Box>

          <Box
            sx={{
              background: '#f1f1f1',
              borderRadius: '25px 25px 0 0',
              // padding: '15px',
              height: 'calc(100vh - 72px)',
              position: 'relative',
              overflow: 'hidden',
            }}
          >
            {/* {isLoading || !data ? (
                    'loading...'
                  ) : (
                    <> */}
            <SingleChatBox List={data?.messages} />
            <FooterSection
              userId={userId}
              addMessage={addMessage}
              MessagesList={data?.message_templates}
              {...{ message, setMessage, isLoadingSendMessage }}
            />
            {/* </>
                  )} */}
          </Box>
        </Box>
      </Grid>
      <Grid item lg={showInfo ? 3 : 0}>
        <UserInfoBox userId={userId} user={data?.user} />
      </Grid>
    </>
  )
}

export default SingleChatSections
