import React from 'react'
import { Box } from '@mui/material'
import { TMessage } from '@/api/chats/type'

function SingleChatBox({ List }: { List: TMessage[] }) {
  return (
    <Box
      sx={{
        height: 'calc(100vh - 138px)',
        overflowY: 'scroll',
        padding: '15px 15px 15px',
        justifyContent: 'flex-end',
        '&::-webkit-scrollbar': {
          width: '0px',
        },
      }}
    >
      {List.map((item) => (
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: item.from_user === 'support' ? 'end' : 'flex-start',
          }}
        >
          {item.has_file ? (
            <>
              <img
                src={item.files[0].file_url}
                alt={item.files[0].file_name}
                style={{
                  maxWidth: '50%',
                  width: 'fit-content',
                  height: '100px',
                }}
              />
            </>
          ) : (
            <Box
              sx={{
                background: item.from_user === 'support' ? '#666' : '#f9f9f9',
                color: item.from_user === 'support' ? '#fff' : '#333',
                borderRadius:
                  item.from_user === 'support'
                    ? '15px 0px 15px 15px'
                    : '0px 15px 15px 15px',
                padding: '10px',
                margin: '2px 0',
                maxWidth: '50%',
                width: 'fit-content',
              }}
            >
              {item.message}
            </Box>
          )}
        </Box>
      ))}
    </Box>
  )
}

export default SingleChatBox
