import * as React from 'react'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import CheckboxInput from '@/components/items/inputField/Checkbox/Checkbox'
import { Grid, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import ExcelFileDragDropField from '@/components/items/inputField/ExcelFileDragDropField'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ pt: 2 }}>{children}</Box>}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

export default function DetailsCardTabs({
  setFieldValue,
  handleChange,
  values,
  touched,
  errors,
}: {
  setFieldValue: any
  values: any
  handleChange: any
  touched: any
  errors: any
}) {
  const [value, setValue] = React.useState(0)

  const handleChangeTab = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }
  const { t } = useTranslation()

  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={value}
          onChange={handleChangeTab}
          aria-label="basic tabs example"
        >
          <Tab label="Details" {...a11yProps(0)} />
          <Tab label="Excel File" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <CheckboxInput
          name={'is_multiple'}
          label={t('pages.products.cards.isMultiple')}
          checked={values.is_multiple}
          onChange={(e, checked) => {
            setFieldValue('is_multiple', checked)
            setFieldValue('card_number', null)
            setFieldValue('card_code', null)
            setFieldValue('exp_date', null)
            setFieldValue('details', null)
          }}
        />
        {values.is_multiple ? (
          <Grid container spacing={2}>
            <Grid item lg={4} xs={12}>
              <TextField
                name="card_number"
                fullWidth
                label={t('pages.products.cards.card_number')}
                value={values.card_number}
                onChange={handleChange}
                error={touched.card_number && !!errors.card_number}
                helperText={touched.card_number && errors.card_number}
              />
            </Grid>
            <Grid item lg={4} xs={12}>
              <TextField
                name="card_code"
                fullWidth
                label={t('pages.products.cards.card_code')}
                value={values.card_code}
                onChange={handleChange}
                error={touched.card_code && !!errors.card_code}
                helperText={touched.card_code && errors.card_code}
              />
            </Grid>
            <Grid
              item
              lg={4}
              xs={12}
              sx={{ display: { lg: 'flex' }, alignItems: 'center', gap: '5px' }}
            >
              <Box>{t('pages.products.cards.exp_date')}</Box>
              <TextField
                name="exp_date"
                fullWidth
                value={values.exp_date}
                onChange={handleChange}
                error={touched.exp_date && !!errors.exp_date}
                helperText={touched.exp_date && errors.exp_date}
                type="date"
              />
            </Grid>
          </Grid>
        ) : (
          <>
            <Grid item lg={4} xs={12}>
              <TextField
                name="details"
                fullWidth
                label={t('pages.products.cards.details')}
                value={values.details}
                onChange={handleChange}
                error={touched.details && !!errors.details}
                helperText={touched.details && errors.details}
                multiline
              />
            </Grid>
          </>
        )}
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ExcelFileDragDropField
          name="excel_file"
          label="Excel File"
          error={touched.excel_file && !!errors.excel_file}
          file={values.excel_file}
          handleChangeFile={(f: any) => setFieldValue('excel_file', f)}
          type="add"
        />
      </TabPanel>
    </Box>
  )
}
