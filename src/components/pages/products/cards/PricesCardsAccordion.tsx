import * as React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { TProductCategory } from "@/api/products/prices/type";
import { Box, Grid } from "@mui/material";
import { useTranslation } from "react-i18next";
export default function PricesCardsAccordion({
  prices,
}: {
  prices: TProductCategory[];
}) {
  const { t } = useTranslation();
  return (
    <Accordion sx={{ marginBottom: "20px" }}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography>{t("pages.products.form.category")}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container spacing={2}>
          {prices.map((item) => (
            <Grid
              item
              lg={3}
              md={4}
              xs={12}
              key={item.id}
              sx={{ textAlign: "center" }}
            >
              <Box sx={{ marginBottom: "5px", fontWeight: 600 }}>
                {item.cards_count}
              </Box>
              <Box>{item.value}</Box>
            </Grid>
          ))}
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
}
