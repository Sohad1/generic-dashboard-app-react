import React from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Grid } from '@mui/material'
import { Form, Formik } from 'formik'
import CompaniesAutoComplete from '../../AutoCompletes/CompaniesAutoComplete'
import ProductsAutoComplete from '../../AutoCompletes/ProductsAutoComplete'
import PricesAutoComplete from '../../AutoCompletes/PricesAutoComplete'
import { TCategory } from '@/api/products/products/type'
import { TFilterCardsParams } from '@/api/products/cards/type'

function FilterCardsForm({
  handleFilterOptions,
}: {
  handleFilterOptions: (option: TFilterCardsParams) => void
}) {
  const { t } = useTranslation()
  const initialValues: {
    company: { id: string; name: string; data: TCategory[] }
    category: { id: string; name: string; data: { id: string; name: string }[] }
    price: { id: string; name: string }
  } = {
    company: null,
    category: null,
    price: null,
  }
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleFilterOptions({
          company_id: values.company?.id,
          category_id: values.category?.id,
          price_id: values.price?.id,
        })
      }}
    >
      {({ values, setFieldValue }) => (
        <Form>
          <Box sx={{ display: 'flex', gap: '15px' }} marginBottom="20px">
            <Grid container spacing={2}>
              <Grid item lg={4} xs={12}>
                <CompaniesAutoComplete
                  name="company"
                  value={values.company}
                  setFieldValue={setFieldValue}
                />
              </Grid>

              <Grid item lg={4} xs={12}>
                <ProductsAutoComplete
                  data={values.company?.data}
                  name="category"
                  value={values.category}
                  setFieldValue={setFieldValue}
                />
              </Grid>
              <Grid item lg={4} xs={12}>
                <PricesAutoComplete
                  name="price"
                  value={values.price}
                  data={values.category?.data}
                  setFieldValue={setFieldValue}
                />
              </Grid>
            </Grid>
            <Box>
              <Button variant="contained" type="submit" sx={{ height: '98%' }}>
                {t('common.search')}
              </Button>
            </Box>
          </Box>
        </Form>
      )}
    </Formik>
  )
}

export default FilterCardsForm
