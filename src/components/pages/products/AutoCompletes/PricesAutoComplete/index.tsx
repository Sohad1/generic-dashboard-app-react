import { TProductCategory } from '@/api/products/prices/type'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  data: { id: string; name: string }[]
  value: TOption
  name: string
  error?: boolean
  errorMessage?: string | string[]
  setFieldValue: (field: string, value: any) => void
}
function PricesAutoComplete({
  data,
  value,
  name,
  error,
  errorMessage,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()
  let prices = data?.map((item) => ({ id: item.id, name: item.name }))
  return (
    <AutoCompleteField
      label={t('pages.products.form.prices')}
      name={name}
      options={prices || []}
      value={value ? Object(value) : null}
      onChange={(_, value) => {
        setFieldValue(name, value)
      }}
      fullWidth
      error={error}
      errorMessage={errorMessage ? String(errorMessage) : undefined}
      disabled={data ? false : true}
    />
  )
}

export default PricesAutoComplete
