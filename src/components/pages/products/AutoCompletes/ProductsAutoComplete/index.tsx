import { TProductCategory } from '@/api/products/prices/type'
import { TCategory } from '@/api/products/products/type'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  data: TCategory[]
  value: TOption
  name: string
  error?: boolean
  errorMessage?: string | string[]
  setFieldValue: (field: string, value: any) => void
}
function ProductsAutoComplete({
  data,
  value,
  name,
  error,
  errorMessage,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()
  let products = data?.map((item) => ({
    id: item.id,
    name: item.name,
    data: item.prices?.map((item: TProductCategory) => ({
      id: item.id,
      name: item.value,
    })),
  }))
  return (
    <AutoCompleteField
      label={t('pages.products.form.product')}
      name={name}
      options={products || []}
      value={value}
      onChange={(_, value) => {
        setFieldValue(name, value)
        setFieldValue('price', null)
      }}
      fullWidth
      error={error}
      errorMessage={errorMessage ? String(errorMessage) : undefined}
      disabled={data ? false : true}
    />
  )
}

export default ProductsAutoComplete
