import { useGetGroupsQuery } from '@/api/products/groups/useGroupsQueries'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import React from 'react'
import { useTranslation } from 'react-i18next'
type Props = {
  value: TOption
  name: string
  error?: boolean
  errorMessage?: string | string[]
  setFieldValue: (field: string, value: any) => void
}
function GroupsAutoComplete({
  value,
  name,
  error,
  errorMessage,
  setFieldValue,
}: Props) {
  const { t, i18n } = useTranslation()
  const { data, isLoading } = useGetGroupsQuery()
  let groups = data?.response?.groups?.data?.map((item) => ({
    id: item.id,
    name: i18n.language === 'ar' ? item.name_ar : item.name_en,
  }))
  return (
    <AutoCompleteField
      label={t('pages.products.form.group')}
      name={name}
      options={groups || []}
      value={value}
      onChange={(_, value: any) => {
        setFieldValue(name, value)
        setFieldValue('group_id', value?.id)
      }}
      fullWidth
      error={error}
      errorMessage={errorMessage ? String(errorMessage) : undefined}
      loading={isLoading}
    />
  )
}

export default GroupsAutoComplete
