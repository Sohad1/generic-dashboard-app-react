import { TCategory } from '@/api/categories/type'
import { useGetCompaniesQuery } from '@/api/products/companies/useCompaniesQueries'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  value: TOption
  name: string
  error?: boolean
  errorMessage?: string | string[]
  setFieldValue: (field: string, value: any) => void
}
function CompaniesAutoComplete({
  value,
  name,
  error,
  errorMessage,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()
  const { data, isLoading } = useGetCompaniesQuery()
  let companies = data?.response?.companies?.data?.map((item) => ({
    id: item.id,
    name: item.name,
    data: item.categories?.map((item: TCategory) => ({
      id: item.id,
      name: item.name,
      prices: item.prices,
    })),
  }))
  return (
    <AutoCompleteField
      label={t('pages.products.form.companies')}
      name={name}
      options={companies || []}
      value={value ? Object(value) : null}
      onChange={(_, value) => {
        setFieldValue(name, value)
        setFieldValue('category', null)
      }}
      fullWidth
      error={error}
      errorMessage={errorMessage ? String(errorMessage) : undefined}
      loading={isLoading}
    />
  )
}

export default CompaniesAutoComplete
