import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

const useUserValidationSchema = (type: 'add' | 'edit') => {
  const { t } = useTranslation()

  const AddUserValidationSchema = Yup.object({
    name: Yup.string().required(`${t('form.required')}`),
    password: Yup.string()
      .min(6, `${t('form.minChar')}`)
      .required(`${t('form.required')}`),
    email: Yup.string()
      .email(`${t('form.valEmail')}`)
      .required(`${t('form.required')}`),
    phone: Yup.string().required(`${t('form.required')}`),
  })

  const EditUserValidationSchema = Yup.object({
    name: Yup.string().required(`${t('form.required')}`),
    email: Yup.string()
      .email(`${t('form.valEmail')}`)
      .required(`${t('form.required')}`),
    phone: Yup.string().required(`${t('form.required')}`),
  })

  return type === 'add' ? AddUserValidationSchema : EditUserValidationSchema
}

export default useUserValidationSchema
