import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { TUserTypes } from "@/api/users/type";
import { useGetUsersInfiniteQuery } from "@/api/users/useUsersQueries";

const useUsersLogic = (userType: TUserTypes) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchUsers, setSearchUsers] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [selectedUser, setSelectedUser] = useState({ id: 0, name: "" });
  const [openDeleteModal, setOpenDeleteModal] = useState(false);

  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetUsersInfiniteQuery(userType, rowsPerPage);

  const handleSearchUsers = (value: string) => {
    console.log(value);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddUser = () => {
    navigate(`/users/addUser?type=${userType}`);
  };
  const handleEditUser = (row: TRow) => {
    navigate(`/users/editUser/${row.id}?type=${userType}`);
  };

  const handleDeleteUser = (user: TRow) => {
    setOpenDeleteModal(true);
    setSelectedUser({
      name: String(user.name),
      id: Number(user.id),
    });
  };

  const handleCloseDeleteModal = () => setOpenDeleteModal(false);

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchUsers,
    isError,
    isLoading,
    selectedUser,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchUsers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchUsers,
    handleAddUser,
    handleEditUser,
    handleDeleteUser,
    handleCloseDeleteModal,
  };
};

export default useUsersLogic;
