import { HeadCell } from '@/type/Table'
import { EUserTypes, TUserTypes } from '@/api/users/type'
import { useTranslation } from 'react-i18next'
const useTableHeader = (userType: TUserTypes) => {
  const { t } = useTranslation()
  const headCells: HeadCell[] = [
    {
      id: 'id',
      numeric: true,
      disablePadding: false,
      label: 'id',
    },
    {
      id: 'name',
      numeric: false,
      disablePadding: false,
      label: t('pages.users.form.name'),
    },
    {
      id: 'email',
      numeric: false,
      disablePadding: false,
      label: t('pages.users.form.email'),
    },
    {
      id: 'phone',
      numeric: false,
      disablePadding: false,
      label: t('pages.users.form.phone'),
    },
  ]

  const roleIdCell = {
    id: 'roleId',
    numeric: false,
    disablePadding: false,
    label: t('pages.users.form.role'),
  }

  if (userType === EUserTypes.user) {
    headCells.push(roleIdCell)
  }

  return headCells
}

export default useTableHeader
