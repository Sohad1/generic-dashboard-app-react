import React from "react";
import { TUserTypes } from "@/api/users/type";
import useUsersLogic from "../hooks/useUsersLogic";
import UsersTable from "./UsersTable";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import { useTranslation } from "react-i18next";
import { Box, Button, Stack } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import DeleteUserModal from "../usersActions/deleteUserModal";
import Loading from "@/components/others/loading";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

type Props = {
  userType: TUserTypes;
};
function UsersList({ userType }: Props) {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);

  const {
    data,
    // error,
    page,
    rowsPerPage,
    searchUsers,
    isError,
    isLoading,
    selectedUser,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchUsers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchUsers,
    handleAddUser,
    handleEditUser,
    handleDeleteUser,
    handleCloseDeleteModal,
  } = useUsersLogic(userType);
  if (isLoading) return <Loading />;
  if (isError) return <></>;
  return (
    <Box>
      <DeleteUserModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          user: selectedUser,
          userType,
        }}
      />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchUsers}
            setValue={setSearchUsers}
            debouncedFunction={handleSearchUsers}
            label={t("common.search")}
            size="small"
          />
          <Can I="store" a="dashboard_api_users">
            <Button
              variant="contained"
              size="small"
              endIcon={<AddIcon />}
              onClick={handleAddUser}
            >
              {t(`common.Adduser`)}
            </Button>
          </Can>
        </Stack>

        {data?.pages[0]?.total === 0 ? (
          <Box component="h2" sx={TEXT_STYLES}>
            {t("pages.users.noUsers")}
          </Box>
        ) : (
          <UsersTable
            {...{
              data: data!,
              userType,
              page,
              order,
              orderBy,
              setOrder,
              setOrderBy,
              rowsPerPage,
              handleChangePage,
              handleChangeRowsPerPage,
              handleEditUser,
              handleDeleteUser,
            }}
          />
        )}
      </Box>
    </Box>
  );
}

export default UsersList;

const TEXT_STYLES = {
  opacity: 0.5,
  margin: "60px 20px",
  textAlign: "center",
};
