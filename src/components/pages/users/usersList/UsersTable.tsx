import React, { Dispatch, SetStateAction } from "react";

import { getComparator, stableSort } from "@/util/table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TableCell } from "@mui/material";
import useTableHeader from "../hooks/useTableHeader";
import { EUserTypes, TUserTypes } from "@/api/users/type";
import { InfiniteData } from "@tanstack/react-query";
import { TOrder, TRow } from "@/type/Table";

import { TGenericPaginationResponse } from "@/api/type";
import { ChangeEvent } from "react";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
type Props = {
  data: InfiniteData<TGenericPaginationResponse<any> | undefined>;
  userType: TUserTypes;
  page: number;
  order: TOrder;
  orderBy: string;
  rowsPerPage: number;
  setOrder: Dispatch<SetStateAction<TOrder>>;
  setOrderBy: Dispatch<SetStateAction<string>>;
  handleChangePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerPage: (event: ChangeEvent<HTMLInputElement>) => void;
  handleEditUser: (user: TRow) => void;
  handleDeleteUser: (user: TRow) => void;
};

function UsersTable({
  data,
  userType,
  page,
  rowsPerPage,
  order,
  orderBy,
  setOrder,
  setOrderBy,
  handleChangePage,
  handleChangeRowsPerPage,
  handleEditUser,
  handleDeleteUser,
}: Props) {
  const headCells = useTableHeader(userType);
  const ability = useContext(AbilityContext);
  return (
    <GenericTable
      page={page}
      count={data?.pages[0]?.total!}
      rowsPerPage={rowsPerPage}
      order={order}
      orderBy={orderBy}
      setOrder={setOrder}
      setOrderBy={setOrderBy}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      rows={
        data?.pages
          ?.map((page) => page?.data)
          .reduce((previous, current) => [...previous!, ...current!]) as TRow[]
      }
      headCells={headCells}
    >
      {stableSort(
        data?.pages
          .map((page) => page?.data)
          .reduce((previous, current) => [...previous!, ...current!]) as TRow[],
        getComparator(order, orderBy)
      )
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row, index) => {
          return (
            <StyledTableRow key={index} hover tabIndex={-1}>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{row.phone}</TableCell>
              {userType === EUserTypes.user ? (
                <TableCell>{row.role_id}</TableCell>
              ) : null}

              <TableCell>
                <TableActions
                  edit={() =>
                    ability.can("update", "dashboard_api_users") &&
                    handleEditUser(row)
                  }
                  delete={() =>
                    ability.can("destroy", "dashboard_api_users") &&
                    handleDeleteUser(row)
                  }
                />
              </TableCell>
            </StyledTableRow>
          );
        })}
    </GenericTable>
  );
}

export default UsersTable;
