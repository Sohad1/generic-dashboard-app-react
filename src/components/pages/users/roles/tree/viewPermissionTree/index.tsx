import { Collapse, Box, Stack, Typography } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { TPermissionTreeByRole } from "@/api/permissions/type";
import { useState } from "react";
const ViewPermissionTree = ({
  permission,
}: {
  permission: TPermissionTreeByRole;
}) => {
  const [collapse, setCollapse] = useState(false);
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"start"} alignItems={"center"}>
        {collapse ? (
          <ExpandMoreIcon
            color={"primary"}
            onClick={() => setCollapse(false)}
          />
        ) : (
          <ChevronRightIcon
            color={"primary"}
            onClick={() => setCollapse(true)}
          />
        )}
        <Typography color={"primary"}>{permission.name}</Typography>
      </Stack>
      <Collapse in={collapse}>
        <Stack direction={"column"} sx={{ ml: 2 }} spacing={1}>
          {permission.children.map((childPermission, index) => (
            <Typography key={index}>{childPermission.name}</Typography>
          ))}
        </Stack>
      </Collapse>
    </Box>
  );
};

export default ViewPermissionTree;
