import { Collapse, Box, Stack } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { TPermission } from "@/pages/roles/type";
import CheckboxInput from "@/components/items/inputField/Checkbox/Checkbox";
const PermissionTree = ({
  permission,
  index,
  setFieldValue,
}: {
  permission: TPermission;
  index: number;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void;
}) => {
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"start"} alignItems={"center"}>
        {permission.checked ? <ExpandMoreIcon /> : <ChevronRightIcon />}
        <CheckboxInput
          name={`permissions.${index}.checked`}
          label={permission.name}
          checked={permission.checked}
          onChange={(e, checked) => {
            setFieldValue(`permissions.${index}.checked`, checked);
            // permission.children.forEach((childPermission, _index) => {
            //   setFieldValue(
            //     `permissions.${index}.children.${_index}.checked`,
            //     checked
            //   );
            // });
          }}
        />
      </Stack>
      <Collapse in={permission.checked}>
        <Stack direction={"column"} sx={{ ml: 4 }}>
          {permission.children.map((childPermission, _index) => (
            <CheckboxInput
              key={_index}
              name={`permissions.${index}.children.${_index}.checked`}
              label={childPermission.name}
              checked={childPermission.checked}
              onChange={(e, checked) => {
                setFieldValue(
                  `permissions.${index}.children.${_index}.checked`,
                  checked
                );
              }}
            />
          ))}
        </Stack>
      </Collapse>
    </Box>
  );
};

export default PermissionTree;
