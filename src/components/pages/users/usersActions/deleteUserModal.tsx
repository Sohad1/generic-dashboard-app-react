import React from 'react'
import { useTranslation } from 'react-i18next'
import { TUserTypes } from '@/api/users/type'
import { useDeleteUserMutation } from '@/api/users/useUsersQueries'
import GenericModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'

type Props = {
  userType: TUserTypes
  open: boolean
  handleClose: () => void
  user: { id: number; name: string }
}
function DeleteUserModal({ userType, user, open, handleClose }: Props) {
  const { t } = useTranslation()

  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteUserMutation(userType, handleClose)

  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${user.name}`,
        handleClose,
        handleSubmit: () => deleteMutate(user.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t('common.delete')!,
      }}
    />
  )
}

export default DeleteUserModal
