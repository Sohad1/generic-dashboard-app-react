import React from 'react'
import { Form, Formik } from 'formik'
import { Grid, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import PasswordField from '@/components/items/inputField/PasswordField/PasswordField'
import useUserValidationSchema from '../hooks/useValidationSchema'
import { EUserTypes, TUserTypes } from '@/api/users/type'
import { useNavigate } from 'react-router-dom'
import {
  useAddUserMutation,
  useEditUserMutation,
} from '@/api/users/useUsersQueries'
import { TUserForm } from '@/pages/users/AddEditUser/type'
import FormActions from '@/components/items/form/FormActions/FormActions'
import RolesAutoComplete from './RolesAutoComplete'

type Props = {
  initialValues: TUserForm
  userType: TUserTypes
  userId: string | undefined
}
function UserForm({ initialValues, userType, userId }: Props) {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const validationSchema = useUserValidationSchema(userId ? 'edit' : 'add')

  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddUserMutation(userType)
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditUserMutation(userType)
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        userId ? editMutate({ body: values }) : addMutate({ body: values })
      }}
      validationSchema={validationSchema}
    >
      {({
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        setFieldValue,
      }) => (
        <Form>
          <Grid container alignItems={'center'} spacing={2}>
            {userType === EUserTypes.user ? (
              <Grid item xs={12} md={6} lg={4}>
                <RolesAutoComplete
                  value={values.role}
                  setFieldValue={setFieldValue}
                  error={touched.name && !!errors.name}
                  errorMessage={touched.name && errors.name}
                />
              </Grid>
            ) : null}
            <Grid item xs={12} md={6} lg={4}>
              <TextField
                name="name"
                fullWidth
                label={t('pages.users.form.name')}
                value={values.name}
                onChange={handleChange}
                error={touched.name && !!errors.name}
                helperText={touched.name && errors.name}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <TextField
                name="email"
                fullWidth
                label={t('pages.users.form.email')}
                value={values.email}
                onChange={handleChange}
                error={touched.email && !!errors.email}
                helperText={touched.email && errors.email}
              />
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <TextField
                name="phone"
                fullWidth
                label={t('pages.users.form.phone')}
                value={values.phone}
                onChange={handleChange}
                error={touched.phone && !!errors.phone}
                helperText={touched.phone && errors.phone}
              />
            </Grid>
            {!userId ? (
              <Grid item xs={12} md={6} lg={4}>
                <PasswordField
                  name="password"
                  fullWidth
                  label={t('pages.users.form.password')}
                  value={values.password}
                  onChange={handleChange}
                  error={touched.password && !!errors.password}
                  helperText={touched.password && errors.password}
                />
              </Grid>
            ) : null}
            <FormActions
              isSubmitting={isAddLoading || isEditLoading}
              submitButtonText={userId ? t('common.edit') : t('common.add')}
              handleCancel={() => navigate(`/users?type=${userType}`)}
            />
          </Grid>
        </Form>
      )}
    </Formik>
  )
}

export default UserForm
