import { useGetRolesQuery } from '@/api/roles/useRolesQueries'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  value: TOption
  setFieldValue: (field: string, value: TOption) => void
  error: boolean
  errorMessage: string
}

function RolesAutoComplete({
  value,
  error,
  errorMessage,
  setFieldValue,
}: Props) {
  const { data: roles, isLoading } = useGetRolesQuery()
  const { t } = useTranslation()

  const options = roles?.response.roles.data.map((item) => {
    const role: TOption = { id: item.id, name: item.name }
    return role
  }) || ['']
  return (
    <AutoCompleteField
      fullWidth
      name="role"
      label={t('pages.users.form.role')}
      onChange={(_, value) => setFieldValue('role', value)}
      {...{ value, error, errorMessage, options, loading: isLoading }}
    />
  )
}

export default RolesAutoComplete
