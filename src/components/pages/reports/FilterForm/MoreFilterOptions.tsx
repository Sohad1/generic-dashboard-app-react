import React from 'react'
import { Menu, Fade, Button, Box } from '@mui/material'
import { useTranslation } from 'react-i18next'
import MoreFilterOptionsForm from './MoreFilterOptionsForm'
import { TReportsTypes } from '@/api/reports/type'
import { TFilterInitialValues } from '../type'
import useFilterFormLogic from './hooks/useFilterFormLogic'

type Props = {
  handleChange: (e: any) => void
  values: TFilterInitialValues
  handleSubmit: (values: TFilterInitialValues) => void
  reportsType: TReportsTypes
  setFieldValue: (field: string, value: any) => void
}

function MoreFilterOptions({
  handleChange,
  values,
  handleSubmit,
  reportsType,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()
  const {
    anchorEl,
    openMoreOptions: open,
    handleOpenMoreOptions,
    handleCloseOpenMoreOptions,
  } = useFilterFormLogic()
  return (
    <Box>
      <Button
        sx={{ height: '98%' }}
        id="fade-button"
        aria-controls={open ? 'fade-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleOpenMoreOptions}
      >
        {t('common.more')}
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleCloseOpenMoreOptions}
        TransitionComponent={Fade}
      >
        <MoreFilterOptionsForm
          handleChange={handleChange}
          handleSubmit={(val: any) => {
            handleSubmit(val)
            handleCloseOpenMoreOptions()
          }}
          setFieldValue={setFieldValue}
          values={values}
          reportsType={reportsType}
        />
      </Menu>
    </Box>
  )
}

export default MoreFilterOptions
