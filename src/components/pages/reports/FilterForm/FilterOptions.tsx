import { Grid, Typography, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { EReportsTypes, TReportsTypes } from '@/api/reports/type'
import SelectField from '@/components/items/inputField/selectField/SelectField'
import useFilterFormLogic from './hooks/useFilterFormLogic'
import CountriesAutoComplete from '@/components/common/CountriesAutoComplete'

type Props = {
  reportsType: TReportsTypes
  values: any
  handleChange: (e: any) => void
  setFieldValue: (field: string, value: any) => void
}

function FilterOptions({
  reportsType,
  values,
  handleChange,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()

  const { OrdersStatusOptions, HaveOrdersOptions } = useFilterFormLogic()

  return (
    <Grid container spacing={2}>
      <Grid item lg={4} xs={12}>
        <CountriesAutoComplete
          name="countryIdSearch"
          multiple={false}
          value={values.countryIdSearch}
          setFieldValue={setFieldValue}
        />
      </Grid>
      {reportsType === EReportsTypes.play ? (
        <Grid item lg={8} xs={12} container spacing={2}>
          <Grid item lg={6} xs={12} sx={BOX_STYLES}>
            <Typography sx={LABEL_STYLES}>
              {t('pages.reports.form.start')}
            </Typography>
            <TextField
              type="date"
              name="startCreatedAtSearch"
              fullWidth
              value={values.startCreatedAtSearch}
              onChange={handleChange}
            />
          </Grid>
          <Grid item lg={6} xs={12} sx={BOX_STYLES}>
            <Typography sx={LABEL_STYLES}>
              {t('pages.reports.form.end')}
            </Typography>
            <TextField
              type="date"
              name="endCreatedAtSearch"
              fullWidth
              value={values.endCreatedAtSearch}
              onChange={handleChange}
            />
          </Grid>
        </Grid>
      ) : (
        <>
          <Grid item lg={4} xs={12}>
            <SelectField
              label={t('pages.reports.form.haveOrders')}
              name="haveOrders"
              onChange={handleChange}
              value={String(values.haveOrders)}
              options={HaveOrdersOptions}
            />
          </Grid>
          <Grid item lg={4} xs={12}>
            <SelectField
              label={t('pages.reports.form.ordersStatus')}
              name="ordersStatus"
              onChange={handleChange}
              value={String(values.ordersStatus)}
              options={OrdersStatusOptions}
            />
          </Grid>
        </>
      )}
    </Grid>
  )
}

export default FilterOptions

const LABEL_STYLES = {
  width: '40px',
}

const BOX_STYLES = {
  display: 'flex',
  gap: '5px',
  alignItems: 'center',
}
