import React from 'react'
import {
  Divider,
  Grid,
  Button,
  Typography,
  TextField,
  Box,
} from '@mui/material'
import { useTranslation } from 'react-i18next'
import { EReportsTypes, TReportsTypes } from '@/api/reports/type'
import useFilterFormLogic from './hooks/useFilterFormLogic'
import RadioButtons from '@/components/items/inputField/radio/RadioButtons'

type Props = {
  handleSubmit: any
  handleChange: any
  values: any
  reportsType: TReportsTypes
  setFieldValue: (field: string, value: any) => void
}

function MoreFilterOptionsForm({
  handleSubmit,
  handleChange,
  values,
  reportsType,
  setFieldValue,
}: Props) {
  const { t } = useTranslation()
  const { AccountStatusOptions } = useFilterFormLogic()
  return (
    <Box sx={MENU_STYLES}>
      {reportsType === EReportsTypes.email ? (
        <>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography component="h3" sx={TITLE_STYLES}>
                {t('pages.reports.form.accountStatus')}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <RadioButtons
                title=""
                name="account_status"
                onChange={(val) => setFieldValue('account_status', val)}
                options={AccountStatusOptions}
                value={values.account_status}
              />
            </Grid>
          </Grid>
          <Divider />
        </>
      ) : null}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="h3" sx={TITLE_STYLES}>
            {t('pages.reports.form.numberOfPoints')}
          </Typography>
        </Grid>
        <Grid item md={6} xs={6} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.min')}
          </Typography>
          <TextField
            type="number"
            fullWidth
            name="startPointsSearch"
            onChange={handleChange}
            value={values.startPointsSearch}
          />
        </Grid>
        <Grid item md={6} xs={6} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.max')}
          </Typography>
          <TextField
            type="number"
            fullWidth
            name="endPointsSearch"
            onChange={handleChange}
            value={values.endPointsSearch}
          />
        </Grid>
      </Grid>
      <Divider />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="h3" sx={TITLE_STYLES}>
            {t('pages.reports.form.numberOfPlays')}
          </Typography>
        </Grid>
        <Grid item md={6} xs={6} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.min')}
          </Typography>
          <TextField
            type="number"
            fullWidth
            name="startPlayCountSearch"
            onChange={handleChange}
            value={values.startPlayCountSearch}
          />
        </Grid>
        <Grid item md={6} xs={6} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.max')}
          </Typography>
          <TextField
            type="number"
            fullWidth
            name="endPlayCountSearch"
            onChange={handleChange}
            value={values.endPlayCountSearch}
          />
        </Grid>
      </Grid>
      <Divider />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography component="h3" sx={TITLE_STYLES}>
            {t('pages.reports.form.date')}
          </Typography>
        </Grid>
        <Grid item xs={12} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.start')}
          </Typography>
          <TextField
            type="date"
            fullWidth
            name="startCreatedAtSearch"
            onChange={handleChange}
            value={values.startCreatedAtSearch}
          />
        </Grid>
        <Grid item xs={12} sx={BOX_STYLES}>
          <Typography sx={LABEL_STYLES}>
            {t('pages.reports.form.end')}
          </Typography>
          <TextField
            type="date"
            fullWidth
            name="endCreatedAtSearch"
            onChange={handleChange}
            value={values.endCreatedAtSearch}
          />
        </Grid>
      </Grid>
      <Button
        variant="contained"
        type="submit"
        sx={{ marginTop: '10px' }}
        onClick={() => handleSubmit(values)}
      >
        {t('common.search')}
      </Button>
    </Box>
  )
}

export default MoreFilterOptionsForm

const MENU_STYLES = {
  display: 'flex',
  flexDirection: 'column',
  gap: '15px',
  width: '320px',
  input: {
    padding: '8.5px 10px !important',
  },
  '@media (max-width:350px)': {
    width: '100%',
  },
  padding: '10px 15px',
}
const TITLE_STYLES = {
  fontWeight: '600',
}

const LABEL_STYLES = {
  width: '40px',
}

const BOX_STYLES = {
  display: 'flex',
  gap: '5px',
  alignItems: 'center',
}
