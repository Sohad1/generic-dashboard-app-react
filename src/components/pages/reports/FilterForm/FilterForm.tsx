import React from 'react'
import { EReportsTypes, TReportsTypes } from '@/api/reports/type'
import FilterOptions from './FilterOptions'
import { useTranslation } from 'react-i18next'
import { Form, Formik } from 'formik'
import MoreFilterOptions from './MoreFilterOptions'
import { Box, Button } from '@mui/material'
import { TFilterInitialValues } from '../type'

type Props = {
  reportsType: TReportsTypes
  handleSubmit: (e: TFilterInitialValues) => void
  initialValues: TFilterInitialValues
}

function FilterForm({ reportsType, handleSubmit, initialValues }: Props) {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleSubmit(values)
      }}
    >
      {({ values, handleChange, setFieldValue }) => (
        <Form>
          <Box sx={{ display: 'flex', gap: '15px' }} marginBottom="20px">
            <FilterOptions
              {...{ values, handleChange, reportsType, setFieldValue }}
            />
            <Box>
              <Button variant="contained" type="submit" sx={{ height: '98%' }}>
                {t('common.search')}
              </Button>
            </Box>
            {reportsType !== EReportsTypes.play ? (
              <MoreFilterOptions
                {...{
                  handleChange,
                  handleSubmit,
                  values,
                  initialValues,
                  reportsType,
                  setFieldValue,
                }}
              />
            ) : null}
          </Box>
        </Form>
      )}
    </Formik>
  )
}

export default FilterForm
