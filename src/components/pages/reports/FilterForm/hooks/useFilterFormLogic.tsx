import { useTranslation } from 'react-i18next'
import {
  TEmailsReportsInitialValues,
  TMainReportsInitialValues,
  TPlaysReportsInitialValues,
} from '../../type'
import React from 'react'

const useFilterFormLogic = () => {
  const { t } = useTranslation()
  const HaveOrdersOptions = [
    {
      value: 0,
      text: t('pages.reports.form.all'),
    },
    {
      value: 1,
      text: t('pages.reports.form.haveOrders'),
    },
    {
      value: 2,
      text: t('pages.reports.form.haveNotOrders'),
    },
  ]
  const OrdersStatusOptions = [
    {
      value: -1,
      text: t('pages.reports.form.all'),
    },
    {
      value: 0,
      text: t('pages.reports.form.pending'),
    },
    {
      value: 1,
      text: t('pages.reports.form.acceptable'),
    },
    {
      value: 2,
      text: t('pages.reports.form.rejected'),
    },
  ]
  const AccountStatusOptions = [
    {
      value: '0',
      label: t('pages.reports.form.all'),
    },
    {
      value: '1',
      label: t('pages.reports.form.blocked'),
    },
    {
      value: '2',
      label: t('pages.reports.form.unblocked'),
    },
  ]

  const initialMainReportsValues: TMainReportsInitialValues = {
    haveOrders: 0,
    ordersStatus: -1,
    countryIdSearch: null,
  }

  const initialEmailsReportsValues: TEmailsReportsInitialValues = {
    ...initialMainReportsValues,
    account_status: 0,
  }

  const initialPlaysReportsValues: TPlaysReportsInitialValues = {
    countryIdSearch: null,
  }

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const openMoreOptions = Boolean(anchorEl)
  const handleOpenMoreOptions = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleCloseOpenMoreOptions = () => {
    setAnchorEl(null)
  }
  return {
    anchorEl,
    openMoreOptions,
    handleOpenMoreOptions,
    handleCloseOpenMoreOptions,
    initialMainReportsValues,
    initialPlaysReportsValues,
    initialEmailsReportsValues,
    HaveOrdersOptions,
    OrdersStatusOptions,
    AccountStatusOptions,
  }
}
export default useFilterFormLogic
