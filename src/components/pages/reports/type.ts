export type TPlaysReportsInitialValues = {
  countryIdSearch?: { id: number; name: string } | number
  startCreatedAtSearch?: string
  endCreatedAtSearch?: string
}

export type TMainReportsInitialValues = TPlaysReportsInitialValues & {
  haveOrders?: number
  ordersStatus?: number
  startPointsSearch?: number
  endPointsSearch?: number
  startPlayCountSearch?: number
  endPlayCountSearch?: number
}

export type TEmailsReportsInitialValues = {
  account_status?: number
} & TMainReportsInitialValues

export type TFilterInitialValues =
  | TMainReportsInitialValues
  | TPlaysReportsInitialValues
  | TEmailsReportsInitialValues
