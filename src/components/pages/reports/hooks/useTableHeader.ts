import { HeadCell } from '@/type/Table'
import { useTranslation } from 'react-i18next'
import { EReportsTypes, TReportsTypes } from '@/api/reports/type'

const useTableHeader = (reportsType: TReportsTypes) => {
  const { t } = useTranslation()
  const mainReportsHeadCell: HeadCell[] = [
    {
      id: 'userName',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.userName'),
    },
    {
      id: 'email',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.email'),
    },
    {
      id: 'phone',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.phone'),
    },
    {
      id: 'createdAt',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.createdAt'),
    },
    {
      id: 'country',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.country'),
    },
    {
      id: 'points',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.points'),
    },
    {
      id: 'plays',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.plays'),
    },
    {
      id: 'referred',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.referred'),
    },
    {
      id: 'status',
      numeric: false,
      disablePadding: false,
      label: t('pages.reports.table.status'),
    },
  ]

  const playsReportHeadCell: HeadCell[] = [
    {
      id: 'country',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.country'),
    },
    {
      id: 'plays',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.plays'),
    },
    {
      id: 'playPoints',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.playPoints'),
    },
    {
      id: 'currentUsers',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.currentUsers'),
    },
    {
      id: 'previousUsers',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.previousUsers'),
    },
    {
      id: 'playsCurrentUsers',
      numeric: true,
      disablePadding: false,
      label: t('pages.reports.table.playsCurrentUsers'),
    },
  ]

  return reportsType === EReportsTypes.main
    ? mainReportsHeadCell
    : playsReportHeadCell
}

export default useTableHeader
