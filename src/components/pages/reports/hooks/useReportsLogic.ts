import { ChangeEvent, useState } from 'react'
import { TReportsTypes } from '@/api/reports/type'
import { useGetReportsInfiniteQuery } from '@/api/reports/useReportsQueries'
import { TFilterInitialValues } from '../type'

const useReportsLogic = (
  reportsType?: TReportsTypes,
  Options?: TFilterInitialValues
) => {
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const [filterOptions, setFilterOptions] = useState(null)
  const handleSubmitFilter = (values: TFilterInitialValues) => {
    setFilterOptions({ ...filterOptions, ...values })
  }

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetReportsInfiniteQuery(reportsType, rowsPerPage, Options)

  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage })
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  return {
    data,
    error,
    page,
    rowsPerPage,
    isError,
    isLoading,
    filterOptions,
    handleSubmitFilter,
    handleChangePage,
    handleChangeRowsPerPage,
  }
}

export default useReportsLogic
