import FilterForm from './FilterForm/FilterForm'
import { EReportsTypes } from '@/api/reports/type'
import ReportsList from './reportsList/ReportsList'
import useFilterFormLogic from './FilterForm/hooks/useFilterFormLogic'
import useReportsLogic from './hooks/useReportsLogic'

function MainReports() {
  const { initialMainReportsValues: initialValues } = useFilterFormLogic()
  const { filterOptions, handleSubmitFilter: handleSubmit } = useReportsLogic()

  return (
    <>
      <FilterForm
        {...{ handleSubmit, initialValues, reportsType: EReportsTypes.main }}
      />
      <ReportsList
        {...{
          reportsType: EReportsTypes.main,
          filterOptions: {
            ...filterOptions,
            countryIdSearch: filterOptions?.countryIdSearch?.id,
          },
        }}
      />
    </>
  )
}

export default MainReports
