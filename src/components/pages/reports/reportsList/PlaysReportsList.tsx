import React from 'react'
import { TPlaysReportsInitialValues } from '../type'
import { useGetPlaysReportsQuery } from '@/api/reports/useReportsQueries'
import Loading from '@/components/others/loading'
import {
  Box,
  Paper,
  TableRow,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Table,
} from '@mui/material'
import { useTranslation } from 'react-i18next'

type Props = {
  options: TPlaysReportsInitialValues
}

function PlaysReportsList({ options }: Props) {
  const { t } = useTranslation()

  const { data, isLoading, isError } = useGetPlaysReportsQuery(options)
  if (isLoading) return <Loading />
  if (isError) return <></>
  return (
    <Box sx={{ width: '100%', overflow: 'auto' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('pages.reports.table.playCount')}</TableCell>
                <TableCell>{t('pages.reports.table.pointsSum')}</TableCell>
                <TableCell>{t('pages.reports.table.userCount')}</TableCell>
                <TableCell>
                  {t('pages.reports.table.userPreviousMonthCount')}
                </TableCell>
                <TableCell>
                  {t('pages.reports.table.userRegisterPlayThisDateCount')}
                </TableCell>
                <TableCell>
                  {t('pages.reports.table.userThisMonthCount')}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell>{data.playCount}</TableCell>
                <TableCell>{data?.pointsSum}</TableCell>
                <TableCell>{data?.userCount}</TableCell>
                <TableCell>{data?.userPreviousMonthCount}</TableCell>
                <TableCell>{data?.userRegisterPlayThisDateCount}</TableCell>
                <TableCell>{data?.userThisMonthCount}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  )
}

export default PlaysReportsList
