import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { getComparator, stableSort } from '@/util/table'
import { TableCell } from '@mui/material'
import { InfiniteData } from '@tanstack/react-query'
import { TGenericPaginationResponse } from '@/api/type'
import { EReportsTypes, TReportsTypes } from '@/api/reports/type'
import useTableHeader from '../hooks/useTableHeader'
import { ChangeEvent, useState } from 'react'
import TableActions from '@/components/items/table/genericTable/tableActions'
import moment from 'moment'
import { TOrder } from '@/type/Table'

type Props = {
  data: InfiniteData<TGenericPaginationResponse<any> | undefined>
  reportsType: TReportsTypes
  page: number
  rowsPerPage: number
  handleChangePage: (event: unknown, newPage: number) => void
  handleChangeRowsPerPage: (event: ChangeEvent<HTMLInputElement>) => void
}
function ReportsTable({
  data,
  reportsType,
  page,
  rowsPerPage,
  handleChangePage,
  handleChangeRowsPerPage,
}: Props) {
  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const headCells = useTableHeader(reportsType)
  // if (reportsType === EReportsTypes.play) return <></>

  return (
    <GenericTable
      page={page}
      count={data?.pages[0]?.total!}
      rowsPerPage={rowsPerPage}
      order={order}
      orderBy={orderBy}
      setOrder={setOrder}
      setOrderBy={setOrderBy}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      rows={
        data?.pages
          ?.map((page) => page?.data)
          .reduce((previous, current) => [...previous!, ...current!]) as any[]
      }
      headCells={headCells}
    >
      {stableSort(
        data?.pages
          .map((page) => page?.data)
          .reduce((previous, current) => [...previous!, ...current!]) as any[],
        getComparator(order, orderBy)
      )
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: any, index) => {
          return (
            <StyledTableRow key={index} hover tabIndex={-1}>
              {reportsType === EReportsTypes.main ? (
                <>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.phone}</TableCell>
                  <TableCell>
                    {moment(row.created_at).format('YYYY-MM-DD , HH:mm')}
                  </TableCell>
                  <TableCell>{row.country?.country_arName}</TableCell>
                  <TableCell>{row.user_points_history_count}</TableCell>
                  <TableCell>عدد مرات اللعب</TableCell>
                  <TableCell>{row.referred?.length}</TableCell>
                  <TableCell>{row.status}</TableCell>
                </>
              ) : null}

              <TableCell>
                <TableActions />
              </TableCell>
            </StyledTableRow>
          )
        })}
    </GenericTable>
  )
}

export default ReportsTable
