import React from 'react'
import Loading from '@/components/others/loading'
import { Box, Typography } from '@mui/material'
import { useGetUserEmailsReportsQuery } from '@/api/reports/useReportsQueries'
import { TEmailsReportsInitialValues } from '../type'
import { useTranslation } from 'react-i18next'

function EmailsBox({
  filterOptions,
}: {
  filterOptions: TEmailsReportsInitialValues
}) {
  const { data, isLoading, isError } =
    useGetUserEmailsReportsQuery(filterOptions)
  const { t } = useTranslation()
  if (isLoading) return <Loading />
  if (isError) return <></>
  return (
    <Box
      sx={{ border: '1px solid #eee', padding: '20px', borderRadius: '10px' }}
    >
      {data?.length === 0 ? (
        <Typography sx={{ textAlign: 'center', opacity: 0.6 }}>
          {t('common.noData')}
        </Typography>
      ) : (
        <> {data}</>
      )}
    </Box>
  )
}

export default EmailsBox
