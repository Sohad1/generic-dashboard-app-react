import { TReportsTypes } from '@/api/reports/type'
import useReportsLogic from '../hooks/useReportsLogic'
import Loading from '@/components/others/loading'
import ReportsTable from './ReportsTable'
import { TFilterInitialValues } from '../type'

type Props = {
  reportsType: TReportsTypes
  filterOptions: TFilterInitialValues
}

function ReportsList({ reportsType, filterOptions }: Props) {
  const {
    data,
    // error,
    page,
    rowsPerPage,
    isError,
    isLoading,
    handleChangePage,
    handleChangeRowsPerPage,
  } = useReportsLogic(reportsType, filterOptions)

  if (isLoading) return <Loading />
  if (isError) return <></>
  return (
    <ReportsTable
      {...{
        data: data!,
        reportsType,
        page,
        rowsPerPage,
        handleChangePage,
        handleChangeRowsPerPage,
      }}
    />
  )
}

export default ReportsList
