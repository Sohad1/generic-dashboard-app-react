import FilterForm from './FilterForm/FilterForm'
import EmailsBox from './reportsList/EmailsBox'
import useFilterFormLogic from './FilterForm/hooks/useFilterFormLogic'
import useReportsLogic from './hooks/useReportsLogic'
import { EReportsTypes } from '@/api/reports/type'

function EmailsReports() {
  const { filterOptions, handleSubmitFilter: handleSubmit } = useReportsLogic()
  const { initialEmailsReportsValues: initialValues } = useFilterFormLogic()

  return (
    <>
      <FilterForm
        {...{ handleSubmit, initialValues, reportsType: EReportsTypes.email }}
      />
      <EmailsBox
        filterOptions={{
          ...filterOptions,
          countryIdSearch: filterOptions?.countryIdSearch?.id,
        }}
      />
    </>
  )
}

export default EmailsReports
