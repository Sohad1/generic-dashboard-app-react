import ReportsList from './reportsList/ReportsList'
import FilterForm from './FilterForm/FilterForm'
import { EReportsTypes } from '@/api/reports/type'
import useFilterFormLogic from './FilterForm/hooks/useFilterFormLogic'
import useReportsLogic from './hooks/useReportsLogic'
import PlaysReportsList from './reportsList/PlaysReportsList'

function PlaysReports() {
  const { initialPlaysReportsValues: initialValues } = useFilterFormLogic()
  const { filterOptions, handleSubmitFilter: handleSubmit } = useReportsLogic()
  return (
    <>
      <FilterForm
        {...{ handleSubmit, initialValues, reportsType: EReportsTypes.play }}
      />
      <PlaysReportsList
        options={{
          ...filterOptions,
          countryIdSearch: filterOptions?.countryIdSearch?.id,
        }}
      />
    </>
  )
}

export default PlaysReports
