import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "#",
    },
    {
      id: "nameAr",
      numeric: false,
      disablePadding: false,
      label: t("pages.countries.table.nameAr"),
    },
    {
      id: "nameEn",
      numeric: false,
      disablePadding: false,
      label: t("pages.countries.table.nameEn"),
    },
    {
      id: "nameDe",
      numeric: false,
      disablePadding: false,
      label: t("pages.countries.table.nameDe"),
    },

    {
      id: "offer_percent",
      numeric: true,
      disablePadding: false,
      label: t("pages.countries.table.offerPercent"),
    },
    {
      id: "prize_value",
      numeric: true,
      disablePadding: false,
      label: t("pages.countries.table.prize_value"),
    },
    {
      id: "register_prize_value",
      numeric: true,
      disablePadding: false,
      label: t("pages.countries.table.register_prize_value"),
    },
  ];

  return headCells;
};

export default useTableHeader;
