import { ChangeEvent, useState } from 'react'
import { useGetCountriesQuery } from '@/api/countries/useCountriesQueries'
import { useNavigate } from 'react-router-dom'
import { TOrder } from '@/type/Table'

const useCountriesLogic = () => {
  const navigate = useNavigate()
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)
  const [searchCountries, setSearchCountries] = useState<string>('')

  const [openModal, setOpenModal] = useState(false)

  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>('')

  const { data, isLoading, isError, error } = useGetCountriesQuery()

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }
  const handleSearchCountries = (value: string) => {
    console.log(value)
  }
  const handleEditCountry = (itemId: number) =>
    navigate(`/countries/editCountry/${itemId}`)

  const handleAddCountry = () => navigate('/countries/addCountry')

  const [OpenRegisterModal, setOpenRegisterModal] = useState(false)
  const [OpenPrizeModal, setOpenPrizeModal] = useState(false)
  const [OpenDiscountModal, setOpenDiscountModal] = useState(false)

  const handleAddDiscount = () => setOpenDiscountModal(true)
  const handleAddPrize = () => setOpenPrizeModal(true)
  const handleAddRegister = () => setOpenRegisterModal(true)

  const handleCloseDiscountModal = () => setOpenDiscountModal(false)
  const handleClosePrizeModal = () => setOpenPrizeModal(false)
  const handleCloseRegisterModal = () => setOpenRegisterModal(false)

  const handleOpenModal = () => setOpenModal(true)
  const handleCloseModal = () => setOpenModal(false)

  return {
    data,
    error,
    page,
    rowsPerPage,
    isError,
    isLoading,
    openModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangePage,
    handleChangeRowsPerPage,
    searchCountries,
    setSearchCountries,
    handleSearchCountries,
    handleEditCountry,
    handleAddCountry,
    handleAddDiscount,
    handleAddPrize,
    handleAddRegister,
    handleOpenModal,
    handleCloseModal,
    OpenRegisterModal,
    handleCloseRegisterModal,
    OpenPrizeModal,
    handleClosePrizeModal,
    OpenDiscountModal,
    handleCloseDiscountModal,
  }
}

export default useCountriesLogic
