// import { useGetCountriesQuery } from '@/api/countries/useCountriesQueries'
// import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
// import React from 'react'
// import { useTranslation } from 'react-i18next'
// import { Form, Formik } from 'formik'
// import { Box, Button, Grid, Stack, TextField } from '@mui/material'
// import LoadingButton from '@/components/others/loadingButton/LoadingButton'

// type Props = {
//   type: 'offer' | 'login' | 'gift'
// }

// function PrizeForm({ type }: Props) {
//   const { data, isLoading: isCountriesLoading } = useGetCountriesQuery()
//   const { i18n, t } = useTranslation()
//   const countries = data?.map((item) => ({
//     id: item.id,
//     name: i18n.language === 'ar' ? item.country_arName : item.country_enName,
//   }))
//   const initialValues = {
//     country: { id: 0, name: '' },
//   }
//   return (
//     <Formik
//       initialValues={initialValues}
//       onSubmit={(values) => {
//         // handleSubmit(values)
//       }}
//     >
//       {({ values, handleChange, setFieldValue }) => (
//         <Form>
//           <Grid
//             container
//             justifyContent={'center'}
//             alignItems={'center'}
//             spacing={2}
//           >
//             <Grid item xs={12} md={10}>
//               <AutoCompleteField
//                 label={t('pages.reports.form.country')}
//                 name="country"
//                 options={countries || [{ id: 0, name: '' }]}
//                 value={values.country}
//                 onChange={(_, value) => {
//                   setFieldValue('country', value)
//                 }}
//                 fullWidth
//                 error={false}
//                 errorMessage=""
//                 {...{
//                   loading: isCountriesLoading,
//                   //   multiple: true,
//                 }}
//               />
//             </Grid>
//             <Grid item xs={12} md={10}>
//               <TextField fullWidth name="prize" />
//             </Grid>
//             <Grid item xs={12}>
//               <Stack direction={'row'} spacing={2}>
//                 <Button>submit</Button>
//                 {/* <Box onClick={handleSubmit}>
//                 <LoadingButton
//                   isSubmitting={isSubmittingLoading!}
//                   buttonText='add'
//                 />
//               </Box>
//               <Button variant="outlined" onClick={handleClose}>
//                 {t('common.cancel')}
//               </Button> */}
//               </Stack>
//             </Grid>
//           </Grid>
//         </Form>
//       )}
//     </Formik>
//   )
// }

// export default PrizeForm

import React from 'react'

function PrizeForm() {
  return <div>PrizeForm</div>
}

export default PrizeForm
