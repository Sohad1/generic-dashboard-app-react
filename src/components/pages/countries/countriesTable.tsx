import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { getComparator, stableSort } from '@/util/table'
import TableCell from '@mui/material/TableCell'
import useTableHeader from './hooks/useTableHeader'
import useCountriesLogic from './hooks/useCountriesLogic'
import { TCountry } from '@/api/countries/type'
import { useTranslation } from 'react-i18next'
import TableActions from '@/components/items/table/genericTable/tableActions'
import { Box, Button, Stack } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { useContext } from 'react'
import { AbilityContext, Can } from '@/libs/casl/can'
import AddRegister from '@/pages/countries/addRegister'
import AddPrize from '@/pages/countries/addPrize'
import AddDiscount from '@/pages/countries/addDiscount'

type Props = {
  data: TCountry[]
  total: number
}

function CountriesTable({ data, total }: Props) {
  const { t } = useTranslation()
  const ability = useContext(AbilityContext)
  const headCells = useTableHeader()
  const {
    page,
    rowsPerPage,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangePage,
    handleChangeRowsPerPage,
    searchCountries,
    setSearchCountries,
    handleSearchCountries,
    // handleEditCountry,
    handleAddDiscount,
    handleAddPrize,
    handleAddRegister,
    OpenRegisterModal,
    handleCloseRegisterModal,
    OpenPrizeModal,
    handleClosePrizeModal,
    OpenDiscountModal,
    handleCloseDiscountModal,
  } = useCountriesLogic()
  return (
    <Box>
      <AddRegister
        open={OpenRegisterModal}
        handleClose={handleCloseRegisterModal}
      />
      <AddPrize open={OpenPrizeModal} handleClose={handleClosePrizeModal} />
      <AddDiscount
        open={OpenDiscountModal}
        handleClose={handleCloseDiscountModal}
      />
      <Stack direction={'row'} justifyContent={'space-between'} mb={3}>
        <DebouncedTextField
          value={searchCountries}
          setValue={setSearchCountries}
          debouncedFunction={handleSearchCountries}
          label={t('common.search')}
          size="small"
        />
        <Box>
          <Can I="add_discount" a="dashboard_api_countries">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddDiscount}
              sx={{ marginLeft: '15px' }}
            >
              Add Discount
            </Button>
          </Can>
          <Can I="add_prize" a="dashboard_api_countries">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddPrize}
              sx={{ marginLeft: '15px' }}
            >
              {t('pages.countries.addPrize')}
            </Button>
          </Can>
          <Can I="add_register" a="dashboard_api_countries">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddRegister}
              sx={{ marginLeft: '15px' }}
            >
              Add Register
            </Button>
          </Can>
        </Box>
      </Stack>
      <GenericTable
        page={page}
        count={total}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data}
        headCells={headCells}
      >
        {stableSort(data, getComparator(order, orderBy))
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.country_arName}</TableCell>
                <TableCell>{row.country_enName}</TableCell>
                <TableCell>{row.country_deName}</TableCell>
                <TableCell>{row.offer_percent}</TableCell>
                <TableCell>{row.prize_value}</TableCell>
                <TableCell>{row.register_prize_value}</TableCell>

                <TableCell>
                  <TableActions />
                  {/* edit={() => handleEditCountry(row.id)} /> */}
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
    </Box>
  )
}

export default CountriesTable
