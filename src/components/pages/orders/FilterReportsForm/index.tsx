import React from 'react'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import CompaniesAutoComplete from '../../products/AutoCompletes/CompaniesAutoComplete'
import ProductsAutoComplete from '../../products/AutoCompletes/ProductsAutoComplete'
import PricesAutoComplete from '../../products/AutoCompletes/PricesAutoComplete'
import { Box, Button, Grid, TextField } from '@mui/material'
import { TCategory } from '@/api/products/products/type'
import { TOrdersReportsOptions } from '@/api/orders/type'

function FilterReportsForm({
  onSubmit,
}: {
  onSubmit: (v: TOrdersReportsOptions) => void
}) {
  const { t } = useTranslation()
  const initialValues: {
    company: { id: string; name: string; data: TCategory[] }
    category: { id: string; name: string; data: { id: string; name: string }[] }
    price: { id: string; name: string }
    orders_count: number
  } = {
    company: null,
    category: null,
    price: null,
    orders_count: null,
  }
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        onSubmit({
          company_id: values.company ? values.company?.id : null,
          category_id: values.category ? values.category?.id : null,
          price_id: values.price ? values.price?.id : null,
          orders_count: values.orders_count,
        })
      }}
    >
      {({ values, setFieldValue, handleChange }) => (
        <Form>
          <Box sx={{ display: 'flex', gap: '15px' }} marginBottom="20px">
            <Grid container spacing={2}>
              <Grid item md={3} xs={12}>
                <CompaniesAutoComplete
                  name="company"
                  value={values.company}
                  setFieldValue={setFieldValue}
                />
              </Grid>

              <Grid item md={3} xs={12}>
                <ProductsAutoComplete
                  data={values.company?.data}
                  name="category"
                  value={values.category}
                  setFieldValue={setFieldValue}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <PricesAutoComplete
                  name="price"
                  value={values.price}
                  data={values.category?.data}
                  setFieldValue={setFieldValue}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <TextField
                  name="orders_count"
                  value={values.orders_count}
                  onChange={handleChange}
                  type="number"
                  fullWidth
                  label={t('pages.orders.ordersCount')}
                />
              </Grid>
            </Grid>
            <Box>
              <Button variant="contained" type="submit" sx={{ height: '98%' }}>
                {t('common.search')}
              </Button>
            </Box>
          </Box>
        </Form>
      )}
    </Formik>
  )
}

export default FilterReportsForm
