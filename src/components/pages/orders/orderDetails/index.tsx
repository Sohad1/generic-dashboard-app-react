import React from 'react'
// import { TParams } from './hooks/type'
// import { useParams } from 'react-router-dom'
// import useOrderDetailsLogic from './hooks/useOrderDetailsLogic'
import { Grid, Typography, Box, Divider } from '@mui/material'
import PrizeTable from './prizeTable'
import ActionsButtons from './ActionsButtons'
import { TOrder } from '@/api/orders/type'
import moment from 'moment'
import { useTranslation } from 'react-i18next'

function OrderDetails({
  type = 'public',
  order,
  handleClose,
}: {
  type?: 'public' | 'paused'
  order: TOrder
  handleClose: () => void
}) {
  const { t } = useTranslation()

  return (
    <Box>
      <Box sx={HEADER_STYLES}>
        <Box sx={TITLE_STYLES}>{t('pages.order.orderDetails')}</Box>
        <ActionsButtons
          id={order.id}
          userId={order.user.id}
          type={type}
          handleCloseModal={handleClose}
        />
      </Box>
      <Box sx={{ padding: '40px 10px ' }}>
        <Grid container spacing={5}>
          <KeyValueComponent
            title={t('pages.order.orderDate')}
            value={moment(order.created_at).format('YYYY-MM-DD')}
          />
          <KeyValueComponent
            title={t('pages.order.userName')}
            value={order.user.name}
          />
          <KeyValueComponent
            title={t('pages.order.ref')}
            value={order.user.referral_id}
          />
          <KeyValueComponent
            title={t('pages.order.createDate')}
            value={moment(order.user.created_at).format('YYYY-MM-DD')}
          />
          {/* <KeyValueComponent title="بلد المستخدم" value={order.user.country} /> */}
          <KeyValueComponent
            title={t('pages.order.category')}
            value={order.price.value}
          />
          <KeyValueComponent
            title={t('pages.order.product')}
            value={order.price.category.name}
          />
          <KeyValueComponent
            title={t('pages.order.process')}
            value={order.price.category.company.name}
          />
          <KeyValueComponent
            title={t('pages.order.buyPrice')}
            value={order.price.type}
          />
        </Grid>

        <Divider sx={{ margin: '50px 0' }} />
        <Box
          sx={{
            background: '#fafafa',
            padding: '0px 0px 50px',
            borderRadius: '10px',
          }}
        >
          <Grid container spacing={5} justifyContent="center">
            <CenterTextComponent
              title={t('pages.order.tasksPoints')}
              value={order.user.tasks_sum_point}
            />
            <CenterTextComponent
              title={t('pages.order.exceptionPoints')}
              value={order.user.referred_count}
            />
            <CenterTextComponent
              title={t('pages.order.notificationPoints')}
              value="189"
            />
            <CenterTextComponent
              title={t('pages.order.currentPoints')}
              value={order.user.points.points}
            />
            <CenterTextComponent
              title={t('pages.order.accountNumber')}
              value="15"
            />
            <CenterTextComponent
              title={t('pages.order.exceptionNumber')}
              value="7"
            />
            <CenterTextComponent
              title={t('pages.order.notification')}
              value="10"
            />
          </Grid>
        </Box>

        <Divider sx={{ margin: '10px 0 50px' }} />
        {order.user?.orders ? <PrizeTable prizes={order.user?.orders} /> : null}
      </Box>
    </Box>
  )
}

export default OrderDetails

const KeyValueComponent = (item: { title: string; value: string }) => (
  <Grid
    item
    md={6}
    xs={12}
    sx={{ display: 'flex', gap: '5px', alignItems: 'center' }}
  >
    <Typography sx={{ fontSize: '16px', fontWeight: 600, opacity: 0.5 }}>
      {item.title} :
    </Typography>{' '}
    <Typography sx={{ fontSize: '16px', fontWeight: 600, opacity: 0.9 }}>
      {item.value}
    </Typography>
  </Grid>
)

const CenterTextComponent = (item: {
  title: string
  value: string | number
}) => (
  <Grid item lg={3} md={6} xs={12} sx={{ textAlign: 'center' }}>
    <Typography
      sx={{ fontSize: '20px', marginBottom: '10px', fontWeight: 600 }}
    >
      {item.value}
    </Typography>
    <Typography sx={{ fontSize: '14px', opacity: 0.8 }}>
      {item.title}
    </Typography>
  </Grid>
)
const HEADER_STYLES = {
  background: '#f1f1f1',
  display: 'flex',
  justifyContent: 'space-between',
  padding: '8px 5px',
  alignItems: 'center',
  borderRadius: '10px',
  position: 'sticky',
  top: 0,
  boxShadow: '0px 5px 10px #f8f8f8',
  zIndex: 2,
}

const TITLE_STYLES = {
  fontWeight: '600',
  fontSize: '18px',
  opacity: 0.8,
  padding: '0px 10px',
}
