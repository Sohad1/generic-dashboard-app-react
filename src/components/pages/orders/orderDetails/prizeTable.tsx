import React from 'react'
import useTableHeader from './hooks/useTableHeader'
import { Box, Typography, TableCell } from '@mui/material'
import { TPrize } from '@/api/auth/type'
import moment from 'moment'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { getComparator, stableSort } from '@/util/table'
import GenericTable from '@/components/items/table/genericTable/Table'
import useOrderDetailsLogic from './hooks/useOrderDetailsLogic'
function PrizeTable({ prizes }: { prizes: any[] }) {
  const headCells = useTableHeader()
  const {
    page,
    rowsPerPage,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangePage,
    handleChangeRowsPerPage,
  } = useOrderDetailsLogic()
  return (
    <Box>
      <Typography>الجوائز</Typography>
      <GenericTable
        page={page}
        count={prizes?.length!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          prizes
            .map((page) => page)
        }
        headCells={headCells}
      >
        {
          prizes.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.status}</TableCell>
                <TableCell> {row.price.value}</TableCell>
                <TableCell>{row.price.type}</TableCell>
                <TableCell>{row.price.point}</TableCell>
                <TableCell>{row.purchase_price}</TableCell>
                <TableCell>{row.price.category.name}</TableCell>
                <TableCell>{row.price.category.company.name}</TableCell>
                <TableCell>
                  {moment(row.created_at).format('YYYY-MM-DD')}
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
    </Box>
  )
}

export default PrizeTable
