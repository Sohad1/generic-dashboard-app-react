import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.status"),
    },
    {
      id: "category",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.category"),
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.type"),
    },
    {
      id: "pricePoints",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.pricePoints"),
    },
    {
      id: "buyPrice",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.buyPrice"),
    },
    {
      id: "prizeType",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.prizeType"),
    },
    {
      id: "company",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.company"),
    },
    {
      id: "date",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.orderDate"),
    },
  ];
  return headCells;
};

export default useTableHeader;
