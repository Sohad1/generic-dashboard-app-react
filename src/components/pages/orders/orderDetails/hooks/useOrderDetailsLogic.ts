import { TOrder } from '@/type/Table'
import { ChangeEvent, useState } from 'react'

function useOrderDetailsLogic() {
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const handleChangePage = (event: unknown, newPage: number) => {
    // if (newPage > page) fetchNextPage({ pageParam: newPage })
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }
  return {
    page,
    rowsPerPage,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangePage,
    handleChangeRowsPerPage,
  }
}

export default useOrderDetailsLogic
