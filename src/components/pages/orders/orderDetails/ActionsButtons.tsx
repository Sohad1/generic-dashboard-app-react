import React from "react";
// import DeleteIcon from '@mui/icons-material/Delete'
import ChatIcon from "@mui/icons-material/Chat";
import { IconButton, Box } from "@mui/material";
import {
  useConfirmOrderMutation,
  usePausedOrderMutation,
  useRejectOrderMutation,
} from "@/api/orders/useReportsQueries";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
function ActionsButtons({
  id,
  type,
  handleCloseModal,
  userId,
}: {
  id: string;
  type?: "public" | "paused";
  handleCloseModal: () => void;
  userId: number;
}) {
  const { t } = useTranslation();
  const { mutate: confirm, isLoading: isLoadingConfirm } =
    useConfirmOrderMutation();
  const { mutate: reject, isLoading: isLoadingReject } =
    useRejectOrderMutation();
  const { mutate: paused, isLoading: isLoadingPaused } =
    usePausedOrderMutation(handleCloseModal);

  const navigate = useNavigate();
  return (
    <Box sx={{ display: "flex", gap: "15px" }}>
      <LoadingButton
        onClick={() => confirm(id)}
        isSubmitting={isLoadingConfirm!}
        buttonText={t("pages.order.confirm")}
      />
      <LoadingButton
        onClick={() => reject(id)}
        isSubmitting={isLoadingReject!}
        buttonText={t("pages.order.reject")}
      />
      {type === "paused" ? null : (
        <LoadingButton
          onClick={() => paused(id)}
          isSubmitting={isLoadingPaused!}
          buttonText={t("pages.order.paused")}
        />
      )}
      <IconButton onClick={() => navigate(`/chat/${userId}`)}>
        <ChatIcon sx={{ fontSize: "20px" }} />
      </IconButton>
      {/* <IconButton>
        <DeleteIcon />
      </IconButton> */}
      {/* <IconButton>
        <ArrowBackIcon />
      </IconButton> */}
    </Box>
  );
}

export default ActionsButtons;
