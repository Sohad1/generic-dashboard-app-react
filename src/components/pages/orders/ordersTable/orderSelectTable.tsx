import React, { ChangeEvent, Dispatch, SetStateAction } from 'react'
import { getComparator, stableSort } from '@/util/table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { TableCell } from '@mui/material'
import Checkbox from '@mui/material/Checkbox'
import useTableHeader from './hooks/useTableHeader'
import { TOrder } from '@/type/Table'
import { TGenericPaginationResponse } from '@/api/type'
import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { Box, Stack } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { useMutateActionsOrders } from '@/api/orders/useReportsQueries'
import moment from 'moment'

type Props = {
  data: TGenericPaginationResponse<any>
  isLoading: boolean
  isError: boolean
  rowsPerPage: number
  page: number
  order: TOrder
  orderBy: string
  setOrder: Dispatch<SetStateAction<TOrder>>
  setOrderBy: Dispatch<SetStateAction<string>>
  handleChangeRowsPerPage: (event: ChangeEvent<HTMLInputElement>) => void
  handleChangePage: (event: unknown, newPage: number) => void
  handleShowOrderDetails: (order: any) => void
  searchOrders: string
  setSearchOrders: Dispatch<SetStateAction<string>>
  handleSearchOrders: (value: string) => void
}

function OrderSelectTable({
  data,
  isLoading,
  isError,
  rowsPerPage,
  page,
  order,
  orderBy,
  setOrder,
  setOrderBy,
  handleChangeRowsPerPage,
  handleChangePage,
  handleShowOrderDetails,
  searchOrders,
  setSearchOrders,
  handleSearchOrders,
}: Props) {
  const { t, i18n } = useTranslation()
  const headCells = useTableHeader()

  const [selected, setSelected] = React.useState<string[]>([])
  const isSelected = (name: string) => selected.indexOf(name) !== -1
  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = stableSort(data?.data, getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: any, index) => row.id)

      setSelected(newSelected)
      return
    }
    setSelected([])
  }

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name)
    let newSelected: string[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }
    setSelected(newSelected)
  }
  console.log(selected)

  const { mutate: downloadCSV, isLoading: isLoadingCSV } =
    useMutateActionsOrders('csv')
  const { mutate: pauseAll, isLoading: isLoadingPause } =
    useMutateActionsOrders('pause')
  const { mutate: approveAll, isLoading: isLoadingApprove } =
    useMutateActionsOrders('approve')

  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <>
      <Stack direction={'row'} justifyContent={'space-between'}>
        <DebouncedTextField
          value={searchOrders}
          setValue={setSearchOrders}
          debouncedFunction={handleSearchOrders}
          label={t('common.search')}
          size="small"
        />

        {selected.length > 0 ? (
          <Box sx={{ display: 'flex', gap: '5px' }}>
            <Box onClick={() => approveAll({ all_order_ids: selected })}>
              <LoadingButton
                buttonText="approve"
                isSubmitting={isLoadingApprove}
              />
            </Box>
            <Box onClick={() => pauseAll({ all_order_ids: selected })}>
              <LoadingButton buttonText="pause" isSubmitting={isLoadingPause} />
            </Box>
            <Box onClick={() => downloadCSV({ all_order_ids: selected })}>
              <LoadingButton
                buttonText="Export as CSV"
                isSubmitting={isLoadingCSV}
              />
            </Box>
          </Box>
        ) : null}
      </Stack>
      <Box
        sx={{
          position: 'relative',
          top: '50px',
          width: '30px',
          left: '5px',
          zIndex: 9999,
        }}
      >
        <Checkbox
          color="primary"
          indeterminate={
            selected.length > 0 && selected.length < data?.data.length
          }
          checked={
            data?.data.length > 0 && selected.length === data?.data.length
          }
          onChange={handleSelectAllClick}
          inputProps={{
            'aria-label': 'select all desserts',
          }}
        />
      </Box>

      <GenericTable
        page={page}
        count={data?.data.length!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data?.data.map((page) => page)}
        headCells={headCells}
      >
        {stableSort(
          data?.data.map((page) => page),
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            const isItemSelected = isSelected(row.id)
            const labelId = `enhanced-table-checkbox-${index}`
            return (
              <>
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell padding="checkbox">
                    <Checkbox
                      color="primary"
                      checked={isItemSelected}
                      inputProps={{
                        'aria-labelledby': labelId,
                      }}
                      onClick={(e: any) => handleClick(e, row.id)}
                    />
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.user?.confirmed_orders_count}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.user?.un_processed_orders_count}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.user?.name}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {i18n.language === 'ar'
                      ? row.user?.country?.country_arName
                      : row.user?.country?.country_enName}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.status}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.user?.referral_id}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.purchase_price}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.price?.value}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {row.price?.category?.name}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {moment(row.user?.created_at).format('YYYY-MM-DD')}
                  </TableCell>
                  <TableCell onClick={() => handleShowOrderDetails(row)}>
                    {moment(row.created_at).format('YYYY-MM-DD')}
                  </TableCell>
                  <TableCell>
                    <TableActions preview={() => handleShowOrderDetails(row)} />
                  </TableCell>
                </StyledTableRow>
              </>
            )
          })}
      </GenericTable>
    </>
  )
}

export default OrderSelectTable
