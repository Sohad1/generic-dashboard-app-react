import { Link, LinkProps } from "@mui/material";
import { FC } from "react";
import { Link as Router } from "react-router-dom";
const RouterLink: FC<LinkProps> = ({ href, children, ...props }) => {
  return (
    <Link component={Router} to={href ?? ""} {...props}>
      {children}
    </Link>
  );
};
export default RouterLink;
