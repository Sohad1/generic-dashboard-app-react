import { useState, useRef, useEffect } from 'react'
import './style.css'
import { Box } from '@mui/material'

// drag drop file component
function ExcelFileDragDropField({
  handleChangeFile,
  file,
  name,
  type = 'add',
  label,
  error,
}: {
  handleChangeFile: (file: any) => void
  file: any
  name: string
  type?: 'add' | 'edit'
  label: string
  error: boolean
}) {
  // drag state
  const [dragActive, setDragActive] = useState(false)
  const [localFile, setLocalFile] = useState(file)

  // ref
  const inputRef = useRef(null)

  useEffect(() => {
    if (typeof file === 'string') setLocalFile(file)
  }, [file])
  // handle drag events
  const handleDrag = function (e: any) {
    e.preventDefault()
    e.stopPropagation()
    if (e.type === 'dragenter' || e.type === 'dragover') {
      setDragActive(true)
    } else if (e.type === 'dragleave') {
      setDragActive(false)
    }
  }

  // triggers when file is dropped
  const handleDrop = function (e: any) {
    e.preventDefault()
    e.stopPropagation()
    setDragActive(false)
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      setLocalFile(e.dataTransfer.files[0])
      handleChangeFile(e.dataTransfer.files[0])
    }
  }
  // triggers when file is selected with click
  const handleChange = function (e: any) {
    e.preventDefault()
    if (e.target.files && e.target.files[0]) {
      setLocalFile(e.target.files[0])
      handleChangeFile(e.target.files[0])
    }
  }

  // triggers the input when the button is clicked
  const onButtonClick = (e: any) => {
    e.preventDefault()
    inputRef.current.click()
  }
  // useEffect(() => {
  //   // setLocalFile(file)
  // }, [])

  // console.log(typeof file === 'string')

  return (
    <>
      <Box className="form-file-upload" onDragEnter={handleDrag}>
        <label
          id={`label-${name}`}
          htmlFor={name}
          className={dragActive ? 'drag-active label-file' : 'label-file'}
          style={{ overflow: 'hidden', borderColor: error ? 'red' : '#cbd5e1' }}
        >
          {localFile ? (
            <Box>
              Added <b style={{ margin: '0px 3px' }}>{localFile.name}</b> file
            </Box>
          ) : (
            <div>
              <p>{label}</p>
              <button className="upload-button" onClick={onButtonClick}>
                Drag and drop or Upload
              </button>
            </div>
          )}
        </label>

        <input
          ref={inputRef}
          id={name}
          className="input-file"
          type="file"
          accept=".csv"
          onChange={handleChange}
        />

        {dragActive && (
          <div
            id="drag-file-element"
            onDragEnter={handleDrag}
            onDragLeave={handleDrag}
            onDragOver={handleDrag}
            onDrop={handleDrop}
          ></div>
        )}
      </Box>
    </>
  )
}

export default ExcelFileDragDropField
