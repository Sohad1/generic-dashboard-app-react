import { TextFieldProps } from "@mui/material";
import { Dispatch, SetStateAction, MutableRefObject } from "react";

type TProps = {
  value: string;

  setValue: Dispatch<SetStateAction<string>>;
  debouncedFunction: (value: string) => void;
} & TextFieldProps;
export default TProps;
