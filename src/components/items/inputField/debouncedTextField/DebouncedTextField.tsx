import { ChangeEvent, useEffect, useMemo, useState, useRef } from "react";
import { debounce } from "lodash";
import TextField from "@mui/material/TextField";
import TProps from "./type";

const DebouncedTextField = ({
  value,
  setValue,

  debouncedFunction,
  ...props
}: TProps) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
    debouncedChangeHandler(e.target.value);
  };
  const debouncedChangeHandler = useMemo(
    () =>
      debounce((e) => {
        debouncedFunction(e);
      }, 1000),
    []
  );
  useEffect(() => {
    return () => {
      debouncedChangeHandler.cancel();
    };
  }, [debouncedChangeHandler]);

  return (
    <TextField {...props} value={value} onChange={handleChange} autoFocus />
  );
};

export default DebouncedTextField;
