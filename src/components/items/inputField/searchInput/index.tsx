import React from "react";
import { Search, SearchIconWrapper, StyledInputBase } from "./style";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
const SearchInput = () => {
  const navigate = useNavigate();
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearchSubmit = (event: any) => {
    event.preventDefault();
    navigate(`/app_users`);
    setSearchTerm("");
  };
  return (
    <Search>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <form onSubmit={handleSearchSubmit}>
        <StyledInputBase
          placeholder="Search…"
          value={searchTerm}
          onChange={(event: any) => setSearchTerm(event.target.value)}
          inputProps={{ "aria-label": "search" }}
        />
      </form>
    </Search>
  );
};

export default SearchInput;
