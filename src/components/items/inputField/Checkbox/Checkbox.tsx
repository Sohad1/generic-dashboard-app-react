import React from 'react'
import { FormControlLabel, Checkbox, Typography } from '@mui/material'
import TFormCheckboxProps from './type'

const CheckboxInput = ({ label, ...props }: TFormCheckboxProps) => {
  return (
    <FormControlLabel
      control={
        <Checkbox {...props} inputProps={{ 'aria-label': 'controlled' }} />
      }
      label={
        <Typography variant="body1" sx={{ color: '#000000' }}>
          {label}
        </Typography>
      }
    />
  )
}

export default CheckboxInput
