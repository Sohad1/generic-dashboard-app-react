import { CheckboxProps } from "@mui/material/Checkbox";

type TFormCheckboxProps = {
  label: string;
} & CheckboxProps;
export default TFormCheckboxProps;
