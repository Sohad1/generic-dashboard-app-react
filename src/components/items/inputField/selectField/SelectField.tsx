import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

type Props = {
  label: string;
  name: string;
  value: string;
  onChange: (e: SelectChangeEvent) => void;
  error?: boolean | undefined;
  helperText?: string | false | undefined;
  options: { value: number | string; text: string }[];
};
function SelectField({
  label,
  name,
  value,
  onChange,
  error,
  helperText,
  options,
}: Props) {
  return (
    <FormControl fullWidth>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label={label}
        onChange={onChange}
        name={name}
      >
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.text}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
export default SelectField;
