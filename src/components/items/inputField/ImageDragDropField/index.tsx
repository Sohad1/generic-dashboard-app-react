import { useState, useRef, useEffect } from 'react'
import './style.css'
import { Box } from '@mui/material'

// drag drop file component
function ImageDragDropField({
  handleChangeFile,
  file,
  name,
  type = 'add',
  label,
  error,
}: {
  handleChangeFile: (file: any) => void
  file: any
  name: string
  type?: 'add' | 'edit'
  label: string
  error: boolean
}) {
  // drag state
  const [dragActive, setDragActive] = useState(false)
  const [localFile, setLocalFile] = useState(file)

  // ref
  const inputRef = useRef(null)

  useEffect(() => {
    if (typeof file === 'string') setLocalFile(file)
  }, [file])
  // handle drag events
  const handleDrag = function (e: any) {
    e.preventDefault()
    e.stopPropagation()
    if (e.type === 'dragenter' || e.type === 'dragover') {
      setDragActive(true)
    } else if (e.type === 'dragleave') {
      setDragActive(false)
    }
  }

  // triggers when file is dropped
  const handleDrop = function (e: any) {
    e.preventDefault()
    e.stopPropagation()
    setDragActive(false)
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      setLocalFile(e.dataTransfer.files[0])
      handleChangeFile(e.dataTransfer.files[0])
    }
  }
  // triggers when file is selected with click
  const handleChange = function (e: any) {
    e.preventDefault()
    if (e.target.files && e.target.files[0]) {
      let x = URL.createObjectURL(e.target.files[0])
      setLocalFile(x)
      handleChangeFile(e.target.files[0])
    }
  }

  // triggers the input when the button is clicked
  const onButtonClick = (e: any) => {
    e.preventDefault()
    inputRef.current.click()
  }
  // useEffect(() => {
  //   // setLocalFile(file)
  // }, [])

  // console.log(typeof file === 'string')

  return (
    <>
      <Box className="form-file-upload" onDragEnter={handleDrag}>
        <label
          id={`label-${name}`}
          htmlFor={name}
          className={dragActive ? 'drag-active label-file' : 'label-file'}
          style={{ overflow: 'hidden', borderColor: error ? 'red' : '#cbd5e1' }}
        >
          {localFile ? (
            <>
              {/* {type === 'add' ? ( */}
              <img
                // src={localFile}
                // src={URL.createObjectURL(file)}
                src={localFile}
                alt="upload"
                style={{ width: '100%', maxWidth: '8rem' }}
              />
              {/* ) : null} */}
              {/* {type === 'edit' ? (
              <img
                // src={localFile}
                // src={URL.createObjectURL(file)}
                src={localFile ? URL.createObjectURL(file) : file}
                alt="upload"
                style={{ width: '100%' }}
              />
            ) : null} */}
            </>
          ) : (
            <div>
              <p>{label}</p>
              <button className="upload-button" onClick={onButtonClick}>
                Drag and drop or Upload
              </button>
            </div>
          )}
        </label>

        <input
          ref={inputRef}
          type="file"
          id={name}
          className="input-file"
          accept="image/*"
          onChange={handleChange}
        />

        {dragActive && (
          <div
            id="drag-file-element"
            onDragEnter={handleDrag}
            onDragLeave={handleDrag}
            onDragOver={handleDrag}
            onDrop={handleDrop}
          ></div>
        )}
      </Box>
    </>
  )
}

export default ImageDragDropField
