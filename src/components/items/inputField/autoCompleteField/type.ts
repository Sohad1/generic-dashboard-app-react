// export type TOption =
//   | {
//       id: string | number
//       name: string
//     }
//   | string
//   | number
// export type TProps = {
//   label: string
//   name: string
//   value?: TOption
//   fullWidth?: boolean
//   onChange: (e: React.SyntheticEvent, value: TOption | any) => void
//   options: TOption[]
//   error: boolean
//   errorMessage: string
//   loading?: boolean
//   disabled?: boolean
//   // multiple?: boolean
// }

export type TOption =
  | {
      id: string | number
      name: string
    }
  | string
export type TProps = {
  label: string
  name: string
  value: TOption
  fullWidth?: boolean
  onChange: (e: React.SyntheticEvent, value: TOption) => void
  options: TOption[]
  error: boolean
  errorMessage: string
  loading?: boolean
  disabled?: boolean
}

export type TMultipleProps = {
  label: string
  name: string
  value?: TOption[] | TOption
  fullWidth?: boolean
  onChange: (e: React.SyntheticEvent, value: TOption | any) => void
  options: TOption[]
  error: boolean
  errorMessage: string
  loading?: boolean
  disabled?: boolean
}
