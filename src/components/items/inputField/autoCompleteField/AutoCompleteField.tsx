import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { TOption, TProps } from "./type";
import { useTranslation } from "react-i18next";

export default function AutoCompleteField({
  label,
  name,
  error,
  value,
  errorMessage,
  options,
  loading = false,
  disabled = false,

  ...props
}: TProps) {
  const { t } = useTranslation();
  return (
    <Autocomplete
      {...props}
      value={value}
      disablePortal
      openOnFocus
      autoHighlight
      loading={loading}
      disabled={disabled}
      loadingText={t("common.loading")}
      noOptionsText={t("common.noOptions")}
      fullWidth
      options={options}
      {...(typeof value === "object" && {
        getOptionLabel: (option: TOption) => {
          return typeof option === "object" ? option.name : null;
        },
      })}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          fullWidth
          name={name}
          error={error}
          helperText={errorMessage}
        />
      )}
    />
  );
}
