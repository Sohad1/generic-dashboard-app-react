import * as React from 'react'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
import { TOption, TMultipleProps } from './type'
import { useTranslation } from 'react-i18next'

export default function MultipleAutoCompleteField({
  label,
  name,
  error,
  value, // remove value from multiple
  errorMessage,
  options,
  loading = false,
  disabled = false,
  ...props
}: TMultipleProps) {
  const { t } = useTranslation()
  return (
    <Autocomplete
      {...props}
      multiple
      loading={loading}
      disabled={disabled}
      disablePortal
      fullWidth
      openOnFocus
      autoHighlight
      loadingText={t('common.loading')}
      noOptionsText={t('common.noOptions')}
      options={options}
      {...(typeof value === 'object' && {
        getOptionLabel: (option: TOption) => {
          return typeof option === 'object' ? option.name : null
        },
      })}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          fullWidth
          name={name}
          error={error}
          helperText={errorMessage}
        />
      )}
    />
  )
}
