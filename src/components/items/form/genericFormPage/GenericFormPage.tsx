import React from 'react'
import { Grid, Paper, Typography, IconButton } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { useNavigate } from 'react-router-dom'

type Props = {
  title: string
  children: React.ReactNode
  lg?: number
  md?: number
  xs?: number
  goBackLink: string
}

function GenericFormPage({
  title,
  children,
  lg = 6,
  md = 9,
  xs = 11,
  goBackLink,
}: Props) {
  const navigate = useNavigate()

  return (
    <Grid container justifyContent={'center'} alignItems={'center'}>
      <Grid item xs={12}>
        <Paper sx={{ padding: 2, position: 'relative' }}>
          <IconButton
            sx={{
              position: 'absolute',
            }}
            onClick={() => {
              navigate(goBackLink)
            }}
          >
            <ArrowBackIcon color="primary" />
          </IconButton>

          <Typography
            color="primary"
            textAlign="center"
            variant="h4"
            component="h4"
          >
            {title}
          </Typography>
        </Paper>
      </Grid>

      <Grid item xs={12} sx={{ mt: 3 }}>
        {children}
      </Grid>
    </Grid>
  )
}

export default GenericFormPage
