import React from 'react'
import { Grid, Button } from '@mui/material'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { useTranslation } from 'react-i18next'

type Props = {
  isSubmitting: boolean
  submitButtonText: string
  handleCancel: () => void
}

function FormActions({ isSubmitting, submitButtonText, handleCancel }: Props) {
  const { t } = useTranslation()
  return (
    <Grid item xs={12} md={12} sx={{ display: 'flex', marginBottom: 5 }}>
      <LoadingButton
        isSubmitting={isSubmitting}
        buttonText={submitButtonText}
      />
      {/* <Button sx={{ marginLeft: 1 }} onClick={handleCancel}>
        {t('common.cancel')}
      </Button> */}
    </Grid>
  )
}

export default FormActions
