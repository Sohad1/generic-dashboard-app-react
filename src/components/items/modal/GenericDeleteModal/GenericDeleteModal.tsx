import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
} from '@mui/material'
import { useTranslation } from 'react-i18next'
import { TGenericModalProps } from '../type'

function GenericDeleteModal({
  open,
  title,
  submitButtonText,
  children,
  isSubmittingLoading,
  handleSubmit,
  handleClose,
  childrenHaveActions = true,
}: TGenericModalProps) {
  const { t } = useTranslation()
  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle sx={TITLE_STYLES}>{title}</DialogTitle>
      {children ? <DialogContent>{children}</DialogContent> : null}

      {childrenHaveActions ? (
        <DialogActions sx={BUTTONS_STYLES}>
          <Stack direction={'row'} spacing={2}>
            <Box onClick={handleSubmit}>
              <LoadingButton
                // color="error"
                isSubmitting={isSubmittingLoading!}
                buttonText={submitButtonText!}
              />
            </Box>
            <Button
              // variant="contained"
              // color={'secondary'}
              onClick={handleClose}
            >
              {t('common.cancel')}
            </Button>
          </Stack>
        </DialogActions>
      ) : null}
    </Dialog>
  )
}

export default GenericDeleteModal
const TITLE_STYLES = {
  textAlign: 'center',
  margin: '20px 20px 10px',
}

const BUTTONS_STYLES = {
  display: 'flex',
  marginBottom: '20px',
  justifyContent: 'center',
}
