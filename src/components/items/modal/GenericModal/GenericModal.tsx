import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
} from '@mui/material'
import { useTranslation } from 'react-i18next'
import { TGenericModalProps } from '../type'

function GenericModal({
  open,
  title,
  submitButtonText,
  children,
  isSubmittingLoading,
  handleSubmit,
  handleClose,
  childrenHaveActions = false,
  maxWidth = false,
}: TGenericModalProps) {
  const { t } = useTranslation()
  return (
    <Dialog
      onClose={handleClose}
      open={open}
      scroll="body"
      maxWidth={maxWidth}

      // sx={{ overflow: 'visible' }}
    >
      {title ? <DialogTitle sx={TITLE_STYLES}>{title}</DialogTitle> : null}
      {children ? (
        <DialogContent>
          <Box sx={{ minWidth: 350, margin: '20px 0px 20px' }}>{children}</Box>
        </DialogContent>
      ) : null}

      {!childrenHaveActions ? (
        <DialogActions sx={BUTTONS_STYLES}>
          <Stack direction={'row'} spacing={2}>
            <Box onClick={handleSubmit}>
              <LoadingButton
                isSubmitting={isSubmittingLoading!}
                buttonText={submitButtonText!}
              />
            </Box>
            <Button variant="outlined" onClick={handleClose}>
              {t('common.cancel')}
            </Button>
          </Stack>
        </DialogActions>
      ) : null}
    </Dialog>
  )
}

export default GenericModal
const TITLE_STYLES = {
  textAlign: 'center',
  margin: '20px 20px',
}

const BUTTONS_STYLES = {
  display: 'flex',
  marginBottom: 5,
  justifyContent: 'center',
}
