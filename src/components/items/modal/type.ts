export type TGenericModalProps = {
  handleSubmit?: () => void
  handleClose?: () => void
  open: boolean
  title?: string
  submitButtonText?: string
  isSubmittingLoading?: boolean
  children?: React.ReactNode
  childrenHaveActions?: boolean
  maxWidth?: 'lg' | 'md' | false
}
