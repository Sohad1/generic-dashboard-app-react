import { HeadCell, TOrder, TRow } from "@/type/Table";
import { Dispatch, SetStateAction } from "react";

export type TEnhancedTableHeaderProps = {
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void;
  headCells: HeadCell[];
  order: TOrder;
  orderBy: string;
};

export type TGenericTableProps = {
  page: number;
  count: number;
  order: TOrder;
  orderBy: string;
  rowsPerPage: number;
  setOrder: Dispatch<SetStateAction<TOrder>>;
  setOrderBy: Dispatch<SetStateAction<string>>;
  handleChangePage: (event: unknown, newPage: number) => void;
  handleChangeRowsPerPage: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rows: TRow[];
  headCells: HeadCell[];
};
