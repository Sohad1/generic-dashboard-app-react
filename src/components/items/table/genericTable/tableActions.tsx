import { IconButton, Stack } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import PreviewIcon from '@mui/icons-material/Preview'
import DeleteIcon from '@mui/icons-material/Delete'
const TableActions = (props: {
  edit?: () => void
  delete?: () => void
  preview?: () => void
}) => {
  return (
    <Stack direction={'row'} justifyContent={'center'} spacing={1}>
      {props.preview ? (
        <IconButton color="primary" onClick={props.preview}>
          <PreviewIcon fontSize="small" />
        </IconButton>
      ) : null}
      {props.edit ? (
        <IconButton color="secondary" onClick={props.edit}>
          <EditIcon fontSize="small" />
        </IconButton>
      ) : null}
      {props.delete ? (
        <IconButton color="error" onClick={props.delete}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      ) : null}
    </Stack>
  )
}

export default TableActions
