import { useGetCountriesQuery } from '@/api/countries/useCountriesQueries'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import MultipleAutoCompleteField from '@/components/items/inputField/autoCompleteField/MultipleAutoCompleteField'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import { useTranslation } from 'react-i18next'

type Props = {
  value: TOption | TOption[]
  name: string
  multiple?: boolean
  error?: boolean
  errorMessage?: string | string[]
  setFieldValue: (field: string, value: any) => void
}

function CountriesAutoComplete({
  value,
  name,
  multiple = false,
  setFieldValue,
  errorMessage,
  error,
}: Props) {
  const { t, i18n } = useTranslation()
  const { data, isLoading } = useGetCountriesQuery()
  let countries = [{ id: 0, name: 'All Countries' }].concat(
    data?.map((item) => ({
      id: item.id,
      name: i18n.language === 'ar' ? item.country_arName : item.country_enName,
    }))
  )
  console.log(value)
  return (
    <>
      {multiple ? (
        <MultipleAutoCompleteField
          label={t('pages.reports.form.country')}
          name={name}
          options={countries}
          value={value}
          onChange={(_, value) => setFieldValue(name, value)}
          fullWidth
          error={error}
          errorMessage={errorMessage ? String(errorMessage) : undefined}
          loading={isLoading}
          {...{
            limitTags: 2,
            defaultValue: value ?? [],
          }}
        />
      ) : (
        <AutoCompleteField
          label={t('pages.reports.form.country')}
          name={name}
          options={countries}
          value={value ? Object(value) : null}
          onChange={(_, value) => setFieldValue(name, value)}
          fullWidth
          error={error}
          errorMessage={errorMessage ? String(errorMessage) : undefined}
          loading={isLoading}
        />
      )}
    </>
  )
}

export default CountriesAutoComplete
