import React from "react";
import { Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

const Forbidden = () => {
  const { t } = useTranslation();
  return (
    <Stack
      justifyContent={"centre"}
      alignItems={"center"}
      sx={{ height: "fill-available" }}
    >
      <Typography>{`${t("common.forbidden")}`}</Typography>
    </Stack>
  );
};

export default Forbidden;
