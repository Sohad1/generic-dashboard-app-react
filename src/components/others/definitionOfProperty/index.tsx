import { Typography } from "@mui/material";
import { Stack } from "@mui/system";
import { TProps } from "./type";

const DefinitionOfProperty = ({ name, value }: TProps) => {
  return (
    <Stack direction="row" spacing={1}>
      <Typography color={"primary"} sx={{ fontWeight: "bold" }}>
        {`${name} : `}
      </Typography>
      <Typography>{value}</Typography>
    </Stack>
  );
};
export default DefinitionOfProperty;
