export type TProps = {
  name: string;
  value: string | number;
};
