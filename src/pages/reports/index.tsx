import { EReportsTypes } from '@/api/reports/type'
import { useSearchParams } from 'react-router-dom'
import { Box } from '@mui/material'
import EmailsReports from '@/components/pages/reports/EmailsReports'
import MainReports from '@/components/pages/reports/MainReports'
import PlaysReports from '@/components/pages/reports/PlaysReports'

function Reports() {
  const [params] = useSearchParams()
  const type: any = params.get('type') || EReportsTypes.main
  const reportsType = Object.values(EReportsTypes).includes(type)
    ? type
    : EReportsTypes.main

  return (
    <Box>
      {reportsType === EReportsTypes.main ? <MainReports /> : null}
      {reportsType === EReportsTypes.play ? <PlaysReports /> : null}
      {reportsType === EReportsTypes.email ? <EmailsReports /> : null}
    </Box>
  )
}

export default Reports
