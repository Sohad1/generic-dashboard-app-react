import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { useRef, useEffect } from "react";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import useAd_wallsLogic from "./hooks/useAdwallsLogic";
import useTableHeader from "./hooks/useTableHeader";
import DeleteAdWallModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

const Ad_walls = () => {
  const { t } = useTranslation();

  const ability = useContext(AbilityContext);

  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchAd_walls,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchAd_walls,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchAd_walls,
    handleAddAd_walls,
    handleEditAd_walls,
    handleSelectForNavigateToPreviewPage,
    handleCloseDeleteAdWallModal,
    handleOpenDeleteAdWallModal,
    handleDeleteAdWall,
  } = useAd_wallsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"} sx={{ mb: 3 }}>
        <DebouncedTextField
          value={searchAd_walls}
          setValue={setSearchAd_walls}
          debouncedFunction={handleSearchAd_walls}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_ad_walls">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddAd_walls}
          >
            {t("pages.ad walls.addadwalls")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.ad_walls.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.ad_walls.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.ad_walls.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.collection_id}</TableCell>
                <TableCell>{row.description}</TableCell>

                <TableCell>
                  <TableActions
                    edit={() => {
                      ability.can("update", "dashboard_api_ad_walls") &&
                        handleEditAd_walls(row);
                    }}
                    preview={() =>
                      ability.can("update", "dashboard_api_ad_walls") &&
                      handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteAdWallModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteAdWallModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")} ${Ad_walls.name}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteAdWall}
        handleClose={handleCloseDeleteAdWallModal}
      />
    </Box>
  );
};

export default Ad_walls;
