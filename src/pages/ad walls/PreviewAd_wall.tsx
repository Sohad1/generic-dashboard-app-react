import { useGetAd_wallQuery } from '@/api/adwalls/useAdwallsQueries'
import { useParams, useNavigate } from 'react-router-dom'
import { Button, Paper, Typography, Box } from '@mui/material'
import { useTranslation } from 'react-i18next'
import Loading from '@/components/others/loading'
const PreviewAd_wall = () => {
  const navigate = useNavigate()
  const { id } = useParams()
  const { t } = useTranslation()
  const { data, isLoading, isError } = useGetAd_wallQuery(id)
  console.log('id', id)
  if (isLoading) return <Loading />

  if (isError) return <></>

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t('pages.ad walls.addadwalls')}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t('pages.ad walls.form.name')} : {data?.response.ad_wall.name}
      </Typography>
      <Paper variant="outlined" />
      <Typography variant="h5" color="primary" sx={{ marginTop: 5 }}>
        {t('pages.ad walls.form.status')}: {data?.response.ad_wall.status}
      </Typography>
      <Paper variant="outlined" />

      <Button
        sx={{ float: 'right' }}
        variant="outlined"
        onClick={() => {
          navigate('/ad walls')
        }}
      >
        {t('pages.categories.cancel')}
      </Button>
    </Box>
  )
}

export default PreviewAd_wall
