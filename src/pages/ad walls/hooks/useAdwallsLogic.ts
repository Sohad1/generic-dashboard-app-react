import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetAd_wallsInfiniteQuery } from "@/api/adwalls/useAdwallsQueries";
import { useNavigate } from "react-router-dom";
import { useDeleteAd_wallMutation } from "@/api/adwalls/useAdwallsQueries";
import { useQueryClient } from "@tanstack/react-query";
const useAd_wallsLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchAd_walls, setSearchAd_walls] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const queryClient = useQueryClient();
  const [AdWall, setAdWall] = useState<TRow>({});

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetAd_wallsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteAd_wallMutation();
  const handleSearchAd_walls = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddAd_walls = () => {
    navigate("/ad walls/addAd_walls");
  };
  const handleEditAd_walls = (row: TRow) => {
    navigate(`/ad walls/editAd_walls/${row.id}`);
  };
  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/ad walls/PreviewAd_wall/${id}`);
  };
  const handleCloseDeleteAdWallModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteAdWallModal = (AdWall: TRow) => {
    setAdWall(AdWall);
    setOpenDeleteModal(true);
  };
  const handleDeleteAdWall = () => {
    deleteMutate(
      { params: { id: AdWall.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["AdWall-infinite-query"]);
            handleCloseDeleteAdWallModal();
          }
        },
      }
    );
  };
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchAd_walls,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchAd_walls,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchAd_walls,
    handleAddAd_walls,
    handleEditAd_walls,
    handleSelectForNavigateToPreviewPage,
    handleCloseDeleteAdWallModal,
    handleOpenDeleteAdWallModal,
    handleDeleteAdWall,
  };
};
export default useAd_wallsLogic;
