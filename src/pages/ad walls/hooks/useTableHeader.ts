import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.ad walls.form.name"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.ad walls.form.status"),
    },
    {
      id: "necessity",
      numeric: false,
      disablePadding: false,
      label: t("pages.ad walls.form.collection"),
    },
    {
      id: "necessity",
      numeric: false,
      disablePadding: false,
      label: t("pages.Ad.form.description"),
    },
  ];
  return headCells;
};

export default useTableHeader;
