import { useTranslation } from "react-i18next";
import * as Yup from "yup";

function useValidationSchema() {
  const { t } = useTranslation();
  const schema = Yup.object().shape({
    status: Yup.string().required(t("form.required")),
    name: Yup.string().required(t("form.required")),
    collection: Yup.object({ id: Yup.number(), name: Yup.string() }).required(
      t("form.required")
    ),
  });

  return schema;
}

export default useValidationSchema;
