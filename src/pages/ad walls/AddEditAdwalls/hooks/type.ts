import { TAD_WALLS } from "@/api/adwalls/type";

export type TInitialValues = TAD_WALLS;
export type TParams = {
  Ad_wallId: string | undefined;
};
