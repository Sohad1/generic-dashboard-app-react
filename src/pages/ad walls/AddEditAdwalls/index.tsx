import { useParams } from "react-router-dom";
import { Grid, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Form, Formik } from "formik";
import { useState } from "react";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import {
  useAddAd_wallMutation,
  useEditAd_wallMutation,
  useGetAd_wallQuery,
} from "@/api/adwalls/useAdwallsQueries";
import useCollectionsLogic from "@/pages/gifts/AddEditGift/hooks";
import { TInitialValues, TParams } from "./hooks/type";
import AutoCompleteField from "@/components/items/inputField/autoCompleteField/AutoCompleteField";
import Loading from "@/components/others/loading";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import GenericFormPage from "@/components/items/form/genericFormPage/GenericFormPage";
import useValidationSchema from "./hooks/useValidation";
const AddEditAd_wall = () => {
  const [selectedCollection, setSelectedCollection] = useState<TOption>();
  const { t } = useTranslation();
  const validationSchema = useValidationSchema();
  const { collections, isCollectionLoading } = useCollectionsLogic({
    value: selectedCollection,
  });
  const { Ad_wallId } = useParams<TParams>();

  const {
    data,
    isLoading: isGetAd_wallLoading,
    isError,
  } = useGetAd_wallQuery(Ad_wallId);
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddAd_wallMutation();
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditAd_wallMutation();
  if (!!Ad_wallId) {
    if (isGetAd_wallLoading) {
      return <Loading />;
    } else if (isError) {
      return <></>;
    }
  }
  const initialValues: TInitialValues = data?.response.ad_wall
    ? { ...data.response.ad_wall }
    : {
        name: "",
        status: "",
        collection_id: null,

        description: null,
        collection: null,
      };
  console.log(data);
  return (
    <GenericFormPage
      title={
        Ad_wallId
          ? t("pages.ad walls.editadwalls")
          : t("pages.ad walls.addadwalls")
      }
      goBackLink="/ad walls"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          let data = { ...values, collection_id: values.collection.id };
          delete data.collection;
          Ad_wallId ? editMutate({ body: data }) : addMutate({ body: data });
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name"
                  fullWidth
                  label={t("pages.ad walls.form.name")}
                  value={values.name}
                  onChange={handleChange}
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  label={t("pages.Ad.form.description")}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="status"
                  fullWidth
                  label={t("pages.ad walls.form.status")}
                  value={values.status}
                  onChange={handleChange}
                  error={touched.status && !!errors.status}
                  helperText={touched.status && errors.status}
                />
              </Grid>

              <Grid item xs={12} md={6} lg={4}>
                <AutoCompleteField
                  label={t("pages.collection.collections")}
                  name="collection"
                  options={collections || [{ id: 0, name: "" }]}
                  value={values.collection}
                  onChange={(_, value) => {
                    setFieldValue("collection", value);
                    setSelectedCollection(value);
                  }}
                  fullWidth
                  error={touched.collection && !!errors.collection}
                  errorMessage=""
                  {...{
                    loading: isCollectionLoading,
                  }}
                />
              </Grid>
              <Grid
                item
                xs={12}
                md={10}
                sx={{ display: "flex", marginBottom: 5 }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={Ad_wallId ? t("common.edit") : t("common.add")}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  );
};

export default AddEditAd_wall;
