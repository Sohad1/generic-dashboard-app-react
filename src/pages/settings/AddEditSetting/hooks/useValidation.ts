import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const schema = Yup.object().shape({
    key: Yup.string().required(t('form.required')),
    value: Yup.string().required(t('form.required')),
    bool: Yup.number().required(t('form.required')),
  })

  return schema
}

export default useValidationSchema
