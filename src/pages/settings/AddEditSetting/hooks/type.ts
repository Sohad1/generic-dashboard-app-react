import { TSetting } from "@/api/settings/type";

export type TInitialValues = TSetting;

export type TParams = {
  settingId: string | undefined;
};
