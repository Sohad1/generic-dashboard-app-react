import {
  useAddSettingMutation,
  useEditSettingMutation,
  useGetSettingQuery,
} from '@/api/settings/useSettingsQueries'
import { Grid, TextField } from '@mui/material'
import { Form, Formik } from 'formik'

import Loading from '@/components/others/loading'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { TInitialValues, TParams } from './hooks/type'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import SelectField from '@/components/items/inputField/selectField/SelectField'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import useValidationSchema from './hooks/useValidation'

const AddEditSetting = () => {
  const { t } = useTranslation()
  const { settingId } = useParams<TParams>()
  const validationSchema = useValidationSchema()

  const {
    data,
    isLoading: isGetSettingLoading,
    isFetching,
    isError,
  } = useGetSettingQuery(settingId)
  const { mutate: addMutate, isLoading: isAddLoading } = useAddSettingMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditSettingMutation()
  if (!!settingId) {
    if (isGetSettingLoading || isFetching) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  console.log(settingId)
  console.log(data?.response.settings)
  const initialValues: TInitialValues = data?.response.settings
    ? { ...data.response.settings }
    : {
        key: '',
        value: '',
        bool: 1,
      }
  return (
    <GenericFormPage
      title={
        settingId
          ? t('pages.settings.editSetting')
          : t('pages.settings.addSetting')
      }
      goBackLink="/settings"
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          settingId ? editMutate({ body: values }) : addMutate({ body: values })
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid
              container
              justifyContent={'center'}
              alignItems={'center'}
              spacing={2}
            >
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="key"
                  fullWidth
                  label={t('pages.settings.key')}
                  value={values.key}
                  onChange={handleChange}
                  error={touched.key && !!errors.key}
                  helperText={touched.key && errors.key}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="value"
                  fullWidth
                  label={t('pages.settings.value')}
                  value={values.value}
                  onChange={handleChange}
                  error={touched.value && !!errors.value}
                  helperText={touched.value && errors.value}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                {/* <TextField
                            name="bool"
                            fullWidth
                            label={t("pages.settings.addEditSetting.necessity")}
                            value={values.bool}
                            onChange={handleChange}
                            error={touched.bool && !!errors.bool}
                            helperText={touched.bool && errors.bool}
                          /> */}
                <SelectField
                  name="bool"
                  label={t('pages.settings.necessity')}
                  value={String(values.bool)}
                  onChange={handleChange}
                  options={[
                    { value: 0, text: 'there is' },
                    { value: 1, text: 'there is not' },
                  ]}
                  error={touched.bool && !!errors.bool}
                />
              </Grid>
              <Grid
                item
                xs={12}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={settingId ? t('common.edit') : t('common.add')}
                />
                {/* <Button
                        sx={{ marginLeft: 1 }}
                        onClick={() => {
                          navigate("/categories");
                        }}
                      >
                        {t("common.cancel")}
                      </Button> */}
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditSetting
