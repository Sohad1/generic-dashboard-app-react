import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";

import { useNavigate } from "react-router-dom";
import { useGetSettingsInfiniteQuery } from "@/api/settings/useSettingsQueries";

const useSettingLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchSettings, setSearchSettings] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectedSetting, setSelectedSetting] = useState({ id: 0, key: "" });

  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetSettingsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchSettings = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddSetting = () => {
    navigate("/settings/addSetting");
  };
  const handleEditSetting = (row: TRow) => {
    navigate(`/settings/editSetting/${row.id}`);
  };
  const handleDeleteSetting = (row: TRow) => {
    setOpenDeleteModal(true);
    setSelectedSetting({
      key: String(row.key),
      id: Number(row.id),
    });
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchSettings,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchSettings,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchSettings,
    handleAddSetting,
    handleEditSetting,
    handleDeleteSetting,
    handleCloseDeleteModal,
    openDeleteModal,
    selectedSetting,
  };
};

export default useSettingLogic;
