import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.settings.key"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.settings.value"),
    },
    {
      id: "necessity",
      numeric: false,
      disablePadding: false,
      label: t("pages.settings.necessity"),
    },
  ];
  return headCells;
};

export default useTableHeader;
