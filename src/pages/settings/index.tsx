import React from "react";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import useSettingLogic from "./hooks/useSettingLogic";
import useTableHeader from "./hooks/useTableHeader";
import DeleteSettingModal from "./deleteSettingModal";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
const Settings = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);

  const headCells = useTableHeader();
  const {
    data,
    // error,
    page,
    rowsPerPage,
    searchSettings,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchSettings,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchSettings,
    handleAddSetting,
    handleEditSetting,
    handleDeleteSetting,
    openDeleteModal,
    handleCloseDeleteModal,
    selectedSetting,
  } = useSettingLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <DeleteSettingModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          setting: selectedSetting,
        }}
      />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchSettings}
            setValue={setSearchSettings}
            debouncedFunction={handleSearchSettings}
            label={t("common.search")}
            size="small"
          />
          <Can I="store" a="dashboard_api_settings">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddSetting}
            >
              {t("pages.settings.addSetting")}
            </Button>
          </Can>
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.settings.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.settings.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.settings.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.key}</TableCell>
                  <TableCell>{row.value}</TableCell>
                  <TableCell>{row.bool}</TableCell>
                  <TableCell>
                    <TableActions
                      edit={() => {
                        ability.can("update", "dashboard_api_settings") &&
                          handleEditSetting(row);
                      }}
                      delete={() => {
                        ability.can("destroy", "dashboard_api_settings") &&
                          handleDeleteSetting(row);
                      }}
                    />
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </GenericTable>
      </Box>
    </Box>
  );
};

export default Settings;
