import { useDeleteSettingMutation } from "@/api/settings/useSettingsQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import React from "react";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  setting: { id: number; key: string };
};
const DeleteSettingModal = ({ setting, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteSettingMutation(handleClose);

  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${setting.key}`,
        handleClose,
        handleSubmit: () => deleteMutate(setting.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t("common.remove")!,
      }}
    />
  );
};

export default DeleteSettingModal;
