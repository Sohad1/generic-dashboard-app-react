import { useDeleteCompetitionMutation } from "@/api/competitions/useCompetitionsQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  competition: { id: number; title: string };
};
const DeleteCompetitionModal = ({ competition, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteCompetitionMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${competition.title}`,
        handleClose,
        handleSubmit: () => deleteMutate(competition.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t("common.delete")!,
      }}
    />
  );
};

export default DeleteCompetitionModal;
