import {
  useAddCompetitionMutation,
  useEditCompetitionMutation,
  useGetCompetitionQuery,
} from '@/api/competitions/useCompetitionsQueries'
import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Button, Grid, Paper, Typography, TextField, Box } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { TInitialValues, TParams } from './hooks/type'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import useValidationSchema from './hooks/useValidation'

const AddEditCompetition = () => {
  const { t } = useTranslation()
  const { competitionId } = useParams<TParams>()
  const validationSchema = useValidationSchema()

  const {
    data,
    isLoading: isGetCompetitionLoading,
    isError,
  } = useGetCompetitionQuery(competitionId)
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddCompetitionMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditCompetitionMutation()
  if (!!competitionId) {
    if (isGetCompetitionLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }

  const initialValues: TInitialValues = data?.response.competition
    ? { ...data.response.competition }
    : {
        title: '',
        description: '',
        points: 0,
        hint: '',
        status: 0,
        date: '',
        image: null,
        image_url: '',
        prices: null,
      }
  console.log(competitionId)
  console.log(data?.response.competition)
  return (
    <GenericFormPage
      title={
        competitionId
          ? t('pages.competitions.addEditCompetition.editCompetition')
          : t('pages.competitions.addEditCompetition.addCompetition')
      }
      goBackLink="/competitions"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          competitionId
            ? editMutate({ body: values })
            : addMutate({ body: values })
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.title')}
                  value={values.title}
                  onChange={handleChange}
                  error={touched.title && !!errors.title}
                  helperText={touched.title && errors.title}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.description')}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="points"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.points')}
                  value={values.points}
                  onChange={handleChange}
                  error={touched.points && !!errors.points}
                  helperText={touched.points && errors.points}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="image_url"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.image_url')}
                  value={values.image_url}
                  onChange={handleChange}
                  error={touched.image_url && !!errors.image_url}
                  helperText={touched.image_url && errors.image_url}
                />
              </Grid>

              <Grid
                item
                xs={12}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={
                    competitionId ? t('common.edit') : t('common.add')
                  }
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditCompetition
