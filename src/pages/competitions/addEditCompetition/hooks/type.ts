import { TCompetition } from "@/api/competitions/type";

export type TInitialValues = TCompetition;

export type TParams = {
  competitionId: string | undefined;
};
