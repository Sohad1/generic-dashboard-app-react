import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const schema = Yup.object().shape({
    title: Yup.string().required(t('form.required')),
    description: Yup.string().required(t('form.required')),
    points: Yup.string().required(t('form.required')),
    image_url: Yup.string().required(t('form.required')),
  })

  return schema
}

export default useValidationSchema
