import {
  useActiveCompetitionMutation,
  useUnActiveCompetitionMutation,
} from "@/api/competitions/useCompetitionsQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  competition: { id: number; title: string; status: number };
};
const StatusCompetitionModal = ({ competition, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: activeMutate, isLoading: isActiveLoading } =
    useActiveCompetitionMutation(handleClose);
  const { mutate: unactiveMutate, isLoading: isUnActiveLoading } =
    useUnActiveCompetitionMutation(handleClose);
  return competition.status === 0 ? (
    <GenericModal
      {...{
        open,
        title: `Are you Sure you want to active the ${competition.title}`,
        handleClose,
        handleSubmit: () => activeMutate(competition.id),
        isSubmittingLoading: isActiveLoading,
        submitButtonText: "Active!",
      }}
    />
  ) : (
    <GenericModal
      {...{
        open,
        title: `Are you Sure you want to unactive the ${competition.title}`,
        handleClose,
        handleSubmit: () => unactiveMutate(competition.id),
        isSubmittingLoading: isUnActiveLoading,
        submitButtonText: "UnActive!",
      }}
    />
  );
};

export default StatusCompetitionModal;
