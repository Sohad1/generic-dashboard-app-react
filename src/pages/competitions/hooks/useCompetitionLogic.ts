import React from "react";
import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetCompetitionsInfiniteQuery } from "@/api/competitions/useCompetitionsQueries";

const useCompetitionLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchCompetitions, setSearchCompetitions] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openArchiveStateModal, setOpenArchiveStateModal] = useState(false);
  const [openStatusModal, setOpenStatusModal] = useState(false);
  const [selectedCompetition, setSelectedCompetition] = useState({
    id: 0,
    status: 0,
    title: "",
  });

  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetCompetitionsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchCompetitions = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddCompetition = () => {
    navigate("/competitions/addCompetition");
  };
  const handleEditCompetition = (row: TRow) => {
    navigate(`/competitions/editCompetition/${row.id}`);
  };
  const handleDeleteCompetition = (row: TRow) => {
    setOpenDeleteModal(true);
    setSelectedCompetition({
      title: String(row.title),
      id: Number(row.id),
      status: Number(row.status),
    });
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);

  const handleArchiveStateCompetition = (row: TRow) => {
    setOpenArchiveStateModal(true);
    setSelectedCompetition({
      title: String(row.title),
      id: Number(row.id),
      status: Number(row.status),
    });
  };
  const handleCloseArchiveStateModal = () => setOpenArchiveStateModal(false);

  const handleStatusCompetition = (row: TRow) => {
    setOpenStatusModal(true);
    setSelectedCompetition({
      title: String(row.title),
      id: Number(row.id),
      status: Number(row.status),
    });
  };
  const handleCloseStatusModal = () => setOpenStatusModal(false);

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCompetitions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCompetitions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCompetitions,
    handleAddCompetition,
    handleEditCompetition,
    handleDeleteCompetition,
    handleCloseDeleteModal,
    openDeleteModal,
    selectedCompetition,
    handleArchiveStateCompetition,
    handleCloseArchiveStateModal,
    openArchiveStateModal,
    handleStatusCompetition,
    handleCloseStatusModal,
    openStatusModal,
  };
};

export default useCompetitionLogic;
