import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "title",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.title"),
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.description"),
    },
    {
      id: "points",
      numeric: true,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.points"),
    },
    {
      id: "image",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.image"),
    },
    {
      id: "archive",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.archive"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.status"),
    },
  ];
  return headCells;
};

export default useTableHeader;
