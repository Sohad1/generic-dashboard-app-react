import Loading from "@/components/others/loading";
import { Avatar, Box, Button, Stack, TableCell } from "@mui/material";
import { TRow } from "@/type/Table";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";

import { useTranslation } from "react-i18next";
import useCompetitionLogic from "./hooks/useCompetitionLogic";
import useTableHeader from "./hooks/useTableHeader";
import TableActions from "@/components/items/table/genericTable/tableActions";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import DeleteCompetitionModal from "./deleteCompetitionLogic";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import ArchiveStateCompetitionModal from "./archiveCompetitionLogic";
import StatusCompetitionModal from "./statusCompetitionLogic";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

const Competitions = () => {
  const ability = useContext(AbilityContext);
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchCompetitions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCompetitions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCompetitions,
    handleAddCompetition,
    handleEditCompetition,
    handleDeleteCompetition,
    openDeleteModal,
    handleCloseDeleteModal,
    handleArchiveStateCompetition,
    openArchiveStateModal,
    handleCloseArchiveStateModal,
    selectedCompetition,
    handleStatusCompetition,
    openStatusModal,
    handleCloseStatusModal,
  } = useCompetitionLogic();
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <DeleteCompetitionModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          competition: selectedCompetition,
        }}
      />
      <ArchiveStateCompetitionModal
        {...{
          open: openArchiveStateModal,
          handleClose: handleCloseArchiveStateModal,
          competition: selectedCompetition,
        }}
      />
      <StatusCompetitionModal
        {...{
          open: openStatusModal,
          handleClose: handleCloseStatusModal,
          competition: selectedCompetition,
        }}
      />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchCompetitions}
            setValue={setSearchCompetitions}
            debouncedFunction={handleSearchCompetitions}
            label={t("common.search")}
            size="small"
          />
          <Can I="store" a="dashboard_api_competitions">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddCompetition}
            >
              {t("pages.competitions.addCompetition")}
            </Button>
          </Can>
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.competitions.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.competitions.data)
              .reduce((previous, current) => [...previous, ...current]) as any[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.competitions.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as any[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>{row.points}</TableCell>

                  <TableCell>
                    <Avatar
                      variant="rounded"
                      src={String(row.image_url)}
                      alt="competition-img"
                    />
                  </TableCell>

                  <TableCell>
                    <Box width={"75%"}>
                      {row.status === 2 ? (
                        <Button
                          variant="outlined"
                          color="success"
                          fullWidth
                          onClick={() => {
                            handleArchiveStateCompetition(row);
                          }}
                        >
                          archive
                        </Button>
                      ) : (
                        <Button
                          variant="outlined"
                          color="error"
                          fullWidth
                          onClick={() => {
                            handleArchiveStateCompetition(row);
                          }}
                        >
                          Unarchive
                        </Button>
                      )}
                    </Box>
                  </TableCell>
                  <TableCell>
                    <Box width={"75%"}>
                      {row.status === 0 ? (
                        <Button
                          variant="outlined"
                          color="error"
                          fullWidth
                          onClick={() => {
                            handleStatusCompetition(row);
                          }}
                        >
                          Unactive
                        </Button>
                      ) : (
                        <Button
                          variant="outlined"
                          color="success"
                          fullWidth
                          onClick={() => {
                            handleStatusCompetition(row);
                          }}
                        >
                          active
                        </Button>
                      )}
                    </Box>
                  </TableCell>
                  <TableCell>
                    <TableActions
                      edit={() =>
                        ability.can("update", "dashboard_api_competitions") &&
                        handleEditCompetition(row)
                      }
                      delete={() =>
                        ability.can("destroy", "dashboard_api_competitions") &&
                        handleDeleteCompetition(row)
                      }
                    />
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </GenericTable>
      </Box>
    </Box>
  );
};

export default Competitions;
