import {
  useArchiveCompetitionMutation,
  useUnArchiveCompetitionMutation,
} from "@/api/competitions/useCompetitionsQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  competition: { id: number; title: string; status: number };
};
const ArchiveStateCompetitionModal = ({
  competition,
  open,
  handleClose,
}: Props) => {
  const { t } = useTranslation();
  const { mutate: archiveMutate, isLoading: isArchiveLoading } =
    useArchiveCompetitionMutation(handleClose);
  const { mutate: unarchiveMutate, isLoading: isUnArchiveLoading } =
    useUnArchiveCompetitionMutation(handleClose);
  return competition.status === 2 ? (
    <GenericModal
      {...{
        open,
        title: `Are you Sure you want to unarchive the ${competition.title}`,
        handleClose,
        handleSubmit: () => unarchiveMutate(competition.id),
        isSubmittingLoading: isUnArchiveLoading,
        submitButtonText: "UnArchive!",
      }}
    />
  ) : (
    <GenericModal
      {...{
        open,
        title: `Are you Sure you want to archive the ${competition.title}`,
        handleClose,
        handleSubmit: () => archiveMutate(competition.id),
        isSubmittingLoading: isArchiveLoading,
        submitButtonText: "Archive!",
      }}
    />
  );
};

export default ArchiveStateCompetitionModal;
