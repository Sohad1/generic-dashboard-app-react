import { useParams } from 'react-router-dom'
import { TextField, Stack } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { Form, Formik } from 'formik'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { TInitialValues, TParams } from '../type'
import Loading from '@/components/others/loading'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import useValidation from './hook/useValidation'

import PermissionTree from '../../../components/pages/users/roles/tree/permissionTree'
import useAddUpdateLogic from './hook/useAddUpdateLogic'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'

const AddUpdateRole = () => {
  const { t } = useTranslation()
  const { roleId } = useParams<TParams>()
  const roleSchema = useValidation()
  const {
    role,
    permissions,
    permissionsParent,
    isRoleLoading,
    isPermissionsLoading,
    isPermissionParentLoading,
    isRoleFetching,
    isRoleError,
    isPermissionsError,
    isPermissionParentError,
    isAddLoading,
    isEditLoading,
    initialValues,
    handleSubmit,
  } = useAddUpdateLogic({ roleId })
  if (!!roleId) {
    if (isRoleLoading || isRoleFetching) {
      return <Loading />
    } else if (isRoleError) {
      return <></>
    }
  }
  return (
    <GenericFormPage
      title={roleId ? t('pages.roles.updateRole') : t('pages.roles.addRole')}
      goBackLink="/roles"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        enableReinitialize
        validationSchema={roleSchema}
      >
        {({
          values,
          touched,
          errors,
          setFieldTouched,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => {
          return (
            <Form>
              <Stack spacing={2}>
                <TextField
                  name="name"
                  fullWidth
                  label={t('pages.roles.form.name')}
                  value={values.name}
                  onBlur={() => {
                    setFieldTouched('name', true)
                  }}
                  onChange={handleChange}
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                />

                <AutoCompleteField
                  name="guard_name"
                  fullWidth
                  label={t('pages.roles.form.guardName')}
                  value={values.guard_name}
                  onChange={(_, value) => setFieldValue('guard_name', value)}
                  options={['web']}
                  error={touched.guard_name && !!errors.guard_name}
                  errorMessage={touched.guard_name && errors.guard_name}
                />
                <Stack sx={{ flexWrap: 'wrap' }} direction={'row'} spacing={2}>
                  {values.permissions?.map((permission, index) => (
                    <PermissionTree
                      key={index}
                      permission={permission}
                      index={index}
                      setFieldValue={setFieldValue}
                    />
                  ))}
                </Stack>
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={roleId ? t('common.edit') : t('common.add')}
                />
              </Stack>
            </Form>
          )
        }}
      </Formik>
    </GenericFormPage>
  )
}

export default AddUpdateRole
