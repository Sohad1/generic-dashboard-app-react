import { useTranslation } from "react-i18next";
import * as Yup from "yup";

const useValidation = () => {
  const { t } = useTranslation();
  const permissionSchema = Yup.object().shape({
    name: Yup.string().required(t("form.required")),
    guard_name: Yup.string().required(t("form.required")),
  });
  return permissionSchema;
};

export default useValidation;
