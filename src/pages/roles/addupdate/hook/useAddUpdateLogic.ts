import React from "react";
import {
  useGetPermissionsParentQuery,
  useGetPermissionsTreeByRoleQuery,
} from "@/api/permissions/usePermissionsQueries";
import {
  useGetRoleQuery,
  useAddRoleMutation,
  useUpdateRoleMutation,
} from "@/api/roles/useRolesQueries";
import { TPermissionTreeByRole } from "@/api/permissions/type";
import { TInitialValues } from "../../type";
import { FormikHelpers } from "formik";
const useAddUpdateLogic = ({ roleId }: { roleId: string }) => {
  const {
    data: role,
    isLoading: isRoleLoading,
    isFetching: isRoleFetching,
    isError: isRoleError,
  } = useGetRoleQuery({ params: { id: roleId } });
  const {
    data: permissions,
    isLoading: isPermissionsLoading,
    isError: isPermissionsError,
  } = useGetPermissionsTreeByRoleQuery({ params: { id: roleId } });
  const {
    data: permissionsParent,
    isLoading: isPermissionParentLoading,
    isError: isPermissionParentError,
  } = useGetPermissionsParentQuery({ params: { with_children: 1 } });
  const { mutate: addMutate, isLoading: isAddLoading } = useAddRoleMutation();

  const { mutate: updateMutate, isLoading: isEditLoading } =
    useUpdateRoleMutation({ params: { id: roleId } });

  const initialValues: TInitialValues = roleId
    ? {
        name: role?.response.role.name,
        guard_name: role?.response.role.guard_name,
        permissions: permissionsParent?.response.permissions.data.map(
          (permission) => {
            const _permission = permission as TPermissionTreeByRole;
            return {
              id: _permission.id,
              name: _permission.name,
              guard_name: _permission.guard_name,
              is_parent: _permission.is_parent,
              parent_id: _permission.parent_id,
              children: _permission.children.map((childePermission) => ({
                id: childePermission.id,
                name: childePermission.name,
                guard_name: childePermission.guard_name,
                is_parent: childePermission.is_parent,
                parent_id: childePermission.parent_id,
                checked: permissions?.response.permissions.data
                  .find((oldPermission) => oldPermission.id === permission.id)
                  ?.children?.find(
                    (oldChildPermission) =>
                      oldChildPermission.id === childePermission.id
                  )
                  ? true
                  : false,
              })),
              checked: permissions?.response.permissions.data.find(
                (oldPermission) => oldPermission.id === permission.id
              )
                ? true
                : false,
            };
          }
        ),
      }
    : {
        name: "",
        guard_name: "",
        permissions: permissionsParent?.response.permissions.data.map(
          (permission) => {
            const _permission = permission as TPermissionTreeByRole;
            return {
              id: _permission.id,
              name: _permission.name,
              guard_name: _permission.guard_name,
              is_parent: _permission.is_parent,
              parent_id: _permission.parent_id,
              children: _permission.children.map((childePermission) => ({
                id: childePermission.id,
                name: childePermission.name,
                guard_name: childePermission.guard_name,
                is_parent: childePermission.is_parent,
                parent_id: childePermission.parent_id,
                checked: false,
              })),
              checked: false,
            };
          }
        ),
      };
  const handleSubmittedData = (values: TInitialValues) => {
    return {
      name: values.name,
      guard_name: values.guard_name,
      permissions: values.permissions
        .filter((permission) => permission.checked)
        .map((permission) =>
          permission.children
            .filter((childPermission) => childPermission.checked)
            .map((childPermission) => childPermission.id)
        )
        .reduce((a, b) => [...a, ...b]),
    };
  };
  const handleSubmit = (
    values: TInitialValues,
    formikHelper: FormikHelpers<TInitialValues>
  ) => {
    roleId
      ? updateMutate({ body: handleSubmittedData(values) })
      : addMutate({ body: handleSubmittedData(values) });
    console.log(handleSubmittedData(values));
    formikHelper.setSubmitting(false);
  };
  return {
    role,
    permissions,
    permissionsParent,
    isRoleLoading,
    isPermissionsLoading,
    isPermissionParentLoading,
    isRoleFetching,
    isRoleError,
    isPermissionsError,
    isPermissionParentError,
    isAddLoading,
    isEditLoading,
    initialValues,
    handleSubmit,
  };
};

export default useAddUpdateLogic;
