import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import DeleteRoleModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import useRolesLogic from "./hooks/useRolesLogic";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

const Roles = () => {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const ability = useContext(AbilityContext);
  const navigate = useNavigate();
  const {
    data,
    role,
    error,
    page,
    rowsPerPage,
    searchRoles,
    openDeleteModal,
    isError,
    isLoading,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchRoles,
    handleDeleteRole,
    handleOpenDeleteRoleModal,
    handleCloseDeleteRoleModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchRoles,
  } = useRolesLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchRoles}
          setValue={setSearchRoles}
          debouncedFunction={handleSearchRoles}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_roles">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={() => navigate("role/add")}
          >
            {t("pages.roles.addRole")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.roles.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.roles.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.roles.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.guard_name}</TableCell>
                <TableCell>
                  <TableActions
                    delete={() =>
                      ability.can("destroy", "dashboard_api_roles") &&
                      handleOpenDeleteRoleModal(row)
                    }
                    edit={() => {
                      ability.can("update", "dashboard_api_roles") &&
                        navigate(`role/${row.id}/update`);
                    }}
                    preview={() => {
                      ability.can("update", "dashboard_api_roles") &&
                        navigate(`role/${row.id}/preview`);
                    }}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteRoleModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")} ${role.name}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteRole}
        handleClose={handleCloseDeleteRoleModal}
      />
    </Box>
  );
};

export default Roles;
