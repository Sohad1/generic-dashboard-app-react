import { ChangeEvent, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useQueryClient } from "@tanstack/react-query";
import {
  useDeleteRoleMutation,
  useGetRolesInfiniteQuery,
} from "@/api/roles/useRolesQueries";
import { TOrder, TRow } from "@/type/Table";

const useRolesLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchRoles, setSearchRoles] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [role, setRole] = useState<TRow>({});

  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetRolesInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });

  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteRoleMutation();

  const handleSearchRoles = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleCloseDeleteRoleModal = () => {
    setOpenDeleteModal(false);
  };
  const handleOpenDeleteRoleModal = (role: TRow) => {
    setRole(role);
    setOpenDeleteModal(true);
  };
  const handleDeleteRole = () => {
    deleteMutate(
      { params: { id: role.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["roles-infinite-query"]);
            handleCloseDeleteRoleModal();
          }
        },
      }
    );
  };
  return {
    data,
    role,
    error,
    page,
    rowsPerPage,
    searchRoles,
    openDeleteModal,
    isError,
    isLoading,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchRoles,
    handleDeleteRole,
    handleOpenDeleteRoleModal,
    handleCloseDeleteRoleModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchRoles,
  };
};

export default useRolesLogic;
