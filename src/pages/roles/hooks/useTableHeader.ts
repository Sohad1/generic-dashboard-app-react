import React from "react";
import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.users.form.name"),
    },
    {
      id: "guard_name",
      numeric: false,
      disablePadding: false,
      label: "Guard Name",
    },
  ];
  return headCells;
};

export default useTableHeader;
