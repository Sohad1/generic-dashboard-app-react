import { useNavigate, useParams } from "react-router-dom";
import { Grid, Paper, Typography, IconButton, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useTranslation } from "react-i18next";
import { TParams } from "../type";
import Loading from "@/components/others/loading";
import { useGetRoleQuery } from "@/api/roles/useRolesQueries";
import { useGetPermissionsTreeByRoleQuery } from "@/api/permissions/usePermissionsQueries";
import DefinitionOfProperty from "@/components/others/definitionOfProperty";
import ViewPermissionTree from "@/components/pages/users/roles/tree/viewPermissionTree";

const PreviewRole = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { roleId } = useParams<TParams>();
  const {
    data: role,
    isLoading: isRoleLoading,
    isFetching: isRoleFetching,
    isError: isRoleError,
  } = useGetRoleQuery({ params: { id: roleId } });
  const {
    data: permissions,
    isLoading: isPermissionsLoading,
    isError: isPermissionsError,
  } = useGetPermissionsTreeByRoleQuery({ params: { id: roleId } });

  if (isRoleLoading || isRoleFetching) {
    return <Loading />;
  } else if (isRoleError) {
    return <></>;
  }

  return (
    <Grid
      container
      justifyContent={"center"}
      alignItems={"center"}
      sx={{ mt: 3 }}
    >
      <Grid item xs={11}>
        <Paper sx={{ p: 3 }}>
          <IconButton
            color="primary"
            onClick={() => {
              navigate("/roles");
            }}
          >
            <ArrowBackIcon fontSize="large" />
          </IconButton>

          <Typography
            color="primary"
            textAlign="center"
            variant="h4"
            sx={{ my: 2 }}
          >
            {t("pages.roles.previewTitle")}
          </Typography>

          <Stack spacing={2}>
            <DefinitionOfProperty
              name={t("pages.roles.form.name")}
              value={role.response.role.name}
            />

            <DefinitionOfProperty
              name={t("pages.roles.form.guardName")}
              value={role.response.role.guard_name}
            />

            <Stack sx={{ flexWrap: "wrap" }} direction={"row"} spacing={2}>
              {permissions?.response.permissions.data.map(
                (permission, index) => (
                  <ViewPermissionTree key={index} permission={permission} />
                )
              )}
            </Stack>
          </Stack>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default PreviewRole;
