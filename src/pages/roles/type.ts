import { TPermissionByRole } from "@/api/permissions/type";

export type TParams = {
  roleId: string | undefined;
};

type TCheckedPermission = TPermissionByRole & { checked: boolean };

export type TPermission = TCheckedPermission & {
  children: TCheckedPermission[];
};
export type TInitialValues = {
  name: string;
  guard_name: string;
  permissions: TPermission[];
};
