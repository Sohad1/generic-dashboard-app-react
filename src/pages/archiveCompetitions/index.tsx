import Loading from "@/components/others/loading";
import React from "react";
import { Avatar, Box, Button, Stack, TableCell } from "@mui/material";
import { TRow } from "@/type/Table";

import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { useTranslation } from "react-i18next";
import useArchiveCompetitionLogic from "./hooks/useArchiveCompetitionLogic";
import useTableHeader from "./hooks/useTableHeader";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import ArchiveCompetitionModal from "./archiveCompetitionLogic";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import UnarchiveIcon from "@mui/icons-material/Unarchive";
const ArchiveCompetition = () => {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchArchiveCompetitions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchArchiveCompetitions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchArchiveCompetitions,
    handleArchiveCompetition,
    openArchiveModal,
    handleCloseArchiveModal,
    selectedArchiveCompetition,
  } = useArchiveCompetitionLogic();
  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <ArchiveCompetitionModal
        {...{
          open: openArchiveModal,
          handleClose: handleCloseArchiveModal,
          competition: selectedArchiveCompetition,
        }}
      />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchArchiveCompetitions}
            setValue={setSearchArchiveCompetitions}
            debouncedFunction={handleSearchArchiveCompetitions}
            label={t("common.search")}
            size="small"
          />
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.competitions.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.competitions.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.competitions.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>{row.points}</TableCell>

                  <TableCell>{row.click}</TableCell>
                  <TableCell>
                    <Avatar
                      variant="rounded"
                      src={String(row.image_url)}
                      alt="competition-img"
                    />
                  </TableCell>
                  <TableCell>
                    <Button
                      variant="outlined"
                      endIcon={<UnarchiveIcon />}
                      onClick={() => {
                        handleArchiveCompetition(row);
                      }}
                    >
                      Unarchive
                    </Button>
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </GenericTable>
      </Box>
    </Box>
  );
};

export default ArchiveCompetition;
