import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "title",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.title"),
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.description"),
    },
    {
      id: "points",
      numeric: true,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.points"),
    },
    {
      id: "clickNumber",
      numeric: true,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.clicknumber"),
    },
    {
      id: "image",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.image"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.competitions.addEditCompetition.unarchive"),
    },
  ];
  return headCells;
};

export default useTableHeader;
