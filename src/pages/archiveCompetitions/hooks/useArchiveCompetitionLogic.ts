import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetArchiveCompetitionsInfiniteQuery } from "@/api/archiveCompetitions/useArchiveCompetitionsQueries";

const useArchiveCompetitionLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [openArchiveModal, setOpenArchiveModal] = useState(false);
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [searchArchiveCompetitions, setSearchArchiveCompetitions] =
    useState<string>("");
  const [selectedArchiveCompetition, setSelectedArchiveCompetition] = useState({
    id: 0,
    title: "",
  });
  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetArchiveCompetitionsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchArchiveCompetitions = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleArchiveCompetition = (row: TRow) => {
    setOpenArchiveModal(true);
    setSelectedArchiveCompetition({
      title: String(row.title),
      id: Number(row.id),
    });
  };
  const handleCloseArchiveModal = () => setOpenArchiveModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchArchiveCompetitions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchArchiveCompetitions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchArchiveCompetitions,
    handleArchiveCompetition,
    handleCloseArchiveModal,
    openArchiveModal,
    selectedArchiveCompetition,
  };
};
export default useArchiveCompetitionLogic;
