import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
import { useUnArchiveCompetitionMutation } from "@/api/archiveCompetitions/useArchiveCompetitionsQueries";

type Props = {
  open: boolean;
  handleClose: () => void;
  competition: { id: number; title: string };
};
const ArchiveCompetitionModal = ({ competition, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: un_archiveMutate, isLoading: isUnArchiveLoading } =
    useUnArchiveCompetitionMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`common.unarchiveTitle`)} ${competition.title}`,
        handleClose,
        handleSubmit: () => un_archiveMutate(competition.id),
        isSubmittingLoading: isUnArchiveLoading,
        submitButtonText: "unarchive",
      }}
    />
  );
};

export default ArchiveCompetitionModal;
