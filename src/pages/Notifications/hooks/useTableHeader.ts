import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.title"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.body"),
    },
    {
      id: "image",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.image"),
    },
    {
      id: "count",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.numberOfClick"),
    },
    {
      id: "date",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.date"),
    },
  ];
  return headCells;
};

export default useTableHeader;
