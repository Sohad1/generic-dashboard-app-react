import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetNotificationsInfiniteQuery } from "@/api/notifications/useNotificationsQueries";
import { useNavigate } from "react-router-dom";

import { useQueryClient } from "@tanstack/react-query";
const useNotificationsLogic = () => {
  const [page, setPage] = useState(0);

  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchNotifications, setSearchNotifications] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetNotificationsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchNotifications = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleAddNotification = () => {
    navigate("/Notifications/AddNotifications");
  };

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchNotifications,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleAddNotification,
    setSearchNotifications,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchNotifications,
  };
};
export default useNotificationsLogic;
