import { useTranslation } from "react-i18next";
import * as Yup from "yup";

function useValidationSchema() {
  const { t } = useTranslation();
  const cardSchema = Yup.object().shape({
    title: Yup.string().required(t("form.required")),
    body: Yup.string().required(t("form.required")),
    url: Yup.string().required(t("form.required")),
    lang: Yup.string().required(t("form.required")),
  });

  return cardSchema;
}

export default useValidationSchema;
