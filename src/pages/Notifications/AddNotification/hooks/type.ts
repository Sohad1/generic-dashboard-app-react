import { TNotifications } from "@/api/notifications/type";

export type TInitialValues = TNotifications;
export type TParams = {
  NotificationsId: string | undefined;
};
