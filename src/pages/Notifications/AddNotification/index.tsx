import { useNavigate } from "react-router-dom";
import {
  Grid,
  IconButton,
  Paper,
  Box,
  Typography,
  TextField,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import { useState } from "react";
import { Form, Formik } from "formik";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import ImageDragDropField from "@/components/items/inputField/ImageDragDropField";
import { useAddNotificationsMutation } from "@/api/notifications/useNotificationsQueries";
import { TInitialValues, TParams } from "./hooks/type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import useCollectionsLogic from "@/pages/gifts/AddEditGift/hooks";
import AutoCompleteField from "@/components/items/inputField/autoCompleteField/AutoCompleteField";
import GenericFormPage from "@/components/items/form/genericFormPage/GenericFormPage";
import useValidationSchema from "./hooks/useValidation";
import CountriesByCollectionsAutoComplete from "@/pages/gifts/AddEditGift/CountriesByCollectionsAutoComplete";
import SelectField from "@/components/items/inputField/selectField/SelectField";
const AddNotification = () => {
  const { t } = useTranslation();
  const [selectedCollection, setSelectedCollection] = useState<TOption>();
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddNotificationsMutation();
  const { collections, isCollectionLoading } = useCollectionsLogic({
    value: selectedCollection,
  });
  const validationSchema = useValidationSchema();

  const initialValues: TInitialValues = {
    body: "",
    title: "",
    image: "",
    created_at: "",
    url: "",
    all_countries: 0,
    id: 0,
    country_ids: null,
    users_count: 0,
    countries: null,
    collection: null,
    country_id: null,
    lang: null,
  };
  return (
    <GenericFormPage
      title={t("pages.notification.sendnotification")}
      goBackLink="/Notifications"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          let data = { ...values, country_ids: values.countries.id };
          delete data.countries;
          addMutate({ body: data });
          console.log(data);
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <ImageDragDropField
                  handleChangeFile={(v) => setFieldValue("image", v)}
                  file={values.image}
                  name="image"
                  type={"add"}
                  label={"image"}
                  error={touched.image && !!errors.image}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title"
                  fullWidth
                  label="title"
                  value={values.title}
                  onChange={handleChange}
                  error={touched.title && !!errors.title}
                  helperText={touched.title && errors.title}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="body"
                  fullWidth
                  label="description"
                  value={values.body}
                  onChange={handleChange}
                  error={touched.body && !!errors.body}
                  helperText={touched.body && errors.body}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="url"
                  fullWidth
                  label="url"
                  value={values.url}
                  onChange={handleChange}
                  error={touched.url && !!errors.url}
                  helperText={touched.url && errors.url}
                />
              </Grid>

              <Grid item xs={12} md={6} lg={4}>
                {/* <AutoCompleteField
                  label={t("pages.notification.languages")}
                  name="languages"
                  options={languages || [{ id: 0, name: "" }]}
                  value={values.lang}
                  onChange={(_, value) => {
                    setFieldValue("lang", value);
                    setSelectedCollection(value);
                  }}
                  fullWidth
                  error={touched.lang && !!errors.lang}
                  errorMessage={""}
                  {...{
                    loading: isCollectionLoading,
                  }}
                /> */}
                <SelectField
                  name="lang"
                  label={t("pages.notification.languages")}
                  value={String(values.lang)}
                  onChange={handleChange}
                  options={[
                    { value: "all", text: "All" },
                    { value: "en", text: "English " },
                    { value: "ar", text: "Arabic" },
                  ]}
                  error={touched.lang && !!errors.lang}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <SelectField
                  name="lang"
                  label={t("pages.notification.form.allCountries")}
                  value={String(values.all_countries)}
                  onChange={handleChange}
                  options={[
                    { value: 1, text: "yes " },
                    { value: 0, text: "no" },
                  ]}
                  error={touched.lang && !!errors.lang}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <AutoCompleteField
                  label={t("pages.collection.collections")}
                  name="collection"
                  options={collections || [{ id: 0, name: "" }]}
                  value={values.collection}
                  onChange={(_, value: any) => {
                    setFieldValue("collection", value);
                    setFieldValue("collection_id", value.id);
                    setSelectedCollection(value);
                  }}
                  fullWidth
                  error={touched.collection && !!errors.collection}
                  errorMessage={""}
                  {...{
                    loading: isCollectionLoading,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <CountriesByCollectionsAutoComplete
                  error={touched.countries && !!errors.countries}
                  value={values.countries}
                  setFieldValue={setFieldValue}
                  disabled={values.collection ? false : true}
                  collectionId={values.collection?.id}
                />
              </Grid>
              <Grid
                item
                xs={12}
                // md={10}
                sx={{ display: "flex", marginBottom: 5 }}
              >
                <LoadingButton
                  isSubmitting={isAddLoading}
                  buttonText={"send notification"}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  );
};
export default AddNotification;
