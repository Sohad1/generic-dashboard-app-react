import { Box, Button, Stack, TableCell, Avatar } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import useNotificationsLogic from "./hooks/useNotificationsLogic";
import useTableHeader from "./hooks/useTableHeader";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

const Notifications = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchNotifications,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleAddNotification,
    setSearchNotifications,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchNotifications,
  } = useNotificationsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"} sx={{ mb: 3 }}>
        <DebouncedTextField
          value={searchNotifications}
          setValue={setSearchNotifications}
          debouncedFunction={handleSearchNotifications}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_notifications">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddNotification}
          >
            {t("pages.notification.sendnotification")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.notifications.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.notifications.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.notifications.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.title}</TableCell>
                <TableCell>{row.body}</TableCell>

                <TableCell>
                  {
                    <Avatar
                      variant="rounded"
                      src={String(row.image_url)}
                      alt="competition-img"
                    />
                  }
                </TableCell>
                <TableCell>{row.users_count}</TableCell>
                <TableCell>{row.created_at}</TableCell>
                <TableActions />
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
};

export default Notifications;
