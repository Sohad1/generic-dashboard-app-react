import { useGetExceptionsInfiniteQuery } from "@/api/referralsExceptions/useExceptionsQuery";
import { TOrder, TRow } from "@/type/Table";
import { ChangeEvent, useState } from "react";
import { useNavigate } from "react-router-dom";

const useExceptionLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [openRemoveExceptionModal, setOpenRemoveExceptionModal] =
    useState(false);

  const [searchExceptions, setSearchExceptions] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [selectedException, setSelectedException] = useState({
    id: 0,
    name: "",
  });
  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetExceptionsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchExceptions = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleRemoveException = (row: TRow) => {
    setOpenRemoveExceptionModal(true);
    setSelectedException({
      name: String(row.name),
      id: Number(row.id),
    });
  };
  const handleCloseRemoveExceptionModal = () =>
    setOpenRemoveExceptionModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchExceptions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchExceptions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchExceptions,
    handleRemoveException,
    handleCloseRemoveExceptionModal,
    openRemoveExceptionModal,
    selectedException,
  };
};

export default useExceptionLogic;
