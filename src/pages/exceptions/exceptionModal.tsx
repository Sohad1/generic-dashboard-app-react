import { useRemoveExceptionMutation } from "@/api/referralsExceptions/useExceptionsQuery";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";

type Props = {
  open: boolean;
  handleClose: () => void;
  exception: { id: number; name: string };
};
const ExceptionModal = ({ exception, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: removeExceptionMutate, isLoading: isRemoveExceptionLoading } =
    useRemoveExceptionMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`common.deleteTitle`)} ${exception.name}`,
        handleClose,
        handleSubmit: () => removeExceptionMutate(exception.id),
        isSubmittingLoading: isRemoveExceptionLoading,
        submitButtonText: t("common.remove")!,
      }}
    />
  );
};

export default ExceptionModal;
