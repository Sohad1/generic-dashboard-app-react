import { Box, Button, Stack, TableCell } from '@mui/material'
import { TRow } from '@/type/Table'
import GenericTable from '@/components/items/table/genericTable/Table'
import { getComparator, stableSort } from '@/util/table'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { useTranslation } from 'react-i18next'
import useTableHeader from './hooks/useTableHeader'
import Loading from '@/components/others/loading'
import useExceptionLogic from './hooks/useExceptionLogic'
import DeleteIcon from '@mui/icons-material/Delete'
import ExceptionModal from './exceptionModal'
import moment from 'moment'

// type Props = {
//   data: InfiniteData<TGenericPaginationResponse<any> | undefined>;
// };
const ReferralExceptions = () => {
  const { t } = useTranslation()
  const headCells = useTableHeader()
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchExceptions,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchExceptions,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchExceptions,
    handleRemoveException,
    handleCloseRemoveExceptionModal,
    openRemoveExceptionModal,
    selectedException,
  } = useExceptionLogic()
  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <Box>
      <ExceptionModal
        {...{
          open: openRemoveExceptionModal,
          handleClose: handleCloseRemoveExceptionModal,
          exception: selectedException,
        }}
      />
      <Box>
        <Stack direction={'row'} justifyContent={'space-between'} mb={3}>
          <DebouncedTextField
            value={searchExceptions}
            setValue={setSearchExceptions}
            debouncedFunction={handleSearchExceptions}
            label={t('common.search')}
            size="small"
          />
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.users.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.users.data)
              .reduce((previous, current) => [...previous, ...current]) as any[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.users.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as any[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>
                    {row.name}
                    {row.is_advertised === 1 ? ' (AdGate)' : ''}
                  </TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.device_id}</TableCell>
                  <TableCell>{row.referral_id}</TableCell>
                  <TableCell>
                    {moment(row.created_at).format('YYYY-MM-DD , HH:mm')}
                  </TableCell>
                  {/* <TableCell>{row.points?.points}</TableCell> */}
                  <TableCell>{row.points?.points}</TableCell>
                  <TableCell>{row.user_points_history_count}</TableCell>
                  <TableCell>{row.referred_count}</TableCell>
                  <TableCell>
                    {row.status === 1 ? 'active' : 'inactive'}
                  </TableCell>

                  <TableCell>{row.top10_referral_exception?.status}</TableCell>

                  <TableCell>
                    <Button
                      variant="outlined"
                      endIcon={<DeleteIcon />}
                      color="error"
                      onClick={() => {
                        handleRemoveException(row)
                      }}
                    >
                      {t('common.remove')}
                    </Button>
                  </TableCell>
                </StyledTableRow>
              )
            })}
        </GenericTable>
      </Box>
    </Box>
  )
}

export default ReferralExceptions
