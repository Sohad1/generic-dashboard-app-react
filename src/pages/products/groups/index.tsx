import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import useGroupsLogic from "./hooks/useGroupsLogic";
import DeleteGroupModal from "./ActionsGroup/DeleteGroupModal";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

function Groups() {
  const { t, i18n } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    page,
    rowsPerPage,
    searchGroups,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchGroups,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchGroups,
    handleAddGroup,
    handleEditGroup,
    handleDeleteGroup,
    openDeleteModal,
    selectedGroup,
    handleCloseModal,
  } = useGroupsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <DeleteGroupModal
        open={openDeleteModal}
        handleClose={handleCloseModal}
        group={selectedGroup}
      />
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchGroups}
          setValue={setSearchGroups}
          debouncedFunction={handleSearchGroups}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_groups">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddGroup}
          >
            {t("pages.groups.addGroup")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.groups.total!}
        rowsPerPage={rowsPerPage}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        rows={
          data?.pages
            .map((page) => page.response.groups.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.groups.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name_en}</TableCell>
                <TableCell>{row.name_ar}</TableCell>
                <TableCell>{row.name_de}</TableCell>

                <TableCell>
                  <TableActions
                    edit={() =>
                      ability.can("update", "dashboard_api_groups") &&
                      handleEditGroup(row)
                    }
                    delete={() =>
                      ability.can("destroy", "dashboard_api_groups") &&
                      handleDeleteGroup({
                        id: row.id,
                        name:
                          i18n.language === "ar" ? row.name_ar : row.name_en,
                      })
                    }
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
}

export default Groups;
