import { useDeleteGroupMutation } from '@/api/products/groups/useGroupsQueries'
import GenericDeleteModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  open: boolean
  handleClose: () => void
  group: { id: string; name: string }
}

function DeleteGroupModal({ open, handleClose, group }: Props) {
  const { t } = useTranslation()

  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteGroupMutation(handleClose)
  return (
    <GenericDeleteModal
      {...{
        open,
        title: `${t(`pages.group.deleteGroup`)} ${group?.name}`,
        handleClose,
        handleSubmit: () => deleteMutate(group?.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t('common.delete')!,
      }}
    />
  )
}

export default DeleteGroupModal
