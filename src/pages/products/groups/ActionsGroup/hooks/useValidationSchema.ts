import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

const useGroupValidationSchema = () => {
  const { t } = useTranslation()

  const validationSchema = Yup.object({
    name_en: Yup.string().required(`${t('form.required')}`),
    name_ar: Yup.string().required(`${t('form.required')}`),
    name_de: Yup.string().required(`${t('form.required')}`),
  })
  return validationSchema
}

export default useGroupValidationSchema
