import { TGroup } from '@/api/products/groups/type'
import {
  useAddGroupMutation,
  useEditGroupMutation,
  useGetGroupInfoQuery,
} from '@/api/products/groups/useGroupsQueries'

function useActionsGroup(groupId: string | undefined) {
  const { data, isLoading, isFetching, isError } = useGetGroupInfoQuery(groupId)

  const initialValues: TGroup = data
    ? {
        id: data.id,
        name_ar: data.name_ar,
        name_en: data.name_en,
        name_de: data.name_de,
      }
    : {
        name_ar: '',
        name_en: '',
        name_de: '',
      }
  const { mutate: addMutate, isLoading: isAddLoading } = useAddGroupMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditGroupMutation()
  return {
    initialValues,
    isLoading,
    isFetching,
    isError,
    editMutate,
    isEditLoading,
    addMutate,
    isAddLoading,
  }
}

export default useActionsGroup
