import { useNavigate, useParams } from 'react-router-dom'
import { TParams } from './hooks/type'
import Loading from '@/components/others/loading'
import useGroupValidationSchema from './hooks/useValidationSchema'
import { useTranslation } from 'react-i18next'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import { Form, Formik } from 'formik'
import { Grid, TextField } from '@mui/material'
import FormActions from '@/components/items/form/FormActions/FormActions'
import useActionsGroup from './hooks/useActionsGroup'

function AddEditGroup() {
  const { groupId } = useParams<TParams>()
  const { t } = useTranslation()
  const validationSchema = useGroupValidationSchema()
  const navigate = useNavigate()
  const {
    initialValues,
    isLoading,
    isError,
    editMutate,
    isEditLoading,
    addMutate,
    isAddLoading,
  } = useActionsGroup(groupId)
  if (!!groupId) {
    if (isLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }

  return (
    <GenericFormPage
      title={
        groupId
          ? `${t(`common.edit`)} ${t(`pages.products.group`)}`
          : `${t(`common.add`)} ${t(`pages.products.group`)}`
      }
      goBackLink="/groups"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          console.log(values)
          groupId ? editMutate({ body: values }) : addMutate({ body: values })
        }}
        validationSchema={validationSchema}
      >
        {({ values, handleChange, touched, errors }) => (
          <Form>
            <Grid container alignItems={'center'} spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name_en"
                  fullWidth
                  label={t('pages.group.name_en')}
                  value={values.name_en}
                  onChange={handleChange}
                  error={touched.name_en && !!errors.name_en}
                  helperText={touched.name_en && errors.name_en}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name_ar"
                  fullWidth
                  label={t('pages.group.name_ar')}
                  value={values.name_ar}
                  onChange={handleChange}
                  error={touched.name_ar && !!errors.name_ar}
                  helperText={touched.name_ar && errors.name_ar}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name_de"
                  fullWidth
                  label={t('pages.group.name_de')}
                  value={values.name_de}
                  onChange={handleChange}
                  error={touched.name_de && !!errors.name_de}
                  helperText={touched.name_de && errors.name_de}
                />
              </Grid>

              <FormActions
                isSubmitting={isAddLoading || isEditLoading}
                submitButtonText={groupId ? t('common.edit') : t('common.add')}
                handleCancel={() => navigate(`/groups`)}
              />
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditGroup
