import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";

import { useNavigate } from "react-router-dom";
import { useGetGroupsInfiniteQuery } from "@/api/products/groups/useGroupsQueries";

function useGroupsLogic() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchGroups, setSearchGroups] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetGroupsInfiniteQuery(rowsPerPage);

  const handleSearchGroups = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddGroup = () => {
    navigate("/Groups/addGroup");
  };
  const handleEditGroup = (row: TRow) => {
    navigate(`/Groups/editGroup/${row.id}`);
  };

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectedGroup, setSelectedGroup] = useState(null);

  const handleDeleteGroup = (group: TRow) => {
    setOpenDeleteModal(true);
    setSelectedGroup({
      name: group.name,
      id: group.id,
    });
  };
  const handleCloseModal = () => setOpenDeleteModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchGroups,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchGroups,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchGroups,
    handleAddGroup,
    handleEditGroup,
    handleDeleteGroup,
    openDeleteModal,
    selectedGroup,
    handleCloseModal,
  };
}

export default useGroupsLogic;
