import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "#",
    },
    {
      id: "nameEn",
      numeric: false,
      disablePadding: false,
      label: t("pages.groups.name_en"),
    },
    {
      id: "nameAr",
      numeric: false,
      disablePadding: false,
      label: t("pages.groups.name_ar"),
    },
    {
      id: "nameDe",
      numeric: false,
      disablePadding: false,
      label: t("pages.groups.name_de"),
    },
  ];
  return headCells;
};

export default useTableHeader;
