import { ChangeEvent, useState } from 'react'
import { TOrder, TRow } from '@/type/Table'
import { useNavigate } from 'react-router-dom'
import { useGetCardsInfiniteQuery } from '@/api/products/cards/useCardsQueries'
import { TFilterCardsParams } from '@/api/products/cards/type'

const useCardsLogic = () => {
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)
  const [searchCards, setSearchCards] = useState<string>('')
  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const [filterOptions, setFilterOptions] = useState<TFilterCardsParams>({})
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>('')
  const [card, setCard] = useState(null)
  const [openDeleteModal, setOpenDeleteModal] = useState(false)
  const navigate = useNavigate()

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetCardsInfiniteQuery(rowsPerPage, filterOptions)

  const handleSearchCards = (value: string) => {
    console.log(value)
  }
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 })
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }
  const handleAddCard = () => {
    navigate('/cards/addCard')
  }
  const handleEditCard = (row: TRow) => {
    navigate(`/cards/editCard/${row.id}`)
  }

  const handleCloseDeleteModal = () => setOpenDeleteModal(false)

  const handleOpenDeleteModal = (card: TRow) => {
    setCard(card)
    setOpenDeleteModal(true)
  }
  const handleDeleteCategory = () => {}

  const handleFilterOptions = (options: TFilterCardsParams) =>
    setFilterOptions(options)
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCards,
    isError,
    isLoading,
    openDeleteModal,
    order,
    orderBy,
    card,
    setOrder,
    setOrderBy,
    handleOpenDeleteModal,
    handleCloseDeleteModal,
    handleDeleteCategory,
    setSearchCards,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCards,
    handleAddCard,
    handleEditCard,
    handleFilterOptions,
  }
}

export default useCardsLogic
