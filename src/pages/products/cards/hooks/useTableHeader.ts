import { HeadCell } from '@/type/Table'
import { useTranslation } from 'react-i18next'
const useTableHeader = () => {
  const { t } = useTranslation()
  const headCells: HeadCell[] = [
    {
      id: 'details',
      numeric: false,
      disablePadding: false,
      label: t('pages.products.cards.details'),
    },
    {
      id: 'categoryGift',
      numeric: false,
      disablePadding: false,
      label: t('pages.products.cards.categoryGift'),
    },
    {
      id: 'giftType',
      numeric: false,
      disablePadding: false,
      label: t('pages.products.cards.giftType'),
    },
    {
      id: 'company',
      numeric: false,
      disablePadding: false,
      label: t('pages.products.cards.company'),
    },
  ]
  return headCells
}

export default useTableHeader
