import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { useParams } from 'react-router-dom'
import { Grid, TextField, Stack } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { Form, Formik } from 'formik'
import CompaniesAutoComplete from '@/components/pages/products/AutoCompletes/CompaniesAutoComplete'
import ProductsAutoComplete from '@/components/pages/products/AutoCompletes/ProductsAutoComplete'
import { TParams } from './type'
import PricesAutoComplete from '@/components/pages/products/AutoCompletes/PricesAutoComplete'
import useActionsCardLogic from './hooks/useActionsCardLogic'
import useValidationSchema from './hooks/useValidationSchema'
import Loading from '@/components/others/loading'
import DetailsCardTabs from '@/components/pages/products/cards/DetailsCardTabs'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'

function AddEditCard() {
  const { t } = useTranslation()
  const { cardId } = useParams<TParams>()
  const {
    initialValues,
    isGetCardLoading,
    isGetCardError,
    addCard,
    editCard,
    isAddLoading,
    isEditLoading,
  } = useActionsCardLogic(cardId)

  const cardSchema = useValidationSchema()
  if (!!cardId) {
    if (isGetCardLoading) {
      return <Loading />
    } else if (isGetCardError) {
      return <></>
    }
  }
  return (
    <GenericFormPage
      title={
        cardId
          ? `${t(`pages.products.cards.updateCard`)}`
          : `${t(`pages.products.cards.addCard`)}`
      }
      goBackLink="/cards"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          let data: any = {
            ...values,
            is_multiple: values.is_multiple ? 1 : 0,
          }
          delete data.category
          delete data.company
          delete data.price
          console.log(data)
          cardId
            ? editCard({ body: { ...data, id: cardId } })
            : addCard({ body: data })
        }}
        validationSchema={cardSchema}
        enableReinitialize
      >
        {({ values, touched, errors, handleChange, setFieldValue }) => {
          console.log(errors, values)
          return (
            <Form>
              <Stack spacing={2}>
                <Grid container spacing={2}>
                  <Grid item lg={4} xs={12}>
                    <CompaniesAutoComplete
                      name="company"
                      value={values.company}
                      setFieldValue={setFieldValue}
                      error={touched.price_id && !!errors.price_id}
                    />
                  </Grid>

                  <Grid item lg={4} xs={12}>
                    <ProductsAutoComplete
                      data={values.company?.data}
                      name="category"
                      value={values.category}
                      setFieldValue={setFieldValue}
                      error={touched.price_id && !!errors.price_id}
                    />
                  </Grid>
                  <Grid item lg={4} xs={12}>
                    <PricesAutoComplete
                      name="price"
                      value={values.price}
                      setFieldValue={(_, v) => {
                        setFieldValue('price', v)
                        setFieldValue('price_id', v.id)
                      }}
                      data={values.category?.data}
                      error={touched.price_id && !!errors.price_id}
                    />
                  </Grid>

                  <Grid item lg={4} xs={12}>
                    <TextField
                      name="name"
                      fullWidth
                      label={t('pages.products.cards.form.title')}
                      value={values.name}
                      onChange={handleChange}
                      error={touched.name && !!errors.name}
                      helperText={touched.name && errors.name}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <DetailsCardTabs
                      {...{
                        errors,
                        handleChange,
                        setFieldValue,
                        touched,
                        values,
                      }}
                    />
                  </Grid>
                </Grid>

                <LoadingButton
                  isSubmitting={isAddLoading || isEditLoading}
                  buttonText={cardId ? t('common.edit') : t('common.add')}
                />
              </Stack>
            </Form>
          )
        }}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditCard
