import { useDeleteCardMutation } from '@/api/products/cards/useCardsQueries'
import GenericDeleteModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  open: boolean
  handleClose: () => void
  card: { id: string; name: string }
}
function DeleteCardModal({ open, handleClose, card }: Props) {
  const { t } = useTranslation()

  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteCardMutation(handleClose)
  return (
    <GenericDeleteModal
      {...{
        open,
        title: `${t(`common.deleteTitle`)} ${card.name}`,
        handleClose,
        handleSubmit: () => deleteMutate(card.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t('common.delete')!,
      }}
    />
  )
}

export default DeleteCardModal
