import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const cardSchema = Yup.object().shape({
    price_id: Yup.string().required(t('form.required')),
    name: Yup.string().required(t('form.required')),
    is_multiple: Yup.boolean().required(t('form.required')),
    details: Yup.string().when('is_multiple', {
      is: true,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
    card_number: Yup.string().when('is_multiple', {
      is: false,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
    exp_date: Yup.string().when('is_multiple', {
      is: false,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
    card_code: Yup.string().when('is_multiple', {
      is: false,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
  })

  return cardSchema
}

export default useValidationSchema
