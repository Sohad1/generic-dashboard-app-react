import {
  useAddCardMutation,
  useEditCardMutation,
  useGetCardInfoQuery,
} from '@/api/products/cards/useCardsQueries'
import { TCategory } from '@/api/products/products/type'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'

function useActionsCardLogic(cardId: string | undefined) {
  const {
    data,
    isLoading: isGetCardLoading,
    isError: isGetCardError,
    isFetching: isGetCardFetching,
  } = useGetCardInfoQuery(cardId!)

  const initialValues: {
    company: { id: string; name: string; data: TCategory[] }
    category: { id: string; name: string; data: { id: string; name: string }[] }
    price: TOption
    price_id: string
    name: string
    is_multiple: boolean
    card_number?: string
    exp_date?: string
    card_code?: string
    details?: string
  } = data
    ? {
        company: {
          id: data.price.id,
          name: data.price?.value,
          data: [data.price.category],
        },
        category: {
          id: data.price.category.id,
          name: data.price.category.name,
          data: data.price.category.prices,
        },
        price: null,
        // { id: data.price.id, name: data.price?.value },
        price_id: data.price_id,
        name: data.name,
        is_multiple: data.card_details.length > 0 ? true : false,
        card_number:
          data.card_details.length > 0
            ? data.card_details?.filter((item) => item.key === 'card_number')[0]
                .value
            : null,
        exp_date:
          data.card_details.length > 0
            ? data.card_details?.filter((item) => item.key === 'exp_date')[0]
                .value
            : null,
        card_code:
          data.card_details.length > 0
            ? data.card_details?.filter((item) => item.key === 'card_code')[0]
                .value
            : null,
        details: data.details,
      }
    : {
        company: null,
        category: null,
        price: null,
        price_id: null,
        name: '',
        is_multiple: false,
        card_number: null,
        exp_date: null,
        card_code: null,
        details: null,
      }

  const { mutate: addCard, isLoading: isAddLoading } = useAddCardMutation()
  const { mutate: editCard, isLoading: isEditLoading } = useEditCardMutation()
  return {
    initialValues,
    isGetCardLoading,
    isGetCardFetching,
    isGetCardError,
    addCard,
    editCard,
    isAddLoading,
    isEditLoading,
  }
}

export default useActionsCardLogic
