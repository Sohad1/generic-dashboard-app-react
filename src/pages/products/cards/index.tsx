import FilterCardsForm from "@/components/pages/products/cards/FilterForm/FilterForm";
import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import useCardsLogic from "./hooks/useCardsLogic";
import DeleteCardModal from "./ActionsCard/DeleteCardModal";
import PricesCardsAccordion from "@/components/pages/products/cards/PricesCardsAccordion";
import { AbilityContext, Can } from "@/libs/casl/can";
function Cards() {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const ability = useContext(AbilityContext);
  const {
    data,
    // error,
    page,
    rowsPerPage,
    searchCards,
    isError,
    isLoading,
    openDeleteModal,
    order,
    orderBy,
    card,
    setOrder,
    setOrderBy,
    handleOpenDeleteModal,
    handleCloseDeleteModal,
    setSearchCards,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCards,
    handleAddCard,
    handleEditCard,
    handleFilterOptions,
  } = useCardsLogic();

  if (isLoading)
    return (
      <Box>
        <FilterCardsForm handleFilterOptions={handleFilterOptions} />
        <Box>
          <Loading />
        </Box>
      </Box>
    );

  if (isError)
    return (
      <Box>
        <FilterCardsForm handleFilterOptions={handleFilterOptions} />
      </Box>
    );
  return (
    <Box>
      <PricesCardsAccordion prices={data.pages[0].response.prices} />

      <FilterCardsForm handleFilterOptions={handleFilterOptions} />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchCards}
            setValue={setSearchCards}
            debouncedFunction={handleSearchCards}
            label={t("common.search")}
            size="small"
          />
          <Can I="store" a="dashboard_api_cards">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddCard}
            >
              {t("pages.products.cards.addCard")}
            </Button>
          </Can>
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0]?.response.cards.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              ?.map((page) => page.response.cards.data)
              .reduce((previous, current) => [...previous, ...current]) as any[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              ?.map((page) => page.response.cards.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as any[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.details}</TableCell>
                  <TableCell>{row.price.value}</TableCell>
                  <TableCell>{row.price.category.name}</TableCell>
                  <TableCell>{row.price.category.company.name}</TableCell>

                  <TableCell>
                    <TableActions
                      edit={() => {
                        ability.can("update", "dashboard_api_cards") &&
                          handleEditCard(row);
                      }}
                      delete={() =>
                        ability.can("destroy", "dashboard_api_cards") &&
                        handleOpenDeleteModal(row)
                      }
                    />
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </GenericTable>
        <DeleteCardModal
          open={openDeleteModal}
          card={{ id: card?.id, name: card?.name }}
          handleClose={handleCloseDeleteModal}
        />
      </Box>
    </Box>
  );
}

export default Cards;
