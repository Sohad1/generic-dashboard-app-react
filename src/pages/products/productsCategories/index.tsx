import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import useProductsCategoriesLogic from "./hooks/useProductsCategoriesLogic";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
import DeleteProductCategoryModal from "./DeleteModal";

const ProductsCategories = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    page,
    rowsPerPage,
    searchCategories,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCategories,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCategories,
    handleAddCategory,
    handleEditCategory,
    handleDeleteCategory,
    openDeleteModal,
    selectedCategory,
    handleCloseModal,
  } = useProductsCategoriesLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchCategories}
          setValue={setSearchCategories}
          debouncedFunction={handleSearchCategories}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_prices">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddCategory}
          >
            {t("pages.categories.addCategory")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.prices.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.prices.data)
            .reduce((previous, current) => [...previous, ...current]) as any[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.prices.data)
            .reduce((previous, current) => [...previous, ...current]) as any[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.value}</TableCell>
                <TableCell>{row.type}</TableCell>
                <TableCell>{row.point}</TableCell>
                <TableCell>{row.offer}</TableCell>
                <TableCell>{row.price}</TableCell>
                <TableCell>{"row.type"}</TableCell>
                <TableCell>{"row.type"}</TableCell>
                <TableCell>
                  <img src={row.image_url} alt={row.value} width={50} />
                </TableCell>{" "}
                <TableCell>
                  <img src={row.logo_url} alt={row.value} width={50} />
                </TableCell>
                <TableCell>
                  <TableActions
                    edit={() =>
                      ability.can("update", "dashboard_api_prices") &&
                      handleEditCategory(row)
                    }
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteProductCategoryModal
        open={openDeleteModal}
        handleClose={handleCloseModal}
        price={selectedCategory}
      />
    </Box>
  );
};

export default ProductsCategories;
