import { Form, Formik } from 'formik'
import { Grid, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import { useNavigate, useParams } from 'react-router-dom'
import FormActions from '@/components/items/form/FormActions/FormActions'
import { TParams } from './hooks/type'
import RadioButtons from '@/components/items/inputField/radio/RadioButtons'
import CheckboxInput from '@/components/items/inputField/Checkbox/Checkbox'
import ImageDragDropField from '@/components/items/inputField/ImageDragDropField'
import CompaniesAutoComplete from '@/components/pages/products/AutoCompletes/CompaniesAutoComplete'
import ProductsAutoComplete from '@/components/pages/products/AutoCompletes/ProductsAutoComplete'
import useActionsPriceLogic from './hooks/useActionsPriceLogic'
import useValidationSchema from './hooks/useValidation'
import Loading from '@/components/others/loading'

function AddEditProductsCategory() {
  const { t } = useTranslation()

  const { categoryId } = useParams<TParams>()
  const navigate = useNavigate()
  const { initialValues, isLoading, isError } = useActionsPriceLogic(categoryId)
  const priceSchema = useValidationSchema()
  if (!!categoryId) {
    if (isLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  return (
    <GenericFormPage
      title={
        categoryId
          ? `${t(`common.edit`)} ${t(`pages.products.Prices`)}`
          : `${t(`common.add`)} ${t(`pages.products.Prices`)}`
      }
      goBackLink="/productsCategories"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          console.log(values)
        }}
        validationSchema={priceSchema}
      >
        {({ values, handleChange, setFieldValue, touched, errors }) => (
          <Form>
            <Grid container alignItems={'center'} spacing={2}>
              <Grid container item xs={12} md={12} spacing={2}>
                <Grid item xs={12} sm={6}>
                  <ImageDragDropField
                    handleChangeFile={(v) => setFieldValue('image', v)}
                    file={values.image}
                    name="image"
                    type={categoryId ? 'edit' : 'add'}
                    label={'image'}
                    error={touched.image && !!errors.image}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <ImageDragDropField
                    handleChangeFile={(v) => setFieldValue('logo', v)}
                    file={values.logo}
                    name="logo"
                    type={categoryId ? 'edit' : 'add'}
                    label={'logo'}
                    error={touched.logo && !!errors.logo}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} container spacing={2}>
                <Grid item lg={4} xs={12}>
                  <CompaniesAutoComplete
                    name="company"
                    value={values.company}
                    setFieldValue={setFieldValue}
                    error={touched.category_id && !!errors.category_id}
                  />
                </Grid>

                <Grid item lg={4} xs={12}>
                  <ProductsAutoComplete
                    data={values.company?.data}
                    name="category"
                    value={values.category}
                    setFieldValue={
                      setFieldValue
                      // (_, v) => {
                      // console.log(v)
                      // setFieldValue('category', v)
                      // setFieldValue('category_id', v.id)
                      // }
                    }
                    error={touched.category_id && !!errors.category_id}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="value"
                  fullWidth
                  label={t('pages.products.form.value')}
                  value={values.value}
                  onChange={handleChange}
                  error={touched.value && !!errors.value}
                  helperText={touched.value && errors.value}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.description')}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>

              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="link"
                  fullWidth
                  label={t('pages.products.form.link')}
                  value={values.link}
                  onChange={handleChange}
                  error={touched.link && !!errors.link}
                  helperText={touched.link && errors.link}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="point"
                  type="number"
                  fullWidth
                  label={t('pages.products.form.point')}
                  value={values.point}
                  onChange={handleChange}
                  error={touched.point && !!errors.point}
                  helperText={touched.point && errors.point}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <RadioButtons
                  title={t('pages.products.form.phone')}
                  name="has_verification"
                  options={[
                    { value: '1', label: 'تأكيد' },
                    { value: '0', label: 'عدم تأكيد' },
                  ]}
                  onChange={(val: string) =>
                    setFieldValue('has_verification', val)
                  }
                  value={values.has_verification}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <RadioButtons
                  title={t('pages.products.form.prizeType')}
                  name="type"
                  options={[
                    { value: '1', label: 'بطاقات' },
                    { value: '2', label: 'شحن' },
                  ]}
                  onChange={(val: string) => setFieldValue('type', val)}
                  value={values.type}
                />
              </Grid>

              <Grid
                container
                item
                xs={12}
                md={12}
                sx={{ alignItems: 'center', marginBottom: '20px' }}
              >
                <Grid item xs={12} md={2} lg={1}>
                  <CheckboxInput
                    label={t('pages.products.form.offer')}
                    onChange={(e) => setFieldValue('offer', e.target.checked)}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  md={6}
                  lg={4}
                  sx={{ visibility: values.offer ? 'visible' : 'hidden' }}
                >
                  <TextField
                    name="offer_point"
                    type="number"
                    fullWidth
                    label={t('pages.products.form.priceInPoints')}
                    value={values.offer_point}
                    onChange={handleChange}
                    error={touched.offer_point && !!errors.offer_point}
                    helperText={touched.offer_point && errors.offer_point}
                  />
                </Grid>
              </Grid>
              <FormActions
                isSubmitting={false}
                // {isAddLoading || isEditLoading}
                submitButtonText={
                  categoryId ? t('common.edit') : t('common.add')
                }
                handleCancel={() => navigate(`/productsCategories`)}
              />
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditProductsCategory
