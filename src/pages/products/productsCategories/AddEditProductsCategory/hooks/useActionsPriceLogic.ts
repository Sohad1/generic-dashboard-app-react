import {
  useAddPriceMutation,
  useEditPriceMutation,
  useGetPriceQuery,
} from '@/api/products/prices/usePricesQueries'
import { TInitialValue } from './type'
import { TCategory } from '@/api/products/products/type'

function useActionsPriceLogic(priceId: string | undefined) {
  const { data, isLoading, isFetching, isError } = useGetPriceQuery(priceId!)

  console.log(data)

  const initialValues: TInitialValue = data
    ? {
        company: {
          id: data.category.company.id,
          name: data.category.company.name,
          data: data.category.company.categories,
        },
        category: {
          id: data.category.id,
          name: data.category.name,
        },
        point: data.point,
        category_id: data.category_id,
        description: data.description,
        has_verification: data.has_verification,
        link: data.link,
        image: data.image_url,
        logo: data.logo_url,
        offer: data.offer,
        type: data.type,
        value: data.value,
      }
    : {
        company: null,
        category: null,
        point: null,
        category_id: null,
        description: '',
        has_verification: '1',
        image: null,
        link: '',
        logo: null,
        offer: 0,
        type: '1',
        value: '',
      }
  const {} = useAddPriceMutation()
  const {} = useEditPriceMutation()
  return {
    initialValues,
    isLoading,
    isFetching,
    isError,
  }
}

export default useActionsPriceLogic
