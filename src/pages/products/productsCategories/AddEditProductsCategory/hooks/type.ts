import { TProductCategory } from '@/api/products/prices/type'
import { TCategory } from '@/api/products/products/type'

export type TParams = {
  categoryId: string | undefined
}

export type TInitialValue = TProductCategory & {
  company: { id: string; name: string; data: TCategory[] }
  category: { id: string; name: string }
}
