import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const priceSchema = Yup.object().shape({
    image: Yup.mixed().required(t('form.fileRequired')),
    logo: Yup.mixed().required(t('form.fileRequired')),
    value: Yup.string().required(t('form.required')),
    point: Yup.number().required(t('form.required')),
    link: Yup.string().required(t('form.required')),
    description: Yup.string().required(t('form.required')),
    category_id: Yup.string().required(t('form.required')),
    offer: Yup.boolean().required(t('form.required')),
    offer_point: Yup.number().when('offer', {
      is: false,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
  })

  return priceSchema
}

export default useValidationSchema
