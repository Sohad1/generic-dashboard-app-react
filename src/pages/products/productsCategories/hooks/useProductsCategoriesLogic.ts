import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetPricesInfiniteQuery } from "@/api/products/prices/usePricesQueries";

const useProductsCategoriesLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchCategories, setSearchCategories] = useState<string>("");

  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");

  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetPricesInfiniteQuery(rowsPerPage);

  const handleSearchCategories = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddCategory = () => {
    navigate("/productsCategories/addCategory");
  };
  const handleEditCategory = (row: TRow) => {
    navigate(`/productsCategories/editCategory/${row.id}`);
  };

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);

  const handleDeleteCategory = (price: TRow) => {
    setOpenDeleteModal(true);
    setSelectedCategory({
      name: price.value,
      id: price.id,
    });
  };
  const handleCloseModal = () => setOpenDeleteModal(false);

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCategories,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCategories,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCategories,
    handleAddCategory,
    handleEditCategory,
    handleDeleteCategory,
    openDeleteModal,
    selectedCategory,
    handleCloseModal,
  };
};

export default useProductsCategoriesLogic;
