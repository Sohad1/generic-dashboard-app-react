import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "value",
      numeric: true,
      disablePadding: false,
      label: t("pages.products.form.value"),
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.type"),
    },
    {
      id: "priceInPoints",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.priceInPoints"),
    },
    {
      id: "offer",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.offer"),
    },
    {
      id: "offerPrice",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.offerPrice"),
    },
    {
      id: "product",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.product"),
    },
    {
      id: "company",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.company"),
    },
    {
      id: "image",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.image"),
    },
    {
      id: "logo",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.form.logo"),
    },
  ];
  return headCells;
};

export default useTableHeader;
