import { useDeletePriceMutation } from '@/api/products/prices/usePricesQueries'
import GenericDeleteModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {
  open: boolean
  handleClose: () => void
  price: { id: string; name: string }
}

function DeleteProductCategoryModal({ open, handleClose, price }: Props) {
  const { t } = useTranslation()

  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeletePriceMutation(handleClose)
  return (
    <GenericDeleteModal
      {...{
        open,
        title: `${t(`pages.prices.deletePrice`)} ${price?.name}`,
        handleClose,
        handleSubmit: () => deleteMutate(price?.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t('common.delete')!,
      }}
    />
  )
}

export default DeleteProductCategoryModal
