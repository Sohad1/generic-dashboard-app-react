import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "#",
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.name"),
    },
    {
      id: "classification",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.classification"),
    },
    {
      id: "iphone",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.iphone"),
    },
    {
      id: "image",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.image"),
    },
    {
      id: "logo",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.logo"),
    },
    {
      id: "price_img",
      numeric: false,
      disablePadding: false,
      label: t("pages.companies.price_img"),
    },
  ];
  return headCells;
};

export default useTableHeader;
