import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetCompaniesInfiniteQuery } from "@/api/products/companies/useCompaniesQueries";

const useCompaniesLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchCompanies, setSearchCompanies] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetCompaniesInfiniteQuery(rowsPerPage);
  const handleSearchCompanies = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddCompany = () => {
    navigate("/companies/addCompany");
  };
  const handleEditCompany = (row: TRow) => {
    navigate(`/companies/editCompany/${row.id}`);
  };

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectedCompany, setSelectedCompany] = useState(null);

  const handleDeleteCompany = (company: TRow) => {
    setOpenDeleteModal(true);
    setSelectedCompany({
      name: company.name,
      id: company.id,
    });
  };
  const handleClose = () => setOpenDeleteModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCompanies,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCompanies,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCompanies,
    handleAddCompany,
    handleEditCompany,
    openDeleteModal,
    selectedCompany,
    handleClose,
    handleDeleteCompany,
  };
};

export default useCompaniesLogic;
