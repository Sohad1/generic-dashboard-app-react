import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useCompanyValidationSchema() {
  const { t } = useTranslation()

  const validationSchema = Yup.object().shape({
    group_id: Yup.string().required(`${t('form.required')}`),
    name: Yup.string().required(`${t('form.required')}`),
    image: Yup.mixed().required('File is required'),
    logo: Yup.mixed().required('File is required'),
    price_image: Yup.mixed().required('File is required'),
  })
  return validationSchema
}

export default useCompanyValidationSchema
