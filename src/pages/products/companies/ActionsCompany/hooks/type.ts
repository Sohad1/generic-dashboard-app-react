export type TParams = {
  companyId?: string | undefined
  id?: string | undefined

}
//id
// group_id
// name
// apple_hide
// color
// image
// logo

export type TInitialValue = {
  group: { id: string; name: string }
  group_id: string
  name: string
  apple_hide: '1' | '0'
  color: string
  image: File | string
  logo: File | string
  price_image: File | string
}
