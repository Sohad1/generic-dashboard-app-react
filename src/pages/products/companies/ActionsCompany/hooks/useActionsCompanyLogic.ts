import {
  useAddCompanyMutation,
  useEditCompanyMutation,
  useGetCompanyInfoQuery,
} from '@/api/products/companies/useCompaniesQueries'
import { useTranslation } from 'react-i18next'
import { TInitialValue } from './type'

function useActionsCompanyLogic(companyId: string) {
  const { t, i18n } = useTranslation()

  const { data, isLoading, isFetching, isError } =
    useGetCompanyInfoQuery(companyId)

  const initialValues: TInitialValue = data
    ? {
        group: {
          id: data.group_id,
          name:
            i18n.language === 'ar' ? data.group.name_ar : data.group.name_en,
        },
        group_id: data.group_id,
        name: data.name,
        apple_hide: data.apple_hide,
        color: data.color,
        image: data.image_url,
        logo: data.logo_url,
        price_image: data.price_image_url,
      }
    : {
        group: null,
        group_id: null,
        name: '',
        apple_hide: '0',
        color: '#fff',
        image: null,
        logo: null,
        price_image: null,
      }

  const iphoneOptions = [
    { value: '0', label: t('pages.companies.visible') },
    { value: '1', label: t('pages.companies.hidden') },
  ]

  const { mutate: addMutate, isLoading: isAddLoading } = useAddCompanyMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditCompanyMutation()

  return {
    initialValues,
    iphoneOptions,
    isLoading,
    isFetching,
    isError,
    addMutate,
    isAddLoading,
    editMutate,
    isEditLoading,
  }
}

export default useActionsCompanyLogic
