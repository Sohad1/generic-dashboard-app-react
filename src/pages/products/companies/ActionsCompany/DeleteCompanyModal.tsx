import { useDeleteCompanyMutation } from '@/api/products/companies/useCompaniesQueries'
import GenericDeleteModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import { useTranslation } from 'react-i18next'

type Props = {
  open: boolean
  handleClose: () => void
  company: { id: string; name: string }
}

function DeleteCompanyModal({ open, handleClose, company }: Props) {
  const { t } = useTranslation()

  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteCompanyMutation(handleClose)
  return (
    <GenericDeleteModal
      {...{
        open,
        title: `${t(`pages.companies.deleteCompany`)} ${company?.name}`,
        handleClose,
        handleSubmit: () => deleteMutate(company?.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t('common.delete')!,
      }}
    />
  )
}

export default DeleteCompanyModal
