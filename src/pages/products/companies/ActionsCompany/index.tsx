import React from 'react'
import useCompanyValidationSchema from './hooks/useValidationSchema'
import useActionsCompanyLogic from './hooks/useActionsCompanyLogic'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import { Form, Formik } from 'formik'
import { Grid, TextField } from '@mui/material'
import { useNavigate, useParams } from 'react-router-dom'
import { TParams } from './hooks/type'
import { useTranslation } from 'react-i18next'
import Loading from '@/components/others/loading'
import RadioButtons from '@/components/items/inputField/radio/RadioButtons'
import FormActions from '@/components/items/form/FormActions/FormActions'
import GroupsAutoComplete from '@/components/pages/products/AutoCompletes/GroupsAutoComplete'
import ImageDragDropField from '@/components/items/inputField/ImageDragDropField'

function AddEditCompany() {
  const { companyId } = useParams<TParams>()
  const { t } = useTranslation()
  const navigate = useNavigate()
  const validationSchema = useCompanyValidationSchema()
  const {
    isLoading,
    isError,
    initialValues,
    iphoneOptions,
    editMutate,
    addMutate,
    isAddLoading,
    isEditLoading,
  } = useActionsCompanyLogic(companyId)

  if (!!companyId) {
    if (isLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  return (
    <GenericFormPage
      title={
        companyId
          ? `${t(`common.edit`)} ${t(`pages.products.company`)}`
          : `${t(`common.add`)} ${t(`pages.products.company`)}`
      }
      goBackLink="/companies"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          let data = { ...values }
          delete data.group
          if (typeof values.image == 'string') delete data.image
          if (typeof values.logo == 'string') delete data.logo
          if (typeof values.price_image == 'string') delete data.price_image

          console.log(data)
          companyId
            ? editMutate({ params: { id: companyId }, body: data })
            : addMutate({ body: data })
        }}
        validationSchema={validationSchema}
      >
        {({ values, handleChange, touched, errors, setFieldValue }) => {
          // console.log(errors, values)
          return (
            <Form>
              <Grid
                container
                // justifyContent={'center'}
                alignItems={'center'}
                spacing={2}
              >
                <Grid container item xs={12} spacing={2}>
                  <Grid item xs={12} sm={4}>
                    <ImageDragDropField
                      handleChangeFile={(v) => {
                        setFieldValue('image', v)
                        console.log(v)
                      }}
                      file={values.image}
                      name="image"
                      type={companyId ? 'edit' : 'add'}
                      label={'image'}
                      error={touched.image && !!errors.image}
                    />
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <ImageDragDropField
                      handleChangeFile={(v) => setFieldValue('logo', v)}
                      file={values.logo}
                      name="logo"
                      type={companyId ? 'edit' : 'add'}
                      label={'logo'}
                      error={touched.logo && !!errors.logo}
                    />
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <ImageDragDropField
                      handleChangeFile={(v) => setFieldValue('price_image', v)}
                      file={values.price_image}
                      name="price_image"
                      type={companyId ? 'edit' : 'add'}
                      label={'price image'}
                      error={touched.price_image && !!errors.price_image}
                    />
                  </Grid>
                </Grid>
                <Grid item xs={12} md={6} lg={4}>
                  <GroupsAutoComplete
                    name="group"
                    value={values.group}
                    setFieldValue={setFieldValue}
                    error={touched.group && !!errors.group}
                    errorMessage={touched.group_id && errors.group_id}
                  />
                </Grid>
                <Grid item xs={12} md={6} lg={4}>
                  <TextField
                    name="name"
                    fullWidth
                    label={t('pages.companies.name')}
                    value={values.name}
                    onChange={handleChange}
                    error={touched.name && !!errors.name}
                    helperText={touched.name && errors.name}
                  />
                </Grid>
                <Grid item xs={12} md={6} lg={4}>
                  <Grid item xs={12} md={10}>
                    <TextField
                      name="color"
                      type="color"
                      fullWidth
                      label={t('pages.companies.color')}
                      value={values.color}
                      onChange={handleChange}
                      error={touched.color && !!errors.color}
                      helperText={touched.color && errors.color}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <RadioButtons
                      title={t('pages.products.form.phone')}
                      name="apple_hide"
                      onChange={(val) => setFieldValue('apple_hide', val)}
                      options={iphoneOptions}
                      value={values.apple_hide}
                    />
                  </Grid>
                </Grid>

                <FormActions
                  isSubmitting={isAddLoading || isEditLoading}
                  submitButtonText={
                    companyId ? t('common.edit') : t('common.add')
                  }
                  handleCancel={() => navigate(`/companies`)}
                />
              </Grid>
            </Form>
          )
        }}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditCompany
