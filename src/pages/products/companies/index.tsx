import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";

import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
import useCompaniesLogic from "./hooks/useCompaniesLogic";
import DeleteCompanyModal from "./ActionsCompany/DeleteCompanyModal";

const Companies = () => {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const ability = useContext(AbilityContext);
  const {
    data,
    page,
    rowsPerPage,
    searchCompanies,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCompanies,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCompanies,
    handleAddCompany,
    handleEditCompany,
    handleDeleteCompany,
    handleClose,
    openDeleteModal,
    selectedCompany,
  } = useCompaniesLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <DeleteCompanyModal
        open={openDeleteModal}
        company={selectedCompany}
        handleClose={handleClose}
      />
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchCompanies}
          setValue={setSearchCompanies}
          debouncedFunction={handleSearchCompanies}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_companies">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddCompany}
          >
            {t("pages.companies.addcompany")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.companies.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.companies.data)
            .reduce((previous, current) => [...previous, ...current]) as any[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page?.response.companies.data)
            .reduce((previous, current) => [...previous, ...current]) as any[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell></TableCell>
                <TableCell>{row.apple_hide == "0" ? "vis" : "hid"}</TableCell>
                <TableCell>
                  <img src={row.image_url} alt={row.name} width={50} />
                </TableCell>
                <TableCell>
                  <img src={row.logo_url} alt={row.name} width={50} />
                </TableCell>
                <TableCell>
                  <img src={row.price_image_url} alt={row.name} width={50} />
                </TableCell>

                <TableCell>
                  <TableActions
                    edit={() =>
                      ability.can("update", "dashboard_api_companies") &&
                      handleEditCompany(row)
                    }
                    delete={() =>
                      ability.can("destroy", "dashboard_api_companies") &&
                      handleEditCompany(row)
                    }
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
};

export default Companies;
