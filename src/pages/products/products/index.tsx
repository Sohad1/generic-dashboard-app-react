import { useTranslation } from 'react-i18next'
import { Box, Button, Stack, TableCell } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import { getComparator, stableSort } from '@/util/table'
import { TRow } from '@/type/Table'
import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import TableActions from '@/components/items/table/genericTable/tableActions'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import Loading from '@/components/others/loading'
import useTableHeader from './hooks/useTableHeader'
import useProductsLogic from './hooks/useProductsLogic'
import { useContext } from 'react'
import { AbilityContext, Can } from '@/libs/casl/can'

const Products = () => {
  const { t, i18n } = useTranslation()

  const ability = useContext(AbilityContext)
  const headCells = useTableHeader()
  const {
    data,
    page,
    rowsPerPage,
    searchProducts,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchProducts,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchProducts,
    handleAddProduct,
    handleEditProduct,
  } = useProductsLogic()

  if (isLoading) return <Loading />

  if (isError) return <></>

  return (
    <Box>
      <Stack direction={'row'} justifyContent={'space-between'} sx={{ mb: 3 }}>
        <DebouncedTextField
          value={searchProducts}
          setValue={setSearchProducts}
          debouncedFunction={handleSearchProducts}
          label={t('common.search')}
          size="small"
        />
        <Can I="store" a="dashboard_api_categories">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddProduct}
          >
            {t('pages.products.addProduct')}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.categories.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.categories.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page?.response.categories.data)
            .reduce((previous, current) => [...previous, ...current]) as any[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.company?.name}</TableCell>
                <TableCell>
                  {row.countries.length < 4 ? (
                    row.countries
                      ?.map((item: any, index: number) =>
                        i18n.language === 'ar'
                          ? item.country_arName
                          : item.country_enName
                      )
                      .join(' , ')
                  ) : (
                    <>
                      {row.countries?.map((item: any, index: number) => {
                        if (index < 4)
                          return i18n.language === 'ar'
                            ? item.country_arName + ','
                            : item.country_enName + ','
                      })}
                      ...
                    </>
                  )}
                </TableCell>

                <TableCell>
                  <TableActions
                    edit={() =>
                      ability.can('update', 'dashboard_api_categories') &&
                      handleEditProduct(row)
                    }
                    delete={() =>
                      ability.can('destroy', 'dashboard_api_categories') &&
                      handleEditProduct(row)
                    }
                  />
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
    </Box>
  )
}

export default Products
