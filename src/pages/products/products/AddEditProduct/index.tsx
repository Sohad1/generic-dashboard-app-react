/* eslint-disable eqeqeq */
import { Form, Formik } from 'formik'
import { Grid, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import useProductValidationSchema from './hooks/useValidationSchema'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import { useNavigate, useParams } from 'react-router-dom'
import { TParams } from './hooks/type'
import FormActions from '@/components/items/form/FormActions/FormActions'
import CountriesAutoComplete from '@/components/common/CountriesAutoComplete'
import CompaniesAutoComplete from '@/components/pages/products/AutoCompletes/CompaniesAutoComplete'
import useActionProductLogic from './hooks/useActionProductLogic'
import Loading from '@/components/others/loading'

function AddEditProduct() {
  const { t } = useTranslation()
  const validationSchema = useProductValidationSchema()
  const { productId } = useParams<TParams>()
  const navigate = useNavigate()
  const {
    initialValues,
    addProduct,
    isAddLoading,
    editProduct,
    isEditLoading,
    isLoading,
    isError,
  } = useActionProductLogic(productId!)
  if (!!productId) {
    if (isLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  return (
    <GenericFormPage
      title={
        productId
          ? `${t(`common.edit`)} ${t(`pages.products.product`)}`
          : `${t(`common.add`)} ${t(`pages.products.product`)}`
      }
      goBackLink="/products"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          let findAllCountriesOption = values.countries.filter(
            (item) => item.id == '0'
          )
          let data = { ...values, company_id: values.companies.id }
          if (findAllCountriesOption.length > 0) {
            data.country_ids = [0]
          } else {
            let countries = values.countries.map((item: any) => item.id)
            data.country_ids = countries
          }
          delete data.companies
          delete data.countries

          productId
            ? editProduct({ params: { id: productId }, body: { ...data } })
            : addProduct({ body: data })
        }}
        validationSchema={validationSchema}
      >
        {({ values, handleChange, setFieldValue, touched, errors }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <CountriesAutoComplete
                  name="countries"
                  multiple={true}
                  value={values.countries}
                  error={touched.countries && !!errors.countries}
                  setFieldValue={setFieldValue}
                  errorMessage=""
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <CompaniesAutoComplete
                  name="companies"
                  value={values.companies}
                  setFieldValue={setFieldValue}
                  error={touched.companies && !!errors.companies}
                  errorMessage=""
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name"
                  fullWidth
                  label={t('pages.products.form.name')}
                  value={values.name}
                  onChange={handleChange}
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  multiline
                  label={t('pages.competitions.addEditCompetition.description')}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>
              <FormActions
                isSubmitting={isAddLoading || isEditLoading}
                submitButtonText={
                  productId ? t('common.edit') : t('common.add')
                }
                handleCancel={() => navigate(`/products`)}
              />
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditProduct
