export type TParams = {
  productId?: string | undefined
  id?: string | undefined
}

export type TInitialValue = {
  countries: { id: string; name: string }[]
  country_ids: { id: string; name: string }[] | [0]
  companies: { id: string; name: string }
  company_id: string
  name: string
  description: string
}
