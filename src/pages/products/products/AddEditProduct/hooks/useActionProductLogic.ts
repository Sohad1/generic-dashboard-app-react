import {
  useAddCategoryMutation,
  useEditCategoryMutation,
  useGetCategoryQuery,
} from '@/api/products/products/useProductsQueries'
import { TInitialValue } from './type'

const useActionProductLogic = (productId: string | undefined) => {
  const { data, isLoading, isError, isFetching } = useGetCategoryQuery(
    productId!
  )
  console.log(data)
  const initialValues: TInitialValue = data
    ? {
        countries: data.response.category.countries.map((item) => ({
          id: String(item.id),
          name: item.country_arName,
        })),
        companies: {
          id: data.response.category.company?.id,
          name: data.response.category.company?.name,
        },
        country_ids: null,
        company_id: data.response.category.company?.id,
        name: data.response.category.name,
        description: data.response.category.description,
      }
    : {
        countries: null,
        companies: null,
        country_ids: null,
        company_id: null,
        name: '',
        description: '',
      }
  const { mutate: addProduct, isLoading: isAddLoading } =
    useAddCategoryMutation()
  const { mutate: editProduct, isLoading: isEditLoading } =
    useEditCategoryMutation()

  return {
    initialValues,
    isLoading,
    isError,
    isFetching,
    addProduct,
    isAddLoading,
    editProduct,
    isEditLoading,
  }
}

export default useActionProductLogic
