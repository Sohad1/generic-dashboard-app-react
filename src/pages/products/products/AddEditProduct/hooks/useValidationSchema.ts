import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

const useProductValidationSchema = () => {
  const { t } = useTranslation()

  const validationSchema = Yup.object({
    countries: Yup.array(Yup.object({ id: Yup.number(), name: Yup.string() }))
      .min(1)
      .required(`${t('form.required')}`),
    companies: Yup.object({ id: Yup.number(), name: Yup.string() }).required(
      `${t('form.required')}`
    ),
    name: Yup.string().required(`${t('form.required')}`),
    description: Yup.string().required(`${t('form.required')}`),
  })
  return validationSchema
}

export default useProductValidationSchema
