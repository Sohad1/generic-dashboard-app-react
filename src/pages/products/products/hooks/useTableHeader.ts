import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "product",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.product"),
    },
    {
      id: "company",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.company"),
    },
    {
      id: "countries",
      numeric: false,
      disablePadding: false,
      label: t("pages.products.countries"),
    },
  ];
  return headCells;
};

export default useTableHeader;
