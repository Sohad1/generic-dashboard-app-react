import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetCategoriesInfiniteQuery } from "@/api/categories/useCategoriesQueries";

import { useNavigate } from "react-router-dom";
const useProductsLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchProducts, setSearchProducts] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetCategoriesInfiniteQuery(rowsPerPage);
  const handleSearchProducts = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddProduct = () => {
    navigate("/products/addProduct");
  };
  const handleEditProduct = (row: TRow) => {
    navigate(`/products/editProduct/${row.id}`);
  };
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchProducts,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchProducts,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchProducts,
    handleAddProduct,
    handleEditProduct,
  };
};

export default useProductsLogic;
