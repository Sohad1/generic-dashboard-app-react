import React from "react";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
import { useDeleteBuildNumberMutation } from "@/api/buildNumber/useBuildNumbersQueries";
type Props = {
  open: boolean;
  handleClose: () => void;
  buildNumber: { id: number; number: string };
};
const DeleteBuildNumberModal = ({ buildNumber, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteBuildNumberMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${buildNumber.number}`,
        handleClose,
        handleSubmit: () => deleteMutate(buildNumber.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t("common.remove")!,
      }}
    />
  );
};

export default DeleteBuildNumberModal;
