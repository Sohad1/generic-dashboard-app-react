import Loading from "@/components/others/loading";
import React from "react";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";

import { useTranslation } from "react-i18next";
import useBuildNumberLogic from "./hooks/useBuildNumberLogic";
import useTableHeader from "./hooks/useTableHeader";
import DeleteBuildNumberModal from "./deleteBuildNumberModal";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";

const BuildNumbers = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchBuildNumbers,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchBuildNumbers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchBuildNumbers,
    handleAddBuildNumber,
    handleEditBuildNumber,
    handleDeleteBuildNumber,
    openDeleteModal,
    handleCloseDeleteModal,
    selectedBuildNumber,
  } = useBuildNumberLogic();
  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <DeleteBuildNumberModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          buildNumber: selectedBuildNumber,
        }}
      />
      <Box>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <DebouncedTextField
            value={searchBuildNumbers}
            setValue={setSearchBuildNumbers}
            debouncedFunction={handleSearchBuildNumbers}
            label={t("common.search")}
            size="small"
          />
          <Can I="store" a="dashboard_api_build_number">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddBuildNumber}
            >
              {t("pages.build_numbers.addBuildNumber")}
            </Button>
          </Can>
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.build_numbers.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.build_numbers.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.build_numbers.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as TRow[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.number}</TableCell>
                  <TableCell>{row.type}</TableCell>
                  <TableCell>
                    {row.force === 0 ? "compulsory" : "optional"}
                  </TableCell>
                  <TableCell>
                    <TableActions
                      edit={() => {
                        ability.can("update", "dashboard_api_build_number") &&
                          handleEditBuildNumber(row);
                      }}
                      delete={() => {
                        ability.can("destroy", "dashboard_api_build_number") &&
                          handleDeleteBuildNumber(row);
                      }}
                    />
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </GenericTable>
      </Box>
    </Box>
  );
};

export default BuildNumbers;
