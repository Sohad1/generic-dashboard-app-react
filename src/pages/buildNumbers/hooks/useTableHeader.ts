import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "number",
      numeric: true,
      disablePadding: false,
      label: t("pages.build_numbers.number"),
    },
    {
      id: "applicationType",
      numeric: false,
      disablePadding: false,
      label: t("pages.build_numbers.applicationType"),
    },
    {
      id: "buildType",
      numeric: false,
      disablePadding: false,
      label: t("pages.build_numbers.buildType"),
    },
  ];
  return headCells;
};

export default useTableHeader;
