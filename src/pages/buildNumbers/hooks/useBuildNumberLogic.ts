import React from "react";
import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";

import { useNavigate } from "react-router-dom";
import { useGetBuildNumbersInfiniteQuery } from "@/api/buildNumber/useBuildNumbersQueries";

const useBuildNumberLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchBuildNumbers, setSearchBuildNumbers] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [selectedBuildNumber, setSelectedBuildNumber] = useState({
    id: 0,
    number: "",
  });

  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetBuildNumbersInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchBuildNumbers = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddBuildNumber = () => {
    navigate("/build_numbers/addBuildNumber");
  };
  const handleEditBuildNumber = (row: TRow) => {
    navigate(`/build_numbers/editBuildNumber/${row.id}`);
  };
  const handleDeleteBuildNumber = (row: TRow) => {
    setOpenDeleteModal(true);
    setSelectedBuildNumber({
      number: String(row.number),
      id: Number(row.id),
    });
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchBuildNumbers,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchBuildNumbers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchBuildNumbers,
    handleAddBuildNumber,
    handleEditBuildNumber,
    handleDeleteBuildNumber,
    handleCloseDeleteModal,
    openDeleteModal,
    selectedBuildNumber,
  };
};

export default useBuildNumberLogic;
