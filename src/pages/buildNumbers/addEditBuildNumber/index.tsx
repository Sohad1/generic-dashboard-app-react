import {
  useAddBuildNumberMutation,
  useEditBuildNumberMutation,
  useGetBuildNumberQuery,
} from '@/api/buildNumber/useBuildNumbersQueries'
import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Grid, TextField } from '@mui/material'
import { Form, Formik } from 'formik'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { TInitialValues, TParams } from './hooks/type'
import SelectField from '@/components/items/inputField/selectField/SelectField'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import useValidationSchema from './hooks/useValidation'

const AddEditBuildNumber = () => {
  const { t } = useTranslation()
  const { buildNumberId } = useParams<TParams>()
  const validationSchema = useValidationSchema()

  const {
    data,
    isLoading: isGetBuildNumberLoading,
    isError,
  } = useGetBuildNumberQuery(buildNumberId)
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddBuildNumberMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditBuildNumberMutation()
  if (!!buildNumberId) {
    if (isGetBuildNumberLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  const initialValues: TInitialValues = data?.response.build_number
    ? { ...data.response.build_number }
    : {
        number: 0,
        type: '',
        force: 0,
      }
  console.log(buildNumberId)
  console.log(data?.response.build_number)
  return (
    <GenericFormPage
      title={
        buildNumberId
          ? t('pages.build_numbers.editBuildNumber')
          : t('pages.build_numbers.addBuildNumber')
      }
      goBackLink="/build_numbers"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          buildNumberId
            ? editMutate({ body: values })
            : addMutate({ body: values })
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="number"
                  fullWidth
                  label={t('pages.build_numbers.number')}
                  value={values.number}
                  onChange={handleChange}
                  error={touched.number && !!errors.number}
                  helperText={touched.number && errors.number}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="type"
                  fullWidth
                  label={t('pages.build_numbers.applicationType')}
                  value={values.type}
                  onChange={handleChange}
                  error={touched.type && !!errors.type}
                  helperText={touched.type && errors.type}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <SelectField
                  name="force"
                  label={t('pages.build_numbers.buildType')}
                  value={String(values.force)}
                  onChange={handleChange}
                  options={[
                    { value: 0, text: 'compulsory' },
                    { value: 1, text: 'optional' },
                  ]}
                  error={touched.force && !!errors.force}
                />
              </Grid>

              {/* <TextField
                        name="bool"
                        fullWidth
                        label={t("pages.settings.addEditSetting.necessity")}
                        value={values.bool}
                        onChange={handleChange}
                        error={touched.bool && !!errors.bool}
                        helperText={touched.bool && errors.bool}
                      /> */}
              {/* <Grid item xs={12} md={10}>
                          <SelectField
                            name="type"
                            label={t("pages.settings.addEditSetting.necessity")}
                            value={String(values.type)}
                            onChange={handleChange}
                            options={[
                              { value: 0, text: "google" },
                              { value: 1, text: "iphone" },
                            ]}
                            error={touched.type && !!errors.type}
                          />
                        </Grid> */}
              <Grid
                item
                xs={12}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={
                    buildNumberId ? t('common.edit') : t('common.add')
                  }
                />
                {/* <Button
                    sx={{ marginLeft: 1 }}
                    onClick={() => {
                      navigate("/categories");
                    }}
                  >
                    {t("common.cancel")}
                  </Button> */}
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditBuildNumber
