import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const cardSchema = Yup.object().shape({
    number: Yup.string().required(t('form.required')),
    type: Yup.string().required(t('form.required')),
    force: Yup.string().required(t('form.required')),
  })

  return cardSchema
}

export default useValidationSchema
