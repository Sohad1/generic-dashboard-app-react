import { TBuildNumber } from "@/api/buildNumber/type";

export type TInitialValues = TBuildNumber;

export type TParams = {
  buildNumberId: string | undefined;
};
