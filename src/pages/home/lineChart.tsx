// install (please try to align the version of installed @nivo packages)
// yarn add @nivo/line
import { ResponsiveLine } from "@nivo/line";

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
const LineChart = () => {
  const data = [
    {
      id: "japan",
      color: "hsl(63, 70%, 50%)",
      data: [
        {
          x: "plane",
          y: 62,
        },
        {
          x: "helicopter",
          y: 100,
        },
        {
          x: "boat",
          y: 163,
        },
        {
          x: "train",
          y: 170,
        },
        {
          x: "subway",
          y: 57,
        },
        {
          x: "bus",
          y: 103,
        },
        {
          x: "car",
          y: 61,
        },
        {
          x: "moto",
          y: 96,
        },
        {
          x: "bicycle",
          y: 169,
        },
        {
          x: "horse",
          y: 285,
        },
        {
          x: "skateboard",
          y: 105,
        },
        {
          x: "others",
          y: 143,
        },
      ],
    },
    {
      id: "france",
      color: "hsl(6, 70%, 50%)",
      data: [
        {
          x: "plane",
          y: 190,
        },
        {
          x: "helicopter",
          y: 86,
        },
        {
          x: "boat",
          y: 237,
        },
        {
          x: "train",
          y: 140,
        },
        {
          x: "subway",
          y: 31,
        },
        {
          x: "bus",
          y: 29,
        },
        {
          x: "car",
          y: 283,
        },
        {
          x: "moto",
          y: 240,
        },
        {
          x: "bicycle",
          y: 87,
        },
        {
          x: "horse",
          y: 124,
        },
        {
          x: "skateboard",
          y: 133,
        },
        {
          x: "others",
          y: 113,
        },
      ],
    },
    {
      id: "us",
      color: "hsl(290, 70%, 50%)",
      data: [
        {
          x: "plane",
          y: 137,
        },
        {
          x: "helicopter",
          y: 162,
        },
        {
          x: "boat",
          y: 294,
        },
        {
          x: "train",
          y: 117,
        },
        {
          x: "subway",
          y: 246,
        },
        {
          x: "bus",
          y: 108,
        },
        {
          x: "car",
          y: 298,
        },
        {
          x: "moto",
          y: 254,
        },
        {
          x: "bicycle",
          y: 62,
        },
        {
          x: "horse",
          y: 269,
        },
        {
          x: "skateboard",
          y: 187,
        },
        {
          x: "others",
          y: 145,
        },
      ],
    },
    {
      id: "germany",
      color: "hsl(301, 70%, 50%)",
      data: [
        {
          x: "plane",
          y: 99,
        },
        {
          x: "helicopter",
          y: 265,
        },
        {
          x: "boat",
          y: 156,
        },
        {
          x: "train",
          y: 176,
        },
        {
          x: "subway",
          y: 228,
        },
        {
          x: "bus",
          y: 194,
        },
        {
          x: "car",
          y: 16,
        },
        {
          x: "moto",
          y: 43,
        },
        {
          x: "bicycle",
          y: 270,
        },
        {
          x: "horse",
          y: 160,
        },
        {
          x: "skateboard",
          y: 17,
        },
        {
          x: "others",
          y: 67,
        },
      ],
    },
    {
      id: "norway",
      color: "hsl(224, 70%, 50%)",
      data: [
        {
          x: "plane",
          y: 26,
        },
        {
          x: "helicopter",
          y: 62,
        },
        {
          x: "boat",
          y: 91,
        },
        {
          x: "train",
          y: 135,
        },
        {
          x: "subway",
          y: 136,
        },
        {
          x: "bus",
          y: 36,
        },
        {
          x: "car",
          y: 45,
        },
        {
          x: "moto",
          y: 53,
        },
        {
          x: "bicycle",
          y: 165,
        },
        {
          x: "horse",
          y: 165,
        },
        {
          x: "skateboard",
          y: 168,
        },
        {
          x: "others",
          y: 208,
        },
      ],
    },
  ];
  return (
    <ResponsiveLine
      data={data}
      margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
      xScale={{ type: "point" }}
      yScale={{
        type: "linear",
        min: "auto",
        max: "auto",
        stacked: true,
        reverse: false,
      }}
      yFormat=" >-.2f"
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: "transportation",
        legendOffset: 36,
        legendPosition: "middle",
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: "count",
        legendOffset: -40,
        legendPosition: "middle",
      }}
      pointSize={10}
      pointColor={{ theme: "background" }}
      pointBorderWidth={2}
      pointBorderColor={{ from: "serieColor" }}
      pointLabelYOffset={-12}
      useMesh={true}
      legends={[
        {
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 100,
          translateY: 0,
          itemsSpacing: 0,
          itemDirection: "left-to-right",
          itemWidth: 80,
          itemHeight: 20,
          itemOpacity: 0.75,
          symbolSize: 12,
          symbolShape: "circle",
          symbolBorderColor: "rgba(0, 0, 0, .5)",
          effects: [
            {
              on: "hover",
              style: {
                itemBackground: "rgba(0, 0, 0, .03)",
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
    />
  );
};
export default LineChart;
