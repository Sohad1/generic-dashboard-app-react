import React from "react";
import {
  Box,
  Card,
  CardContent,
  Typography,
  Grid,
  SvgIconProps,
} from "@mui/material";

type props = {
  title: string;
  value: number;
  icon: React.ReactElement<SvgIconProps>;
};

const StatsCard = ({ title, value, icon }: props) => {
  return (
    <Card
      sx={{
        minWidth: 210,
        backgroundColor: "#fff",
        boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
        borderRadius: "10px",
        margin: "10px 20px",
      }}
    >
      <CardContent>
        <Grid container alignItems="center">
          <Grid item xs={9}>
            <Typography
              sx={{ fontSize: 14, fontWeight: "bold", color: "#444" }}
            >
              {title}
            </Typography>
            <Typography
              sx={{
                fontSize: 25,
                fontWeight: "bold",
                color: "#333",
                marginTop: "10px",
              }}
            >
              {value}
            </Typography>
          </Grid>
          <Grid item xs={12} md={3}>
            <Typography sx={{ color: "#444" }}>{icon}</Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default StatsCard;
