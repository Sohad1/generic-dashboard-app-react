import React from "react";
import { Button, Paper, Typography, Box } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";

import { useTranslation } from "react-i18next";
import { useGetHomeQuery } from "@/api/home/useHomeQueries";
import StatsCard from "./statsCard";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import GroupIcon from "@mui/icons-material/Group";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import PendingActionsIcon from "@mui/icons-material/PendingActions";
import AssignmentTurnedInIcon from "@mui/icons-material/AssignmentTurnedIn";
import PieChart from "./pieChart";
import BarChart from "./barChart";
import LineChart from "./lineChart";
import Loading from "@/components/others/loading";
const Home = () => {
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetHomeQuery();
  console.log(data?.response.pie);
  if (Array.isArray(data?.response.pie)) {
    console.log("Array");
  }
  return (
    <Box>
      {/* <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Card sx={CARD_STYLE}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {t("pages.home.аllusers")}
            </Typography>
            <Paper variant="outlined" />
            <Typography variant="h3" component="h2" sx={{ marginTop: 2 }}>
              {data?.response.home?.users_count}
            </Typography>
          </CardContent>
        </Card>
        <Card sx={CARD_STYLE}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {t("pages.home.Thelast30minutes")}
            </Typography>
            <Paper variant="outlined" />
            <Typography variant="h3" component="h2" sx={{ marginTop: 2 }}>
              {data?.response.home?.last_30}
            </Typography>
          </CardContent>
        </Card>
        <Card sx={CARD_STYLE}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {t("pages.home.Pendingorders ")}
            </Typography>
            <Paper variant="outlined" />
            <Typography variant="h3" component="h2" sx={{ marginTop: 2 }}>
              {data?.response.home?.orders_count}
            </Typography>
          </CardContent>
        </Card>
        <Card sx={CARD_STYLE}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {t("pages.home.Executedorders")}
            </Typography>
            <Paper variant="outlined" />
            <Typography variant="h3" component="h2" sx={{ marginTop: 2 }}>
              {data?.response.home?.orders_approved_count}
            </Typography>
          </CardContent>
        </Card>
      </Box> */}
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        width={"100%"}
        mb={5}
      >
        <StatsCard
          title="Total Users"
          value={data?.response.users_count}
          icon={<GroupIcon fontSize="large" />}
        />
        <StatsCard
          title={t("pages.home.Thelast30minutes")}
          value={data?.response.last_30}
          icon={<QueryBuilderIcon fontSize="large" />}
        />
        <StatsCard
          title={t("pages.home.Pendingorders ")}
          value={data?.response.orders_count}
          icon={<PendingActionsIcon fontSize="large" />}
        />
        <StatsCard
          title={t("pages.home.Executedorders")}
          value={data?.response.orders_approved_count}
          icon={<AssignmentTurnedInIcon fontSize="large" />}
        />
      </Box>
      <Box mt={2} width={"100%"}>
        <Box display={"flex"} justifyContent={"center"} mb={5}>
          <Box width={"50%"} height={300}>
            {isLoading ? <Loading /> : <PieChart data={data?.response.pie} />}
          </Box>
          <Box width={"50%"} height={300}>
            <BarChart />
          </Box>
        </Box>
        <Box width={"99%"} height={300}>
          <LineChart />
        </Box>
      </Box>
    </Box>
  );
};

export default Home;
const CARD_STYLE = {
  filter: "drop-shadow(3px 10px 8px rgba(0,0,0,0.50))",
  width: 200,
  height: 170,
  marginLeft: 7,
  background: " linear-gradient(147deg, #0060b1  10%, #fff 100%)",
  borderRadius: 3,
  color: "#fff",
};
