// install (please try to align the version of installed @nivo packages)
// yarn add @nivo/bar
import { ResponsiveBar } from "@nivo/bar";

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.

const BarChart = () => {
  const data = [
    {
      country: "AD",
      "hot dog": 18,
      "hot dogColor": "hsl(225, 70%, 50%)",
      burger: 109,
      burgerColor: "hsl(159, 70%, 50%)",
      sandwich: 99,
      sandwichColor: "hsl(151, 70%, 50%)",
      kebab: 125,
      kebabColor: "hsl(43, 70%, 50%)",
      fries: 36,
      friesColor: "hsl(48, 70%, 50%)",
      donut: 198,
      donutColor: "hsl(324, 70%, 50%)",
    },
    {
      country: "AE",
      "hot dog": 129,
      "hot dogColor": "hsl(72, 70%, 50%)",
      burger: 52,
      burgerColor: "hsl(14, 70%, 50%)",
      sandwich: 98,
      sandwichColor: "hsl(147, 70%, 50%)",
      kebab: 159,
      kebabColor: "hsl(326, 70%, 50%)",
      fries: 46,
      friesColor: "hsl(267, 70%, 50%)",
      donut: 37,
      donutColor: "hsl(119, 70%, 50%)",
    },
    {
      country: "AF",
      "hot dog": 50,
      "hot dogColor": "hsl(26, 70%, 50%)",
      burger: 46,
      burgerColor: "hsl(284, 70%, 50%)",
      sandwich: 16,
      sandwichColor: "hsl(227, 70%, 50%)",
      kebab: 105,
      kebabColor: "hsl(298, 70%, 50%)",
      fries: 183,
      friesColor: "hsl(104, 70%, 50%)",
      donut: 147,
      donutColor: "hsl(224, 70%, 50%)",
    },
    {
      country: "AG",
      "hot dog": 70,
      "hot dogColor": "hsl(208, 70%, 50%)",
      burger: 143,
      burgerColor: "hsl(352, 70%, 50%)",
      sandwich: 188,
      sandwichColor: "hsl(360, 70%, 50%)",
      kebab: 70,
      kebabColor: "hsl(57, 70%, 50%)",
      fries: 0,
      friesColor: "hsl(121, 70%, 50%)",
      donut: 95,
      donutColor: "hsl(168, 70%, 50%)",
    },
    {
      country: "AI",
      "hot dog": 123,
      "hot dogColor": "hsl(322, 70%, 50%)",
      burger: 135,
      burgerColor: "hsl(179, 70%, 50%)",
      sandwich: 179,
      sandwichColor: "hsl(359, 70%, 50%)",
      kebab: 112,
      kebabColor: "hsl(323, 70%, 50%)",
      fries: 20,
      friesColor: "hsl(276, 70%, 50%)",
      donut: 102,
      donutColor: "hsl(68, 70%, 50%)",
    },
    {
      country: "AL",
      "hot dog": 5,
      "hot dogColor": "hsl(17, 70%, 50%)",
      burger: 14,
      burgerColor: "hsl(9, 70%, 50%)",
      sandwich: 54,
      sandwichColor: "hsl(353, 70%, 50%)",
      kebab: 6,
      kebabColor: "hsl(121, 70%, 50%)",
      fries: 170,
      friesColor: "hsl(142, 70%, 50%)",
      donut: 49,
      donutColor: "hsl(26, 70%, 50%)",
    },
    {
      country: "AM",
      "hot dog": 30,
      "hot dogColor": "hsl(328, 70%, 50%)",
      burger: 116,
      burgerColor: "hsl(242, 70%, 50%)",
      sandwich: 134,
      sandwichColor: "hsl(312, 70%, 50%)",
      kebab: 130,
      kebabColor: "hsl(25, 70%, 50%)",
      fries: 165,
      friesColor: "hsl(245, 70%, 50%)",
      donut: 83,
      donutColor: "hsl(47, 70%, 50%)",
    },
  ];
  return (
    <ResponsiveBar
      data={data}
      keys={["hot dog", "burger", "sandwich", "kebab", "fries", "donut"]}
      indexBy="country"
      margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
      padding={0.25}
      valueScale={{ type: "linear" }}
      indexScale={{ type: "band", round: true }}
      colors={{ scheme: "nivo" }}
      defs={[
        {
          id: "dots",
          type: "patternDots",
          background: "inherit",
          color: "#38bcb2",
          size: 4,
          padding: 1,
          stagger: true,
        },
        {
          id: "lines",
          type: "patternLines",
          background: "inherit",
          color: "#eed312",
          rotation: -45,
          lineWidth: 6,
          spacing: 10,
        },
      ]}
      fill={[
        {
          match: {
            id: "fries",
          },
          id: "dots",
        },
        {
          match: {
            id: "sandwich",
          },
          id: "lines",
        },
      ]}
      borderColor={{
        from: "color",
        modifiers: [["darker", 1.6]],
      }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: "country",
        legendPosition: "middle",
        legendOffset: 32,
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: "food",
        legendPosition: "middle",
        legendOffset: -40,
      }}
      labelSkipWidth={12}
      labelSkipHeight={12}
      labelTextColor={{
        from: "color",
        modifiers: [["darker", 1.6]],
      }}
      legends={[
        {
          dataFrom: "keys",
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 120,
          translateY: 0,
          itemsSpacing: 2,
          itemWidth: 100,
          itemHeight: 20,
          itemDirection: "left-to-right",
          itemOpacity: 0.85,
          symbolSize: 20,
          effects: [
            {
              on: "hover",
              style: {
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
      role="application"
      ariaLabel="Nivo bar chart demo"
      barAriaLabel={(e: any) =>
        e.id + ": " + e.formattedValue + " in country: " + e.indexValue
      }
    />
  );
};
export default BarChart;
