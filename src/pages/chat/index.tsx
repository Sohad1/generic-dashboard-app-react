import { Grid, Box, Typography } from '@mui/material'
import React from 'react'
import { useGetChatPageQuery } from '@/api/chats/useChatQueries'
import Loading from '@/components/others/loading'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import ChatCard from '@/components/pages/chat/card'
import SingleChatSections from '@/components/pages/chat/SingleChatSections'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function Chat() {
  const { userId } = useParams<any>()

  const {
    data: contactsData,
    isLoading: isLoadingContacts,
    isError: isErrorContacts,
  } = useGetChatPageQuery(null)

  const { t } = useTranslation()
  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    }
  }
  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue)
  }
  if (isLoadingContacts) return <Loading />
  if (isErrorContacts) return <></>
  return (
    <Box sx={{ display: 'flex', height: '100vh', overflow: 'hidden' }}>
      <Box
        sx={{
          height: '100vh',
          width: '80px',
          background: '#3a3657',
        }}
      ></Box>
      <Grid container spacing={0}>
        <Grid item md={3}>
          <Box sx={CONTACTS_STYLES}>
            <Box
              sx={{
                padding: '15px 20px 0',
                height: '60px',
              }}
            >
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="basic tabs example"
              >
                <Tab label={t('pages.chat.Unread')} {...a11yProps(0)} />
                <Tab label={t('pages.chat.alreadyRead')} {...a11yProps(1)} />
              </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
              {contactsData.contacts?.map((item: any) => (
                <ChatCard key={item.id} item={item} />
              ))}
            </TabPanel>
            <TabPanel value={value} index={1}>
              {contactsData.contacts_archive?.map((item: any) => (
                <ChatCard key={item.id} item={item} />
              ))}
            </TabPanel>
          </Box>
        </Grid>

        {!userId ? (
          <Grid item lg={9}>
            <Box sx={BOX_STYLES}>{t('pages.chat.chooseChat')}</Box>
          </Grid>
        ) : (
          <SingleChatSections userId={userId!} />
        )}
      </Grid>
    </Box>
  )
}

export default Chat

const CONTACTS_STYLES = {
  height: 'calc(100vh - 0px)',
  //   borderRight: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'column',
  gap: '15px',
  overflowY: 'scroll',
  '&::-webkit-scrollbar': {
    width: 0,
  },
}

const BOX_STYLES = {
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  fontWeight: 900,
  fontSize: '25px',
  opacity: 0.3,
  background: '#f1f1f1',
}
