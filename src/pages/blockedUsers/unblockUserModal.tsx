import {
  useUnBlockProfileMutation,
  useUnBlockUserMutation,
} from "@/api/profile/useProfileQueries";

import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  user: { id: number; name: string };
};
const UnBlockUserModal = ({ user, open, handleClose }: Props) => {
  const { t } = useTranslation();

  const { mutate: unblockMutate, isLoading: isUnBlockLoading } =
    useUnBlockUserMutation(handleClose, user.id);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.unblockUser`)} ${user.name}`,
        handleClose,
        handleSubmit: () => unblockMutate(user.id),
        isSubmittingLoading: isUnBlockLoading,
        submitButtonText: t("common.unblock")!,
      }}
    />
  );
};

export default UnBlockUserModal;
