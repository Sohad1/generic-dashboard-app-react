import React from 'react'
import { useTranslation } from 'react-i18next'
import useTableHeader from './hooks/useTableHeader'
import Loading from '@/components/others/loading'
import useBlockedUsersLogic from './hooks/useBlockedUsersLogic'
import { Avatar, Box, Button, Stack, TableCell } from '@mui/material'
import { TRow } from '@/type/Table'
import AddIcon from '@mui/icons-material/Add'
import GenericTable from '@/components/items/table/genericTable/Table'
import { getComparator, stableSort } from '@/util/table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import moment from 'moment'
import UnBlockUserModal from './unblockUserModal'

const BlockedUsers = () => {
  const { t } = useTranslation()
  const headCells = useTableHeader()
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchBlockedUsers,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchBlockedUsers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchBlockedUsers,

    //handleDeleteBlockedUser,
    handleUnBlockUser,
    openDeleteModal,
    handleCloseDeleteModal,
    selectedUser,
  } = useBlockedUsersLogic()
  if (isLoading) return <Loading />

  if (isError) return <></>

  return (
    <Box>
      <UnBlockUserModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          user: selectedUser,
        }}
      />
      <Box>
        <Stack direction={'row'} justifyContent={'space-between'} mb={3}>
          <DebouncedTextField
            value={searchBlockedUsers}
            setValue={setSearchBlockedUsers}
            debouncedFunction={handleSearchBlockedUsers}
            label={t('common.search')}
            size="small"
          />
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.data)
              .reduce((previous, current) => [...previous, ...current]) as any[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as any[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.device_id}</TableCell>
                  <TableCell>{row.referral_id}</TableCell>
                  <TableCell>
                    {moment(row.created_at).format('YYYY-MM-DD')}
                  </TableCell>
                  <TableCell>{row.points?.points}</TableCell>
                  <TableCell>{row.referred_count}</TableCell>
                  <TableCell>
                    <TableActions delete={() => handleUnBlockUser(row)} />
                  </TableCell>
                </StyledTableRow>
              )
            })}
        </GenericTable>
      </Box>
    </Box>
  )
}

export default BlockedUsers
