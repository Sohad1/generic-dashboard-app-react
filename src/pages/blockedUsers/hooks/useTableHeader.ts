import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.name"),
    },
    {
      id: "email",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.email"),
    },
    {
      id: "device_id",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.deviceID"),
    },
    {
      id: "referral_id",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.referralID"),
    },
    {
      id: "created_at",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.date"),
    },
    {
      id: "point",
      numeric: true,
      disablePadding: false,
      label: t("pages.blockedUsers.point"),
    },
    {
      id: "referred_count",
      numeric: false,
      disablePadding: false,
      label: t("pages.blockedUsers.referralCount"),
    },
  ];
  return headCells;
};

export default useTableHeader;
