import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetTasksInfiniteQuery } from "@/api/tasks/useTasksQueries";
import { useGetBlockedUsersInfiniteQuery } from "@/api/blockedUsers/useBlockedUsersQueries";
const useBlockedUsersLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchBlockedUsers, setSearchBlockedUsers] = useState<string>("");
  const [openUnBlockModal, setOpenUnBlockModal] = useState(false);
  const [selectedBlockedUser, setSelectedBlockedUser] = useState({
    id: 0,
    title: "",
  });
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [selectedUser, setSelectedUser] = useState({
    id: 0,
    name: "",
  });
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetBlockedUsersInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchBlockedUsers = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
  };
  //   const handleAddTask = () => {
  //     navigate("/tasks/addTask");
  //   };
  //   const handleEditTask = (row: TRow) => {
  //     navigate(`/tasks/editTask/${row.id}`);
  //   };
  const handleUnBlockUser = (row: TRow) => {
    setOpenDeleteModal(true);
    setSelectedUser({
      name: String(row.name),
      id: Number(row.id),
    });
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchBlockedUsers,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchBlockedUsers,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchBlockedUsers,
    //handleAddTask,
    //handleEditTask,
    handleUnBlockUser,
    handleCloseDeleteModal,
    openDeleteModal,
    selectedUser,
  };
};

export default useBlockedUsersLogic;
