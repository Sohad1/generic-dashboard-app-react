export type TParams = {
  userId: string | undefined
}

export type TUserForm = {
  name: string
  password?: string
  email: string
  phone: string
  id?: number
  role?: { id: number; name: string }
  role_id?: number
}

export type TInitialValues = TUserForm
