import { EUserTypes } from '@/api/users/type'
import Loading from '@/components/others/loading'
import { useParams, useSearchParams } from 'react-router-dom'
import { TInitialValues, TParams } from './type'
import { useGetUserInfoQuery } from '@/api/users/useUsersQueries'
import { useTranslation } from 'react-i18next'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import UserForm from '@/components/pages/users/usersActions/userForm'

function AddEditUser() {
  const { t } = useTranslation()
  const [params] = useSearchParams()
  const type: any = params.get('type') || EUserTypes.user
  const userType = Object.values(EUserTypes).includes(type)
    ? type
    : EUserTypes.user

  const { userId } = useParams<TParams>()

  const {
    data,
    isLoading: isGetUserLoading,
    isFetching,
    isError,
  } = useGetUserInfoQuery(userId, userType)

  if (!!userId) {
    if (isGetUserLoading || isFetching) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  const initialValues: TInitialValues = data
    ? {
        id: data.id,
        name: data.name,
        email: data.email,
        phone: data.phone ?? '',
      }
    : {
        name: '',
        email: '',
        phone: '',
        password: '',
      }
  const roleId = data
    ? {
        role: { id: data.role_id, name: '' },
      }
    : {
        role: null,
      }

  return (
    <GenericFormPage
      title={
        userId
          ? `${t(`common.edit`)} ${t(`pages.users.${userType}`)}`
          : `${t(`common.add`)} ${t(`pages.users.${userType}`)}`
      }
      goBackLink="/users"
    >
      <UserForm
        userType={userType}
        initialValues={
          userType === EUserTypes.user
            ? { ...initialValues, ...roleId }
            : initialValues
        }
        userId={userId}
      />
    </GenericFormPage>
  )
}

export default AddEditUser
