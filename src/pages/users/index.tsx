import { useSearchParams } from "react-router-dom";
import { EUserTypes } from "@/api/users/type";
import UsersList from "@/components/pages/users/usersList/UsersList";

const Users = () => {
  const [params] = useSearchParams();
  const type: any = params.get("type") || EUserTypes.user;
  const userType = Object.values(EUserTypes).includes(type)
    ? type
    : EUserTypes.user;

  return <UsersList userType={userType} />;
};

export default Users;
