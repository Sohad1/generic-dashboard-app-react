import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
import TableActions from "@/components/items/table/genericTable/tableActions";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import useMessageTemplateLogic from "./hooks/useMessageTemplateLogic";
import useTableHeader from "./hooks/useTableHeader";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import { number } from "yup";
import DeleteMessageTemplateModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
const MessageTemplates = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchMessage_template,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleEditMessage_template,
    handleAddMessage_template,
    handleDeleteMessageTemplate,
    handleCloseDeleteMessageTemplateModal,
    handleSelectForNavigateToPreviewPage,
    setSearchMessage_template,
    handleOpenDeleteMessageTemplateModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchMessage_template,
  } = useMessageTemplateLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchMessage_template}
          setValue={setSearchMessage_template}
          debouncedFunction={handleSearchMessage_template}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_message_templates">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddMessage_template}
          >
            {t("pages.Message_template.AddMessage_template")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.message_templates.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.message_templates.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.message_templates.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.text}</TableCell>

                <TableCell>
                  <TableActions
                    edit={() => {
                      ability.can(
                        "update",
                        "dashboard_api_message_templates"
                      ) && handleEditMessage_template(row);
                    }}
                    preview={() =>
                      ability.can(
                        "update",
                        "dashboard_api_message_templates"
                      ) && handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteMessageTemplateModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteMessageTemplateModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteMessageTemplate}
        handleClose={handleCloseDeleteMessageTemplateModal}
      />
    </Box>
  );
};

export default MessageTemplates;
