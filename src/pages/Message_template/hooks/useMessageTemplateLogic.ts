import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetMe_TeInfiniteQuery } from "@/api/message_templates/useM_TQueries";
import { useNavigate } from "react-router-dom";
import { useDeleteMe_TeMutation } from "@/api/message_templates/useM_TQueries";
import { useQueryClient } from "@tanstack/react-query";
const useMessageTemplateLogic = () => {
  const [page, setPage] = useState(0);
  const queryClient = useQueryClient();
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchMessage_template, setSearchMessage_template] =
    useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [MessageTemplates, setMessageTemplates] = useState<TRow>({});
  const navigate = useNavigate();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);

  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteMe_TeMutation();

  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetMe_TeInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchMessage_template = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/Message_template/previewMe_Te/${id}`);
  };
  const handleAddMessage_template = () => {
    navigate("/Message_template/AddMessage_template");
  };
  const handleEditMessage_template = (row: TRow) => {
    navigate(`/Message_template/editMessage_template/${row.id}`);
  };
  const handleCloseDeleteMessageTemplateModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteMessageTemplateModal = (MessageTemplates: TRow) => {
    setMessageTemplates(MessageTemplates);
    setOpenDeleteModal(true);
  };
  const handleDeleteMessageTemplate = () => {
    deleteMutate(
      { params: { id: MessageTemplates.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["MessageTemplate-infinite-query"]);
            handleCloseDeleteMessageTemplateModal();
          }
        },
      }
    );
  };
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchMessage_template,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleEditMessage_template,
    handleAddMessage_template,
    handleDeleteMessageTemplate,
    handleCloseDeleteMessageTemplateModal,
    handleSelectForNavigateToPreviewPage,
    setSearchMessage_template,
    handleOpenDeleteMessageTemplateModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchMessage_template,
  };
};
export default useMessageTemplateLogic;
