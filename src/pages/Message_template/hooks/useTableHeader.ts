import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.Message_template.text"),
    },
  ];
  return headCells;
};

export default useTableHeader;
