import { useGetMe_TeQuery } from "@/api/message_templates/useM_TQueries";
import { useParams, useNavigate } from "react-router-dom";
import { Button, Paper, Typography, Box } from "@mui/material";
import { useTranslation } from "react-i18next";
import Loading from "@/components/others/loading";
const PreviewMessage_template = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetMe_TeQuery(id);
  console.log("id", id);
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t("pages.Message_template.PreviewMessage_template")}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t("pages.Message_template.text")} :{" "}
        {data?.response.message_template.text}
      </Typography>
      <Paper variant="outlined" />

      <Button
        sx={{ float: "right" }}
        variant="outlined"
        onClick={() => {
          navigate("/Message_template");
        }}
      >
        {t("pages.categories.cancel")}
      </Button>
    </Box>
  );
};

export default PreviewMessage_template;
