import { useNavigate, useParams } from 'react-router-dom'
import { Button, Grid, Box, Paper, Typography, TextField } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { Form, Formik } from 'formik'
import ArrowBackIcon from '@mui/material/Icon'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import {
  useAddMe_TetMutation,
  useGetMe_TeQuery,
  useEditMe_TetMutation,
} from '@/api/message_templates/useM_TQueries'
import { TInitialValues, TParams } from './hooks/type'
import Loading from '@/components/others/loading'
import useValidationSchema from './hooks/useValidation'

const AddEditMessage_template = () => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const { Me_TeId } = useParams<TParams>()
  const validationSchema = useValidationSchema()

  const {
    data,
    isLoading: isGetMessage_templateLoading,
    isFetching,
    isError,
  } = useGetMe_TeQuery(Me_TeId)
  const { mutate: addMutate, isLoading: isAddLoading } = useAddMe_TetMutation()
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditMe_TetMutation()
  if (!!Me_TeId) {
    if (isGetMessage_templateLoading || isFetching) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  const initialValues: TInitialValues = data?.response.message_template
    ? { ...data.response.message_template }
    : {
        text: '',
      }
  return (
    <Grid
      container
      justifyContent={'center'}
      alignItems={'center'}
      sx={{ mt: 3 }}
    >
      <Grid item xs={11} md={9} lg={6}>
        <Paper>
          <Box position="relative" right={0} top={1} left={1} margin={0}>
            <Button
              sx={{}}
              onClick={() => {
                navigate('/Message_template')
              }}
            >
              <ArrowBackIcon fontSize="large" />
            </Button>
          </Box>
          <Grid
            container
            justifyContent={'center'}
            alignItems="center"
            spacing={2}
          >
            <Grid item container xs={10} lg={9} spacing={2}>
              <Grid item xs={12}>
                <Typography
                  color="primary"
                  textAlign="center"
                  variant="h4"
                  component="h4"
                >
                  {Me_TeId
                    ? t('pages.Message_template.editMessage_template')
                    : t('pages.Message_template.AddMessage_template')}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Formik
                  initialValues={initialValues}
                  onSubmit={(values) => {
                    Me_TeId
                      ? editMutate({ body: values })
                      : addMutate({ body: values })
                  }}
                  validationSchema={validationSchema}
                >
                  {({
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                  }) => (
                    <Form>
                      <Grid
                        container
                        justifyContent={'center'}
                        alignItems={'center'}
                        spacing={2}
                      >
                        <Grid item xs={12} md={10}>
                          <TextField
                            type="text"
                            name="text"
                            fullWidth
                            label={t('pages.Message_template.text')}
                            value={values.text}
                            onChange={handleChange}
                            error={touched.text && !!errors.text}
                            helperText={touched.text && errors.text}
                          />
                        </Grid>

                        <Grid
                          item
                          xs={12}
                          md={10}
                          sx={{ display: 'flex', marginBottom: 5 }}
                        >
                          <LoadingButton
                            isSubmitting={
                              isSubmitting || isAddLoading || isEditLoading
                            }
                            buttonText={
                              Me_TeId ? t('common.edit') : t('common.add')
                            }
                          />
                          <Button
                            sx={{ marginLeft: 1 }}
                            onClick={() => {
                              navigate('/Message_template')
                            }}
                          >
                            {t('common.cancel')}
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  )}
                </Formik>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  )
}

export default AddEditMessage_template
