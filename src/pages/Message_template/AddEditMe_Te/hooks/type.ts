import { TMessage_templates } from "@/api/message_templates/type";

export type TInitialValues = TMessage_templates;
export type TParams = {
  Me_TeId: string | undefined;
};
