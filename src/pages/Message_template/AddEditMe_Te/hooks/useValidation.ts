import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const schema = Yup.object().shape({
    text: Yup.string().required(t('form.required')),
  })

  return schema
}

export default useValidationSchema
