import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "title",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.title"),
    },
    {
      id: "descr",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.description"),
    },
    {
      id: "url",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.link"),
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.type"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.status"),
    },
    {
      id: "point",
      numeric: true,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.points"),
    },
    {
      id: "img",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.icon"),
    },
    {
      id: "limit_clicks",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.limitClicks"),
    },
    {
      id: "users_count",
      numeric: false,
      disablePadding: false,
      label: t("pages.tasks.addEditTask.clickCount"),
    },
  ];
  return headCells;
};

export default useTableHeader;
