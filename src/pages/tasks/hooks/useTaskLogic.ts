import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
import { useGetTasksInfiniteQuery } from "@/api/tasks/useTasksQueries";
const useTaskLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchTasks, setSearchTasks] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [selectedTask, setSelectedTask] = useState({
    id: 0,
    title: "",
  });
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const navigate = useNavigate();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetTasksInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const handleSearchTasks = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddTask = () => {
    navigate("/tasks/addTask");
  };
  const handleEditTask = (row: TRow) => {
    navigate(`/tasks/editTask/${row.id}`);
  };
  const handleDeleteTask = (row: TRow) => {
    setOpenDeleteModal(true);
    setSelectedTask({
      title: String(row.title),
      id: Number(row.id),
    });
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchTasks,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchTasks,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchTasks,
    handleAddTask,
    handleEditTask,
    handleDeleteTask,
    handleCloseDeleteModal,
    openDeleteModal,
    selectedTask,
  };
};

export default useTaskLogic;
