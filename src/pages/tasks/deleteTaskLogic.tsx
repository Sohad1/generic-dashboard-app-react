import { useDeleteTaskMutation } from "@/api/tasks/useTasksQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  task: { id: number; title: string };
};
const DeleteTaskModal = ({ task, open, handleClose }: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteTaskMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${task.title}`,
        handleClose,
        handleSubmit: () => deleteMutate(task.id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t("common.remove")!,
      }}
    />
  );
};

export default DeleteTaskModal;
