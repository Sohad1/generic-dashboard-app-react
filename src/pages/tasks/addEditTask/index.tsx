import React from 'react'
import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Grid, TextField } from '@mui/material'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { TInitialValues, TParams } from './hooks/type'
import {
  useAddTaskMutation,
  useEditTaskMutation,
  useGetTaskQuery,
} from '@/api/tasks/useTasksQueries'
import CountriesAutoComplete from '@/components/common/CountriesAutoComplete'
import ImageDragDropField from '@/components/items/inputField/ImageDragDropField'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import CheckboxInput from '@/components/items/inputField/Checkbox/Checkbox'
import useValidationSchema from './hooks/useValidation'
const AddEditTask = () => {
  const { t } = useTranslation()
  const { taskId } = useParams<TParams>()
  const validationSchema = useValidationSchema()

  const { data, isLoading: isGetTaskLoading, isError } = useGetTaskQuery(taskId)
  const { mutate: addMutate, isLoading: isAddLoading } = useAddTaskMutation()
  const { mutate: editMutate, isLoading: isEditLoading } = useEditTaskMutation()
  if (!!taskId) {
    if (isGetTaskLoading) {
      return <Loading />
    } else if (isError) {
      return <></>
    }
  }
  const initialValues: TInitialValues = data?.response.task
    ? { ...data.response.task, img: data.response.task.img_url }
    : {
        title: '',
        descr: '',
        points: 0,
        img: '',
        url: '',
        type: '',
        status: 0,
        limit_clicks: 0,
        users_count: 0,
        country: [],
        to: '',
        // permanent
      }
  // console.log(taskId)
  // console.log(data?.response.task)

  return (
    <GenericFormPage
      title={
        taskId
          ? t('pages.tasks.addEditTask.editTask')
          : t('pages.tasks.addEditTask.addTask')
      }
      goBackLink="/tasks"
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          taskId ? editMutate({ body: values }) : addMutate({ body: values })
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <ImageDragDropField
                  error={!!errors.img || touched.img}
                  file={values.img}
                  handleChangeFile={(v) => setFieldValue('img', v)}
                  label="Icon"
                  name="img"
                  type={taskId ? 'edit' : 'add'}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <CountriesAutoComplete
                  name="country_ids"
                  multiple={true}
                  value={values.country}
                  setFieldValue={setFieldValue}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title"
                  fullWidth
                  label="title"
                  value={values.title}
                  onChange={handleChange}
                  error={touched.title && !!errors.title}
                  helperText={touched.title && errors.title}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="descr"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.description')}
                  value={values.descr}
                  onChange={handleChange}
                  error={touched.descr && !!errors.descr}
                  helperText={touched.descr && errors.descr}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="url"
                  fullWidth
                  label="Link"
                  value={values.url}
                  onChange={handleChange}
                  error={touched.url && !!errors.url}
                  helperText={touched.url && errors.url}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="points"
                  type="number"
                  fullWidth
                  label={t('pages.competitions.addEditCompetition.points')}
                  value={values.points}
                  onChange={handleChange}
                  error={touched.points && !!errors.points}
                  helperText={touched.points && errors.points}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="limit_clicks"
                  fullWidth
                  type="number"
                  label="Limit Clicks"
                  value={values.limit_clicks}
                  onChange={handleChange}
                  error={touched.limit_clicks && !!errors.limit_clicks}
                  helperText={touched.limit_clicks && errors.limit_clicks}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="status"
                  fullWidth
                  type="number"
                  label="Status"
                  value={values.status}
                  onChange={handleChange}
                  error={touched.status && !!errors.status}
                  helperText={touched.status && errors.status}
                />
              </Grid>
              <Grid
                container
                item
                xs={12}
                md={12}
                sx={{ alignItems: 'center', marginBottom: '20px' }}
              >
                <Grid item xs={12} md={2} lg={1}>
                  <CheckboxInput
                    label="permanent"
                    onChange={(e) =>
                      setFieldValue('permanent', e.target.checked)
                    }
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  md={6}
                  lg={4}
                  // sx={{ visibility: values.to ? 'visible' : 'hidden' }}
                >
                  <TextField
                    name="to"
                    fullWidth
                    type="date"
                    label="To"
                    value={values.to}
                    onChange={handleChange}
                    error={touched.to && !!errors.to}
                    helperText={touched.to && errors.to}
                  />
                </Grid>
              </Grid>

              <Grid
                item
                xs={12}
                md={10}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={taskId ? t('common.edit') : t('common.add')}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  )
}

export default AddEditTask
