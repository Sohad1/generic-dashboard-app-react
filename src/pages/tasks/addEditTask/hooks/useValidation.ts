import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const cardSchema = Yup.object().shape({
    country_ids: Yup.string().required(t('form.required')),
    title: Yup.string().required(t('form.required')),
    descr: Yup.string().required(t('form.required')),
    url: Yup.string().required(t('form.required')),
    points: Yup.number().required(t('form.required')),
    limit_clicks: Yup.number().required(t('form.required')),
    status: Yup.number().required(t('form.required')),
    permanent: Yup.boolean().required(t('form.required')),
    to: Yup.number().when('permanent', {
      is: false,
      then: (schema) => schema.nullable(),
      otherwise: (schema) => schema.required(t('form.required')),
    }),
  })

  return cardSchema
}

export default useValidationSchema
