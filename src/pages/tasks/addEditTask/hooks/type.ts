import { TTask } from "@/api/tasks/type";

export type TInitialValues = TTask;

export type TParams = {
  taskId: string | undefined;
};
