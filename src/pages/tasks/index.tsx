import React from 'react'
import { useTranslation } from 'react-i18next'
import useTableHeader from './hooks/useTableHeader'
import Loading from '@/components/others/loading'
import useTaskLogic from './hooks/useTaskLogic'
import { Avatar, Box, Button, Stack, TableCell } from '@mui/material'
import { TRow } from '@/type/Table'
import AddIcon from '@mui/icons-material/Add'
import GenericTable from '@/components/items/table/genericTable/Table'
import { getComparator, stableSort } from '@/util/table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import DeleteTaskModal from './deleteTaskLogic'
import { useContext } from 'react'
import { AbilityContext, Can } from '@/libs/casl/can'

const Tasks = () => {
  const { t } = useTranslation()
  const ability = useContext(AbilityContext)
  const headCells = useTableHeader()
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchTasks,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchTasks,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchTasks,
    handleAddTask,
    handleEditTask,
    handleDeleteTask,
    openDeleteModal,
    handleCloseDeleteModal,
    selectedTask,
  } = useTaskLogic()
  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <Box>
      <DeleteTaskModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          task: selectedTask,
        }}
      />
      <Box>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <DebouncedTextField
            value={searchTasks}
            setValue={setSearchTasks}
            debouncedFunction={handleSearchTasks}
            label={t('common.search')}
            size="small"
          />
          <Can I="store" a="dashboard_api_tasks">
            <Button
              variant="contained"
              endIcon={<AddIcon />}
              onClick={handleAddTask}
            >
              {t('pages.tasks.addTask')}
            </Button>
          </Can>
        </Stack>
        <GenericTable
          page={page}
          count={data?.pages[0].response.tasks.total!}
          rowsPerPage={rowsPerPage}
          order={order}
          orderBy={orderBy}
          setOrder={setOrder}
          setOrderBy={setOrderBy}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          rows={
            data?.pages
              .map((page) => page.response.tasks.data)
              .reduce((previous, current) => [...previous, ...current]) as any[]
          }
          headCells={headCells}
        >
          {stableSort(
            data?.pages
              .map((page) => page.response.tasks.data)
              .reduce((previous, current) => [
                ...previous,
                ...current,
              ]) as any[],
            getComparator(order, orderBy)
          )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row: any, index) => {
              return (
                <StyledTableRow key={index} hover tabIndex={-1}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.desc}</TableCell>
                  <TableCell>{row.url}</TableCell>
                  <TableCell>{row.type}</TableCell>
                  <TableCell>{row.status}</TableCell>
                  <TableCell>{row.points}</TableCell>

                  <TableCell>
                    <Avatar
                      variant="rounded"
                      src={String(row.img)}
                      alt="task-img"
                    />
                  </TableCell>
                  <TableCell>{row.limit_clicks}</TableCell>
                  <TableCell>{row.users_count}</TableCell>

                  <TableCell>
                    <TableActions
                      edit={() =>
                        ability.can('update', 'dashboard_api_tasks') &&
                        handleEditTask(row)
                      }
                      delete={() =>
                        ability.can('destroy', 'dashboard_api_tasks') &&
                        handleDeleteTask(row)
                      }
                    />
                  </TableCell>
                </StyledTableRow>
              )
            })}
        </GenericTable>
      </Box>
    </Box>
  )
}

export default Tasks
