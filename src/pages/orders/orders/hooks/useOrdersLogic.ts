import { ChangeEvent, useState } from 'react'
import { TOrder } from '@/type/Table'
import { useGetOrdersInfiniteQuery } from '@/api/orders/useReportsQueries'
import { TOrdersReportsOptions } from '@/api/orders/type'

const useOrdersLogic = () => {
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(5)
  const [searchOrders, setSearchOrders] = useState<string>('')
  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  // const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>('')

  const [filterOptions, setFilterOptions] = useState<TOrdersReportsOptions>({})

  const handleSearchFilter = (v: TOrdersReportsOptions) => {
    setFilterOptions({ ...filterOptions, ...v })
  }

  const { data, isLoading, isError, error } = useGetOrdersInfiniteQuery(
    // rowsPerPage,
    filterOptions
  )
  const handleSearchOrders = (value: string) => {
    console.log(value)
  }
  const handleChangePage = (event: unknown, newPage: number) => {
    // if (newPage > page) fetchNextPage({ pageParam: newPage })
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const [openDetailsModal, setOpenDetailsModal] = useState(false)
  const [selectedOrder, setSelectedOrder] = useState(null)

  const handleShowOrderDetails = (order: any) => {
    setOpenDetailsModal(true)
    setSelectedOrder(order)
  }
  const handleCloseDetails = () => setOpenDetailsModal(false)

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchOrders,
    isError,
    isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchOrders,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchOrders,
    openDetailsModal,
    selectedOrder,
    handleShowOrderDetails,
    handleCloseDetails,
    handleSearchFilter,
  }
}

export default useOrdersLogic
