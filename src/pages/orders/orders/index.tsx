import { Box } from '@mui/material'
import useOrdersLogic from './hooks/useOrdersLogic'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'
import OrderDetails from '@/components/pages/orders/orderDetails'
import FilterReportsForm from '@/components/pages/orders/FilterReportsForm'
import OrderSelectTable from '@/components/pages/orders/ordersTable/orderSelectTable'

function Orders() {
  const {
    data,
    isLoading,
    isError,
    searchOrders,
    rowsPerPage,
    page,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangeRowsPerPage,
    handleChangePage,
    setSearchOrders,
    handleSearchOrders,
    openDetailsModal,
    selectedOrder,
    handleCloseDetails,
    handleShowOrderDetails,
    handleSearchFilter,
  } = useOrdersLogic()

  return (
    <Box>
      <FilterReportsForm onSubmit={handleSearchFilter} />

      <OrderSelectTable
        {...{
          data,
          isLoading,
          isError,
          rowsPerPage,
          page,
          order,
          orderBy,
          setOrder,
          setOrderBy,
          handleChangeRowsPerPage,
          handleChangePage,
          handleShowOrderDetails,
          searchOrders,
          setSearchOrders,
          handleSearchOrders,
        }}
      />

      <GenericModal
        open={openDetailsModal}
        childrenHaveActions={true}
        handleClose={handleCloseDetails}
        title=""
        maxWidth="md"
      >
        <OrderDetails order={selectedOrder} handleClose={handleCloseDetails} />
      </GenericModal>
    </Box>
  )
}

export default Orders
