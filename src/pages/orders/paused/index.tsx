import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { Box, Stack } from '@mui/material'
import { useTranslation } from 'react-i18next'
import useOrdersLogic from './hooks/useOrdersLogic'
import Loading from '@/components/others/loading'
import { getComparator, stableSort } from '@/util/table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { TableCell } from '@mui/material'
import useTableHeader from './hooks/useTableHeader'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'
import OrderDetails from '@/components/pages/orders/orderDetails'
import moment from 'moment'

function PausedOrders() {
  const { t, i18n } = useTranslation()
  const headCells = useTableHeader()
  const {
    data,
    isLoading,
    isError,
    searchOrders,
    rowsPerPage,
    page,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleChangeRowsPerPage,
    handleChangePage,
    setSearchOrders,
    handleSearchOrders,
    openDetailsModal,
    selectedOrder,
    handleCloseDetails,
    handleShowOrderDetails,
  } = useOrdersLogic()

  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <Box>
      <Stack direction={'row'} justifyContent={'space-between'}>
        <DebouncedTextField
          value={searchOrders}
          setValue={setSearchOrders}
          debouncedFunction={handleSearchOrders}
          label={t('common.search')}
          size="small"
        />
        {/* <DebouncedTextField
          value={ordersCount}
          setValue={setSearchOrdersCount}
          debouncedFunction={handleSearchOrdersCount}
          label={t('common.ordersCount')}
          size="small"
        /> */}
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page?.data)
            .reduce((previous, current) => [...previous, ...current]) as any[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page?.data)
            .reduce((previous, current) => [...previous, ...current]) as any[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.user?.confirmed_orders_count}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.user?.un_processed_orders_count}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.user?.name}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {i18n.language === 'ar'
                    ? row.user?.country?.country_arName
                    : row.user?.country?.country_enName}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.status}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.user?.referral_id}
                </TableCell>

                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.purchase_price}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.price?.value}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {row.price?.category?.name}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {moment(row.user?.created_at).format('YYYY-MM-DD')}
                </TableCell>
                <TableCell onClick={() => handleShowOrderDetails(row)}>
                  {moment(row.created_at).format('YYYY-MM-DD')}
                </TableCell>
                <TableCell>
                  <TableActions preview={() => handleShowOrderDetails(row)} />
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
      <GenericModal
        open={openDetailsModal}
        childrenHaveActions={true}
        handleClose={handleCloseDetails}
        title=""
        maxWidth="md"
      >
        <OrderDetails
          type="paused"
          order={selectedOrder}
          handleClose={handleCloseDetails}
        />
      </GenericModal>
    </Box>
  )
}

export default PausedOrders
