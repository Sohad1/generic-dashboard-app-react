import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "agree",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.agree"),
    },
    {
      id: "process",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.process"),
    },
    {
      id: "userName",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.userName"),
    },
    {
      id: "country",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.country"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.status"),
    },
    {
      id: "ref",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.ref"),
    },
    {
      id: "buyPrice",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.buyPrice"),
    },
    {
      id: "category",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.category"),
    },
    {
      id: "product",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.product"),
    },
    {
      id: "createDate",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.createDate"),
    },
    {
      id: "orderDate",
      numeric: false,
      disablePadding: false,
      label: t("pages.order.orderDate"),
    },
  ];
  return headCells;
};

export default useTableHeader;
