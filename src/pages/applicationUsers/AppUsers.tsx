import { TFilterInitialValues } from './type'
import useAppUserLogic from './filterForm/hooks/useAppUserLogic'
import Loading from '@/components/others/loading'
import AppUsersTable from './AppUsersTable'
type Props = {
  filterOptions: TFilterInitialValues
}
const AppUsers = ({ filterOptions }: Props) => {
  const {
    data,
    // error,
    page,
    rowsPerPage,
    isError,
    isLoading,
    handleChangePage,
    handleChangeRowsPerPage,
    handlePreviewProfile,
    handleUserGift,
    openSendGift,
    selectedItem,
    handleCloseSendGiftModal,
  } = useAppUserLogic(filterOptions)
  if (isLoading) return <Loading />
  if (isError) return <></>
  return (
    <AppUsersTable
      {...{
        data: data!,
        page,
        rowsPerPage,
        handleChangePage,
        handleChangeRowsPerPage,
        handlePreviewProfile,
        handleUserGift,
        openSendGift,
        selectedItem,
        handleCloseSendGiftModal,
      }}
    />
  )
}

export default AppUsers
