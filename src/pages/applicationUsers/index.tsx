import React from "react";
import { Box } from "@mui/material";
import FilterForm from "./filterForm/FilterForm";
import useFilterFormLogic from "./filterForm/hooks/useFilterFormLogic";
import useAppUserLogic from "./filterForm/hooks/useAppUserLogic";
import AppUsers from "./AppUsers";
const ApplicationUser = () => {
  const { initialFilterValues: initialValues } = useFilterFormLogic();
  const { filterOptions, handleSubmitFilter: handleSubmit } = useAppUserLogic();

  return (
    <Box>
      <FilterForm {...{ handleSubmit, initialValues }} />
      <AppUsers
        {...{
          filterOptions: {
            ...filterOptions,
            countryIdSearch: filterOptions?.countryIdSearch?.id,
          },
        }}
      />
    </Box>
  );
};

export default ApplicationUser;
