export type TFilterInitialValues = {
  haveOrders?: number;
  ordersStatus?: number;
  countryIdSearch?: { id: number; name: string } | number;
};
export type TApplicationUsersPaginationParams = {
  pagination?: 1 | 0;
  limit?: number;
  page?: number;
} & TFilterInitialValues;
