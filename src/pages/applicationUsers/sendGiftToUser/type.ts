export type TInitialValues = {
  user_id: number;
  point: number;
  all_countries: number;
};

export type TParams = {
  userId: string | undefined;
};
