import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Button, Grid, Paper, Typography, TextField, Box } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import { TInitialValues, TParams } from './type'
import { useSendGiftToUserMutation } from '@/api/gifts/useGiftsQueries'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'

const SendGiftToUser = ({
  open,
  handleClose,
  userId,
  userName,
}: {
  open: boolean
  handleClose: () => void
  userId: number | string
  userName: string | number
}) => {
  const { t } = useTranslation()
  // const navigate = useNavigate()
  // const { userId } = useParams<TParams>()

  const { mutate: addMutate, isLoading: isAddLoading } =
    useSendGiftToUserMutation(handleClose)

  const initialValues: TInitialValues = {
    user_id: null,
    point: 0,
    all_countries: 0,
  }

  return (
    <Formik initialValues={initialValues} onSubmit={(values) => {}}>
      {({
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        setFieldValue,
      }) => {
        return (
          <Form>
            <GenericModal
              open={open}
              title={`Send Gift to User ${userName}`}
              handleClose={handleClose}
              childrenHaveActions={true}
            >
              <Grid
                container
                justifyContent={'center'}
                alignItems={'center'}
                spacing={2}
              >
                <Grid item xs={12} md={10}>
                  <TextField
                    name="point"
                    type="number"
                    fullWidth
                    label={t('pages.competitions.addEditCompetition.points')}
                    value={values.point}
                    onChange={handleChange}
                    error={touched.point && !!errors.point}
                    helperText={touched.point && errors.point}
                  />
                </Grid>

                <Grid
                  item
                  xs={12}
                  md={10}
                  sx={{
                    display: 'flex',
                    marginBottom: 5,
                  }}
                >
                  <Box
                    onClick={() =>
                      addMutate({
                        body: { ...values, user_id: Number(userId) },
                      })
                    }
                  >
                    <LoadingButton
                      isSubmitting={isAddLoading}
                      buttonText="send"
                    />
                  </Box>
                  <Button onClick={() => handleClose()}>
                    {t('common.cancel')}
                  </Button>
                </Grid>
              </Grid>
            </GenericModal>
          </Form>
        )
      }}
    </Formik>
  )
}

export default SendGiftToUser
