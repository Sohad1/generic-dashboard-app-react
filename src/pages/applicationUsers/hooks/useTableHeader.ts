import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";
const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: false,
      disablePadding: false,
      label: "ID",
    },
    {
      id: "userName",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.userName"),
    },
    {
      id: "email",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.email"),
    },
    {
      id: "phone",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.phone"),
    },
    {
      id: "createdAt",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.createdAt"),
    },
    {
      id: "country",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.country"),
    },
    {
      id: "points",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.points"),
    },
    {
      id: "plays",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.plays"),
    },
    {
      id: "referred",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.referred"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.status"),
    },
    {
      id: "exception",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.exception"),
    },
    {
      id: "gift",
      numeric: false,
      disablePadding: false,
      label: t("pages.reports.table.gift"),
    },
  ];
  return headCells;
};

export default useTableHeader;
