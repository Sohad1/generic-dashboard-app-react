import { ChangeEvent, useState } from "react";
import { TFilterInitialValues } from "../../type";
import { useGetApplicationUsersInfiniteQuery } from "@/api/applicationUsers/useApplicationUsersQueries";
import { useNavigate } from "react-router-dom";
import { TRow, TOrder } from "@/type/Table";

const useAppUserLogic = (Options?: TFilterInitialValues) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const navigate = useNavigate();
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [filterOptions, setFilterOptions] = useState(null);
  const handleSubmitFilter = (values: TFilterInitialValues) => {
    setFilterOptions({ ...filterOptions, ...values });
  };

  const { data, isLoading, isError, error, fetchNextPage } =
    useGetApplicationUsersInfiniteQuery(
      {
        params: {
          limit: rowsPerPage,
          search_input: debouncedSearchValue,
          order_by: orderBy,
          type: order,
        },
      },

      Options
    );

  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handlePreviewProfile = (row: TRow) => {
    navigate(`/profile/${row.id}`);
  };

  const [openSendGift, setOpenSendGift] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const handleCloseSendGiftModal = () => setOpenSendGift(false);
  const handleUserGift = (row: TRow) => {
    setSelectedItem(row);
    setOpenSendGift(true);
  };
  return {
    data,
    error,
    page,
    rowsPerPage,
    isError,
    isLoading,
    filterOptions,
    handleSubmitFilter,
    handleChangePage,
    handleChangeRowsPerPage,
    handlePreviewProfile,
    handleUserGift,
    openSendGift,
    selectedItem,
    handleCloseSendGiftModal,
  };
};

export default useAppUserLogic;
