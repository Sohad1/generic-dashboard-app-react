import FilterOptions from "./FilterOptions";
import { useTranslation } from "react-i18next";
import { Form, Formik } from "formik";
import { Box, Button } from "@mui/material";
import { TFilterInitialValues } from "../type";

type Props = {
  handleSubmit: (e: TFilterInitialValues) => void;
  initialValues: TFilterInitialValues;
};

function FilterForm({ handleSubmit, initialValues }: Props) {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleSubmit(values);
        console.log(values);
      }}
    >
      {({ values, handleChange, setFieldValue }) => (
        <Form>
          <Box sx={{ display: "flex", gap: "15px" }} marginBottom="20px">
            <FilterOptions {...{ values, handleChange, setFieldValue }} />
            <Box>
              <Button variant="contained" type="submit" sx={{ height: "98%" }}>
                {t("common.search")}
              </Button>
            </Box>
          </Box>
        </Form>
      )}
    </Formik>
  );
}

export default FilterForm;
