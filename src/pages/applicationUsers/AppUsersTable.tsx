import GenericTable from '@/components/items/table/genericTable/Table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { getComparator, stableSort } from '@/util/table'
import { Button, TableCell } from '@mui/material'
import { InfiniteData } from '@tanstack/react-query'
import { TGenericPaginationResponse } from '@/api/type'
import { ChangeEvent, useState } from 'react'
import TableActions from '@/components/items/table/genericTable/tableActions'
import moment from 'moment'
import useTableHeader from './hooks/useTableHeader'
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard'
import { TOrder, TRow } from '@/type/Table'
import SendGiftToUser from './sendGiftToUser'
import { useTranslation } from 'react-i18next'
type Props = {
  data: InfiniteData<TGenericPaginationResponse<any> | undefined>
  page: number
  rowsPerPage: number
  handleChangePage: (event: unknown, newPage: number) => void
  handleChangeRowsPerPage: (event: ChangeEvent<HTMLInputElement>) => void
  handlePreviewProfile: (row: TRow) => void
  handleUserGift: (row: TRow) => void
  openSendGift: boolean
  selectedItem: TRow
  handleCloseSendGiftModal: () => void
}
function AppUsersTable({
  data,
  page,
  rowsPerPage,
  handleChangePage,
  handleChangeRowsPerPage,
  handlePreviewProfile,
  handleUserGift,
  openSendGift,
  selectedItem,
  handleCloseSendGiftModal,
}: Props) {
  const headCells = useTableHeader()
  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const { t } = useTranslation()
  return (
    <GenericTable
      page={page}
      count={data?.pages[0]?.total!}
      rowsPerPage={rowsPerPage}
      order={order}
      orderBy={orderBy}
      setOrder={setOrder}
      setOrderBy={setOrderBy}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      rows={
        data?.pages
          ?.map((page) => page?.data)
          .reduce((previous, current) => [...previous!, ...current!]) as any[]
      }
      headCells={headCells}
    >
      {stableSort(
        data?.pages
          .map((page) => page?.data)
          .reduce((previous, current) => [...previous, ...current]) as any[],
        getComparator(order, orderBy)
      )
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: any, index) => {
          return (
            <StyledTableRow key={index} hover tabIndex={-1}>
              <TableCell>{row.id}</TableCell>
              <TableCell onClick={() => handlePreviewProfile(row)}>
                {row.name}
              </TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{row.phone}</TableCell>
              <TableCell>
                {moment(row.created_at).format('YYYY-MM-DD , HH:mm')}
              </TableCell>
              <TableCell>{row.country?.country_arName}</TableCell>
              <TableCell>{row.user_points_history_count}</TableCell>
              <TableCell>عدد مرات اللعب</TableCell>
              <TableCell>{row.referred.length}</TableCell>
              <TableCell>{row.status === 1 ? 'active' : 'inactive'}</TableCell>
              <TableCell>
                {row.top10_referral_exception?.status === 1 ? '1' : '0'}
              </TableCell>
              <TableCell>
                <Button
                  variant="outlined"
                  color="error"
                  fullWidth
                  endIcon={<CardGiftcardIcon />}
                  onClick={() => {
                    handleUserGift(row)
                  }}
                >
                  {t('pages.reports.table.gift')}
                </Button>
              </TableCell>
              <TableCell>
                <TableActions preview={() => handlePreviewProfile(row)} />
              </TableCell>
            </StyledTableRow>
          )
        })}
      <SendGiftToUser
        open={openSendGift}
        handleClose={handleCloseSendGiftModal}
        userId={selectedItem?.id}
        userName={selectedItem?.name}
      />
    </GenericTable>
  )
}

export default AppUsersTable
