import React from "react";
import { useParams } from "react-router-dom";
import { useGetPermissionQuery } from "@/api/permissions/usePermissionsQueries";
import { TParams } from "../type";
import Loading from "@/components/others/loading";
import { Divider, Paper, Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import DefinitionOfProperty from "@/components/others/definitionOfProperty";

const PreviewPermission = () => {
  const { permissionId } = useParams<TParams>();
  const { t } = useTranslation();
  const {
    data: permission,
    isLoading: isGetPermissionLoading,
    isFetching: isGetPermissionFetching,
    isError: isGetPermissionError,
  } = useGetPermissionQuery({ params: { id: permissionId } });
  if (!!permissionId) {
    if (isGetPermissionLoading || isGetPermissionFetching) {
      return <Loading />;
    } else if (isGetPermissionError) {
      return <></>;
    }
  }
  return (
    <Paper sx={{ m: "auto", p: 4 }}>
      <Stack spacing={2}>
        <Typography variant="h4" component={"p"} textAlign={"center"}>
          {t("pages.permissions.previewTitle")}
        </Typography>
        <Stack direction="row" spacing={2}>
          <DefinitionOfProperty
            name={t("pages.permissions.form.name")}
            value={permission.response.permission.name}
          />
          <DefinitionOfProperty
            name={t("pages.permissions.form.guardName")}
            value={permission.response.permission.guard_name}
          />
          {!!!permission.response.permission.is_parent && (
            <DefinitionOfProperty
              name={t("pages.permissions.form.parent")}
              value={permission.response.permission.parent.name}
            />
          )}
        </Stack>
        <Divider />
        <Stack spacing={1}>
          <Typography color={"primary"} sx={{ fontWeight: "bold" }}>
            {t("pages.permissions.form.roles")}
          </Typography>
          <Stack direction="row" spacing={2}>
            {permission.response.permission.roles.map((role) => (
              <Typography key={role.id}>{role.name}</Typography>
            ))}
          </Stack>
        </Stack>
      </Stack>
    </Paper>
  );
};

export default PreviewPermission;
