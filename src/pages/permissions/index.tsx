import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import DeletePermissionModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import usePermissionsLogic from "./hooks/usePermissionsLogic";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
const Permissions = () => {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const ability = useContext(AbilityContext);
  const navigate = useNavigate();
  const {
    data,
    permission,
    error,
    page,
    rowsPerPage,
    searchPermissions,
    openDeleteModal,
    isError,
    isLoading,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPermissions,
    handleDeletePermission,
    handleOpenDeletePermissionModal,
    handleCloseDeletePermissionModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPermissions,
  } = usePermissionsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchPermissions}
          setValue={setSearchPermissions}
          debouncedFunction={handleSearchPermissions}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_permission">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={() => {
              navigate("permission/add");
            }}
          >
            {t("pages.permissions.addPermission")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.permissions.total!}
        rowsPerPage={rowsPerPage}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        rows={
          data?.pages
            .map((page) => page.response.permissions.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.permissions.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.guard_name}</TableCell>
                <TableCell>
                  <TableActions
                    delete={() => {
                      ability.can("destroy", "dashboard_api_permission") &&
                        handleOpenDeletePermissionModal(row);
                    }}
                    edit={() => {
                      ability.can("update", "dashboard_api_permission") &&
                        navigate(`permission/${row.id}/update`);
                    }}
                    preview={() => {
                      ability.can("update", "dashboard_api_permission") &&
                        navigate(`permission/${row.id}/preview`);
                    }}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeletePermissionModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")} ${permission.name}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeletePermission}
        handleClose={handleCloseDeletePermissionModal}
      />
    </Box>
  );
};

export default Permissions;
