import { useTranslation } from "react-i18next";
import * as Yup from "yup";

const useValidation = () => {
  const { t } = useTranslation();
  const permissionSchema = Yup.object().shape({
    is_parent: Yup.boolean().required(t("form.required")),
    name: Yup.string()
      .when("is_parent", {
        is: true,
        then: (schema) => schema.matches(/^dashboard_api_[a-zA-Z]+$/),
        otherwise: (schema) =>
          schema.matches(
            /^dashboard_api_[a-zA-Z]+\.(view|index|update|delete|store)$/
          ),
      })
      .required(t("form.required")),
    parent: Yup.object()
      .shape({ id: Yup.string(), name: Yup.string() })
      .when("is_parent", {
        is: true,
        then: (schema) => schema.nullable(),
        otherwise: (schema) => schema.required(t("form.required")),
      }),
  });
  return permissionSchema;
};

export default useValidation;
