import { FormikHelpers } from "formik";
import {
  useAddPermissionMutation,
  useUpdatePermissionMutation,
  useGetPermissionQuery,
  useGetPermissionsParentQuery,
} from "@/api/permissions/usePermissionsQueries";
import { useGetRolesQuery } from "@/api/roles/useRolesQueries";
import { TRole } from "@/api/roles/type";
import { TInitialValues } from "../type";
import { useQueryClient } from "@tanstack/react-query";
import { TGenericResponse } from "@/api/type";
import { TPermissionParentResponse } from "@/api/permissions/type";

const useAddUpdateLogic = ({ permissionId }: { permissionId: string }) => {
  const queryClient = useQueryClient();
  const {
    data: permission,
    isLoading: isGetPermissionLoading,
    isFetching: isGetPermissionFetching,
    isError: isGetPermissionError,
  } = useGetPermissionQuery({ params: { id: permissionId } });

  const {
    data: roles,
    isLoading: isGetRolesLoading,
    isError: isGetRolesError,
  } = useGetRolesQuery();
  const {
    data: permissionsParent,
    isLoading: isGetPermissionParentLoading,
    isError: isGetPermissionParentError,
  } = useGetPermissionsParentQuery({});
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddPermissionMutation();

  const { mutate: updateMutate, isLoading: isEditLoading } =
    useUpdatePermissionMutation({ params: { id: permissionId } });

  const _roles = roles?.response.roles.data.map((role) => ({
    ...role,
    checked: false,
  })) as (TRole & { checked: boolean })[];
  const initialValues: TInitialValues = permission?.response.permission
    ? {
        id: permission.response.permission.id,
        name: permission.response.permission.name,
        guard_name: permission.response.permission.guard_name,
        roles: _roles.map((_role) => ({
          ..._role,
          checked: permission?.response.permission.roles.find(
            (role) => role.id === _role.id
          )
            ? true
            : false,
        })),
        is_parent:
          permission.response.permission.is_parent === 1 ? true : false,
        parent: permission.response.permission.parent,
      }
    : {
        name: "",
        guard_name: "",
        roles: _roles,
        is_parent: false,
        parent: null,
      };
  const handleSubmittedData = (values: TInitialValues) => {
    return {
      name: values.name,
      guard_name: values.guard_name,
      roles: values.roles.filter((role) => role.checked).map((role) => role.id),
      is_parent: values.is_parent ? 1 : (0 as 0 | 1),
      ...(values?.parent?.id && { parent_id: values?.parent?.id }),
    };
  };
  const handleSubmit = (
    values: TInitialValues,
    formikHelper: FormikHelpers<TInitialValues>
  ) => {
    permissionId
      ? updateMutate(
          { body: handleSubmittedData(values) },
          {
            onSuccess: () => {
              queryClient.invalidateQueries(["`get-permissions-parent-query`"]);
              formikHelper.resetForm();
            },
          }
        )
      : addMutate(
          { body: handleSubmittedData(values) },
          {
            onSuccess: () => {
              queryClient.invalidateQueries(["`get-permissions-parent-query`"]);
              formikHelper.resetForm();
            },
          }
        );
    formikHelper.setSubmitting(false);
  };
  return {
    permissionsParent,
    isGetPermissionLoading,
    isGetRolesLoading,
    isGetPermissionParentLoading,
    isAddLoading,
    isEditLoading,
    isGetPermissionFetching,
    isGetPermissionError,
    isGetRolesError,
    isGetPermissionParentError,
    initialValues,
    handleSubmit,
  } as {
    permissionsParent: TGenericResponse<TPermissionParentResponse>;
    isGetPermissionLoading: boolean;
    isGetRolesLoading: boolean;
    isGetPermissionParentLoading: boolean;
    isAddLoading: boolean;
    isEditLoading: boolean;
    isGetPermissionFetching: boolean;
    isGetPermissionError: boolean;
    isGetRolesError: boolean;
    isGetPermissionParentError: boolean;
    initialValues: TInitialValues;
    handleSubmit: (
      values: TInitialValues,
      formikHelper: FormikHelpers<TInitialValues>
    ) => void;
  };
};

export default useAddUpdateLogic;
