import { useNavigate, useParams } from 'react-router-dom'
import {
  Grid,
  Paper,
  Typography,
  TextField,
  IconButton,
  Stack,
} from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { useTranslation } from 'react-i18next'
import { Form, Formik } from 'formik'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { TParams } from '../type'
import Loading from '@/components/others/loading'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import useValidation from './hook/useValidation'
import CheckboxInput from '@/components/items/inputField/Checkbox/Checkbox'
import useAddUpdateLogic from './hook/useAddUpdateLogic'
import { TNameAndId } from '@/type'
import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'

const AddUpdatePermission = () => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const { permissionId } = useParams<TParams>()
  const permissionSchema = useValidation()
  const {
    permissionsParent,
    isGetPermissionLoading,
    isGetRolesLoading,
    isGetPermissionParentLoading,
    isAddLoading,
    isEditLoading,
    isGetPermissionFetching,
    isGetPermissionError,
    isGetRolesError,
    isGetPermissionParentError,
    initialValues,
    handleSubmit,
  } = useAddUpdateLogic({ permissionId })

  if (!!permissionId) {
    if (isGetPermissionLoading || isGetPermissionFetching) {
      return <Loading />
    } else if (isGetPermissionError) {
      return <></>
    }
  }

  return (
    <GenericFormPage
      title={
        permissionId
          ? t('pages.permissions.updatePermission')
          : t('pages.permissions.addPermission')
      }
      goBackLink="/permissions"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={permissionSchema}
        enableReinitialize
      >
        {({
          values,
          touched,
          errors,
          setFieldTouched,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => {
          console.log(errors, values)
          return (
            <Form>
              <Stack spacing={2}>
                <CheckboxInput
                  name={'is_parent'}
                  label={t('pages.permissions.form.isParent')}
                  checked={values.is_parent}
                  onChange={(e, checked) => {
                    setFieldValue('is_parent', checked)
                    setFieldValue('parent', null)
                  }}
                />
                <TextField
                  name="name"
                  fullWidth
                  label={t('pages.permissions.form.name')}
                  value={values.name}
                  onBlur={() => {
                    setFieldTouched('name', true)
                  }}
                  onChange={handleChange}
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                />

                <AutoCompleteField
                  name="guard_name"
                  fullWidth
                  label={t('pages.permissions.form.guardName')}
                  value={values.guard_name}
                  onChange={(_, value) => setFieldValue('guard_name', value)}
                  options={['web']}
                  error={touched.guard_name && !!errors.guard_name}
                  errorMessage={touched.guard_name && errors.guard_name}
                />

                {!values.is_parent && (
                  <AutoCompleteField
                    name="parent"
                    label={t('pages.permissions.form.parent')}
                    value={values.parent}
                    onChange={(e, permissionParent) =>
                      setFieldValue('parent', permissionParent)
                    }
                    loading={isGetPermissionParentLoading}
                    disabled={isGetPermissionParentError}
                    error={isGetPermissionParentError}
                    errorMessage=""
                    options={permissionsParent?.response.permissions.data ?? []}
                  />
                )}
                {isGetRolesLoading ? (
                  <Loading />
                ) : isGetRolesError ? (
                  <></>
                ) : (
                  <Stack direction="row">
                    {values.roles?.map((role, index) => (
                      <CheckboxInput
                        key={index}
                        name={`roles.${index}.checked`}
                        label={role.name}
                        checked={values.roles[index].checked}
                        onChange={(e, checked) => {
                          setFieldValue(`roles.${index}.checked`, checked)
                        }}
                      />
                    ))}
                  </Stack>
                )}
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={permissionId ? t('common.edit') : t('common.add')}
                />
              </Stack>
            </Form>
          )
        }}
      </Formik>
    </GenericFormPage>
  )
}

export default AddUpdatePermission
