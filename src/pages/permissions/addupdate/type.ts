import { TPermission } from "@/api/permissions/type";
import { TRole } from "@/api/roles/type";
import { TNameAndId } from "@/type";

export type TInitialValues = TPermission & {
  roles: (TRole & { checked: boolean })[];
  is_parent: boolean;
  parent: TNameAndId | null;
};
