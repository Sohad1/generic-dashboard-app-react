import React from "react";
import { useTranslation } from "react-i18next";

import { HeadCell } from "@/type/Table";
const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.users.form.name"),
    },
    {
      id: "guard_name",
      numeric: false,
      disablePadding: false,
      label: t("pages.permissions.form.guardName"),
    },
  ];
  return headCells;
};

export default useTableHeader;
