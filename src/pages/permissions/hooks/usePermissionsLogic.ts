import { ChangeEvent, useState } from "react";
import {
  useDeletePermissionMutation,
  useGetPermissionsInfiniteQuery,
} from "@/api/permissions/usePermissionsQueries";
import { useQueryClient } from "@tanstack/react-query";
import { TOrder, TRow } from "@/type/Table";
const usePermissionsLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [searchPermissions, setSearchPermissions] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [permission, setPermission] = useState<TRow>({});
  const queryClient = useQueryClient();
  const { data, isLoading, isError, error, fetchNextPage } =
    useGetPermissionsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeletePermissionMutation();
  const handleSearchPermissions = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleCloseDeletePermissionModal = () => {
    setOpenDeleteModal(false);
  };
  const handleOpenDeletePermissionModal = (permission: TRow) => {
    setPermission(permission);
    setOpenDeleteModal(true);
  };
  const handleDeletePermission = () => {
    deleteMutate(
      { params: { id: permission.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["permissions-infinite-query"]);
            handleCloseDeletePermissionModal();
          }
        },
      }
    );
  };

  return {
    data,
    permission,
    error,
    page,
    rowsPerPage,
    searchPermissions,
    openDeleteModal,
    isError,
    isLoading,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPermissions,
    handleDeletePermission,
    handleOpenDeletePermissionModal,
    handleCloseDeletePermissionModal,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPermissions,
  };
};

export default usePermissionsLogic;
