import { useGetGiftQuery } from "@/api/gifts/useGiftsQueries";
import { useParams, useNavigate } from "react-router-dom";
import { Button, Paper, Typography, Box } from "@mui/material";
import { useTranslation } from "react-i18next";
import Loading from "@/components/others/loading";
const PreviewGift = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetGiftQuery(id);
  console.log("id", id);
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t("pages.gifts.PreviewGifts")}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t("pages.gifts.form.number of point")} : {data?.response.gift.point}
      </Typography>
      <Paper variant="outlined" />
      <Typography variant="h5" color="primary" sx={{ marginTop: 5 }}>
        {t("pages.gifts.form.date")}: {data?.response.gift.created_at}
      </Typography>

      <Paper variant="outlined" sx={{ marginBottom: 5 }} />
      <Button
        sx={{ float: "right" }}
        variant="outlined"
        onClick={() => {
          navigate("/gifts");
        }}
      >
        {t("pages.categories.cancel")}
      </Button>
    </Box>
  );
};

export default PreviewGift;
