import { TGifts } from "@/api/gifts/type";

export type TInitialValues = TGifts;
export type TParams = {
  giftId: string | undefined;
};
