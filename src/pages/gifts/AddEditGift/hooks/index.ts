import { useGetCollectionsQuery } from '@/api/collections/useCollectionsQueries'
import { useGetCountriesByCollectionIdQuery } from '@/api/countries/useCountriesQueries'
import { TParams } from '@/pages/collection/AddEditCollection/hooks/type'
import { useParams } from 'react-router-dom'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import { object } from 'yup'
const useCollectionsLogic = ({
  value,
}: {
  value: { id: string | number; name: string } | TOption
}) => {
  const getIdFormValue = () => {
    let selectedId
    if (value) {
      Object.entries(value).map(([key, val]) => {
        if (key === 'id') {
          selectedId = val
        }
      })
    }
    return selectedId
  }
  const id = getIdFormValue()
  const { collectionId } = useParams()
  const { data: collection, isLoading: isCollectionLoading } =
    useGetCollectionsQuery()

  return {
    collections: collection?.response?.collections?.data?.map((item) => ({
      id: item.id,
      name: item.name,
    })),
    isCollectionLoading,

    // isCountriesLoading,
  }
}

export default useCollectionsLogic
