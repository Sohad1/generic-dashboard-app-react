import { useTranslation } from "react-i18next";
import * as Yup from "yup";

function useValidationSchema() {
  const { t } = useTranslation();
  const cardSchema = Yup.object().shape({
    point: Yup.string().required(t("form.required")),
    // countries:
  });

  return cardSchema;
}

export default useValidationSchema;
