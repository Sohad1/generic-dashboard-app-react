import { useNavigate, useParams } from "react-router-dom";
import {
  Button,
  Grid,
  Box,
  Paper,
  Typography,
  TextField,
  imageListClasses,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import { Form, Formik, Field } from "formik";
import { ChangeEvent, useState } from "react";
import ArrowBackIcon from "@mui/material/Icon";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import {
  useAddGiftMutation,
  useEditGiftMutation,
  useGetGiftQuery,
} from "@/api/gifts/useGiftsQueries";

import { TInitialValues, TParams } from "./hooks/type";
import Loading from "@/components/others/loading";
import SelectField from "@/components/items/inputField/selectField/SelectField";
import AutoCompleteField from "@/components/items/inputField/autoCompleteField/AutoCompleteField";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import { TGifts } from "@/api/gifts/type";
import useCollectionsLogic from "./hooks";
import CountriesByCollectionsAutoComplete from "./CountriesByCollectionsAutoComplete";
import GenericFormPage from "@/components/items/form/genericFormPage/GenericFormPage";
import useValidationSchema from "./hooks/useValidation";

const AddEditGift = () => {
  const [selectedCollection, setSelectedCollection] = useState<TOption>();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { giftId } = useParams<TParams>();
  const validationSchema = useValidationSchema();

  const [options, setoptions] = useState<number[]>([]);
  const {
    data,
    isLoading: isGetGiftLoading,
    isFetching,
    isError,
  } = useGetGiftQuery(giftId);
  const { mutate: addMutate, isLoading: isAddLoading } = useAddGiftMutation();
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditGiftMutation();
  const { collections, isCollectionLoading } = useCollectionsLogic({
    value: selectedCollection,
  });
  if (!!giftId) {
    if (isGetGiftLoading || isFetching) {
      return <Loading />;
    } else if (isError) {
      return <></>;
    }
  }

  const initialValues: TInitialValues = data?.response.gift
    ? { ...data.response.gift }
    : {
        point: 0,
        created_at: "",
        country_id: null,
        gift_users_count: 0,
        countries: null,
        collection: null,
        all_countries: null,
        gift_country_ids: null,
      };

  return (
    <GenericFormPage
      title={giftId ? t("pages.gifts.editgifts") : t("pages.gifts.addgifts")}
      goBackLink="/gifts"
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          let data = { ...values, gift_country_ids: values.countries.id };
          delete data.countries;
          giftId ? editMutate({ body: data }) : addMutate({ body: data });
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="point"
                  fullWidth
                  label={t("pages.gifts.form.number of point")}
                  value={values.point}
                  onChange={handleChange}
                  error={touched.point && !!errors.point}
                  helperText={touched.point && errors.point}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <AutoCompleteField
                  label={t("pages.collection.collections")}
                  name="collection"
                  options={collections || [{ id: 0, name: "" }]}
                  value={values.collection}
                  onChange={(_, value: any) => {
                    setFieldValue("collection", value);
                    setFieldValue("collection_id", value.id);
                    setSelectedCollection(value);
                  }}
                  fullWidth
                  error={touched.collection && !!errors.collection}
                  errorMessage=""
                  {...{
                    loading: isCollectionLoading,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <CountriesByCollectionsAutoComplete
                  error={touched.countries && !!errors.countries}
                  value={values.countries}
                  setFieldValue={setFieldValue}
                  disabled={values.collection ? false : true}
                  collectionId={values.collection?.id}
                />
              </Grid>
              <Grid item xs={12} sx={{ display: "flex", marginBottom: 5 }}>
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading || isEditLoading}
                  buttonText={giftId ? t("common.edit") : t("common.add")}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  );
};

export default AddEditGift;
