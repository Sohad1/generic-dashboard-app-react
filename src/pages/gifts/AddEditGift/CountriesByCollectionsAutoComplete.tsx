import { useGetCountriesByCollectionIdQuery } from "@/api/countries/useCountriesQueries";
import AutoCompleteField from "@/components/items/inputField/autoCompleteField/AutoCompleteField";
import MultipleAutoCompleteField from "@/components/items/inputField/autoCompleteField/MultipleAutoCompleteField";
import React from "react";
import { useTranslation } from "react-i18next";
type Props = {
  setFieldValue: (field: string, value: any) => void;
  error: boolean;
  value: any;
  collectionId: number;
  disabled: boolean;
};

function CountriesByCollectionsAutoComplete({
  setFieldValue,
  error,
  value,
  collectionId,
  disabled,
}: Props) {
  const { t } = useTranslation();
  const { data, isLoading: isCountriesLoading } =
    useGetCountriesByCollectionIdQuery(collectionId!);

  let countries = data?.data.map((item) => ({
    id: item.id,
    name: item.country_enName,
  }));

  console.log(countries);

  return (
    <MultipleAutoCompleteField
      label={t("pages.products.countries")}
      name="countries"
      options={countries || [{ id: 0, name: "" }]}
      onChange={(_, value) => {
        setFieldValue("countries", value);
      }}
      value={value}
      fullWidth
      error={error}
      errorMessage=""
      {...{
        loading: isCountriesLoading || isCountriesLoading,
        disabled,
      }}
    />
  );
}

export default CountriesByCollectionsAutoComplete;
