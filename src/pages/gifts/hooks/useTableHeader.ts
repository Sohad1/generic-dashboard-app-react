import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.gifts.form.number of point"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.gifts.form.date"),
    },
    {
      id: "necessity",
      numeric: false,
      disablePadding: false,
      label: t("pages.notification.form.numberOfClick"),
    },
  ];
  return headCells;
};

export default useTableHeader;
