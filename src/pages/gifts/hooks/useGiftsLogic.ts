import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetGiftsInfiniteQuery } from "@/api/gifts/useGiftsQueries";
import { useNavigate } from "react-router-dom";
import { useDeleteGiftMutation } from "@/api/gifts/useGiftsQueries";
import { useQueryClient } from "@tanstack/react-query";
import Gifts from "..";
const useGiftsLogic = () => {
  const [page, setPage] = useState(0);
  const queryClient = useQueryClient();
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchGifts, setSearchGifts] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [Gifts, setGifts] = useState<TRow>({});
  const navigate = useNavigate();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);

  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetGiftsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteGiftMutation();
  const handleSearchGifts = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/gifts/PreviewGift/${id}`);
  };
  const handleAddGift = () => {
    navigate("/gifts/addGift");
  };
  const handleEditGift = (row: TRow) => {
    navigate(`/gifts/editGift/${row.id}`);
  };
  const handleCloseDeleteGiftModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteGiftModal = (Gifts: TRow) => {
    setGifts(Gifts);
    setOpenDeleteModal(true);
  };
  const handleDeleteGift = () => {
    deleteMutate(
      { params: { id: Gifts.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["gift-infinite-query"]);
            handleCloseDeleteGiftModal();
          }
        },
      }
    );
  };

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchGifts,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleEditGift,
    handleAddGift,
    handleDeleteGift,
    handleOpenDeleteGiftModal,
    handleCloseDeleteGiftModal,
    handleSelectForNavigateToPreviewPage,
    setSearchGifts,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchGifts,
  };
};
export default useGiftsLogic;
