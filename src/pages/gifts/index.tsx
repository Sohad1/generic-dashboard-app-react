import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import useGiftsLogic from "./hooks/useGiftsLogic";
import useTableHeader from "./hooks/useTableHeader";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import { number } from "yup";
import DeleteGiftsModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useContext } from "react";
import { AbilityContext, Can } from "@/libs/casl/can";
const Gifts = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchGifts,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleEditGift,
    handleAddGift,
    handleDeleteGift,
    handleOpenDeleteGiftModal,
    handleCloseDeleteGiftModal,
    handleSelectForNavigateToPreviewPage,
    setSearchGifts,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchGifts,
  } = useGiftsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchGifts}
          setValue={setSearchGifts}
          debouncedFunction={handleSearchGifts}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="dashboard_api_gifts">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddGift}
          >
            {t("pages.gifts.addgifts")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.gifts.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.gifts.data)
            .reduce((previous, current) => [...previous, ...current]) as any[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.gifts.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.point}</TableCell>
                <TableCell>{row.created_at}</TableCell>
                <TableCell>{row.gift_users_count}</TableCell>
                <TableCell>
                  <TableActions
                    //   edit={() => {
                    // ability.can("update", "dashboard_api_gifts") &&
                    // handleEditGift(row);
                    //   }}
                    preview={() =>
                      ability.can("update", "dashboard_api_gifts") &&
                      handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteGiftModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteGiftsModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteGift}
        handleClose={handleCloseDeleteGiftModal}
      />
    </Box>
  );
};

export default Gifts;
