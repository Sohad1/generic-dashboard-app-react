import React from "react";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
import {
  useConfirmQuery,
  useRejectQuery,
} from "@/api/deleteOrders/useDeleteOrdersQueries";

type Props = {
  open: boolean;
  handleClose: () => void;
  deleteOrders: { id: number; user_id: number; status: number };
};
const DeleteOrdersConfirmModal = ({
  deleteOrders,
  open,
  handleClose,
}: Props) => {
  const { t } = useTranslation();
  const {
    mutate: deleteOrdersConfirmMutate,
    isLoading: isDeleteOrdersLoading,
  } = useConfirmQuery(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`common.confirmTitle`)} ${deleteOrders.user_id} `,
        handleClose,
        handleSubmit: () => deleteOrdersConfirmMutate(deleteOrders.id),
        isSubmittingLoading: isDeleteOrdersLoading,
        submitButtonText: "ok",
      }}
    />
  );
};

export default DeleteOrdersConfirmModal;
