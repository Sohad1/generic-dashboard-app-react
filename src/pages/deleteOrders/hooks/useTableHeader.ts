import { HeadCell } from '@/type/Table'
import { useTranslation } from 'react-i18next'

const useTableHeader = () => {
  const { t } = useTranslation()
  const headCells: HeadCell[] = [
    {
      id: '#',
      numeric: true,
      disablePadding: false,
      label: '',
    },

    {
      id: 'key',
      numeric: false,
      disablePadding: false,
      label: t('pages.DeleteOrders.form.user'),
    },
    {
      id: 'value',
      numeric: false,
      disablePadding: false,
      label: t('pages.DeleteOrders.form.reason'),
    },
    {
      id: 'value',
      numeric: false,
      disablePadding: false,
      label: t('pages.DeleteOrders.form.status'),
    },
  ]
  return headCells
}
export default useTableHeader
