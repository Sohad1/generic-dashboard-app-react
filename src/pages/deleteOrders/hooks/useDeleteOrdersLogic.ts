import { ChangeEvent, useState } from 'react'
import { TOrder, TRow } from '@/type/Table'
import {
  useGetDeleteOrdersInfiniteQuery,
  useConfirmQuery,
} from '@/api/deleteOrders/useDeleteOrdersQueries'

const useDeleteOrdersLogic = () => {
  const [openConfirmModal, setOpenConfirmModal] = useState(false)
  const [openStatusModal, setOpenStatusModal] = useState(false)

  const [selectedDeleteOrder, setSelectedDeleteOrder] = useState({
    id: 0,
    user_id: 0,
    status: 0,
  })
  const [order, setOrder] = useState<TOrder>('asc')
  const [orderBy, setOrderBy] = useState<string>('')
  const [selectedIds, setSelectedIds] = useState<number[]>([])
  const [page, setPage] = useState(0)
  const [openDeleteModal, setOpenDeleteModal] = useState(false)
  const [DeleteOrder, setDeleteOrder] = useState<TRow>({})
  const [rowsPerPage, setRowsPerPage] = useState(5)
  const [searchDeleteOrder, setSearchDeleteOrder] = useState<string>('')
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>('')
  const { data, isLoading, isError, error, refetch } =
    useGetDeleteOrdersInfiniteQuery({
      params: {
        // limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    })

  console.log(data)
  const handleSearchDeleteOrder = (value: string) => {
    console.log(value)
  }
  const handleChangePage = (event: unknown, newPage: number) => {
    // if (newPage > page) fetchNextPage({ pageParam: newPage })
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleDeleteDeleteOrder = (ids: string[]) => {
    setOpenDeleteModal(true)
    // setSelectedIds(ids)
  }
  const handleCloseDeleteModal = () => setOpenDeleteModal(false)

  const handleConfirm = (row: TRow) => {
    setOpenConfirmModal(true)
    setSelectedDeleteOrder({
      user_id: Number(row.user_id),
      id: Number(row.id),
      status: Number(row.status),
    })
  }
  const handleCloseDeleteOrderModal = () => setOpenConfirmModal(false)

  const handleRejectDeleteOrder = (row: TRow) => {
    setOpenStatusModal(true)
    setSelectedDeleteOrder({
      user_id: Number(row.user_id),
      id: Number(row.id),
      status: Number(row.status),
    })
  }
  const handleCloseStatusModal = () => setOpenStatusModal(false)

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchDeleteOrder,
    isError,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    selectedIds,
    openDeleteModal,
    openConfirmModal,
    openStatusModal,
    isLoading,
    selectedDeleteOrder,

    handleConfirm,
    setSearchDeleteOrder,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchDeleteOrder,
    handleCloseStatusModal,
    handleDeleteDeleteOrder,
    handleRejectDeleteOrder,
    handleCloseDeleteModal,
    handleCloseDeleteOrderModal,
  }
}
export default useDeleteOrdersLogic
