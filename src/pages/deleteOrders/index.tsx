import { Box, Button, Stack, TableCell } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import GenericTable from '@/components/items/table/genericTable/Table'
import { getComparator, stableSort } from '@/util/table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { TRow } from '@/type/Table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import Loading from '@/components/others/loading'
import { useTranslation } from 'react-i18next'
import useDeleteOrdersLogic from './hooks/useDeleteOrdersLogic'
import { useContext, useState } from 'react'
import { AbilityContext, Can } from '@/libs/casl/can'
import useTableHeader from './hooks/useTableHeader'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { number } from 'yup'
import DeletePermissionModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import DeleteOrdersConfirmModal from './DeleteOrdersConfirmModel'
import DeleteOrdersRejectModal from './DeleteOrdersRejectModel'
import DeleteOrdersDeleteAllModal from './DeleteOrdersDeleteAllModel'
import Checkbox from '@mui/material/Checkbox'

const DeleteOrders = () => {
  const { t } = useTranslation()
  const headCells = useTableHeader()
  const ability = useContext(AbilityContext)
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchDeleteOrder,
    isError,

    openDeleteModal,
    openConfirmModal,
    openStatusModal,
    isLoading,
    selectedIds,
    selectedDeleteOrder,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleConfirm,
    setSearchDeleteOrder,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchDeleteOrder,
    handleCloseStatusModal,
    handleDeleteDeleteOrder,
    handleRejectDeleteOrder,
    handleCloseDeleteModal,
    handleCloseDeleteOrderModal,
  } = useDeleteOrdersLogic()

  // const filterIds = data?.pages.map((page) =>
  //   page.response.delete_orders.data.map((result) => result.id)
  // )

  // const flatFilterIds = filterIds?.flatMap((result) => result)
  // console.log('filterids', filterIds)

  const [selected, setSelected] = useState<string[]>([])
  const isSelected = (name: string) => selected.indexOf(name) !== -1
  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = stableSort(
        data?.response.delete_orders.data,
        getComparator(order, orderBy)
      )
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: any, index) => row.id)

      setSelected(newSelected)
      return
    }
    setSelected([])
  }

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name)
    let newSelected: string[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }
    setSelected(newSelected)
  }

  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <Box>
      <DeleteOrdersConfirmModal
        {...{
          open: openConfirmModal,
          handleClose: handleCloseDeleteOrderModal,
          deleteOrders: selectedDeleteOrder,
        }}
      />
      <DeleteOrdersDeleteAllModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          deleteOrders: selected,
        }}
      />
      <DeleteOrdersRejectModal
        {...{
          open: openStatusModal,
          handleClose: handleCloseStatusModal,
          deleteOrders: selectedDeleteOrder,
        }}
      />

      <Stack direction={'row'} justifyContent={'space-between'}>
        <DebouncedTextField
          value={searchDeleteOrder}
          setValue={setSearchDeleteOrder}
          debouncedFunction={handleSearchDeleteOrder}
          label={t('common.search')}
          size="small"
        />
        {selected.length > 0 ? (
          <Button
            variant="contained"
            onClick={() => {
              handleDeleteDeleteOrder(selected)
            }}
          >
            {t('pages.DeleteOrders.deleteall')}
          </Button>
        ) : null}
      </Stack>

      <Box
        sx={{
          position: 'relative',
          top: '50px',
          width: '30px',
          left: '5px',
          zIndex: 9999,
        }}
      >
        <Checkbox
          color="primary"
          indeterminate={
            selected.length > 0 &&
            selected.length < data?.response.delete_orders.data.length
          }
          checked={
            data?.response.delete_orders.data.length > 0 &&
            selected.length === data?.response.delete_orders.data.length
          }
          onChange={handleSelectAllClick}
          inputProps={{
            'aria-label': 'select all desserts',
          }}
        />
        {/* Select All */}
      </Box>
      <GenericTable
        page={page}
        count={data?.response.delete_orders.data.length!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data?.response.delete_orders.data.map((page) => page)}
        headCells={headCells}
      >
        {stableSort(
          data?.response.delete_orders.data.map((page) => page),
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            const isItemSelected = isSelected(row.id)
            const labelId = `enhanced-table-checkbox-${index}`
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell padding="checkbox">
                  <Checkbox
                    color="primary"
                    checked={isItemSelected}
                    inputProps={{
                      'aria-labelledby': labelId,
                    }}
                    onClick={(e: any) => handleClick(e, row.id)}
                  />
                </TableCell>
                <TableCell>{row.user?.name}</TableCell>
                <TableCell>{row.reason}</TableCell>
                <TableCell>
                  {t(`pages.order.deleteStatus-${row.status}`)}
                </TableCell>

                <TableCell
                  sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    gap: '10px',
                  }}
                >
                  <Button
                    variant="contained"
                    onClick={() => {
                      handleConfirm(row)
                    }}
                  >
                    {t('pages.order.confirm')}
                  </Button>

                  <Button
                    variant="contained"
                    // sx={{ background: "red" }}
                    onClick={() => {
                      handleRejectDeleteOrder(row)
                    }}
                  >
                    {t('pages.order.reject')}
                  </Button>
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
    </Box>
  )
}

export default DeleteOrders
