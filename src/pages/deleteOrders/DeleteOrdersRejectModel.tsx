import React from "react";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
import {
  useConfirmQuery,
  useRejectQuery,
} from "@/api/deleteOrders/useDeleteOrdersQueries";

type Props = {
  open: boolean;
  handleClose: () => void;
  deleteOrders: { id: number; user_id: number; status: number };
};

const DeleteOrdersRejectModal = ({
  deleteOrders,
  open,
  handleClose,
}: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteOrdersRejectMutate, isLoading: isDeleteOrdersLoading } =
    useRejectQuery(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`common.rejectTitle`)} ${deleteOrders.user_id} `,
        handleClose,
        handleSubmit: () => deleteOrdersRejectMutate(deleteOrders.id),
        isSubmittingLoading: isDeleteOrdersLoading,
        submitButtonText: "ok",
      }}
    />
  );
};
export default DeleteOrdersRejectModal;
