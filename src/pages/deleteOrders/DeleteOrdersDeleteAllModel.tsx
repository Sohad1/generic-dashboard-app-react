import React from 'react'
import GenericModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import { useTranslation } from 'react-i18next'
import {
  useConfirmQuery,
  useRejectQuery,
  useDeleteAllQuery,
} from '@/api/deleteOrders/useDeleteOrdersQueries'

type Props = {
  open: boolean
  handleClose: () => void
  deleteOrders: string[]
}
const DeleteOrdersDeleteAllModal = ({
  open,
  handleClose,
  deleteOrders,
}: Props) => {
  const { t } = useTranslation()
  const {
    mutate: deleteOrdersDeleteAllMutate,
    isLoading: isDeleteOrdersLoading,
  } = useDeleteAllQuery(handleClose)
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`common.deleteTitle`)}`,
        handleClose,
        handleSubmit: () => deleteOrdersDeleteAllMutate(deleteOrders),
        isSubmittingLoading: isDeleteOrdersLoading,
        submitButtonText: 'ok',
      }}
    />
  )
}

export default DeleteOrdersDeleteAllModal
