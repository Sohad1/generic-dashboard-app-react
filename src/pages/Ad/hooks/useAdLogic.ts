import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetAdsInfiniteQuery } from "@/api/Ad/useAdQueries";
import { useNavigate } from "react-router-dom";
import { useDeleteAdMutation } from "@/api/Ad/useAdQueries";
import { useQueryClient } from "@tanstack/react-query";
const useAdsLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchAds, setSearchAds] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const [Ad, setAd] = useState<TRow>({});

  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetAdsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  const { mutate: deleteMutate, isLoading: isDeleting } = useDeleteAdMutation();
  const handleSearchAds = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddAd = () => {
    navigate("/Ad/AddAd");
  };
  const handleEditAd = (row: TRow) => {
    navigate(`/Ad/editAd/${row.id}`);
  };
  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/Ad/PreviewAd/${id}`);
  };
  const handleCloseDeletAdModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteAdModal = (Ads: TRow) => {
    setAd(Ads);
    setOpenDeleteModal(true);
  };
  const handleDeleteAd = () => {
    deleteMutate(
      { params: { id: Ad.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["Ad-infinite-query"]);
            handleCloseDeletAdModal();
          }
        },
      }
    );
  };

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchAds,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchAds,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchAds,
    handleAddAd,
    handleEditAd,
    handleSelectForNavigateToPreviewPage,
    handleDeleteAd,
    handleOpenDeleteAdModal,
    handleCloseDeletAdModal,
  };
};
export default useAdsLogic;
