import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();
  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "key",
      numeric: false,
      disablePadding: false,
      label: t("pages.Ad.form.title"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.Ad.form.description"),
    },
    {
      id: "necessity",
      numeric: false,
      disablePadding: false,
      label: t("pages.Ad.form.Ad"),
    },
  ];
  return headCells;
};

export default useTableHeader;
