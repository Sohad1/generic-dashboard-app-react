import { useGetAdQuery } from "@/api/Ad/useAdQueries";
import { useParams, useNavigate } from "react-router-dom";
import { Button, Paper, Typography, Box } from "@mui/material";
import { useTranslation } from "react-i18next";
import Loading from "@/components/others/loading";
const PreviewAd = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetAdQuery(id);
  console.log("id", id);
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t("pages.Ad.PreviewAd")}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t("pages.Ad.form.title")} : {data?.response.ad.title}
      </Typography>
      <Paper variant="outlined" />
      <Typography variant="h5" color="primary" sx={{ marginTop: 5 }}>
        {t("pages.Ad.form.description")}: {data?.response.ad.description}
      </Typography>
      <Paper variant="outlined" />

      <Paper variant="outlined" sx={{ marginBottom: 5 }} />
      <Button
        sx={{ float: "right" }}
        variant="outlined"
        onClick={() => {
          navigate("/Ad");
        }}
      >
        {t("pages.categories.cancel")}
      </Button>
    </Box>
  );
};

export default PreviewAd;
