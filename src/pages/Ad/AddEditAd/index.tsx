import { useNavigate, useParams } from "react-router-dom";
import { Button, Grid, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Form, Formik } from "formik";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import ImageDragDropField from "@/components/items/inputField/ImageDragDropField";
import {
  useAddAdMutation,
  useEditAdMutation,
  useGetAdQuery,
} from "@/api/Ad/useAdQueries";
import { TInitialValues, TParams } from "./hooks/type";
import Loading from "@/components/others/loading";
import GenericFormPage from "@/components/items/form/genericFormPage/GenericFormPage";
import useValidationSchema from "./hooks/useValidation";

const AddEditAd = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { AdId } = useParams<TParams>();
  const validationSchema = useValidationSchema();
  const { data, isLoading: isGetAdLoading, isError } = useGetAdQuery(AdId);
  const { mutate: addMutate, isLoading: isAddLoading } = useAddAdMutation();
  const { mutate: editMutate, isLoading: isEditLoading } = useEditAdMutation();
  if (!!AdId) {
    if (isGetAdLoading) {
      return <Loading />;
    } else if (isError) {
      return <></>;
    }
  }
  const initialValues: TInitialValues = data?.response.ad
    ? { ...data.response.ad }
    : {
        description: "",
        title: "",
        description_de: "",
        image: null,
        description_en: "",
        title_de: "",
        title_en: "",
        url: "",
        image_url: null,
      };

  return (
    <GenericFormPage
      title={AdId ? t("pages.Ad.editAd") : t("pages.Ad.addAd")}
      goBackLink="/Ad"
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          console.log(values);

          AdId ? editMutate({ body: values }) : addMutate({ body: values });
        }}
        validationSchema={validationSchema}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <ImageDragDropField
                  handleChangeFile={(v) => {
                    setFieldValue("image", v);
                  }}
                  file={values.image}
                  name="image"
                  type={AdId ? "edit" : "add"}
                  label={"image"}
                  error={touched.image && !!errors.image}
                />
              </Grid>

              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title"
                  fullWidth
                  label={t("pages.Ad.form.title")}
                  value={values.title}
                  onChange={handleChange}
                  error={touched.title && !!errors.title}
                  helperText={touched.title && errors.title}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title_de"
                  fullWidth
                  label={t("pages.Ad.form.title_de")}
                  value={values.title_de}
                  onChange={handleChange}
                  error={touched.title_de && !!errors.title_de}
                  helperText={touched.title_de && errors.title_de}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="title_en"
                  fullWidth
                  label={t("pages.Ad.form.title_en")}
                  value={values.title_en}
                  onChange={handleChange}
                  error={touched.title_en && !!errors.title_en}
                  helperText={touched.title_en && errors.title_en}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  label={t("pages.Ad.form.description")}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description_de"
                  fullWidth
                  label={t("pages.Ad.form.description_de")}
                  value={values.description_de}
                  onChange={handleChange}
                  error={touched.description_de && !!errors.description_de}
                  helperText={touched.description_de && errors.description_de}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description_en"
                  fullWidth
                  label={t("pages.Ad.form.description_en")}
                  value={values.description_en}
                  onChange={handleChange}
                  error={touched.description_en && !!errors.description_en}
                  helperText={touched.description_en && errors.description_en}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="url"
                  fullWidth
                  label={t("pages.Ad.form.url")}
                  value={values.url}
                  onChange={handleChange}
                  error={touched.url && !!errors.url}
                  helperText={touched.url && errors.url}
                />
              </Grid>
              <Grid
                item
                xs={12}
                md={10}
                sx={{ display: "flex", marginBottom: 5 }}
              >
                <LoadingButton
                  isSubmitting={isAddLoading || isEditLoading}
                  buttonText={AdId ? t("common.edit") : t("common.add")}
                />
                <Button
                  sx={{ marginLeft: 1 }}
                  onClick={() => {
                    navigate("/Ad");
                  }}
                >
                  {t("common.cancel")}
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  );
};

export default AddEditAd;
