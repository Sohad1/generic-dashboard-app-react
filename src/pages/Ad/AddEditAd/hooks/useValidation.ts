import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'

function useValidationSchema() {
  const { t } = useTranslation()
  const validationSchema = Yup.object().shape({
    title: Yup.string().required(t('form.required')),
    title_de: Yup.string().required(t('form.required')),
    title_en: Yup.string().required(t('form.required')),
    description: Yup.string().required(t('form.required')),
    description_de: Yup.string().required(t('form.required')),
    description_en: Yup.string().required(t('form.required')),
    url: Yup.string().required(t('form.required')),
    image: Yup.mixed().required(t('form.required')),
  })

  return validationSchema
}

export default useValidationSchema
