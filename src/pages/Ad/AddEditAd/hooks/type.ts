import { TADS } from "@/api/Ad/type";

export type TInitialValues = TADS;
export type TParams = {
  AdId: string | undefined;
};
