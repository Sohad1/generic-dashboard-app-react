import { Box, Button, Stack, TableCell, Avatar } from "@mui/material";
import { useContext } from "react";
import AddIcon from "@mui/icons-material/Add";
import GenericTable from "@/components/items/table/genericTable/Table";
import { getComparator, stableSort } from "@/util/table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { TRow } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import { useTranslation } from "react-i18next";
import { AbilityContext, Can } from "@/libs/casl/can";
import useAdsLogic from "./hooks/useAdLogic";
import useTableHeader from "./hooks/useTableHeader";
import DeleteAdModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
const Ads = () => {
  const { t } = useTranslation();
  const ability = useContext(AbilityContext);
  const headCells = useTableHeader();
  const {
    data,
    error,
    page,
    rowsPerPage,

    searchAds,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchAds,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchAds,
    handleAddAd,
    handleEditAd,
    handleSelectForNavigateToPreviewPage,
    handleDeleteAd,
    handleOpenDeleteAdModal,
    handleCloseDeletAdModal,
  } = useAdsLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchAds}
          setValue={setSearchAds}
          debouncedFunction={handleSearchAds}
          label={t("common.search")}
          size="small"
        />

        <Can I="store" a="dashboard_api_ads">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddAd}
          >
            {t("pages.Ad.addAd")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.ads.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.ads.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.ads.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.title}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>
                  {
                    <Avatar
                      variant="rounded"
                      src={String(row.image_url)}
                      alt="competition-img"
                    />
                  }
                </TableCell>
                <TableCell>
                  <TableActions
                    edit={() => {
                      ability.can("update", "dashboard_api_ads") &&
                        handleEditAd(row);
                    }}
                    preview={() =>
                      ability.can("update", "dashboard_api_ads") &&
                      handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteAdModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteAdModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")} `}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteAd}
        handleClose={handleCloseDeletAdModal}
      />
    </Box>
  );
};

export default Ads;
