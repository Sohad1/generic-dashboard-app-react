import { SxProps, Theme } from "@mui/material/styles";
export const LOGIN_PAGE_CONTAINER: SxProps<Theme> = {
  backgroundImage: (theme: Theme) =>
    `linear-gradient(90deg, ${theme.palette.primary.main} 50%, white 50%)`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  backgroundSize: "cover",
  width: "100vw",
  height: "100vh",
};
export const PAPER_SX: SxProps<Theme> = {
  mx: "auto",
  my: 10,
  py: 5,
  backdropFilter: "blur(8px)",
  bgcolor: "rgba(255,255,255,.7)",
  boxShadow: (theme) =>
    `0 0 3px 0px ${theme.palette.primary.main}, -1px -1px 3px 0px white`,
};
