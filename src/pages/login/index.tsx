import { Grid, Paper, Typography, Box, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { LOGIN_PAGE_CONTAINER, PAPER_SX } from "./style";
import { useFormik } from "formik";
import { TInitialValues } from "./type";
import PasswordField from "@/components/items/inputField/PasswordField/PasswordField";
// import Checkbox from "@/components/items/inputField/Checkbox/Checkbox";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import { useAuthMutation } from "@/api/auth/useAuth";

const initialValues: TInitialValues = {
  email: "admin@demo.com",
  password: "12345678",
};
const LoginPage = () => {
  const { t } = useTranslation();

  const { mutate, data, isLoading } = useAuthMutation();

  const { handleSubmit, values, touched, errors, handleChange, isSubmitting } =
    useFormik<TInitialValues>({
      initialValues,
      enableReinitialize: true,
      onSubmit(values, formikHelpers) {
        mutate({ body: values });
        formikHelpers.setSubmitting(false);
      },
    });

  return (
    <Grid
      container
      justifyContent={"center"}
      alignItems={"center"}
      sx={LOGIN_PAGE_CONTAINER}
    >
      <Grid item xs={11} md={9} lg={6}>
        <Paper sx={PAPER_SX}>
          <Grid
            container
            justifyContent={"center"}
            alignItems="center"
            spacing={2}
          >
            <Grid item xs={0} lg={6}>
              <Box component={"h1"} sx={{ textAlign: "center" }}>
                Logo
              </Box>
            </Grid>
            <Grid item container xs={12} lg={6} spacing={2}>
              <Grid item xs={12}>
                <Typography
                  color="primary"
                  textAlign="center"
                  variant="h4"
                  component="h4"
                >
                  {t("pages.login.title")}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography
                  textAlign="center"
                  variant="body1"
                  sx={{ color: "#000000" }}
                  component="h4"
                >
                  {t("pages.login.subtitle")}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <form onSubmit={handleSubmit}>
                  <Grid
                    container
                    justifyContent={"center"}
                    alignItems={"center"}
                    spacing={2}
                  >
                    <Grid item xs={12} md={10}>
                      <TextField
                        name="email"
                        fullWidth
                        label={t("pages.login.form.email")}
                        value={values.email}
                        onChange={handleChange}
                        error={touched.email && !!errors.email}
                        helperText={touched.email && errors.email}
                      />
                    </Grid>
                    <Grid item xs={12} md={10}>
                      <PasswordField
                        name="password"
                        fullWidth
                        label={t("pages.login.form.password")}
                        value={values.password}
                        onChange={handleChange}
                        error={touched.password && !!errors.password}
                        helperText={touched.password && errors.password}
                      />
                    </Grid>
                    {/* <Grid item xs={12} md={10}>
                      <Checkbox
                        name="rememberMe"
                        label={t("pages.login.form.rememberMe")}
                      />
                    </Grid> */}
                    <Grid item xs={12} md={10}>
                      <LoadingButton
                        isSubmitting={isSubmitting || isLoading}
                        buttonText={t("form.submit")}
                      />
                    </Grid>
                  </Grid>
                </form>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default LoginPage;
