import { useGetCollectionsQuery } from "@/api/collections/useCollectionsQueries";

import { TOption } from "@/components/items/inputField/autoCompleteField/type";
const useCollectionsLogic = ({
  value,
}: {
  value: { id: string | number; name: string } | TOption;
}) => {
  const { data: collection, isLoading: isCollectionLoading } =
    useGetCollectionsQuery();

  return {
    collections: collection?.response?.collections?.data?.map((item) => ({
      id: item.id,
      name: item.name,
    })),
    isCollectionLoading,
  };
};

export default useCollectionsLogic;
