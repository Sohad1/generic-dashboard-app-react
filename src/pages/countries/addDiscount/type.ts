import { TCollections } from "@/api/collections/type";
import { TCountry } from "@/api/countries/type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";

// export type TInitialValues = TCountry;
export type TInitialValues = { offer_percent: number; collection: TOption };
