import Loading from '@/components/others/loading'
import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Button, Grid, Paper, Typography, TextField, Box } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { useState } from 'react'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import useCollectionsLogic from '../addDiscount/hooks'
import { TRegisterInitialValues } from './type'
import { useAddRegisterMutation } from '@/api/countries/useCountriesQueries'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'

const AddRegister = ({
  open,
  handleClose,
}: {
  open: boolean
  handleClose: () => void
}) => {
  const { t } = useTranslation()
  const [selectedCollection, setSelectedCollection] = useState<TOption>()

  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddRegisterMutation(handleClose)
  const { collections, isCollectionLoading } = useCollectionsLogic({
    value: selectedCollection,
  })
  console.log(collections)
  const initialValues: TRegisterInitialValues = {
    collection: null,
    register_prize_value: 0,
  }

  return (
    <GenericModal
      open={open}
      handleClose={handleClose}
      title="Add Register"
      childrenHaveActions={true}
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          console.log(values)
          addMutate({ body: values })
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid
              container
              justifyContent={'center'}
              alignItems={'center'}
              spacing={2}
            >
              <Grid item xs={12} md={10}>
                <AutoCompleteField
                  label={t('pages.collection.collections')}
                  name="collection"
                  options={collections || [{ id: 0, name: 'all collections' }]}
                  value={values.collection}
                  onChange={(_, value) => {
                    setFieldValue('collection', value)
                    setSelectedCollection(value)
                  }}
                  fullWidth
                  error={touched.collection && !!errors.collection}
                  errorMessage={touched.collection && errors.collection}
                  {...{
                    loading: isCollectionLoading,
                  }}
                />
              </Grid>

              <Grid item xs={12} md={10}>
                <TextField
                  name="register_prize_value"
                  type="number"
                  fullWidth
                  label="Register Prize Value%"
                  value={values.register_prize_value}
                  onChange={handleChange}
                  error={
                    touched.register_prize_value &&
                    !!errors.register_prize_value
                  }
                  helperText={
                    touched.register_prize_value && errors.register_prize_value
                  }
                />
              </Grid>

              <Grid
                item
                xs={12}
                md={10}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                  gap: '10px',
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading}
                  buttonText={t('common.add')}
                />
                <Button variant="outlined" onClick={handleClose}>
                  {t('common.cancel')}
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericModal>
  )
}

export default AddRegister
