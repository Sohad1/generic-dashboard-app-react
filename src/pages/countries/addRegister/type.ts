import { TOption } from "@/components/items/inputField/autoCompleteField/type";

export type TRegisterInitialValues = {
  register_prize_value: number;
  collection: TOption;
};
