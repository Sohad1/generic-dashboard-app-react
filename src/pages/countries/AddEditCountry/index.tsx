import GenericFormPage from '@/components/items/form/genericFormPage/GenericFormPage'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { TParams } from './type'
// import CountryForm from './CountryForm'

export default function AddEditCountry() {
  const { countryId } = useParams<TParams>()
  const { t } = useTranslation()
  return (
    <GenericFormPage
      title={
        countryId
          ? `${t(`common.edit`)} ${t(`pages.countries.country`)}`
          : `${t(`common.add`)} ${t(`pages.countries.country`)}`
      }
      goBackLink="/countries"
    >
      {/* <CountryForm /> */}
    </GenericFormPage>
  )
}
