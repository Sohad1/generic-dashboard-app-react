import LoadingButton from '@/components/others/loadingButton/LoadingButton'
import { Grid, TextField, Button } from '@mui/material'
import { Form, Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import AutoCompleteField from '@/components/items/inputField/autoCompleteField/AutoCompleteField'
import { useState } from 'react'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import useCollectionsLogic from '../addDiscount/hooks'
import { useAddPrizeMutation } from '@/api/countries/useCountriesQueries'
import { TPrizeInitialValues } from './type'
import GenericModal from '@/components/items/modal/GenericModal/GenericModal'
const AddPrize = ({
  open,
  handleClose,
}: {
  open: boolean
  handleClose: () => void
}) => {
  const { t } = useTranslation()
  const [selectedCollection, setSelectedCollection] = useState<TOption>()

  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddPrizeMutation(handleClose)
  const { collections, isCollectionLoading } = useCollectionsLogic({
    value: selectedCollection,
  })
  console.log(collections)
  const initialValues: TPrizeInitialValues = {
    collection: null,
    prize_value: 0,
  }

  return (
    <GenericModal
      open={open}
      handleClose={handleClose}
      title={t('pages.countries.addPrize')}
      childrenHaveActions={true}
    >
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          console.log(values)
          addMutate({ body: values })
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid
              container
              justifyContent={'center'}
              alignItems={'center'}
              spacing={2}
            >
              <Grid item xs={12} md={10}>
                <AutoCompleteField
                  label={t('pages.collection.collections')}
                  name="collection"
                  options={collections || [{ id: 0, name: 'all collections' }]}
                  value={values.collection}
                  onChange={(_, value) => {
                    setFieldValue('collection', value)
                    setSelectedCollection(value)
                  }}
                  fullWidth
                  error={touched.collection && !!errors.collection}
                  errorMessage={touched.collection && errors.collection}
                  {...{
                    loading: isCollectionLoading,
                  }}
                />
              </Grid>

              <Grid item xs={12} md={10}>
                <TextField
                  name="prize_value"
                  type="number"
                  fullWidth
                  label={t('pages.collection.form.prize_value')}
                  value={values.prize_value}
                  onChange={handleChange}
                  error={touched.prize_value && !!errors.prize_value}
                  helperText={touched.prize_value && errors.prize_value}
                />
              </Grid>

              <Grid
                item
                xs={12}
                md={10}
                sx={{
                  display: 'flex',
                  marginBottom: 5,
                  gap: '10px',
                }}
              >
                <LoadingButton
                  isSubmitting={isSubmitting || isAddLoading}
                  buttonText={t('common.add')}
                />
                <Button variant="outlined" onClick={handleClose}>
                  {t('common.cancel')}
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericModal>
  )
}

export default AddPrize
