import { TOption } from "@/components/items/inputField/autoCompleteField/type";

export type TPrizeInitialValues = { prize_value: number; collection: TOption };
