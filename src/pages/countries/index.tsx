import Loading from '@/components/others/loading'
import { Box } from '@mui/material'
import useCountriesLogic from '@/components/pages/countries/hooks/useCountriesLogic'
import CountriesTable from '@/components/pages/countries/countriesTable'
// import GenericModal from '@/components/items/modal/GenericModal/GenericModal'
// import PrizeForm from '@/components/pages/countries/PrizeForm'

function Countries() {
  const {
    data,
    isLoading,
    isError,
    // openModal,
    // handleCloseModal,
    // handleOpenModal,
    // handleAddCountry,
  } = useCountriesLogic()

  if (isLoading) return <Loading />
  if (isError) return <></>
  return (
    <Box>
      {/* <GenericModal
        title="text"
        open={openModal}
        handleClose={handleCloseModal}
        children={<PrizeForm type="gift" />}
        childrenHaveActions={true}
      /> */}
      {/* <Grid container spacing={2}>
        <Grid
          item
          lg={9}
          sx={{ display: 'flex', flexWrap: 'wrap', gap: '10px' }}
        >
          <Button variant="outlined" onClick={handleOpenModal}>
            Add General Offer
          </Button>
          <Button variant="outlined">Add prize in login</Button>
          <Button variant="outlined">Add prize in buy first Gift</Button>
        </Grid>
        <Grid item lg={3} textAlign="right">
          <Button variant="contained" onClick={handleAddCountry}>
            Add Country
          </Button>
        </Grid>
      </Grid> */}
      <CountriesTable data={data} total={data?.length!} />
    </Box>
  )
}

export default Countries
