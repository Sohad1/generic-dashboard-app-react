import {
  useBlockProfileMutation,
  useUnBlockProfileMutation,
} from "@/api/profile/useProfileQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  //   user: { id: number; name: string };
  user_id: number;
  user_name: string;
  status: number;
};
const BlockProfileModal = ({
  user_id,
  user_name,
  open,
  handleClose,
  status,
}: Props) => {
  const { t } = useTranslation();
  const { mutate: blockMutate, isLoading: isBlockLoading } =
    useBlockProfileMutation(handleClose, user_id);
  const { mutate: unblockMutate, isLoading: isUnBlockLoading } =
    useUnBlockProfileMutation(handleClose, user_id);
  return (
    <GenericModal
      {...{
        open,
        title: `${
          status === 0
            ? t(`pages.users.blockUser`)
            : t(`pages.users.unblockUser`)
        } ${user_name}`,
        handleClose,
        handleSubmit: () =>
          status === 0 ? blockMutate(user_id) : unblockMutate(user_id),
        isSubmittingLoading: status === 0 ? isBlockLoading : isUnBlockLoading,
        submitButtonText:
          status === 0 ? t("common.block") : t("common.unblock")!,
      }}
    />
  );
};

export default BlockProfileModal;
