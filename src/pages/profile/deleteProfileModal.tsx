import { useDeleteProfileMutation } from "@/api/profile/useProfileQueries";
import { useDeleteTaskMutation } from "@/api/tasks/useTasksQueries";
import GenericModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { useTranslation } from "react-i18next";
type Props = {
  open: boolean;
  handleClose: () => void;
  //   user: { id: number; name: string };
  user_id: number;
  user_name: string;
};
const DeleteProfileModal = ({
  user_id,
  user_name,
  open,
  handleClose,
}: Props) => {
  const { t } = useTranslation();
  const { mutate: deleteMutate, isLoading: isDeleteLoading } =
    useDeleteProfileMutation(handleClose);
  return (
    <GenericModal
      {...{
        open,
        title: `${t(`pages.users.deleteUser`)} ${user_name}`,
        handleClose,
        handleSubmit: () => deleteMutate(user_id),
        isSubmittingLoading: isDeleteLoading,
        submitButtonText: t("common.delete")!,
      }}
    />
  );
};

export default DeleteProfileModal;
