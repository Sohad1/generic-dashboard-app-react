export type TInitialValues = {
  user_id: number;
  points: number;
  type: number;
};

export type TParams = {
  profileId: string | undefined;
};
