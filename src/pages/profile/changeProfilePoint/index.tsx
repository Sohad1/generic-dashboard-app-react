import Loading from "@/components/others/loading";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import {
  Button,
  Grid,
  Paper,
  Typography,
  TextField,
  Box,
  Select,
  MenuItem,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Form, Formik } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";
import { TInitialValues, TParams } from "./type";
import { useChangeProfilePointMutation } from "@/api/profile/useProfileQueries";

const ChangeProfilePoint = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { profileId } = useParams<TParams>();

  const { mutate: addMutate, isLoading: isAddLoading } =
    useChangeProfilePointMutation(Number(profileId));

  const initialValues: TInitialValues = {
    user_id: Number(profileId),
    points: 0,
    type: 1,
  };

  return (
    <Grid
      container
      justifyContent={"center"}
      alignItems={"center"}
      sx={{ mt: 3 }}
    >
      <Grid item xs={11} md={9} lg={6}>
        <Paper>
          <Box position="relative" right={0} top={1} left={1} margin={0}>
            <Button
              sx={{}}
              onClick={() => {
                navigate(`/profile/${profileId}`);
              }}
            >
              <ArrowBackIcon fontSize="large" />
            </Button>
          </Box>
          <Grid
            container
            justifyContent={"center"}
            alignItems="center"
            spacing={2}
            marginTop={-4}
          >
            <Grid item container xs={10} lg={9} spacing={2}>
              <Grid item xs={12}>
                <Typography
                  color="primary"
                  textAlign="center"
                  variant="h4"
                  component="h4"
                >
                  {` ${t("pages.Profile.changePointToUser")} ${profileId}`}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Formik
                  initialValues={initialValues}
                  onSubmit={(values) => {
                    addMutate({ body: values });
                  }}
                >
                  {({
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                  }) => (
                    <Form>
                      <Grid
                        container
                        justifyContent={"center"}
                        alignItems={"center"}
                        spacing={2}
                      >
                        <Grid item xs={12} md={10}>
                          <TextField
                            name="points"
                            type="number"
                            fullWidth
                            label={t(
                              "pages.competitions.addEditCompetition.points"
                            )}
                            value={values.points}
                            onChange={handleChange}
                            error={touched.points && !!errors.points}
                            helperText={touched.points && errors.points}
                          />
                        </Grid>
                        <Grid item xs={12} md={10}>
                          {/* <TextField
                            name="type"
                            type="number"
                            fullWidth
                            label="type"
                            value={values.type}
                            onChange={handleChange}
                            error={touched.type && !!errors.type}
                            helperText={touched.type && errors.type}
                          /> */}
                          <Select
                            value={values.type}
                            name="type"
                            fullWidth
                            label="type"
                            onChange={handleChange}
                            error={touched.type && !!errors.type}
                          >
                            <MenuItem value={1}>{t("common.add")}</MenuItem>
                            <MenuItem value={2}>{t("common.remove")}</MenuItem>
                            <MenuItem value={3}>{t("common.edit")}</MenuItem>
                          </Select>
                        </Grid>

                        <Grid
                          item
                          xs={12}
                          md={10}
                          sx={{
                            display: "flex",
                            marginBottom: 5,
                          }}
                        >
                          <LoadingButton
                            isSubmitting={isSubmitting || isAddLoading}
                            buttonText={t("common.add")}
                          />
                        </Grid>
                      </Grid>
                    </Form>
                  )}
                </Formik>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default ChangeProfilePoint;
