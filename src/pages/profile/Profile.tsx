import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  List,
  ListItem,
  ListItemText,
  TableRow,
  Typography,
  Grid,
} from '@mui/material'
import { round } from 'lodash'
import PrizeTable from './tables/prizeTable'
import usePrizeLogic from './tables/prizeTable/hooks/usePrizeLogic'
import { useParams } from 'react-router-dom'
import { TParams } from './tables/type'
import { useGetProfileQuery } from '@/api/profile/useProfileQueries'
import Loading from '@/components/others/loading'
import ReferredTable from './tables/referredTable'
import WinHistoryTable from './tables/winHistoryTable'
import { useState } from 'react'
import moment from 'moment'
import DeleteProfileModal from './deleteProfileModal'
import BlockProfileModal from './blockProfileModal'
import { useTranslation } from 'react-i18next'

const Profile = () => {
  const [selectedTable, setSelectedTable] = useState('prizeTable')
  const { t } = useTranslation()
  const handleTableChange = (table: any) => {
    setSelectedTable(table)
  }

  const { profileId } = useParams<TParams>()
  const {
    data: profileData,
    isLoading: isGetTaskLoading,
    isFetching,
    isError: profileIsError,
  } = useGetProfileQuery(profileId)
  console.log(profileData)
  const {
    //data,
    //error,
    page,
    rowsPerPage,
    searchPrizes,
    // isError,
    //isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPrizes,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPrizes,
    handleChangePoint,
    handleDeleteProfile,
    openDeleteModal,
    handleCloseDeleteModal,
    handleBlockProfile,
    openBlockModal,
    handleCloseBlockModal,
  } = usePrizeLogic()
  if (isGetTaskLoading) return <Loading />

  return (
    <Box>
      <DeleteProfileModal
        {...{
          open: openDeleteModal,
          handleClose: handleCloseDeleteModal,
          user_id: Number(profileId),
          user_name: profileData.name,
        }}
      />
      <BlockProfileModal
        {...{
          open: openBlockModal,
          handleClose: handleCloseBlockModal,
          user_id: Number(profileId),
          user_name: profileData.name,
          status: profileData.change_information_block.status,
        }}
      />
      <Box
        display={'flex'}
        justifyContent={'space-between'}
        alignItems={'flex-start'}
        gap={5}
        padding={2}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}
      >
        <Box width={'45%'}>
          <List>
            <ListItem
              sx={{
                borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
              }}
            >
              <ListItemText
                primary={t('pages.Profile.name')}
                secondary={profileData.name}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.email')}
                secondary={profileData.email}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.referralID')}
                secondary={profileData.referral_id}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.country')}
                secondary={profileData.country?.country_enName}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.date')}
                secondary={moment(profileData.created_at).format('YYYY-MM-DD')}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.deviceID')}
                secondary={profileData.device_id}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.adDeviceID')}
                secondary={profileData.ads_device_id}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.phone')}
                secondary={profileData.phone}
              />
            </ListItem>
            <ListItem sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.1)' }}>
              <ListItemText
                primary={t('pages.Profile.invitedBy')}
                secondary={profileData.phone}
              />
            </ListItem>
          </List>
        </Box>
        <Container sx={{ margin: '0px', width: 370 }}>
          <Card sx={{ width: 330 }}>
            <Box justifyContent={'center'} display={'flex'}>
              <CardMedia
                sx={{
                  height: 125,
                  width: 125,
                  borderRadius: '100%',
                  boxShadow: '0px 10px 15px -3px rgba(0,0,0,0.1)',
                  margin: 2,
                }}
                image={profileData.img}
                title="green"
              />
            </Box>
            <Box justifyContent={'center'} display={'flex'}>
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {profileData.name}
                </Typography>
                <Typography
                  variant="body2"
                  color={'green'}
                  textAlign={'center'}
                >
                  {profileData.status === 1 ? 'active' : 'pending'}
                </Typography>
              </CardContent>
            </Box>
            <Box justifyContent={'center'} display={'flex'} marginBottom={3}>
              <CardActions>
                <Button
                  size="small"
                  variant="contained"
                  color="error"
                  onClick={() => handleBlockProfile()}
                >
                  {profileData.change_information_block.status === 1
                    ? t('pages.reports.form.unblocked')
                    : t('pages.reports.form.blocked')}
                </Button>
                <Button
                  size="small"
                  variant="contained"
                  color="error"
                  onClick={() => handleDeleteProfile()}
                >
                  {t('common.remove')}
                </Button>
                <Button size="small" variant="contained">
                  {t('pages.Profile.chat')}
                </Button>
                <Button
                  size="small"
                  variant="contained"
                  onClick={() => handleChangePoint(Number(profileId))}
                >
                  {t('pages.Profile.points')}
                </Button>
              </CardActions>
            </Box>
          </Card>
          <Box
            boxShadow={2}
            borderRadius={1}
            marginTop={2}
            width={330}
            display={'flex'}
            justifyContent={'space-around'}
          >
            <Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.PlayCount')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.user_points_history_count === null
                    ? '0'
                    : profileData.user_points_history_count}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.notification')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.gifts_count === null
                    ? '0'
                    : profileData.gifts_count}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {t('pages.Profile.taskPoint')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.tasks_sum_point === null
                    ? '0'
                    : profileData.tasks_sum_point}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
                width={145}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.notification')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.gifts_sum_point === null
                    ? '0'
                    : profileData.gifts_sum_point}
                </Typography>
              </Box>
            </Box>
            <Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
                width={145}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.referredCount')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.referred_count === null
                    ? '0'
                    : profileData.referred_count}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.playPoint')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.user_points_history_sum_points === null
                    ? '0'
                    : profileData.user_points_history_sum_points}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.referredPoint')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.user_referral_points_sum_points === null
                    ? '0'
                    : profileData.user_referral_points_sum_points}
                </Typography>
              </Box>
              <Box
                textAlign={'center'}
                margin={1.5}
                boxShadow={1}
                borderRadius={1}
              >
                <Typography variant="h6">
                  {' '}
                  {t('pages.Profile.currentPoint')}
                </Typography>
                <Typography color={'GrayText'}>
                  {profileData.points?.points}
                </Typography>
              </Box>
            </Box>
          </Box>
        </Container>
      </Box>
      <Box
        marginTop={3}
        boxShadow={2}
        borderRadius={1}
        paddingLeft={1}
        paddingRight={1}
      >
        <Grid container spacing={2}>
          <Grid item xs={12} marginLeft={1}>
            <Button
              onClick={() => handleTableChange('prizeTable')}
              variant={selectedTable === 'prizeTable' ? 'contained' : 'text'}
            >
              {t('pages.Profile.prizeTable')}
            </Button>
            <Button
              onClick={() => handleTableChange('referredTable')}
              variant={selectedTable === 'referredTable' ? 'contained' : 'text'}
            >
              {t('pages.Profile.referredTable')}
            </Button>
            <Button
              onClick={() => handleTableChange('winHistoryTable')}
              variant={
                selectedTable === 'winHistoryTable' ? 'contained' : 'text'
              }
            >
              {t('pages.Profile.winHistoryTable')}
            </Button>
          </Grid>
          <Grid item xs={12}>
            {selectedTable === 'prizeTable' && (
              <PrizeTable
                data={profileData?.orders}
                total={profileData?.orders.length!}
              />
            )}
            {selectedTable === 'referredTable' && (
              <ReferredTable
                data={profileData?.users_referred_info}
                total={profileData?.users_referred_info.length!}
              />
            )}
            {selectedTable === 'winHistoryTable' && (
              <WinHistoryTable
                data={profileData?.user_points_history}
                total={profileData?.user_points_history.length!}
              />
            )}
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}

export default Profile
