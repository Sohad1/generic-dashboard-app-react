import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.status"),
    },
    {
      id: "details",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.details"),
    },
    {
      id: "value",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.value"),
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.type"),
    },
    {
      id: "point",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.points"),
    },
    {
      id: "price",
      numeric: true,
      disablePadding: false,
      label: t("pages.Profile.price"),
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.name"),
    },
    {
      id: "nameC",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.nameC"),
    },
    {
      id: "created_at",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.date"),
    },
  ];
  return headCells;
};

export default useTableHeader;
