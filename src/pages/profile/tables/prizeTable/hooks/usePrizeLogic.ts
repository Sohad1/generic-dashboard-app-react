import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useNavigate } from "react-router-dom";
const usePrizeLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [searchPrizes, setSearchPrizes] = useState<string>("");
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openBlockModal, setOpenBlockModal] = useState(false);
  const [selectedPrize, setSelectedPrize] = useState({
    id: 0,
    title: "",
  });
  const navigate = useNavigate();

  // const [selectedProfile, setSelectedProfile] = useState({
  //   id: 0,
  //   name: "",
  // });
  // const { data, isLoading, isError, error, fetchNextPage } =
  //   useGetPrizesInfiniteQuery(rowsPerPage);
  const handleSearchPrizes = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    // if (newPage > page) fetchNextPage({ pageParam: newPage });
    // setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDeleteProfile = () => {
    setOpenDeleteModal(true);
  };
  const handleCloseDeleteModal = () => setOpenDeleteModal(false);
  const handleBlockProfile = () => {
    setOpenBlockModal(true);
  };
  const handleCloseBlockModal = () => setOpenBlockModal(false);
  const handleChangePoint = (id: number) => {
    navigate(`/profile/change-point/${id}`);
  };

  return {
    //data,
    //error,
    page,
    rowsPerPage,
    searchPrizes,
    //isError,
    // isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPrizes,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPrizes,
    handleDeleteProfile,
    openDeleteModal,
    handleCloseDeleteModal,
    handleBlockProfile,
    openBlockModal,
    handleCloseBlockModal,
    handleChangePoint,
  };
};

export default usePrizeLogic;
