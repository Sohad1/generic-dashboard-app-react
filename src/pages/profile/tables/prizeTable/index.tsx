import { Box, Stack, Typography } from "@mui/material";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableCell from "@mui/material/TableCell";

import { useTranslation } from "react-i18next";
import useTableHeader from "./hooks/useTableHeader";
import usePrizeLogic from "./hooks/usePrizeLogic";
import TableActions from "@/components/items/table/genericTable/tableActions";

import { TOrders } from "@/api/profile/type";
import moment from "moment";

type Props = {
  data: TOrders[];
  total: number;
};

const PrizeTable = ({ data, total }: Props) => {
  console.log(data);
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const {
    //error,
    page,
    rowsPerPage,
    searchPrizes,
    //isError,
    //isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPrizes,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPrizes,
  } = usePrizeLogic();
  //if (isLoading) return <Loading />;

  //if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <Typography variant="caption" marginTop={0} paddingLeft={1}>
          Prize Table
        </Typography>
      </Stack>
      <GenericTable
        page={page}
        count={total}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data.map((page) => page) as any[]}
        headCells={headCells}
      >
        {data
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.details}</TableCell>
                <TableCell>{row.price?.value}</TableCell>
                <TableCell>{row.price?.type}</TableCell>
                <TableCell>{row.price?.point}</TableCell>
                <TableCell>{row.price?.price}</TableCell>
                <TableCell>{row.price?.category?.name}</TableCell>
                <TableCell>
                  {row.orders?.price?.category?.company?.name}
                </TableCell>
                <TableCell>
                  {moment(row.created_at).format("YYYY-MM-DD")}
                </TableCell>
                <TableCell>
                  <TableActions />
                  {/* edit={() => handleEditCountry(row.id)} /> */}
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
};

export default PrizeTable;
