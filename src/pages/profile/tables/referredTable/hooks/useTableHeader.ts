import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "referred_points",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.referredPoints"),
    },
    {
      id: "status",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.status"),
    },
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.name"),
    },
    {
      id: "country",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.country"),
    },
    {
      id: "email",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.email"),
    },
    {
      id: "device_id",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.deviceID"),
    },
    {
      id: "referral_id",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.referralID"),
    },
    {
      id: "created_at",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.date"),
    },
    {
      id: "points",
      numeric: true,
      disablePadding: false,
      label: t("pages.Profile.points"),
    },
  ];
  return headCells;
};

export default useTableHeader;
