import { Box, Stack, Typography } from "@mui/material";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableCell from "@mui/material/TableCell";

import { useTranslation } from "react-i18next";
import useTableHeader from "./hooks/useTableHeader";

import TableActions from "@/components/items/table/genericTable/tableActions";

import { TUsers_referred_info } from "@/api/profile/type";
import moment from "moment";
import usePrizeLogic from "../prizeTable/hooks/usePrizeLogic";

type Props = {
  data: TUsers_referred_info[];
  total: number;
};

const ReferredTable = ({ data, total }: Props) => {
  console.log(data);
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const {
    //error,
    page,
    rowsPerPage,
    searchPrizes,
    //isError,
    //isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPrizes,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPrizes,
  } = usePrizeLogic();
  //if (isLoading) return <Loading />;

  //if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <Typography variant="caption" marginTop={0} paddingLeft={1}>
          Referred Table
        </Typography>
      </Stack>
      <GenericTable
        page={page}
        count={total}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data.map((page) => page) as any[]}
        headCells={headCells}
      >
        {data
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.referred_points}</TableCell>
                <TableCell>{row.status}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.country?.country_enName}</TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell>{row.device_id}</TableCell>
                <TableCell>{row.referral_id}</TableCell>

                <TableCell>
                  {moment(row.created_at).format("YYYY-MM-DD")}
                </TableCell>
                <TableCell>{row.points?.points}</TableCell>
                <TableCell>
                  <TableActions />
                  {/* edit={() => handleEditCountry(row.id)} /> */}
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
};

export default ReferredTable;
