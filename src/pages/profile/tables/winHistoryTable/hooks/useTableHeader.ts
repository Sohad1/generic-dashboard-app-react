import { HeadCell } from "@/type/Table";
import { useTranslation } from "react-i18next";

const useTableHeader = () => {
  const { t } = useTranslation();

  const headCells: HeadCell[] = [
    {
      id: "id",
      numeric: true,
      disablePadding: false,
      label: "Id",
    },
    {
      id: "points",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.points"),
    },
    {
      id: "from",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.status"),
    },
    {
      id: "created_at",
      numeric: false,
      disablePadding: false,
      label: t("pages.Profile.date"),
    },
  ];
  return headCells;
};

export default useTableHeader;
