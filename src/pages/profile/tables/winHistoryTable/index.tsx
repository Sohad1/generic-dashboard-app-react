import { Box, Stack, Typography } from "@mui/material";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import { getComparator, stableSort } from "@/util/table";
import TableCell from "@mui/material/TableCell";

import { useTranslation } from "react-i18next";
import useTableHeader from "./hooks/useTableHeader";

import Loading from "@/components/others/loading";
import { TOrder } from "@/type/Table";
import TableActions from "@/components/items/table/genericTable/tableActions";

import { TUser_points_history } from "@/api/profile/type";
import moment from "moment";
import usePrizeLogic from "../prizeTable/hooks/usePrizeLogic";

type Props = {
  data: TUser_points_history[];
  total: number;
};

const WinHistoryTable = ({ data, total }: Props) => {
  console.log(data);
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const {
    //error,
    page,
    rowsPerPage,
    searchPrizes,
    //isError,
    //isLoading,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchPrizes,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchPrizes,
  } = usePrizeLogic();
  //if (isLoading) return <Loading />;

  //if (isError) return <></>;
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <Typography variant="caption" marginTop={0} paddingLeft={1}>
          Win History Table
        </Typography>
      </Stack>
      <GenericTable
        page={page}
        count={total}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={data.map((page) => page) as any[]}
        headCells={headCells}
      >
        {data
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.points}</TableCell>
                <TableCell>{row.from}</TableCell>
                <TableCell>
                  {moment(row.created_at).format("YYYY-MM-DD")}
                </TableCell>
                <TableCell>
                  <TableActions />
                  {/* edit={() => handleEditCountry(row.id)} /> */}
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
    </Box>
  );
};

export default WinHistoryTable;
