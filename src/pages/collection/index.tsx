import { Box, Button, Stack, TableCell } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import GenericTable from '@/components/items/table/genericTable/Table'
import { getComparator, stableSort } from '@/util/table'
import { StyledTableRow } from '@/components/items/table/genericTable/style'
import { TRow } from '@/type/Table'
import TableActions from '@/components/items/table/genericTable/tableActions'
import Loading from '@/components/others/loading'
import { useTranslation } from 'react-i18next'
import useCollectionLogic from './hooks/useCollectionLogic'
import { useContext } from 'react'
import { AbilityContext, Can } from '@/libs/casl/can'
import useTableHeader from './hooks/useTableHeader'
import DebouncedTextField from '@/components/items/inputField/debouncedTextField/DebouncedTextField'
import { number } from 'yup'
import DeletePermissionModal from '@/components/items/modal/GenericDeleteModal/GenericDeleteModal'
import { AnyCnameRecord } from 'dns'
const Collections = () => {
  const { t } = useTranslation()
  const headCells = useTableHeader()
  const ability = useContext(AbilityContext)
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchCollection,
    isError,
    isLoading,
    openDeleteModal,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCollection,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCollection,
    handleCloseDeletCollectionModal,
    handleDeleteCollection,
    handleSelectForNavigateToPreviewPage,
    handleOpenDeleteCollectionModal,
    handleAddCollection,
    handleEditCollection,
  } = useCollectionLogic()

  if (isLoading) return <Loading />

  if (isError) return <></>
  return (
    <Box>
      <Stack direction={'row'} justifyContent={'space-between'} mb={3}>
        <DebouncedTextField
          value={searchCollection}
          setValue={setSearchCollection}
          debouncedFunction={handleSearchCollection}
          label={t('common.search')}
          size="small"
        />
        <Can I="store" a="dashboard_api_collections">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddCollection}
          >
            {t('pages.collection.addcollection')}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.collections.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.collections.data)
            .reduce((previous, current) => [...previous, ...current]) as any[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.collections.data)
            .reduce((previous, current) => [...previous, ...current]) as any[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row: any, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.description}</TableCell>

                <TableCell>
                  <TableActions
                    edit={() => {
                      ability.can('update', 'dashboard_api_collections') &&
                        handleEditCollection(row)
                    }}
                    preview={() =>
                      ability.can('update', 'dashboard_api_collections') &&
                      handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteCollectionModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            )
          })}
      </GenericTable>
      <DeletePermissionModal
        open={openDeleteModal}
        title={`${t('common.deleteTitle')} ${Collections.name}`}
        submitButtonText={t('common.delete')}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteCollection}
        handleClose={handleCloseDeletCollectionModal}
      />
    </Box>
  )
}

export default Collections
