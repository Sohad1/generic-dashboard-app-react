import { useTranslation } from "react-i18next";
import * as Yup from "yup";

function UseValidationSchema() {
  const { t } = useTranslation();

  const validationSchema = Yup.object({
    name: Yup.string().required(`${t("form.required")}`),
    description: Yup.string().required(`${t("form.required")}`),
    country_ids: Yup.array()
      .min(1, `${t("form.required")}`)
      .required(`${t("form.required")}`),
  });
  return validationSchema;
}

export default UseValidationSchema;
