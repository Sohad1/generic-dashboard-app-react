import { TCollections } from "@/api/collections/type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";

export type TInitialValues = {
  name: string;
  id: number;
  description: string;
  country_ids: number[];
  countries?: TOption[];
};
export type TParams = {
  collectionId: string | undefined;
};
