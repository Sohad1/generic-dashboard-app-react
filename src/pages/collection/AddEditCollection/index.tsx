import { useNavigate, useParams } from "react-router-dom";
import { Button, Grid, Box, Paper, Typography, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Form, Formik } from "formik";
import ArrowBackIcon from "@mui/material/Icon";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
import {
  useAddCollectionMutation,
  useGetCollectionQuery,
  useEditCollectionMutation,
} from "@/api/collections/useCollectionsQueries";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import { TParams, TInitialValues } from "./hooks/type";
import Loading from "@/components/others/loading";
import CountriesAutoComplete from "@/components/common/CountriesAutoComplete";
import UseValidationSchema from "./hooks/useValidationSchema";
import GenericFormPage from "@/components/items/form/genericFormPage/GenericFormPage";
import { values } from "lodash";
const AddEditCollection = () => {
  const { t, i18n } = useTranslation();
  const validationSchema = UseValidationSchema();

  const { collectionId } = useParams<TParams>();

  const {
    data,
    isLoading: isGetCollectionLoading,
    // isFetching,
    isError,
  } = useGetCollectionQuery(collectionId);
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddCollectionMutation();
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditCollectionMutation();
  if (!!collectionId) {
    if (isGetCollectionLoading) {
      return <Loading />;
    } else if (isError) {
      return <></>;
    }
  }

  const initialValues: TInitialValues = data
    ? {
        id: data.response.collection.id,
        name: data.response.collection.name,
        description: data.response.collection.description,
        country_ids: data.response.collection.countries?.map((item) => item.id),
        countries: data.response.collection.countries?.map((item) => ({
          id: item.id,
          name:
            i18n.language === "ar" ? item.country_arName : item.country_enName,
        })),
      }
    : {
        name: "",
        id: 0,
        description: "",
        country_ids: [],
        countries: [],
      };

  return (
    <GenericFormPage
      title={
        collectionId
          ? t("pages.collection.editcollection")
          : t("pages.collection.addcollection")
      }
      goBackLink="/collection"
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          collectionId
            ? editMutate({ body: values })
            : addMutate({ body: values });
        }}
      >
        {({
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          setFieldValue,
        }) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6} lg={4}>
                <CountriesAutoComplete
                  name="countries"
                  multiple={true}
                  value={values.countries}
                  error={touched.country_ids && !!errors.country_ids}
                  setFieldValue={(_, v) => {
                    setFieldValue("countries", v);
                    setFieldValue(
                      "country_ids",
                      v.map((item: any) => item.id)
                    );
                  }}
                  errorMessage={touched.country_ids && errors.country_ids}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="name"
                  fullWidth
                  label={t("pages.collection.form.name")}
                  value={values.name}
                  onChange={handleChange}
                  error={touched.name && !!errors.name}
                  helperText={touched.name && errors.name}
                />
              </Grid>

              <Grid item xs={12} md={6} lg={4}>
                <TextField
                  name="description"
                  fullWidth
                  label={t("pages.collection.form.description")}
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && !!errors.description}
                  helperText={touched.description && errors.description}
                />
              </Grid>

              <Grid
                item
                xs={12}
                md={10}
                sx={{ display: "flex", marginBottom: 5 }}
              >
                <LoadingButton
                  isSubmitting={isAddLoading || isEditLoading}
                  buttonText={collectionId ? t("common.edit") : t("common.add")}
                />
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </GenericFormPage>
  );
};

export default AddEditCollection;
