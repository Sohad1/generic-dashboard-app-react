import { useGetCollectionQuery } from "@/api/collections/useCollectionsQueries";
import { useParams, useNavigate } from "react-router-dom";
import { Button, Paper, Typography, Box } from "@mui/material";
import { useTranslation } from "react-i18next";
import Loading from "@/components/others/loading";
const PreviewCollection = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetCollectionQuery(id);
  console.log("id", id);
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t("pages.collection.Previewcollection")}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t("pages.collection.form.name")} : {data?.response.collection.name}
      </Typography>
      <Paper variant="outlined" />
      <Typography variant="h5" color="primary" sx={{ marginTop: 5 }}>
        {t("pages.collection.form.description")}:{" "}
        {data?.response.collection.description}
      </Typography>

      <Paper variant="outlined" sx={{ marginBottom: 5 }} />
      <Button
        sx={{ float: "right" }}
        variant="outlined"
        onClick={() => {
          navigate("/collection");
        }}
      >
        {t("pages.categories.cancel")}
      </Button>
    </Box>
  );
};

export default PreviewCollection;
