import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetCollectionsInfiniteQuery } from "@/api/collections/useCollectionsQueries";
import { useNavigate } from "react-router-dom";
import { useDeleteCollectionMutation } from "@/api/collections/useCollectionsQueries";
import { useQueryClient } from "@tanstack/react-query";
const useCollectionLogic = () => {
  const [page, setPage] = useState(0);
  const queryClient = useQueryClient();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchCollection, setSearchCollection] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [Collection, setCollection] = useState<TRow>({});
  const navigate = useNavigate();
  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteCollectionMutation();
  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetCollectionsInfiniteQuery({
      params: {
        limit: rowsPerPage,
        search_input: debouncedSearchValue,
        order_by: orderBy,
        type: order,
      },
    });
  console.log(data);
  const handleSearchCollection = (value: string) => {
    setDebouncedSearchValue(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/collection/previewCollection/${id}`);
  };
  const handleAddCollection = () => {
    navigate("/collection/AddCollection");
  };
  const handleEditCollection = (row: TRow) => {
    navigate(`/collection/EditCollection/${row.id}`);
  };

  const handleCloseDeletCollectionModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteCollectionModal = (Collections: TRow) => {
    setCollection(Collections);
    setOpenDeleteModal(true);
  };
  const handleDeleteCollection = () => {
    deleteMutate(
      { params: { id: Collection.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["collection-infinite-query"]);
            handleCloseDeletCollectionModal();
          }
        },
      }
    );
  };
  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCollection,
    isError,
    isLoading,
    openDeleteModal,
    isDeleting,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    setSearchCollection,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCollection,
    handleCloseDeletCollectionModal,
    handleDeleteCollection,
    handleSelectForNavigateToPreviewPage,
    handleOpenDeleteCollectionModal,
    handleAddCollection,
    handleEditCollection,
  };
};
export default useCollectionLogic;
