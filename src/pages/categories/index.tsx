import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Box, Button, Stack, TableCell } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getComparator, stableSort } from "@/util/table";
import { TRow } from "@/type/Table";
import GenericTable from "@/components/items/table/genericTable/Table";
import { StyledTableRow } from "@/components/items/table/genericTable/style";
import TableActions from "@/components/items/table/genericTable/tableActions";
import DebouncedTextField from "@/components/items/inputField/debouncedTextField/DebouncedTextField";
import Loading from "@/components/others/loading";
import useTableHeader from "./hooks/useTableHeader";
import useCategoriesLogic from "./hooks/useCategoriesLogic";
import DeleteCategoriesModal from "@/components/items/modal/GenericDeleteModal/GenericDeleteModal";
import { AbilityContext, Can } from "@/libs/casl/can";
const Categories = () => {
  const { t } = useTranslation();
  const headCells = useTableHeader();
  const ability = useContext(AbilityContext);
  const {
    data,
    error,
    page,
    rowsPerPage,
    searchCategories,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleOpenDeleteCategoryModal,
    handleCloseDeletCategoryModal,
    handleDeleteCategory,
    setSearchCategories,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCategories,
    handleAddCategory,
    handleEditCategory,

    handleSelectForNavigateToPreviewPage,
  } = useCategoriesLogic();

  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Stack direction={"row"} justifyContent={"space-between"}>
        <DebouncedTextField
          value={searchCategories}
          setValue={setSearchCategories}
          debouncedFunction={handleSearchCategories}
          label={t("common.search")}
          size="small"
        />
        <Can I="store" a="category">
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddCategory}
          >
            {t("pages.categories.addCategory")}
          </Button>
        </Can>
      </Stack>
      <GenericTable
        page={page}
        count={data?.pages[0].response.categories.total!}
        rowsPerPage={rowsPerPage}
        order={order}
        orderBy={orderBy}
        setOrder={setOrder}
        setOrderBy={setOrderBy}
        handleChangePage={handleChangePage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        rows={
          data?.pages
            .map((page) => page.response.categories.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[]
        }
        headCells={headCells}
      >
        {stableSort(
          data?.pages
            .map((page) => page.response.categories.data)
            .reduce((previous, current) => [...previous, ...current]) as TRow[],
          getComparator(order, orderBy)
        )
          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((row, index) => {
            return (
              <StyledTableRow key={index} hover tabIndex={-1}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>
                  <TableActions
                    edit={() => {
                      ability.can("update", "category") &&
                        handleEditCategory(row);
                    }}
                    preview={() =>
                      ability.can("view", "category") &&
                      handleSelectForNavigateToPreviewPage(row.id)
                    }
                    delete={() => handleOpenDeleteCategoryModal(row)}
                  />
                </TableCell>
              </StyledTableRow>
            );
          })}
      </GenericTable>
      <DeleteCategoriesModal
        open={openDeleteModal}
        title={`${t("common.deleteTitle")} ${Categories.name}`}
        submitButtonText={t("common.delete")}
        isSubmittingLoading={isDeleting}
        handleSubmit={handleDeleteCategory}
        handleClose={handleCloseDeletCategoryModal}
      />
    </Box>
  );
};

export default Categories;
