import { ChangeEvent, useState } from "react";
import { TOrder, TRow } from "@/type/Table";
import { useGetCategoriesInfiniteQuery } from "@/api/categories/useCategoriesQueries";
import { useDeleteCategoryMutation } from "@/api/categories/useCategoriesQueries";
import { useNavigate } from "react-router-dom";
import { useQueryClient } from "@tanstack/react-query";
const useCategoriesLogic = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchCategories, setSearchCategories] = useState<string>("");
  const [order, setOrder] = useState<TOrder>("asc");
  const [orderBy, setOrderBy] = useState<string>("");
  const [debouncedSearchValue, setDebouncedSearchValue] = useState<string>("");
  const [Categories, setCategories] = useState<TRow>({});
  const queryClient = useQueryClient();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const navigate = useNavigate();

  const { data, isLoading, isError, error, fetchNextPage, refetch } =
    useGetCategoriesInfiniteQuery(rowsPerPage);
  const { mutate: deleteMutate, isLoading: isDeleting } =
    useDeleteCategoryMutation();
  const handleSearchCategories = (value: string) => {
    console.log(value);
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    if (newPage > page) fetchNextPage({ pageParam: newPage + 1 });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleAddCategory = () => {
    navigate("/categories/addCategory");
  };
  const handleEditCategory = (row: TRow) => {
    navigate(`/categories/editCategory/${row.id}`);
  };

  const handleCloseDeletCategoryModal = () => {
    setOpenDeleteModal(false);
  };

  const handleOpenDeleteCategoryModal = (Categories: TRow) => {
    setCategories(Categories);
    setOpenDeleteModal(true);
  };
  const handleDeleteCategory = () => {
    deleteMutate(
      { params: { id: Categories.id as string } },
      {
        onSuccess(data, variables, context) {
          if (data.code === 200) {
            queryClient.invalidateQueries(["Category-infinite-query"]);
            handleCloseDeletCategoryModal();
          }
        },
      }
    );
  };

  const handleSelectForNavigateToPreviewPage = (id: number | string) => {
    navigate(`/categories/PreviewCategories/${id}`);
  };

  return {
    data,
    error,
    page,
    rowsPerPage,
    searchCategories,
    isError,
    isLoading,
    isDeleting,
    openDeleteModal,
    order,
    orderBy,
    setOrder,
    setOrderBy,
    handleOpenDeleteCategoryModal,
    handleCloseDeletCategoryModal,
    handleDeleteCategory,
    setSearchCategories,
    handleChangePage,
    handleChangeRowsPerPage,
    handleSearchCategories,
    handleAddCategory,
    handleEditCategory,

    handleSelectForNavigateToPreviewPage,
  };
};

export default useCategoriesLogic;
