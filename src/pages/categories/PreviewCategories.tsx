import { useGetCategoryQuery } from "@/api/categories/useCategoriesQueries";
import { useParams, useNavigate } from "react-router-dom";
import { Button, Grid, Paper, Typography, Box } from "@mui/material";
import { useTranslation } from "react-i18next";
import Loading from "@/components/others/loading";
const PreviewCategories = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { t } = useTranslation();
  const { data, isLoading, isError } = useGetCategoryQuery(id);
  console.log("id", id);
  if (isLoading) return <Loading />;

  if (isError) return <></>;

  return (
    <Box>
      <Typography
        color="primary"
        textAlign="center"
        variant="h4"
        component="h4"
        sx={{ marginBottom: 10 }}
      >
        {t("pages.categories.PreviewCategory")}
      </Typography>

      <Typography marginTop="20" variant="h5" color="primary">
        {t("common.Name")} : {data?.response.category.name}
      </Typography>
      <Paper variant="outlined" />
      <Typography variant="h5" color="primary" sx={{ marginTop: 5 }}>
        {t("common.description")}:{data?.response.category.description}
      </Typography>
      <Paper variant="outlined" sx={{ marginBottom: 5 }} />
      <Button
        sx={{ float: "right" }}
        variant="outlined"
        onClick={() => {
          navigate("/categories");
        }}
      >
        {t("pages.categories.cancel")}
      </Button>
    </Box>
  );
};

export default PreviewCategories;
