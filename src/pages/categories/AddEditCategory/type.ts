import { TCategory } from "@/api/categories/type";

export type TInitialValues = TCategory;
export type TParams = {
  categoryId: string | undefined;
};
