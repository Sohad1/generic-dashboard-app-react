import { useNavigate, useParams } from "react-router-dom";

import { Button, Grid, Box, Paper, Typography, TextField } from "@mui/material";

import { useTranslation } from "react-i18next";
import { Form, Formik } from "formik";
import {
  useAddCategoryMutation,
  useEditCategoryMutation,
  useGetCategoryQuery,
} from "@/api/categories/useCategoriesQueries";
import { TInitialValues, TParams } from "./type";
import Loading from "@/components/others/loading";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import LoadingButton from "@/components/others/loadingButton/LoadingButton";
const AddEditCategory = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { categoryId } = useParams<TParams>();

  const {
    data,
    isLoading: isGetCategoryLoading,
    isFetching,
    isError,
  } = useGetCategoryQuery(categoryId);
  const { mutate: addMutate, isLoading: isAddLoading } =
    useAddCategoryMutation();
  const { mutate: editMutate, isLoading: isEditLoading } =
    useEditCategoryMutation();
  if (!!categoryId) {
    if (isGetCategoryLoading || isFetching) {
      return <Loading />;
    } else if (isError) {
      return <></>;
    }
  }
  const initialValues: TInitialValues = data?.response.category
    ? { ...data.response.category }
    : {
        name: "",
        description: "",
      };
  return (
    <Grid
      container
      justifyContent={"center"}
      alignItems={"center"}
      sx={{ mt: 3 }}
    >
      <Grid item xs={11} md={9} lg={6}>
        <Paper>
          <Box position="relative" right={0} top={1} left={1} margin={0}>
            <Button
              sx={{}}
              onClick={() => {
                navigate("/categories");
              }}
            >
              <ArrowBackIcon fontSize="large" />
            </Button>
          </Box>
          <Grid
            container
            justifyContent={"center"}
            alignItems="center"
            spacing={2}
          >
            <Grid item container xs={10} lg={9} spacing={2}>
              <Grid item xs={12}>
                <Typography
                  color="primary"
                  textAlign="center"
                  variant="h4"
                  component="h4"
                >
                  {categoryId
                    ? t("pages.categories.EditCategory")
                    : t("pages.categories.addCategory")}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Formik
                  initialValues={initialValues}
                  onSubmit={(values) => {
                    categoryId
                      ? editMutate({ body: values })
                      : addMutate({ body: values });
                  }}
                >
                  {({
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                  }) => (
                    <Form>
                      <Grid
                        container
                        justifyContent={"center"}
                        alignItems={"center"}
                        spacing={2}
                      >
                        <Grid item xs={12} md={10}>
                          <TextField
                            name="name"
                            fullWidth
                            label="Category"
                            value={values.name}
                            onChange={handleChange}
                            error={touched.name && !!errors.name}
                            helperText={touched.name && errors.name}
                          />
                        </Grid>
                        <Grid item xs={12} md={10}>
                          <TextField
                            name="description"
                            fullWidth
                            label="Description"
                            value={values.description}
                            onChange={handleChange}
                            error={touched.description && !!errors.description}
                            helperText={
                              touched.description && errors.description
                            }
                          />
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          md={10}
                          sx={{ display: "flex", marginBottom: 5 }}
                        >
                          <LoadingButton
                            isSubmitting={
                              isSubmitting || isAddLoading || isEditLoading
                            }
                            buttonText={
                              categoryId ? t("common.edit") : t("common.add")
                            }
                          />
                          <Button
                            sx={{ marginLeft: 1 }}
                            onClick={() => {
                              navigate("/categories");
                            }}
                          >
                            {t("common.cancel")}
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  )}
                </Formik>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default AddEditCategory;
