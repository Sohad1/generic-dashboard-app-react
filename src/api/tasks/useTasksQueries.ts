import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { addTask, deleteTask, editTask, getTask, getTasks } from ".";
import { TPayload } from "./type";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
const useGetTasksInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "tasks-infinite-query",

      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getTasks({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};

const useGetTaskQuery = (taskId: string | undefined) => {
  return useQuery({
    queryKey: ["get-task-query"],
    queryFn: () => getTask(taskId!),
    cacheTime: 0,
    enabled: !!taskId,
  });
};
const useAddTaskMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["add-task-mutation"],
    mutationFn: (payload: TPayload) => addTask(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/tasks");
      }
    },
  });
};
const useEditTaskMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["edit-competition-mutation"],
    mutationFn: (payload: TPayload) => editTask(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/tasks");
      }
    },
  });
};
const useDeleteTaskMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["delete-task-mutation"],
    mutationFn: (id: number) => deleteTask(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};
export {
  useGetTasksInfiniteQuery,
  useGetTaskQuery,
  useAddTaskMutation,
  useEditTaskMutation,
  useDeleteTaskMutation,
};
