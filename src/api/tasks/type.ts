import { TParams } from '@/pages/tasks/addEditTask/hooks/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'
import { TCountry } from '../countries/type'
import { TOption } from '@/components/items/inputField/autoCompleteField/type'

export type TTasksResponse = {
  tasks: TGenericPaginationResponse<TTask>
}
export type TTaskResponse = {
  task: TTask
}
export type TTask = {
  id?: number
  title: string
  descr: string
  url: string
  type: string
  status: number
  points: number
  img: string
  img_url?: string
  limit_clicks: number | null
  users_count: number | null
  // country_ids: number;
  country: TOption[]
  to: string | null
}
export type TPayload = TGenericRequestPayload<TTask, TParams>
