import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TPayload, TTaskResponse, TTasksResponse } from "./type";
const getTasks = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TTasksResponse>>(
    API_ROUTES.TASKS.GET_TASKS,
    { params: payload.params }
  );
  return data;
};
const getTask = async (taskId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TTaskResponse>>(
    API_ROUTES.TASKS.GET_TASK,
    {
      params: { id: taskId },
    }
  );
  return data;
};
const addTask = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.TASKS.ADD_TASK}`,
    payload.body
  );
  console.log(data);
  return data;
};
const editTask = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.TASKS.EDIT_TASK}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const deleteTask = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.TASKS.DELETE_TASK, {
    params: { id },
  });
  return data;
};
export { getTasks, getTask, addTask, editTask, deleteTask };
