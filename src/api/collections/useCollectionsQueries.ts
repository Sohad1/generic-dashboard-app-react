import {
  getCollections,
  getCollection,
  addCollection,
  editCollection,
  deleteCollection,
} from ".";
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { toast } from "react-toastify";
const useGetCollectionsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "collections-infinite-query",

      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getCollections({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.permissions.current_page !==
      //     pages[0].response.permissions.last_page
      //   );
      return true;
    },
  });
};
const useGetCollectionsQuery = () => {
  return useQuery({
    queryKey: [`get-collection`],
    queryFn: async () => {
      return getCollections({});
    },
  });
};

const useGetCollectionQuery = (collectionId: string | undefined) => {
  return useQuery({
    queryKey: ["get-collection-query"],
    queryFn: () => getCollection(collectionId!),
    cacheTime: 0,
    enabled: !!collectionId,
  });
};
const useEditCollectionMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editcollection-mutation"],
    mutationFn: (payload: TPayload) => editCollection(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/collection");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useAddCollectionMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addcollection-mutation"],
    mutationFn: (payload: TPayload) => addCollection(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/collection");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useDeleteCollectionMutation = () => {
  const query = useQueryClient();
  return useMutation({
    mutationKey: [`delete-Collection-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteCollection(payload),
    onSuccess(data, variables, context) {
      query.refetchQueries(["collections-infinite-query"]);
      toast.success("Operation successfully");
    },
  });
};
export {
  useGetCollectionsInfiniteQuery,
  useGetCollectionQuery,
  useAddCollectionMutation,
  useEditCollectionMutation,
  useDeleteCollectionMutation,
  useGetCollectionsQuery,
};
