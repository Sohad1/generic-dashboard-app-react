import { number, string } from "yup";
import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";
import {
  TInitialValues,
  TParams,
} from "@/pages/collection/AddEditCollection/hooks/type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import { TCountry } from "../countries/type";

export type TCollectionsResponse = {
  collections: TGenericPaginationResponse<TCollections>;
};
export type TAllCollectionsResponse = {
  collections: Array<TCollections>;
};
export type TCollectionResponse = {
  collection: TCollections;
};
export type TCollections = {
  id?: number;
  name: string;
  status: number;
  description: string;
  countries: TCountry[];
};
export type TPayload = TGenericRequestPayload<TInitialValues, TParams>;
