import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TIdParam,
  TGenericRequestPayload,
} from "../type";
import { TCollectionsResponse, TCollectionResponse, TPayload } from "./type";
const getCollections = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TCollectionsResponse>
  >(API_ROUTES.COLLECTIONS.GET, { params: payload.params });
  return data;
};

const getCollection = async (collectionId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TCollectionResponse>>(
    API_ROUTES.COLLECTIONS.GET_COLLECTION,
    {
      params: { id: collectionId },
    }
  );
  return data;
};
const deleteCollection = async (
  payload: TGenericRequestPayload<null, TIdParam>
) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.COLLECTIONS.DELETE,
    { params: payload.params }
  );
  return data;
};
const editCollection = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COLLECTIONS.UPDATE}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const addCollection = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COLLECTIONS.ADD}`,
    payload.body
  );
  console.log(data);
  return data;
};
export {
  getCollections,
  getCollection,
  deleteCollection,
  editCollection,
  addCollection,
};
