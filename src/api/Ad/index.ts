import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TADSResponse, TADResponse, TPayload } from "./type";
import { createFormData } from "@/util/helpers";

const getAds = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TADSResponse>>(
    API_ROUTES.AD.GET,
    { params: payload.params }
  );
  return data;
};
const getAd = async (AdId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TADResponse>>(
    API_ROUTES.AD.GET_AD,
    {
      params: { id: AdId },
    }
  );
  return data;
};
const deleteAd = async (payload: TGenericRequestPayload<null, TIdParam>) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.AD.DELETE,
    { params: payload.params }
  );
  return data;
};
const editAd = async (payload: TPayload) => {
  const formData = createFormData(payload.body);
  const { data } = await apiInstance.post(`${API_ROUTES.AD.UPDATE}`, formData, {
    params: payload.params,
  });
  console.log(data);
  return data;
};
const addAd = async (payload: TPayload) => {
  const formData = createFormData(payload.body);
  const { data } = await apiInstance.post(`${API_ROUTES.AD.ADD}`, formData);
  console.log(data);
  return data;
};
export { getAds, getAd, deleteAd, addAd, editAd };
