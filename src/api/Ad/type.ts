import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";
import { TParams } from "@/pages/Ad/AddEditAd/hooks/type";

export type TADSResponse = {
  ads: TGenericPaginationResponse<TADS>;
};
export type TADResponse = {
  ad: TADS;
};
export type TADS = {
  id?: number;

  title: string;
  description: string;
  image_url?: string;
  image: File | string;
  url: string;
  title_en: string;
  title_de: string;
  description_en: string;
  description_de: string;
};
export type TPayload = TGenericRequestPayload<TADS, TParams>;
