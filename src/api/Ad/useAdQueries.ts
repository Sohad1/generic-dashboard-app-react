import { getAds, getAd, addAd, editAd, deleteAd } from ".";
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { toast } from "react-toastify";
const useGetAdsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "ads-infinite-query",

      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getAds({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useGetAdQuery = (AdId: string | undefined) => {
  return useQuery({
    queryKey: ["get-Ad-query"],
    queryFn: () => getAd(AdId!),
    cacheTime: 0,
    enabled: !!AdId,
  });
};
const useEditAdMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editAd-mutation"],
    mutationFn: (payload: TPayload) => editAd(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/Ad");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useAddAdMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addAd-mutation"],
    mutationFn: (payload: TPayload) => addAd(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/ad ");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useDeleteAdMutation = () => {
  const query = useQueryClient();
  return useMutation({
    mutationKey: [`delete-Ad-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteAd(payload),
    onSuccess(data, variables, context) {
      query.refetchQueries(["ads-infinite-query"]);
      toast.success("Operation successfully");
    },
  });
};
export {
  useGetAdsInfiniteQuery,
  useGetAdQuery,
  useAddAdMutation,
  useEditAdMutation,
  useDeleteAdMutation,
};
