import { TParams } from '@/pages/categories/AddEditCategory/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'
import { TProductCategory } from '../products/prices/type'
export type TCategoriesResponse = {
  categories: TGenericPaginationResponse<TCategory>
}
export type TCategoryResponse = {
  category: TCategory
}
export type TCategory = {
  id?: number
  description?: string
  name: string
  prices?: any
}
export type TPayload = TGenericRequestPayload<TCategory, TParams>
