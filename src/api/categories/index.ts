import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TPayload, TCategoriesResponse, TCategoryResponse } from "./type";

const getCategories = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TCategoriesResponse>>(
    API_ROUTES.CATEGORIES.GET_CATEGORIES,
    { params }
  );
  return data;
};
const getCategory = async (categoryId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TCategoryResponse>>(
    API_ROUTES.CATEGORIES.GET_CATEGORY,
    {
      params: { id: categoryId },
    }
  );
  return data;
};
const addCategory = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.CATEGORIES.ADD_CATEGORY}`,
    payload.body
  );
  console.log(data);
  return data;
};

const editCategory = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.CATEGORIES.UPDATE_CATEGORY}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};

const deleteCategory = async (
  payload: TGenericRequestPayload<null, TIdParam>
) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.CATEGORIES.DELETE_GATEGORIES,
    { params: payload.params }
  );
  return data;
};

export {
  getCategories,
  getCategory,
  addCategory,
  editCategory,
  deleteCategory,
};
