import { useInfiniteQuery, useQuery, useMutation } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import {
  addCategory,
  editCategory,
  getCategories,
  getCategory,
  deleteCategory,
} from ".";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";

import { TPayload } from "./type";

const useGetCategoriesInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: ["categories-infinite-query"],
    queryFn: ({ pageParam = 1 }) => {
      return getCategories({
        pagination: 1,
        limit,
        page: pageParam,
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};

const useGetCategoryQuery = (categoryId: string | undefined) => {
  return useQuery({
    queryKey: [`get-category-${categoryId}-query`],
    queryFn: () => getCategory(categoryId!),
    cacheTime: 0,
    enabled: !!categoryId,
  });
};
const useAddCategoryMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addCategory-mutation"],
    mutationFn: (payload: TPayload) => addCategory(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/categories");
      }
    },
  });
};
const useDeleteCategoryMutation = () => {
  return useMutation({
    mutationKey: [`delete-Category-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteCategory(payload),
  });
};

const useEditCategoryMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editCategory-mutation"],
    mutationFn: (payload: TPayload) => editCategory(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/categories");
      }
    },
  });
};

export {
  useGetCategoriesInfiniteQuery,
  useGetCategoryQuery,
  useAddCategoryMutation,
  useEditCategoryMutation,
  useDeleteCategoryMutation,
};
