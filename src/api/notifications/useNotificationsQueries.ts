import { useInfiniteQuery, useQuery, useMutation } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { getNotifications, addNotification } from ".";
import { toast } from "react-toastify";
const useGetNotificationsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "Notifications-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getNotifications({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.permissions.current_page !==
      //     pages[0].response.permissions.last_page
      //   );
      return true;
    },
  });
};
const useAddNotificationsMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addNotifications-mutation"],
    mutationFn: (payload: TPayload) => addNotification(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/Notifications");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
export { useAddNotificationsMutation, useGetNotificationsInfiniteQuery };
