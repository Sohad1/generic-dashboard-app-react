import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import { TParams } from "@/pages/Message_template/AddEditMe_Te/hooks/type";
export type TNotificationsResponse = {
  notifications: TGenericPaginationResponse<TNotifications>;
};
export type TNotificationResponse = {
  notification: TNotifications;
};
export type TNotifications = {
  id?: number;
  title?: String | number;
  body: string;
  image?: any;
  countries: any;
  all_countries: number;
  url?: string;
  created_at?: string;
  country_id: number;
  collection?: any;
  image_url?: string;
  lang: string;
  country_ids: TOption;
  users_count: number;
};
export type TPayload = TGenericRequestPayload<TNotifications, TParams>;
