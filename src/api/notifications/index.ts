import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { createFormData } from "@/util/helpers";
import { TNotificationsResponse, TPayload } from "./type";
const getNotifications = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TNotificationsResponse>
  >(API_ROUTES.NOTIFICATIONS.GET, { params: payload.params });
  return data;
};
const addNotification = async (payload: TPayload) => {
  const formData = createFormData(payload.body);
  const { data } = await apiInstance.post(
    `${API_ROUTES.NOTIFICATIONS.ADD}`,
    formData
  );
  console.log(data);
  return data;
};
export { getNotifications, addNotification };
