import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";

export type TBlockedUsersResponse = {
  app_users: TGenericPaginationResponse<TBlockedUser>;
};
export type TBlockedUserResponse = {
  blockedUser: TBlockedUser;
};
export type TBlockedUser = {
  id?: number;
  name: string;
  email: string;
  device_id: string;
  referral_id: string;
  created_at: string;
  points: TPoints;
  referred_count: number;
};
export type TPoints = {
  points: number;
};
// export type TPayload = TGenericRequestPayload<TBlockedUser, TParams>;
