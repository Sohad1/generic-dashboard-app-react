import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";

import { TBlockedUsersResponse } from "./type";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
const getBlockedUsers = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TBlockedUsersResponse>
  >(API_ROUTES.BLOCKED_USERS.GET_BLOCKED_USERS, { params: payload.params });
  return data.response.app_users;
};
export { getBlockedUsers };
