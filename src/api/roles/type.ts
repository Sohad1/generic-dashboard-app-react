import { TPermissionByRole } from "../permissions/type";
import { TGenericPaginationResponse } from "../type";

export type TResponse = {
  roles: TGenericPaginationResponse<TRole>;
};
export type TRoleResponse = {
  role: TRole;
};
export type TRoleWithPermissionResponse = TRole & {
  permissions: TPermissionByRole;
};
export type TRole = {
  id?: string;
  name: string;
  guard_name: string;
};
export type TRequestRole = TRole & { permissions: number[] };
