import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { addRole, deleteRole, getRole, getRoles, updateRole } from ".";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { useNavigate } from "react-router-dom";
import { TRequestRole } from "./type";

const useGetRolesInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "roles-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getRoles({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.roles.current_page !==
      //     pages[0].response.roles.last_page
      //   );
      return true;
    },
  });
};
const useGetRolesQuery = () => {
  return useQuery({
    queryKey: ["get-roles-query"],
    queryFn: () =>
      getRoles({
        params: {
          pagination: 0,
        },
      }),
    cacheTime: 0,
  });
};
const useGetRoleQuery = (payload: TGenericRequestPayload<null, TIdParam>) => {
  const { params } = payload;
  return useQuery({
    queryKey: [`get-role-${params?.id}-query`],
    queryFn: () => getRole(payload),
    cacheTime: 0,
    enabled: !!params?.id,
  });
};
const useAddRoleMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["add-role-mutation"],
    mutationFn: (payload: TGenericRequestPayload<TRequestRole, null>) =>
      addRole(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/roles");
      }
    },
  });
};

const useUpdateRoleMutation = (
  payload: TGenericRequestPayload<null, TIdParam>
) => {
  const navigate = useNavigate();
  const { params } = payload;
  return useMutation({
    mutationKey: [`update-role-${params.id}-mutation`],
    mutationFn: (payload: TGenericRequestPayload<TRequestRole, null>) =>
      updateRole(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/roles");
      }
    },
  });
};
const useDeleteRoleMutation = () => {
  return useMutation({
    mutationKey: [`delete-role-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteRole(payload),
  });
};
export {
  useGetRolesInfiniteQuery,
  useGetRolesQuery,
  useGetRoleQuery,
  useAddRoleMutation,
  useUpdateRoleMutation,
  useDeleteRoleMutation,
};
