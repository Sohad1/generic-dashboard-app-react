import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericResponse,
  TGenericPaginationParams,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TResponse, TRole, TRequestRole, TRoleResponse } from "./type";

const getRoles = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TResponse>>(
    API_ROUTES.ROLES.GET_ROLES,
    { params: payload.params }
  );
  return data;
};
const getRole = async (payload: TGenericRequestPayload<null, TIdParam>) => {
  const { data } = await apiInstance.get<TGenericResponse<TRoleResponse>>(
    API_ROUTES.ROLES.GET_ROLE,
    { params: payload.params }
  );
  return data;
};
const addRole = async (payload: TGenericRequestPayload<TRequestRole, null>) => {
  const { data } = await apiInstance.post<TGenericResponse<TRole>>(
    API_ROUTES.ROLES.ADD_ROLE,
    payload.body
  );
  return data;
};
const updateRole = async (
  payload: TGenericRequestPayload<TRequestRole, null>
) => {
  const { data } = await apiInstance.post<TGenericResponse<TRole>>(
    API_ROUTES.ROLES.UPDATE_ROLE,
    payload.body
  );
  return data;
};
const deleteRole = async (payload: TGenericRequestPayload<null, TIdParam>) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.ROLES.DELETE_ROLE,
    { params: payload.params }
  );
  return data;
};
export { getRoles, getRole, addRole, updateRole, deleteRole };
