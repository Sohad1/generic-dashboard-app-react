import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import {
  activeCompetition,
  addCompetition,
  archiveCompetition,
  deleteCompetition,
  editCompetition,
  getCompetition,
  getCompetitions,
  unactiveCompetition,
  unarchiveCompetition,
} from ".";
import { TPayload } from "./type";
import { toast } from "react-toastify";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";

const useGetCompetitionsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "competitions-infinite-query",

      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getCompetitions({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useGetCompetitionQuery = (competitionId: string | undefined) => {
  return useQuery({
    queryKey: ["get-competition-query"],
    queryFn: () => getCompetition(competitionId!),
    cacheTime: 0,
    enabled: !!competitionId,
  });
};
const useAddCompetitionMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["add-competition-mutation"],
    mutationFn: (payload: TPayload) => addCompetition(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/competitions");
      }
      toast.success("Added successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useEditCompetitionMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["edit-competition-mutation"],
    mutationFn: (payload: TPayload) => editCompetition(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/competitions");
      }
      toast.success("Edited successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useDeleteCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["delete-competition-mutation"],
    mutationFn: (id: number) => deleteCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
      toast.success("Deleted successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useArchiveCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["archive-competition-mutation"],
    mutationFn: (id: number) => archiveCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
      toast.success("Archived successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useUnArchiveCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["unarchive-competition-mutation"],
    mutationFn: (id: number) => unarchiveCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
      toast.success("UnArchived successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useActiveCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["active-competition-mutation"],
    mutationFn: (id: number) => activeCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
      toast.success("Activated successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useUnActiveCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["unactive-competition-mutation"],
    mutationFn: (id: number) => unactiveCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
      toast.success("deactivated succeffuly");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};

export {
  useGetCompetitionsInfiniteQuery,
  useGetCompetitionQuery,
  useAddCompetitionMutation,
  useEditCompetitionMutation,
  useDeleteCompetitionMutation,
  useArchiveCompetitionMutation,
  useUnArchiveCompetitionMutation,
  useActiveCompetitionMutation,
  useUnActiveCompetitionMutation,
};
