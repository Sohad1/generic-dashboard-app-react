import { TParams } from '@/pages/competitions/addEditCompetition/hooks/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'

export type TCompetitionsResponse = {
  competitions: TGenericPaginationResponse<TCompetition>
}
export type TCompetitionResponse = {
  competition: TCompetition
}

export type TCompetition = {
  id?: number
  title: string
  description: string
  points: number
  image: File
  image_url?: string
  status?: number
  url?: string
  hint?: string
  created_at?: string
  date?: string
  min_referred_count?: number
  prices: TPrices[]
}
export type TPrices = {
  count: number
  price: number
}
export type TPayload = TGenericRequestPayload<TCompetition, TParams>
