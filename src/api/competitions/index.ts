import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TCompetitionResponse, TCompetitionsResponse, TPayload } from "./type";

const getCompetitions = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TCompetitionsResponse>
  >(API_ROUTES.COMPETITIONS.GET_COMPETITIONS, { params: payload.params });
  return data;
};
const getCompetition = async (competitionId: string) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TCompetitionResponse>
  >(API_ROUTES.COMPETITIONS.GET_COMPETITION, {
    params: { id: competitionId },
  });
  return data;
};
const addCompetition = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COMPETITIONS.ADD_COMPETITION}`,
    payload.body
  );
  console.log(data);
  return data;
};
const editCompetition = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COMPETITIONS.UPDATE_COMPETITION}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const deleteCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.COMPETITIONS.DELETE_COMPETITIONS,
    {
      params: { id },
    }
  );
  return data;
};
const archiveCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.COMPETITIONS.ARCHIVE_COMPETITION,
    {
      params: { id },
    }
  );
  return data;
};
const unarchiveCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.COMPETITIONS.UNARCHIVE_COMPETITION,
    {
      params: { id },
    }
  );
  return data;
};
const activeCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.COMPETITIONS.ACTIVE_COMPETITION,
    {
      params: { id },
    }
  );
  return data;
};
const unactiveCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.COMPETITIONS.UNACTIVE_COMPETITION,
    {
      params: { id },
    }
  );
  return data;
};
export {
  getCompetitions,
  getCompetition,
  addCompetition,
  editCompetition,
  deleteCompetition,
  archiveCompetition,
  unarchiveCompetition,
  activeCompetition,
  unactiveCompetition,
};
