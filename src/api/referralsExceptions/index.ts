import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TExceptionsResponse } from "./type";

const getReferralsExceptions = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TExceptionsResponse>>(
    API_ROUTES.REFERRALS_EXCEPTIONS.GET_REFERRALS_EXCEPTIONS,
    { params: payload.params }
  );
  return data;
};
const removeReferralsException = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.REFERRALS_EXCEPTIONS.REMOVE_REFERRALS_EXCEPTION,
    {
      params: { id },
    }
  );
  return data;
};
export { getReferralsExceptions, removeReferralsException };
