import { useInfiniteQuery, useMutation } from "@tanstack/react-query";
import { getReferralsExceptions, removeReferralsException } from ".";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
const useGetExceptionsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "referrals-exceptions-infinite-query",

      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getReferralsExceptions({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useRemoveExceptionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["remove-referrals-exception-mutation"],
    mutationFn: (id: number) => removeReferralsException(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};
export { useGetExceptionsInfiniteQuery, useRemoveExceptionMutation };
