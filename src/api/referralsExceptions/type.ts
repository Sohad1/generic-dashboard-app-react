import { TGenericPaginationResponse } from "../type";

export type TExceptionsResponse = {
  users: TGenericPaginationResponse<TExceptions>;
};
export type TExceptions = {
  id?: number;
  name: string;
  email: string;
  device_id: string;
  referral_id: string;
  is_advertised: number;
  created_at: string;
  points: TPoint;
  user_points_history_count: number;
  referred_count: number;
  status: string;
  top10_referral_exception: Ttop10_referral_exception;
};

type TPoint = {
  answer_count: number;
  const_points: string;
  correct_answer_count: number;
  created_at: string;
  id: number;
  inter_ad_count: number;
  points: number;
  reward_ad_count: number;
  reward_ad_points: number;
  updated_at: string;
  user_id: number;
};
type Ttop10_referral_exception = {
  id: number;
  user_id: number;
  status: number;
  created_at: string | null;
  updated_at: string | null;
};
