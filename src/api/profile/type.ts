import { TGenericPaginationResponse } from "../type";

export type TUserResponse = {
  user: TUser;
};
export type TUser = {
  id?: number;
  isAdmin: number;
  img: string | null;
  name: string;
  email: string;
  email_verified_at: null;
  created_at: string;
  updated_at: string | null;
  fcm_token: null;
  is_confirmed: number;
  activation_code: string;
  activation_code_exp_date: string;
  device_id: string | null;
  referral_id: string | null;
  country_id: string | null | number;
  country: TCountry | null;
  status: number;
  deleted: number;
  apple: number;
  lang: string;
  phone: string | null;
  age: string | null;
  gender: string | number | null;
  ads_device_id: null;
  support_chat: number;
  files_support: number;
  is_advertised: number;
  special_offer_register: number;
  user_points_history_count: number;
  referred_count: number;
  gifts_count: number;
  tasks_sum_point: null | number;
  user_points_history_sum_points: number | null;
  gifts_sum_point: null | number;
  user_referral_points_sum_points: null;

  points: null | TPoints;
  users_referred_info: TUsers_referred_info[];
  orders: TOrders[];
  user_points_history: TUser_points_history[];
  change_information_block: TChange_information_block;
};
export type TUser_points_history = {
  id?: number;
  points: string;
  from: string;
  created_at: string;
};
export type TUsers_referred_info = {
  id?: number;
  referred_points: number;
  status: number;
  name: string;
  country: TCountry;
  email: string;
  device_id: string;
  referral_id: string;
  created_at: string;
  points: TPoints;
};
export type TPoints = {
  points: number;
};
export type TCountry = {
  country_enName: string;
};
export type TOrders = {
  id?: number;
  details: string;
  status: number;
  created_at: string | null;
  purchase_price: number;
  price: TPrice;
};
export type TPrice = {
  point: number;
  value: string;
  type: number;
  category: TCategory;
};
export type TCategory = { name: string; company: TCompany };
export type TCompany = { name: string };
export type TChange_information_block = { status: number };
