import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import { TGenericResponse } from "../type";
import { TUserResponse } from "./type";

const getProfile = async (profileId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TUserResponse>>(
    API_ROUTES.PROFILE.GET_PROFILE,
    {
      params: { id: profileId },
    }
  );
  return data.response.user;
};
const deleteProfile = async (profileId: number) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PROFILE.DELETE_PROFILE}`,
    { id: profileId }
  );
  console.log(data);
  return data;
};
const blockProfile = async (profileId: number) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PROFILE.BLOCK_PROFILE}`,
    { id: profileId }
  );
  console.log(data);
  return data;
};
const unblockProfile = async (profileId: number) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PROFILE.UNBLOCK_PROFILE}`,
    { id: profileId }
  );
  console.log(data);
  return data;
};
const changeProfilePoint = async (payload: {
  user_id: number;
  points: number;
  type: number;
}) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PROFILE.CHANGE_POINT}`,
    payload
  );
  console.log(data);
  return data;
};
export {
  getProfile,
  deleteProfile,
  blockProfile,
  unblockProfile,
  changeProfilePoint,
};
