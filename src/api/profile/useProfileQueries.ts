import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import {
  blockProfile,
  changeProfilePoint,
  deleteProfile,
  getProfile,
  unblockProfile,
} from '.'
import { useNavigate } from 'react-router-dom'

const useGetProfileQuery = (profileId: string | undefined) => {
  return useQuery({
    queryKey: [`get-profile-query-${profileId}`],
    queryFn: () => getProfile(profileId!),

    cacheTime: 0,
    enabled: !!profileId,
  })
}
const useDeleteProfileMutation = (handleCloseModal: () => void) => {
  const navigate = useNavigate()

  return useMutation({
    mutationKey: ['delete-task-mutation'],
    mutationFn: (id: number) => deleteProfile(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      navigate('/app_users')
    },
  })
}
const useBlockProfileMutation = (
  handleCloseModal: () => void,
  profile_id: number
) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['block-task-mutation'],
    mutationFn: (id: number) => blockProfile(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      // navigate(`/profile/${profile_id}`)
      query.refetchQueries([`get-profile-query-${profile_id}`])
    },
  })
}
const useUnBlockProfileMutation = (
  handleCloseModal: () => void,
  profile_id: number
) => {
  const query = useQueryClient()

  return useMutation({
    mutationKey: ['unblock-profile-mutation'],
    mutationFn: (id: number) => unblockProfile(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      // navigate(`/profile/${profile_id}`)
      query.refetchQueries([`get-profile-query-${profile_id}`])
    },
  })
}
const useUnBlockUserMutation = (
  handleCloseModal: () => void,
  profile_id: number
) => {
  const query = useQueryClient()

  return useMutation({
    mutationKey: ['unblock-user-mutation'],
    mutationFn: (id: number) => unblockProfile(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      // navigate('/blockedUsers')
      query.refetchQueries([`blockedUsers-infinite-query`])
    },
  })
}
const useChangeProfilePointMutation = (profile_id: number) => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['SendGiftToUser-mutation'],
    mutationFn: (payload: {
      body: { user_id: number; points: number; type: number }
    }) => changeProfilePoint(payload.body),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/profile/${profile_id}`)
      }
    },
  })
}

export {
  useGetProfileQuery,
  useDeleteProfileMutation,
  useBlockProfileMutation,
  useUnBlockProfileMutation,
  useUnBlockUserMutation,
  useChangeProfilePointMutation,
}
