import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TAD_WALLSResponse, TAD_WALLResponse, TPayload } from "./type";

const getAd_walls = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TAD_WALLSResponse>>(
    API_ROUTES.AD_WALLS.GET,
    { params: payload.params }
  );
  return data;
};
const getAd_wall = async (Ad_wallId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TAD_WALLResponse>>(
    API_ROUTES.AD_WALLS.GET_AD_WALL,
    {
      params: { id: Ad_wallId },
    }
  );
  return data;
};
const deleteAd_Wall = async (
  payload: TGenericRequestPayload<null, TIdParam>
) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.AD_WALLS.DELETE,
    { params: payload.params }
  );
  return data;
};
const editAd_wall = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.AD_WALLS.UPDATE}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const addAd_wall = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.AD_WALLS.ADD}`,
    payload.body
  );
  console.log(data);
  return data;
};
export { getAd_walls, getAd_wall, deleteAd_Wall, addAd_wall, editAd_wall };
