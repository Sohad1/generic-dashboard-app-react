import {
  getAd_walls,
  getAd_wall,
  addAd_wall,
  editAd_wall,
  deleteAd_Wall,
} from ".";
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { toast } from "react-toastify";

const useGetAd_wallsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "adwalls-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getAd_walls({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.permissions.current_page !==
      //     pages[0].response.permissions.last_page
      //   );
      return true;
    },
  });
};
const useGetAd_wallQuery = (Ad_wallId: string | undefined) => {
  return useQuery({
    queryKey: ["get-Adwall-query"],
    queryFn: () => getAd_wall(Ad_wallId!),
    cacheTime: 0,
    enabled: !!Ad_wallId,
  });
};
const useEditAd_wallMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editAd_wall-mutation"],
    mutationFn: (payload: TPayload) => editAd_wall(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/Ad walls");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useAddAd_wallMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addAd_wall-mutation"],
    mutationFn: (payload: TPayload) => addAd_wall(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/ad walls");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useDeleteAd_wallMutation = () => {
  const query = useQueryClient();
  return useMutation({
    //
    mutationKey: [`delete-AdWall-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteAd_Wall(payload),
    onSuccess(data, variables, context) {
      query.refetchQueries(["adwalls-infinite-query"]);
      toast.success("Operation successfully");
    },
  });
};
export {
  useGetAd_wallsInfiniteQuery,
  useGetAd_wallQuery,
  useAddAd_wallMutation,
  useEditAd_wallMutation,
  useDeleteAd_wallMutation,
};
