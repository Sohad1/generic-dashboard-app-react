import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";
import { TParams } from "@/pages/ad walls/AddEditAdwalls/hooks/type";
import { TOption } from "@/components/items/inputField/autoCompleteField/type";
export type TAD_WALLSResponse = {
  ad_walls: TGenericPaginationResponse<TAD_WALLS>;
};
export type TAD_WALLResponse = {
  ad_wall: TAD_WALLS;
};
export type TAD_WALLS = {
  id?: number;
  description: string;
  name: string;
  collection_id: TOption;
  status: string;
  collection?: any;
};
export type TPayload = TGenericRequestPayload<TAD_WALLS, TParams>;
