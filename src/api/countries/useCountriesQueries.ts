import { useMutation, useQuery } from '@tanstack/react-query'
import {
  addDiscount,
  addPrize,
  addRegister,
  getCountries,
  getCountriesByCollectionId,
} from '.'
import { useNavigate } from 'react-router-dom'
import { TPayload, TPayloadPrize, TPayloadRegister } from './type'

const useGetCountriesQuery = () => {
  return useQuery({
    queryKey: [`get-countries`],
    queryFn: async () => {
      return getCountries()
    },
  })
}
const useGetCountriesByCollectionIdQuery = (
  collectionId: number | undefined
) => {
  return useQuery({
    queryKey: ['get-Countries-query', collectionId],
    queryFn: () => getCountriesByCollectionId(collectionId!),
    cacheTime: 0,
  })
}
const useAddDiscountMutation = (handleClose: () => void) => {
  // const navigate = useNavigate();
  return useMutation({
    mutationKey: ['add-discount-mutation'],
    mutationFn: (payload: TPayload) => addDiscount(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        handleClose()

        // navigate("/countries");
      }
    },
  })
}
const useAddPrizeMutation = (handleClose: () => void) => {
  // const navigate = useNavigate();
  return useMutation({
    mutationKey: ['add-prize-mutation'],
    mutationFn: (payload: TPayloadPrize) => addPrize(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        handleClose()
        // navigate("/countries");
      }
    },
  })
}
const useAddRegisterMutation = (handleClose: () => void) => {
  // const navigate = useNavigate();
  return useMutation({
    mutationKey: ['add-register-mutation'],
    mutationFn: (payload: TPayloadRegister) => addRegister(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        handleClose()
        // navigate("/countries");
      }
    },
  })
}
export {
  useGetCountriesQuery,
  useGetCountriesByCollectionIdQuery,
  useAddDiscountMutation,
  useAddPrizeMutation,
  useAddRegisterMutation,
}
