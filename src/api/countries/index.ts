import API_ROUTES from '@/constants/apiRoutes'
import apiInstance from '../apiInstance'
import {
  TCountriesByCollectionsResponse,
  TCountriesResponse,
  TPayload,
  TPayloadPrize,
  TPayloadRegister,
} from './type'
import { TGenericResponse } from '../type'

const getCountries = async () => {
  const { data } = await apiInstance.get<TGenericResponse<TCountriesResponse>>(
    API_ROUTES.COUNTRIES.GET
  )
  return data.response.countries
}

const getAllCountries = async () => {
  const { data } = await apiInstance.get<TGenericResponse<TCountriesResponse>>(
    API_ROUTES.COUNTRIES.GET
  )
  return data
}
const getCountriesByCollectionId = async (collectionId: number) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TCountriesByCollectionsResponse>
  >(API_ROUTES.COUNTRIES.GET_COUNTRIES, {
    params: { collection_id: collectionId },
  })
  return data.response.countries
}
const addDiscount = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COUNTRIES.ADD_DISCOUNT}`,
    payload.body
  )
  console.log(data)
  return data
}
const addPrize = async (payload: TPayloadPrize) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COUNTRIES.ADD_PRIZE}`,
    payload.body
  )
  console.log(data)
  return data
}
const addRegister = async (payload: TPayloadRegister) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.COUNTRIES.ADD_REGISTER}`,
    payload.body
  )
  console.log(data)
  return data
}
export {
  getCountries,
  getAllCountries,
  getCountriesByCollectionId,
  addDiscount,
  addPrize,
  addRegister,
}
