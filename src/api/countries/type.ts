import { TOption } from '@/components/items/inputField/autoCompleteField/type'
import { TCollections } from '../collections/type'
import { TGenericRequestPayload } from '../type'
import { TInitialValues } from '@/pages/countries/addDiscount/type'
import { TPrizeInitialValues } from '@/pages/countries/addPrize/type'
import { TRegisterInitialValues } from '@/pages/countries/addRegister/type'

export type TCountry = {
  apple: number
  country_arName: string
  country_arNationality: string
  country_code: string
  country_deName: string
  country_enName: string
  country_enNationality: string
  created_at: string
  id: number
  iso_code: string
  offer: number
  offer_percent: number
  prize: number
  prize_value: number
  register_prize: number
  register_prize_value: number
  support_chat: number
  updated_at: string
}

export type TCountriesResponse = {
  countries: Array<TCountry>
}
export type TCountriesByCollectionsResponse = {
  countries: { data: Array<TCountry> }
}
export type TCountryResponse = {
  country: TCountry
}
export type TPayload = TGenericRequestPayload<TInitialValues, null>
export type TPayloadPrize = TGenericRequestPayload<TPrizeInitialValues, null>
export type TPayloadRegister = TGenericRequestPayload<
  TRegisterInitialValues,
  null
>
