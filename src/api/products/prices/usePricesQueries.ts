import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import { useNavigate } from 'react-router-dom'
import {
  addProductCategory,
  deletePrice,
  editProductCategory,
  getProductCategories,
  getProductsCategories,
} from '.'

const useGetPricesInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: ['prices-infinite-query'],
    queryFn: ({ pageParam = 1 }) => {
      return getProductsCategories({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}
const useGetAllPricesQuery = () => {
  return useQuery({
    queryKey: ['getAll-prices-query'],
    queryFn: () =>
      getProductsCategories({
        pagination: 0,
      }),
  })
}
const useGetPriceQuery = (priceId: string | undefined) => {
  return useQuery({
    queryKey: [`get-price-${priceId}-query`],
    queryFn: () => getProductCategories(priceId!),
    cacheTime: 0,
    enabled: !!priceId,
  })
}
const useAddPriceMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['addPrice-mutation'],
    mutationFn: (payload: any) => addProductCategory(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate('/categories')
      }
    },
  })
}

const useEditPriceMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['editPrice-mutation'],
    mutationFn: (payload: any) => editProductCategory(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate('/categories')
      }
    },
  })
}
const useDeletePriceMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deletePrice-mutation'],
    mutationFn: (id: string) => {
      return deletePrice(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`prices-infinite-query`])
      query.refetchQueries([`getAll-prices-query`])
      handleCloseModal()
    },
  })
}

export {
  useGetPricesInfiniteQuery,
  useGetPriceQuery,
  useAddPriceMutation,
  useEditPriceMutation,
  useGetAllPricesQuery,
  useDeletePriceMutation,
}
