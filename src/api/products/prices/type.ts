import { TGenericPaginationResponse, TGenericRequestPayload } from '@/api/type'
import { TParams } from '@/pages/categories/AddEditCategory/type'
import { TCategory } from '../products/type'
export type TProductsCategoriesResponse = {
  prices: TGenericPaginationResponse<TProductCategory>
}
export type TCategoryResponse = {
  category: TProductCategory
}

export type TProductCategoryResponse = {
  price: TProductCategory
}
export type TProductCategory = {
  id?: string
  category_id: number
  category?: TCategory
  description: string
  value: string
  point: number
  type: string
  has_verification: string
  offer: number
  offer_point?: number
  link: string
  color?: string
  image: any
  logo: any
  cards_count?: number
  image_url?: string
  logo_url?: string
}
export type TPayload = TGenericRequestPayload<TProductCategory, TParams>
