import API_ROUTES from '@/constants/apiRoutes'
// import { TPayload } from './type'
import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import {
  TPayload,
  TProductCategory,
  TProductCategoryResponse,
  TProductsCategoriesResponse,
} from './type'

const getProductsCategories = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TProductsCategoriesResponse>
  >(API_ROUTES.PRODUCTS.CATEGORIES.GET, { params })
  return data
}
const getProductCategories = async (categoryId: string) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TProductCategoryResponse>
  >(API_ROUTES.PRODUCTS.CATEGORIES.GET_BY_ID, {
    params: { id: categoryId },
  })
  return data.response.price
}
const addProductCategory = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.CATEGORIES.ADD}`,
    payload.body
  )
  return data
}

const editProductCategory = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.CATEGORIES.UPDATE}`,
    payload.body,
    {
      params: payload.params,
    }
  )
  return data
}

const deletePrice = async (priceId: string) => {
  const { data } = await apiInstance.delete(
    API_ROUTES.PRODUCTS.CATEGORIES.DELETE,
    {
      params: { id: priceId },
    }
  )
  return data
}

export {
  getProductsCategories,
  getProductCategories,
  addProductCategory,
  editProductCategory,
  deletePrice,
}
