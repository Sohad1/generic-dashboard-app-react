import { TGenericPaginationResponse, TGenericRequestPayload } from '@/api/type'
import { TParams } from '@/pages/categories/AddEditCategory/type'

export type TGroupsResponse = {
  groups: TGenericPaginationResponse<TGroup>
}
export type TGroupResponse = {
  group: TGroup
}
export type TGroup = {
  id?: string | number
  name_ar: string
  name_en: string
  name_de: string
}
export type TPayload = TGenericRequestPayload<TGroup, TParams>
