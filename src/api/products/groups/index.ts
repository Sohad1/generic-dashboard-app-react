import API_ROUTES from '@/constants/apiRoutes'
// import { TPayload } from './type'
import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import { TPayload, TGroupsResponse, TGroupResponse } from './type'

const getGroups = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TGroupsResponse>>(
    API_ROUTES.PRODUCTS.GROUPS.GET,
    { params }
  )
  return data
}
const getGroup = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TGroupResponse>>(
    API_ROUTES.PRODUCTS.GROUPS.GET_BY_ID,
    {
      params: { id },
    }
  )
  return data.response.group
}
const addGroup = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.GROUPS.ADD}`,
    payload.body
  )
  return data
}

const editGroup = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.GROUPS.UPDATE}`,
    payload.body,
    {
      params: payload.params,
    }
  )
  return data
}

const deleteGroup = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.PRODUCTS.GROUPS.DELETE, {
    params: { id },
  })
  return data
}

export { getGroups, getGroup, addGroup, editGroup, deleteGroup }
