import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import { addGroup, deleteGroup, editGroup, getGroup, getGroups } from '.'
import { TPayload } from './type'
import { useNavigate } from 'react-router-dom'

const useGetGroupsInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: ['groups-infinite-query'],
    queryFn: ({ pageParam = 1 }) => {
      return getGroups({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetGroupsQuery = () => {
  return useQuery({
    queryKey: ['getAll-groups-query'],
    queryFn: () => {
      return getGroups({
        pagination: 0,
      })
    },
  })
}

const useGetGroupInfoQuery = (id: string | undefined) => {
  return useQuery({
    queryKey: [`group-${id}-info`],
    queryFn: async () => {
      return getGroup(id!)
    },
    cacheTime: 0,
    enabled: !!id,
    onSuccess(data) {
      // console.log(data)
    },
  })
}

const useAddGroupMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['addGroup-mutation'],
    mutationFn: (payload: TPayload) => addGroup(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/groups`)
      }
    },
    onError: (error) => {
      console.log(error)
    },
  })
}

const useEditGroupMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['editGroup-mutation'],
    mutationFn: (payload: TPayload) => {
      return editGroup(payload)
    },
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/groups`)
      }
    },
  })
}

const useDeleteGroupMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deleteGroup-mutation'],
    mutationFn: (id: string) => {
      return deleteGroup(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`groups-infinite-query`])
      query.refetchQueries([`getAll-groups-query`])
      handleCloseModal()
    },
  })
}

export {
  useGetGroupsInfiniteQuery,
  useGetGroupsQuery,
  useGetGroupInfoQuery,
  useAddGroupMutation,
  useEditGroupMutation,
  useDeleteGroupMutation,
}
