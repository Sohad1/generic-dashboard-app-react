import { TParams } from '@/pages/products/products/AddEditProduct/hooks/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../../type'
import { TCompany } from '../companies/type'
import { TCountry } from '@/api/countries/type'
export type TCategoriesResponse = {
  categories: TGenericPaginationResponse<TCategory>
}
export type TCategoryResponse = {
  category: TCategory
}
export type TCategory = {
  id?: string
  description?: string
  name: string
  prices?: any
  company?: TCompany
  countries?: any[]
}
export type TPayload = TGenericRequestPayload<TCategory, TParams>
