import {
  TGenericPaginationParams,
  TGenericPaginationResponse,
  TGenericRequestPayload,
} from '@/api/type'
import { TProductCategory } from '../prices/type'
import { TParams } from '@/pages/products/cards/ActionsCard/type'

export type TCardsResponse = {
  prices: TProductCategory[]
  cards: TGenericPaginationResponse<TCard>
}
export type TCardResponse = {
  card: TCard
}
export type TCard = {
  id?: string
  name: string
  details: string
  price_id: string
  created_at: string
  updated_at: string
  user_id: null
  competition: number
  is_multiple: 0 | 1
  card_details?: { key: string; title: string; value: string }[]
  price?: TProductCategory
}
export type TPayload = TGenericRequestPayload<TCard, TParams>

export type TCardsParams = TGenericPaginationParams & TFilterCardsParams

export type TFilterCardsParams = {
  company_id?: string
  category_id?: string
  price_id?: string
}
