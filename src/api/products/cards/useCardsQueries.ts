import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import { TFilterCardsParams, TPayload } from './type'
import { useNavigate } from 'react-router-dom'
import { addCard, deleteCard, editCard, getCard, getCards } from '.'

const useGetCardsInfiniteQuery = (
  limit: number,
  options: TFilterCardsParams
) => {
  return useInfiniteQuery({
    queryKey: ['cards-infinite-query', ...(options ? [options] : [])],
    queryFn: ({ pageParam = 1 }) => {
      return getCards({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetCardsQuery = () => {
  return useQuery({
    queryKey: ['getAll-cards-query'],
    queryFn: () => {
      return getCards({
        pagination: 0,
      })
    },
  })
}

const useGetCardInfoQuery = (id: string | undefined) => {
  return useQuery({
    queryKey: [`card-${id}-info`],
    queryFn: async () => {
      return getCard(id!)
    },
    cacheTime: 0,
    enabled: !!id,
    onSuccess(data) {
      // console.log(data)
    },
  })
}

const useAddCardMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['addCard-mutation'],
    mutationFn: (payload: TPayload) => addCard(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/cards`)
      }
    },
    onError: (error) => {
      console.log(error)
    },
  })
}

const useEditCardMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['editCard-mutation'],
    mutationFn: (payload: TPayload) => {
      return editCard(payload)
    },
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/cards`)
      }
    },
  })
}

const useDeleteCardMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deleteCard-mutation'],
    mutationFn: (id: string) => {
      return deleteCard(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`cards-infinite-query`])
      query.refetchQueries([`getAll-cards-query`])
      handleCloseModal()
    },
  })
}

export {
  useGetCardsInfiniteQuery,
  useGetCardsQuery,
  useGetCardInfoQuery,
  useAddCardMutation,
  useEditCardMutation,
  useDeleteCardMutation,
}
