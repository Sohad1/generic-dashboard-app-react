import API_ROUTES from '@/constants/apiRoutes'
// import { TPayload } from './type'
import apiInstance from '@/api/apiInstance'
import { TGenericResponse } from '@/api/type'
import { TPayload, TCardsResponse, TCardResponse, TCardsParams } from './type'
import { createFormData } from '@/util/helpers'

const getCards = async (params: TCardsParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TCardsResponse>>(
    API_ROUTES.PRODUCTS.CARDS.GET,
    { params }
  )
  return data
}
const getCard = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TCardResponse>>(
    API_ROUTES.PRODUCTS.CARDS.GET_BY_ID,
    {
      params: { id },
    }
  )
  return data.response.card
}
const addCard = async (payload: TPayload) => {
  const formData = createFormData(payload.body)
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.CARDS.ADD}`,
    formData
  )
  return data
}

const editCard = async (payload: TPayload) => {
  const formData = createFormData(payload.body)
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.CARDS.UPDATE}`,
    formData
  )
  return data
}

const deleteCard = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.PRODUCTS.CARDS.DELETE, {
    params: { id },
  })
  return data
}

export { getCards, getCard, addCard, editCard, deleteCard }
