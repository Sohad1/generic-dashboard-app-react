import { TGenericPaginationResponse, TGenericRequestPayload } from '@/api/type'
import { TParams } from '@/pages/products/companies/ActionsCompany/hooks/type'
import { TGroup } from '../groups/type'

export type TCompaniesResponse = {
  companies: TGenericPaginationResponse<TCompany>
}
export type TCompanyResponse = {
  company: TCompany
}
export type TCompanyParams = {
  id?: string
  group_id: string
  name: string
  apple_hide: '0' | '1'
  color: string
  image: any
  logo: any
  price_image: any
  categories?: any
  image_url?: string
  logo_url?: string
  price_image_url?: string
}
export type TCompany = {
  id?: string
  group_id: string
  name: string
  apple_hide: '0' | '1'
  color: string
  image: any
  logo: any
  price_image: any
  categories?: any
  image_url?: string
  logo_url?: string
  price_image_url?: string
  group?: TGroup
}
export type TPayload = TGenericRequestPayload<TCompanyParams, TParams>
