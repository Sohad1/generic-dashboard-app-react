import API_ROUTES from '@/constants/apiRoutes'
// import { TPayload } from './type'
import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import { TPayload, TCompanyResponse, TCompaniesResponse } from './type'
import { createFormData } from '@/util/helpers'

const getCompanies = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TCompaniesResponse>>(
    API_ROUTES.PRODUCTS.COMPANIES.GET,
    { params }
  )
  return data
}
const getCompany = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TCompanyResponse>>(
    API_ROUTES.PRODUCTS.COMPANIES.GET_BY_ID,
    {
      params: { id },
    }
  )
  return data.response.company
}
const addCompany = async (payload: TPayload) => {
  const formData = await createFormData(payload.body)
  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.COMPANIES.ADD}`,
    formData
  )
  return data
}

const editCompany = async (payload: TPayload) => {
  const formData = await createFormData(payload.body)

  const { data } = await apiInstance.post(
    `${API_ROUTES.PRODUCTS.COMPANIES.UPDATE}`,
    formData,
    {
      params: payload.params,
    }
  )
  return data
}

const deleteCompany = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.PRODUCTS.COMPANIES.DELETE, {
    params: { id },
  })
  return data
}

export { getCompanies, getCompany, addCompany, editCompany, deleteCompany }
