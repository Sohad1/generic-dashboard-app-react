import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import { TPayload } from './type'
import { useNavigate } from 'react-router-dom'
import {
  addCompany,
  deleteCompany,
  editCompany,
  getCompanies,
  getCompany,
} from '.'

const useGetCompaniesInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: [`companies-${limit}-infinite-query`],
    queryFn: ({ pageParam = 1 }) => {
      return getCompanies({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetCompaniesQuery = () => {
  return useQuery({
    queryKey: ['getAll-companies-query'],
    queryFn: () => {
      return getCompanies({
        pagination: 0,
      })
    },
  })
}

const useGetCompanyInfoQuery = (id: string) => {
  return useQuery({
    queryKey: [`company-${id}-info`],
    queryFn: async () => {
      return getCompany(id!)
    },
    cacheTime: 0,
    enabled: !!id,
    onSuccess(data) {
      // console.log(data)
    },
  })
}

const useAddCompanyMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['addCompany-mutation'],
    mutationFn: (payload: TPayload) => addCompany(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/companies`)
      }
    },
    onError: (error) => {
      console.log(error)
    },
  })
}

const useEditCompanyMutation = () => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['editCompany-mutation'],
    mutationFn: (payload: TPayload) => {
      return editCompany(payload)
    },
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/companies`)
      }
    },
  })
}

const useDeleteCompanyMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deleteCompany-mutation'],
    mutationFn: (id: string) => {
      return deleteCompany(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`companies-infinite-query`])
      query.refetchQueries([`getAll-companies-query`])
      handleCloseModal()
    },
  })
}

export {
  useGetCompaniesInfiniteQuery,
  useGetCompaniesQuery,
  useGetCompanyInfoQuery,
  useAddCompanyMutation,
  useEditCompanyMutation,
  useDeleteCompanyMutation,
}
