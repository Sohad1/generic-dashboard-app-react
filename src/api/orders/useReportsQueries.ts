import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import {
  confirmOrder,
  deleteOrder,
  getOrders,
  getOrdersArchive,
  getPausedOrders,
  pauseOrder,
  rejectOrder,
} from '.'
import { TOrdersReportsOptions } from './type'

const useGetOrdersInfiniteQuery = (
  // limit: number,
  filterOptions: TOrdersReportsOptions
) => {
  return useQuery({
    queryKey: [
      `orders-infinite-query`,
      ...(filterOptions ? [filterOptions] : []),
    ],
    queryFn: async () => {
      return getOrders({
        // pagination: 0,
        ...filterOptions,
      }).then((res) => res.response.orders)
    },
  })
  // return useInfiniteQuery({
  //   queryKey: [
  //     `orders-infinite-query`,
  //     ...(filterOptions ? [filterOptions] : []),
  //   ],
  //   queryFn: ({ pageParam = 1 }) => {
  //     return getOrders({
  //       pagination: 1,
  //       limit,
  //       page: pageParam,
  //       ...filterOptions,
  //     })
  //   },
  //   getNextPageParam: (lastPage, pages) => {
  //     return true
  //   },
  // })
}

const useGetPausedOrdersInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: [`paused-orders-infinite-query`],
    queryFn: ({ pageParam = 1 }) => {
      return getPausedOrders({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetOrdersArchiveInfiniteQuery = (limit: number) => {
  return useInfiniteQuery({
    queryKey: [`orders-archive-infinite-query`],
    queryFn: ({ pageParam = 1 }) => {
      return getOrdersArchive({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const usePausedOrderMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['pauseOrder-mutation'],
    mutationFn: (id: string) => {
      return pauseOrder(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`orders-infinite-query`])
      query.refetchQueries([`paused-orders-infinite-query`])
      query.refetchQueries([`orders-archive-infinite-query`])

      handleCloseModal()
    },
  })
}

const useConfirmOrderMutation = () => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['confirmOrder-mutation'],
    mutationFn: (id: string) => {
      return confirmOrder(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`orders-infinite-query`])
      query.refetchQueries([`paused-orders-infinite-query`])
      query.refetchQueries([`orders-archive-infinite-query`])

      // handleCloseModal()
    },
  })
}
const useRejectOrderMutation = () => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['rejectOrder-mutation'],
    mutationFn: (id: string) => {
      return rejectOrder(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`orders-infinite-query`])
      query.refetchQueries([`paused-orders-infinite-query`])
      query.refetchQueries([`orders-archive-infinite-query`])

      // handleCloseModal()
    },
  })
}
const useDeleteOrderMutation = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deleteOrder-mutation'],
    mutationFn: (id: string) => {
      return deleteOrder(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`orders-infinite-query`])
      query.refetchQueries([`paused-orders-infinite-query`])
      query.refetchQueries([`orders-archive-infinite-query`])

      handleCloseModal()
    },
  })
}

const useMutateActionsOrders = (type: 'csv' | 'pause' | 'approve') => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['csv-mutation'],
    mutationFn: ({
      all_order_ids,
      csv_export,
    }: {
      all_order_ids: string[]
      csv_export?: 1 | 0
    }) => {
      return getOrders({
        // pagination: 0,
        all_order_ids,
        csv_export: type === 'csv' ? 1 : 0,
        pause: type === 'pause' ? 1 : 0,
        approve: type === 'approve' ? 1 : 0,
      })
    },
    onSuccess(data, variables, context) {
      if (data.code === 200 && type === 'csv') {
        console.log(data.response.file_name)
        let a = document.createElement('a')
        a.setAttribute('target', '_blank')
        a.setAttribute(
          'href',
          `https://stgtrading.de/hadeya-react/public/dashboard/api/order/get-file?file_name=${data.response.file_name}`
        )

        document.body.appendChild(a)
        a.click()
      }
      query.refetchQueries([`orders-infinite-query`])
      query.refetchQueries([`paused-orders-infinite-query`])
      query.refetchQueries([`orders-archive-infinite-query`])
    },
  })
}

export {
  useGetOrdersInfiniteQuery,
  useGetPausedOrdersInfiniteQuery,
  useGetOrdersArchiveInfiniteQuery,
  usePausedOrderMutation,
  useConfirmOrderMutation,
  useRejectOrderMutation,
  useDeleteOrderMutation,
  useMutateActionsOrders,
}
