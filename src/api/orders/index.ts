import API_ROUTES from '@/constants/apiRoutes'
import apiInstance from '../apiInstance'
import {
  TOrdersResponse,
  TOrdersPaginationParams,
  TOrdersReportsPaginationParams,
  TOrdersReportsOptions,
} from './type'
import { TGenericResponse } from '../type'

const getOrders = async (params: TOrdersReportsOptions) => {
  console.log(params)
  const { data } = await apiInstance.get<TGenericResponse<TOrdersResponse>>(
    API_ROUTES.ORDERS.GET_PUBLIC,
    {
      params,
    }
  )
  return data
}
const getPausedOrders = async (params: TOrdersPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TOrdersResponse>>(
    API_ROUTES.ORDERS.GET_PAUSED,
    {
      params,
    }
  )
  return data.response.orders
}

const getOrdersArchive = async (params: TOrdersPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TOrdersResponse>>(
    API_ROUTES.ORDERS.GET_ARCHIVE,
    {
      params,
    }
  )
  return data.response.orders
}

//change
const getOrdersReports = async (params: TOrdersReportsPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TOrdersResponse>>(
    API_ROUTES.REPORTS.MAIN.GET,
    {
      params,
    }
  )
  return data.response.reports
}

const rejectOrder = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.ORDERS.ACTIONS.REJECT, {
    params: { id },
  })
  return data
}
const confirmOrder = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.ORDERS.ACTIONS.CONFIRM, {
    params: { id },
  })
  return data
}
const pauseOrder = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.ORDERS.ACTIONS.PAUSE, {
    params: { id },
  })
  return data
}
const deleteOrder = async (id: string) => {
  const { data } = await apiInstance.get(API_ROUTES.ORDERS.ACTIONS.DELETE, {
    params: { id },
  })
  return data
}

export {
  getOrders,
  getPausedOrders,
  getOrdersArchive,
  getOrdersReports,
  rejectOrder,
  confirmOrder,
  pauseOrder,
  deleteOrder,
}
