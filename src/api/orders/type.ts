import { TUser } from '../auth/type'
import { TProductCategory } from '../products/prices/type'
import { TGenericPaginationResponse } from '../type'

export type TOrdersPaginationParams = {
  pagination?: 1 | 0
  limit?: number
  page?: number
}

export type TOrdersReportsOptions = {
  all_order_ids?: string[]
  csv_export?: 0 | 1
  approve?: 0 | 1
  pause?: 0 | 1
  company_id?: number | string
  category_id?: number | string
  price_id?: number | string
  orders_count?: number | string
}

export type TOrdersReportsPaginationParams = TOrdersPaginationParams &
  TOrdersReportsOptions

export type TOrdersResponse = {
  reports?: TGenericPaginationResponse<TOrder>
  orders: TGenericPaginationResponse<TOrder>
  file_name?: string
}

export type TOrder = {
  id: string
  price_id: number
  user_id: number
  details: string
  status: 0
  created_at: string | null
  updated_at: string
  card_id: null
  purchase_price: string
  special_offer_id: null
  user: TUser
  price?: TProductCategory
}
