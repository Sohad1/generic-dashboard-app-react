import { TUser } from '../auth/type'
import { TMessage_templates } from '../message_templates/type'

export type TGetChatResponse = {
  contacts: TContact[]
  contacts_archive: TContact[]
  messages: TMessage[]
  message_templates: TMessage_templates[]
  user: TUser & { files_support: 0 | 1 }
}

export type TMessage = {
  id: string
  from_user: string
  to_user: string
  message: string
  read: 0 | 1
  created_at: null

  
  updated_at: string
  has_file: 1 | 0
  files: {
    id: number
    message_id: number
    file_name: string
    created_at: string
    updated_at: string
    file_url: string
  }[]
}
export type TContact = {
  id: string
  from_user: string
  to_user: string
  message: TMessage
  read: 0 | 1
  created_at: null
  updated_at: string
  has_file: 0 | 1
  name: string
  user_id: string
  img: string
  email: string
}
