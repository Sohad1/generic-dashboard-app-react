import API_ROUTES from '@/constants/apiRoutes'
import apiInstance from '../apiInstance'
import { TGenericResponse } from '../type'
import { TGetChatResponse } from './type'
import { createFormData } from '@/util/helpers'

const getChatPage = async (user_id: any) => {
  const { data } = await apiInstance.get<TGenericResponse<TGetChatResponse>>(
    API_ROUTES.CHAT.GET_CHAT_PAGE,
    {
      params: { user_id },
    }
  )
  return data.response
}

const putUserSupportFiles = async (params: any) => {
  const { data } = await apiInstance.get<TGenericResponse<any>>(
    API_ROUTES.CHAT.PUT_USER_SUPPORT_FILES,
    {
      params,
    }
  )
  return data
}

const sendMessage = async (payload: any) => {
  const formData = await createFormData(payload)

  const { data } = await apiInstance.post<TGenericResponse<any>>(
    API_ROUTES.CHAT.SEND_MESSAGE,
    formData
  )
  return data
}

export { getChatPage, putUserSupportFiles, sendMessage }
