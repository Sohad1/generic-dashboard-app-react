import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { getChatPage, putUserSupportFiles, sendMessage } from '.'

const useGetChatPageQuery = (userId: string | undefined) => {
  return useQuery({
    queryKey: ['get-chat-page-query', userId],
    queryFn: () => getChatPage(userId!),
    cacheTime: 0,
  })
}


const useSendMessageMutation = (
  userId: string,
  handleCloseModal?: () => void
) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['sendMessage-mutation'],
    mutationFn: (data: { to_user: string; message?: string; image?: any }) => {
      return sendMessage(data)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries(['get-chat-page-query', userId])
      handleCloseModal?.()
    },
  })
}

const usePutUserSupportFilesMutation = (userId: string) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['sendMessage-mutation'],
    mutationFn: (data: { user_id: string; files_support: 0 | 1 }) => {
      return putUserSupportFiles(data)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries(['get-chat-page-query', userId])
      // handleCloseModal()
    },
  })
}

export {
  useGetChatPageQuery,
  useSendMessageMutation,
  usePutUserSupportFilesMutation,
}
