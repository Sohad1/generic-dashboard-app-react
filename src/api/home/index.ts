import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TIdParam,
  TGenericRequestPayload,
} from "../type";
import { THome, THomeResponse } from "./type";

const getHome = async () => {
  const { data } = await apiInstance.get<TGenericResponse<THome>>(
    API_ROUTES.HOME.GET
  );
  return data;
};
export { getHome };
