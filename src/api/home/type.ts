import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";

export type THomeResponse = {
  home: THome;
};
export type THome = {
  users_count?: number;
  last_30?: number;
  orders_count?: number;
  orders_approved_count?: number;
  pie: TPie[];
};
export type TPie = {
  id: string;
  label: string;
  value: number;
  color: string;
};
