import { getHome } from ".";
import { useQuery } from "@tanstack/react-query";

const useGetHomeQuery = () => {
  return useQuery({
    queryKey: [`get-home`],
    queryFn: async () => {
      return getHome();
    },
  });
};
export { useGetHomeQuery };
