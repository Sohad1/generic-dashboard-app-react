import API_ROUTES from '@/constants/apiRoutes'
import apiInstance from '../apiInstance'
import {
  TGenericPaginationParams,
  TGenericResponse,
  TIdParam,
  TGenericRequestPayload,
} from '../type'
import { TDeleteOrdersResponse, TDeleteOrderResponse } from './type'
const getDeleteOrders = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TDeleteOrdersResponse>
  >(API_ROUTES.DELETE_ORDERS.GET, { params: payload.params })
  return data
}

const Confirm = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.DELETE_ORDERS.CONFIRM, {
    params: { id },
  })
  return data
}

const Reject = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.DELETE_ORDERS.REJECT, {
    params: { id },
  })
  return data
}
const DeleteAll = async (delete_orders_ids: string[]) => {
  const { data } = await apiInstance.post(API_ROUTES.DELETE_ORDERS.DELETE_ALL, {
    delete_orders_ids,
  })
  return data
}

export { getDeleteOrders, Confirm, Reject, DeleteAll }
