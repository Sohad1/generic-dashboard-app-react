import { number, string } from 'yup'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'

export type TDeleteOrdersResponse = {
  delete_orders: { data: TDeleteOrders[] }
}
export type TDeleteOrderResponse = {
  delete_order: TDeleteOrders
}
export type TDeleteOrders = {
  id?: number
  user_id?: number
  reason?: string
  status?: number
}
