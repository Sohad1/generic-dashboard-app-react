import { getDeleteOrders, Confirm, Reject, DeleteAll } from '.'
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from '@tanstack/react-query'
import { useNavigate } from 'react-router-dom'

import { add } from 'lodash'
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from '../type'

const useGetDeleteOrdersInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload
  return useQuery({
    queryKey: [
      'DeleteOrders-infinite-query',
      params.search_input,
      params.order_by,
      params.type,
    ],
    queryFn: async () => {
      return getDeleteOrders({
        params: {
          pagination: 0,
        },
      })
    },
  })
  // return useInfiniteQuery({
  //   queryKey: [
  //     'DeleteOrders-infinite-query',

  //     params.search_input,
  //     params.order_by,
  //     params.type,
  //     // params.limit,
  //   ],
  //   queryFn: ({ pageParam = 0 }) =>
  //     getDeleteOrders({
  //       params: {
  //         pagination: 0,
  //         // page: pageParam,
  //         ...params,
  //       },
  //     }),
  //   getNextPageParam: (lastPage, pages) => {
  //     //   return (
  //     //     lastPage.response.permissions.current_page !==
  //     //     pages[0].response.permissions.last_page
  //     //   );
  //     return true
  //   },
  // })
}

const useConfirmQuery = (handleCloseModal: () => void) => {
  const query = useQueryClient()

  return useMutation({
    mutationKey: ['confirm-mutation'],
    mutationFn: (id: number) => Confirm(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      query.refetchQueries(['DeleteOrders-infinite-query'])
    },
  })
}
const useRejectQuery = (handleCloseModal: () => void) => {
  const query = useQueryClient()

  return useMutation({
    mutationKey: ['reject-mutation'],
    mutationFn: (id: number) => Reject(id),
    onSuccess(data, variables, context) {
      handleCloseModal()
      query.refetchQueries(['DeleteOrders-infinite-query'])
    },
  })
}
const useDeleteAllQuery = (handleCloseModal: () => void) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['delete_all-mutation'],
    mutationFn: (ids: string[]) => DeleteAll(ids),
    onSuccess(data, variables, context) {
      handleCloseModal()
      query.refetchQueries(['DeleteOrders-infinite-query'])
    },
  })
}

export {
  useGetDeleteOrdersInfiniteQuery,
  useConfirmQuery,
  useRejectQuery,
  useDeleteAllQuery,
}
