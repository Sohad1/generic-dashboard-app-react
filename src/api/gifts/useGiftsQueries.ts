import {
  getGifts,
  getGift,
  editGift,
  addGift,
  deleteGift,
  sendGift,
  sendGiftToUser,
} from ".";
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { toast } from "react-toastify";
const useGetGiftsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "gift-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getGifts({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.permissions.current_page !==
      //     pages[0].response.permissions.last_page
      //   );
      return true;
    },
  });
};
const useGetGiftQuery = (giftId: string | undefined) => {
  return useQuery({
    queryKey: ["get-gift-query"],
    queryFn: () => getGift(giftId!),
    cacheTime: 0,
    enabled: !!giftId,
  });
};
const useEditGiftMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editGift-mutation"],
    mutationFn: (payload: TPayload) => editGift(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/gifts");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useAddGiftMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addGifts-mutation"],
    mutationFn: (payload: TPayload) => addGift(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/gifts");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useDeleteGiftMutation = () => {
  const query = useQueryClient();
  return useMutation({
    mutationKey: [`delete-Gift-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteGift(payload),
    onSuccess(data, variables, context) {
      query.refetchQueries(["gift-infinite-query"]);
      toast.success("Operation successfully");
    },
  });
};
const useSendGiftMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["send-gift-mutation"],
    mutationFn: (payload: TPayload) => sendGift(payload),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};
const useSendGiftToUserMutation = (handleClose: () => void) => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["SendGiftToUser-mutation"],
    mutationFn: (payload: {
      body: { user_id: number; point: number; all_countries: number };
    }) => sendGiftToUser(payload.body),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        handleClose();
        // navigate("/app_users");
      }
    },
  });
};
export {
  useGetGiftsInfiniteQuery,
  useGetGiftQuery,
  useEditGiftMutation,
  useAddGiftMutation,
  useDeleteGiftMutation,
  useSendGiftMutation,
  useSendGiftToUserMutation,
};
