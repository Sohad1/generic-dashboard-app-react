import { TOption } from "@/components/items/inputField/autoCompleteField/type";
import { TGenericPaginationResponse, TGenericRequestPayload } from "../type";
import { TParams } from "@/pages/gifts/AddEditGift/hooks/type";

export type TGiftsResponse = {
  gifts: TGenericPaginationResponse<TGifts>;
};
export type TGiftResponse = {
  gift: TGifts;
};
export type TGifts = {
  id?: number;
  point: number;
  created_at: string;
  all_countries: number;
  gift_users_count: number;
  country_id: number;
  collection?: any;
  gift_country_ids: TOption;
  countries?: any;
};
export type TPayload = TGenericRequestPayload<TGifts, TParams>;
