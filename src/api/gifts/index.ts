import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TGiftsResponse, TGiftResponse, TPayload } from "./type";
const getGifts = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TGiftsResponse>>(
    API_ROUTES.GIFTS.GET,
    { params: payload.params }
  );
  return data;
};
const getGift = async (giftId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TGiftResponse>>(
    API_ROUTES.GIFTS.GET_GIFT,
    {
      params: { id: giftId },
    }
  );
  return data;
};
const deleteGift = async (payload: TGenericRequestPayload<null, TIdParam>) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.GIFTS.DELETE,
    { params: payload.params }
  );
  return data;
};
const editGift = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.GIFTS.UPDATE}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const addGift = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.GIFTS.ADD}`,
    payload.body
  );
  console.log(data);
  return data;
};
const sendGift = async (payload: TPayload) => {
  const { data } = await apiInstance.post(API_ROUTES.GIFTS.SEND, payload.body, {
    params: payload.params,
  });
  return data;
};

const sendGiftToUser = async (payload: {
  user_id: number;
  point: number;
  all_countries: number;
}) => {
  const { data } = await apiInstance.post(`${API_ROUTES.GIFTS.SEND}`, payload);
  console.log(data);
  return data;
};
export {
  getGifts,
  getGift,
  deleteGift,
  editGift,
  addGift,
  sendGift,
  sendGiftToUser,
};
