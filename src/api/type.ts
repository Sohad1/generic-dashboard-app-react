export type TGenericResponse<R> = {
  map(arg0: (item: { id: any; name: any }) => { id: any; name: any }): unknown;
  success: boolean;
  code: number;
  message: string;
  response: R;
};
export type TGenericPaginationResponse<D> = {
  current_page: number;
  data: D[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Link[];
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: null;
  to: number;
  total: number;
};
export type TGenericPaginationParams = Partial<{
  pagination: 0 | 1;
  limit: number;
  page: number;
  search_input?: string;
  order_by?: string;
  type?: string;
}>;

export type TGenericRequestPayload<B, P> = {
  body?: B;
  params?: P;
};
export type TIdParam = Partial<{
  id: string;
}>;
export type Link = {
  url: null | string;
  label: string;
  active: boolean;
};
