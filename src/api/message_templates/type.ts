import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'
import { TParams } from '@/pages/Message_template/AddEditMe_Te/hooks/type'

export type TMessage_templatesResponse = {
  message_templates: TGenericPaginationResponse<TMessage_templates>
}
export type TMessage_templateResponse = {
  message_template: TMessage_templates
}
export type TMessage_templates = {
  id?: number
  text: String | number
}
export type TPayload = TGenericRequestPayload<TMessage_templates, TParams>
