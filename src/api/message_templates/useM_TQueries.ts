import {
  getMessage_templates,
  getMessage_template,
  editMessage_template,
  addMessage_template,
  deleteMessage_template,
} from ".";
import {
  useInfiniteQuery,
  useQuery,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { TPayload } from "./type";
import { add } from "lodash";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
import { toast } from "react-toastify";
const useGetMe_TeInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "Message_templates-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) =>
      getMessage_templates({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      }),
    getNextPageParam: (lastPage, pages) => {
      //   return (
      //     lastPage.response.permissions.current_page !==
      //     pages[0].response.permissions.last_page
      //   );
      return true;
    },
  });
};
const useGetMe_TeQuery = (Me_TeId: string | undefined) => {
  return useQuery({
    queryKey: ["get-Message_template-query"],
    queryFn: () => getMessage_template(Me_TeId!),
    cacheTime: 0,
    enabled: !!Me_TeId,
  });
};
const useDeleteMe_TeMutation = () => {
  const query = useQueryClient();

  return useMutation({
    mutationKey: [`delete-Message_template-mutation`],
    mutationFn: (payload: TGenericRequestPayload<null, TIdParam>) =>
      deleteMessage_template(payload),
    onSuccess(data, variables, context) {
      query.refetchQueries(["Message_templates-infinite-query"]);
      toast.success("Operation successfully");
    },
  });
};
const useEditMe_TetMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editMessage_template-mutation"],
    mutationFn: (payload: TPayload) => editMessage_template(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/Message_template");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
const useAddMe_TetMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addMessage_template-mutation"],
    mutationFn: (payload: TPayload) => addMessage_template(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/Message_template");
      }
      toast.success("Operation successfully");
    },
    onError(error) {
      toast.error(`Operation failed`);
    },
  });
};
export {
  useGetMe_TeInfiniteQuery,
  useGetMe_TeQuery,
  useAddMe_TetMutation,
  useEditMe_TetMutation,
  useDeleteMe_TeMutation,
};
