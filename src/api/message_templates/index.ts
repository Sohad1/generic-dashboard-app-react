import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import {
  TMessage_templatesResponse,
  TMessage_templateResponse,
  TPayload,
} from "./type";
const getMessage_templates = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TMessage_templatesResponse>
  >(API_ROUTES.MESSAGE_TEMPLATES.GET_MESSAGE_TEMPLATES, {
    params: payload.params,
  });
  return data;
};
const getMessage_template = async (me_teId: string) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TMessage_templateResponse>
  >(API_ROUTES.MESSAGE_TEMPLATES.GET_MESSAGE_TEMPLATE, {
    params: { id: me_teId },
  });
  return data;
};

const deleteMessage_template = async (
  payload: TGenericRequestPayload<null, TIdParam>
) => {
  const { data } = await apiInstance.get<TGenericResponse<string>>(
    API_ROUTES.MESSAGE_TEMPLATES.DELETE_MESSAGE_TEMPLATES,
    { params: payload.params }
  );
  return data;
};

const editMessage_template = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.MESSAGE_TEMPLATES.UPDATE_MESSAGE_TEMPLATES}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const addMessage_template = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.MESSAGE_TEMPLATES.ADD_MESSAGE_TEMPLATES}`,
    payload.body
  );
  console.log(data);
  return data;
};
export {
  getMessage_templates,
  getMessage_template,
  editMessage_template,
  deleteMessage_template,
  addMessage_template,
};
