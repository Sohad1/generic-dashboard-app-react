import {
  useInfiniteQuery,
  useMutation,
  useQuery,
  useQueryClient,
} from '@tanstack/react-query'
import { EUserTypes, TPayload, TUserTypes } from './type'
import {
  addAdmin,
  deleteAdmin,
  editAdmin,
  getAdminById,
  getAdmins,
} from './handlerAdmin'
import {
  addSuperAdmin,
  deleteSuperAdmin,
  editSuperAdmin,
  getSuperAdminById,
  getSuperAdmins,
} from './handlerSuperAdmin'
import { useNavigate } from 'react-router-dom'
import {
  addUser,
  deleteUser,
  editUser,
  getUserById,
  getUsers,
} from './handlerUser'

const useGetUsersInfiniteQuery = (userType: TUserTypes, limit: number) => {
  return useInfiniteQuery({
    queryKey: [`${userType}-infinite-query`],
    queryFn: ({ pageParam = 1 }) => {
      if (userType === EUserTypes.superAdmin)
        return getSuperAdmins({
          pagination: 1,
          limit,
          page: pageParam,
        })
      if (userType === EUserTypes.admin)
        return getAdmins({
          pagination: 1,
          limit,
          page: pageParam,
        })
      return getUsers({
        pagination: 1,
        limit,
        page: pageParam,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetUserInfoQuery = (userId: string | undefined, userType: string) => {
  return useQuery({
    queryKey: [`${userType}-${userId}-info`],
    queryFn: async () => {
      if (userType === EUserTypes.user) return getUserById(userId!)
      if (userType === EUserTypes.admin) return getAdminById(userId!)
      if (userType === EUserTypes.superAdmin) return getSuperAdminById(userId!)
    },
    cacheTime: 0,
    enabled: !!userId,
    onSuccess(data) {
      console.log(data)
    },
  })
}

const useAddUserMutation = (userType: TUserTypes) => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['addUser-mutation'],
    mutationFn: (payload: TPayload) => {
      if (userType === EUserTypes.admin) return addAdmin(payload)
      if (userType === EUserTypes.superAdmin) return addSuperAdmin(payload)
      const data = {
        body: { ...payload.body, role_id: payload.body.role.id },
      }
      delete data.body.role

      return addUser(data)
    },
    onSuccess(data, variables, context) {
      // console.log(data)
      if (data.code === 200) {
        navigate(`/users?type=${userType}`)
      }
    },
    onError: (error) => {
      console.log(error)
    },
  })
}

const useEditUserMutation = (userType: TUserTypes) => {
  const navigate = useNavigate()
  return useMutation({
    mutationKey: ['editUser-mutation'],
    mutationFn: (payload: TPayload) => {
      if (userType === EUserTypes.admin) return editAdmin(payload)
      if (userType === EUserTypes.superAdmin) return editSuperAdmin(payload)
      return editUser(payload)
    },
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate(`/users?type=${userType}`)
      }
    },
  })
}

const useDeleteUserMutation = (
  userType: TUserTypes,
  handleCloseModal: () => void
) => {
  const query = useQueryClient()
  return useMutation({
    mutationKey: ['deleteUser-mutation'],
    mutationFn: (id: number) => {
      if (userType === EUserTypes.admin) return deleteAdmin(id)
      if (userType === EUserTypes.superAdmin) return deleteSuperAdmin(id)
      return deleteUser(id)
    },
    onSuccess(data, variables, context) {
      query.refetchQueries([`${userType}-infinite-query`])
      handleCloseModal()
    },
  })
}

export {
  useGetUsersInfiniteQuery,
  useGetUserInfoQuery,
  useAddUserMutation,
  useEditUserMutation,
  useDeleteUserMutation,
}
