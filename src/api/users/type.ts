import { TUserForm } from '@/pages/users/AddEditUser/type'
import { TUser } from '../auth/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'

export type TUserTypes = 'user' | 'admin' | 'super_admin'

export enum EUserTypes {
  user = 'user',
  admin = 'admin',
  superAdmin = 'super_admin',
}

//user
export type TUsersResponse = {
  users: TGenericPaginationResponse<TUser>
}
export type TUserResponse = {
  user: TUser
}

//admin
export type TAdminsResponse = {
  admins: TGenericPaginationResponse<TUser>
}
export type TAdminResponse = {
  admin: TUser
}

//super admin
export type TSuperAdminsResponse = {
  super_admins: TGenericPaginationResponse<TUser>
}
export type TSuperAdminResponse = {
  super_admin: TUser
}

export type TParams = {
  id: string | undefined
}

export type TPayload = TGenericRequestPayload<TUserForm, TParams>
