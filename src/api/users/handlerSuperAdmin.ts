import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import API_ROUTES from '@/constants/apiRoutes'
import { TPayload, TSuperAdminResponse, TSuperAdminsResponse } from './type'

const getSuperAdmins = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TSuperAdminsResponse>
  >(API_ROUTES.USERS.SUPER_ADMIN.GET, { params })
  return data.response.super_admins
}

const getSuperAdminById = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TSuperAdminResponse>>(
    API_ROUTES.USERS.SUPER_ADMIN.GET_BY_ID,
    { params: { id } }
  )
  return data.response.super_admin
}
const addSuperAdmin = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.SUPER_ADMIN.ADD,
    payload.body
  )
  return data
}

const editSuperAdmin = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.SUPER_ADMIN.UPDATE,
    payload.body
  )
  return data
}

const deleteSuperAdmin = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.USERS.SUPER_ADMIN.DELETE, {
    params: { id },
  })
  return data
}

export {
  getSuperAdmins,
  getSuperAdminById,
  addSuperAdmin,
  editSuperAdmin,
  deleteSuperAdmin,
}
