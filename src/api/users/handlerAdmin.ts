import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import API_ROUTES from '@/constants/apiRoutes'
import { TAdminResponse, TAdminsResponse, TPayload } from './type'

const getAdmins = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TAdminsResponse>>(
    API_ROUTES.USERS.ADMIN.GET,
    { params }
  )
  return data.response.admins
}
const getAdminById = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TAdminResponse>>(
    API_ROUTES.USERS.ADMIN.GET_BY_ID,
    { params: { id } }
  )
  return data.response.admin
}

const addAdmin = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.ADMIN.ADD,
    payload.body
  )
  return data
}
const editAdmin = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.ADMIN.UPDATE,
    payload.body
  )
  return data
}
const deleteAdmin = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.USERS.ADMIN.DELETE, {
    params: { id },
  })
  return data
}
export { getAdmins, getAdminById, addAdmin, editAdmin, deleteAdmin }
