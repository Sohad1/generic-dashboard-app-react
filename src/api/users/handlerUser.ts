import apiInstance from '@/api/apiInstance'
import { TGenericPaginationParams, TGenericResponse } from '@/api/type'
import API_ROUTES from '@/constants/apiRoutes'
import { TUserResponse, TUsersResponse, TPayload } from './type'

const getUsers = async (params: TGenericPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<TUsersResponse>>(
    API_ROUTES.USERS.USERS.GET,
    { params }
  )
  return data.response.users
}
const getUserById = async (id: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TUserResponse>>(
    API_ROUTES.USERS.USERS.GET_BY_ID,
    { params: { id } }
  )
  return data.response.user
}

const addUser = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.USERS.ADD,
    payload.body
  )
  return data
}
const editUser = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    API_ROUTES.USERS.USERS.UPDATE,
    payload.body
  )
  return data
}
const deleteUser = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.USERS.USERS.DELETE, {
    params: { id },
  })
  return data
}
export { getUsers, getUserById, addUser, editUser, deleteUser }
