import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import {
  addSetting,
  deleteSetting,
  editSetting,
  getSetting,
  getSettings,
} from ".";
import { TPayload } from "./type";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
const useGetSettingsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "settings-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getSettings({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useGetSettingQuery = (settingId: string | undefined) => {
  return useQuery({
    queryKey: ["get-category-query"],
    queryFn: () => getSetting(settingId!),
    cacheTime: 0,
    enabled: !!settingId,
  });
};
const useAddSettingMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["addSetting-mutation"],
    mutationFn: (payload: TPayload) => addSetting(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/settings");
      }
    },
  });
};
const useEditSettingMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["editSetting-mutation"],
    mutationFn: (payload: TPayload) => editSetting(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/settings");
      }
    },
  });
};
const useDeleteSettingMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["deleteSetting-mutation"],
    mutationFn: (id: number) => deleteSetting(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};
export {
  useGetSettingsInfiniteQuery,
  useGetSettingQuery,
  useAddSettingMutation,
  useEditSettingMutation,
  useDeleteSettingMutation,
};
