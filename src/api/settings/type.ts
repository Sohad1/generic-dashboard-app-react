import { TParams } from '@/pages/settings/AddEditSetting/hooks/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'

export type TSettingsResponse = {
  settings: TGenericPaginationResponse<TSetting>
}
export type TSettingResponse = {
  settings: TSetting
}
export type TSetting = {
  id?: number
  key: string
  value: string
  bool: number
}
export type TPayload = TGenericRequestPayload<TSetting, TParams>
