import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TSettingsResponse, TSettingResponse, TPayload } from "./type";

const getSettings = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TSettingsResponse>>(
    API_ROUTES.SETTINGS.GET_SETTINGS,
    { params: payload.params }
  );
  return data;
};
const getSetting = async (settingId: string) => {
  const { data } = await apiInstance.get<TGenericResponse<TSettingResponse>>(
    API_ROUTES.SETTINGS.GET_SETTING,
    {
      params: { id: settingId },
    }
  );
  return data;
};
const addSetting = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.SETTINGS.ADD_SETTING}`,
    payload.body
  );
  console.log(data);
  return data;
};
const editSetting = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.SETTINGS.UPDATE_SETTING}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const deleteSetting = async (id: number) => {
  const { data } = await apiInstance.get(API_ROUTES.SETTINGS.DELETE_SETTING, {
    params: { id },
  });
  return data;
};
export { getSettings, getSetting, addSetting, editSetting, deleteSetting };
