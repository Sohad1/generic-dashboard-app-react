import { useInfiniteQuery, useQuery } from '@tanstack/react-query'
import { TEmailsReportsPaginationParams, TReportsTypes } from './type'
import { getMainReports, getPalyReports, getUserEmailsReports } from '.'
import {
  TFilterInitialValues,
  TPlaysReportsInitialValues,
} from '@/components/pages/reports/type'

const useGetUserEmailsReportsQuery = (
  options: TEmailsReportsPaginationParams
) => {
  return useQuery({
    queryKey: ['get-users-emails-reports', ...(options ? [options] : [])],
    queryFn: async () =>
      getUserEmailsReports({
        ...options,
      }),
  })
}
const useGetReportsInfiniteQuery = (
  reportsType: TReportsTypes,
  limit: number,
  options: TFilterInitialValues
) => {
  return useInfiniteQuery({
    queryKey: [
      `${reportsType}-reports-infinite-query`,
      ...(options ? [options] : []),
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getMainReports({
        pagination: 1,
        limit,
        page: pageParam,
        ...options,
      })
    },
    getNextPageParam: (lastPage, pages) => {
      return true
    },
  })
}

const useGetPlaysReportsQuery = (options: TPlaysReportsInitialValues) => {
  return useQuery({
    queryKey: ['get-plays-reports', ...(options ? [options] : [])],
    queryFn: async () =>
      getPalyReports({
        ...options,
      }),
  })
}

export {
  useGetReportsInfiniteQuery,
  useGetUserEmailsReportsQuery,
  useGetPlaysReportsQuery,
}
