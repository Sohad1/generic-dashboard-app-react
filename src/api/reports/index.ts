import API_ROUTES from '@/constants/apiRoutes'
import apiInstance from '../apiInstance'
import {
  TEmailsReportsPaginationParams,
  TMainReportsPaginationParams,
  TMainReportsResponse,
  TPlayReportsResponse,
} from './type'
import { TGenericResponse } from '../type'
import { TPlaysReportsInitialValues } from '@/components/pages/reports/type'

const getMainReports = async (params: TMainReportsPaginationParams) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TMainReportsResponse>
  >(API_ROUTES.REPORTS.MAIN.GET, {
    params,
  })
  return data.response.reports
}

const getPalyReports = async (params: TPlaysReportsInitialValues) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TPlayReportsResponse>
  >(API_ROUTES.REPORTS.PLAY.GET, {
    params,
  })
  return data.response
}

const getUserEmailsReports = async (params: TEmailsReportsPaginationParams) => {
  const { data } = await apiInstance.get<TGenericResponse<String>>(
    API_ROUTES.REPORTS.USER_EMAILS.GET,
    {
      params,
    }
  )
  return data.response
}

export { getMainReports, getPalyReports, getUserEmailsReports }
