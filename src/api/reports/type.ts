import {
  TEmailsReportsInitialValues,
  TMainReportsInitialValues,
} from '@/components/pages/reports/type'
import { TCountry } from '../countries/type'
import { TGenericPaginationResponse } from '../type'

export type TReportsTypes = 'main' | 'plays' | 'user-emails'

export enum EReportsTypes {
  main = 'main',
  play = 'plays',
  email = 'user-emails',
}

export type TMainReportsPaginationParams = {
  pagination?: 1 | 0
  limit?: number
  page?: number
} & TMainReportsInitialValues

export type TEmailsReportsPaginationParams = TEmailsReportsInitialValues

export type TPlayReportsResponse = {
  userRegisterPlayThisDateCount: number
  playCount: number
  userPreviousMonthCount: number
  userThisMonthCount: number
  userCount: number
  pointsSum: number
}
export type TMainReportsResponse = {
  reports: TGenericPaginationResponse<TMainReports>
}

export type TMainReports = {
  activation_code: string
  activation_code_exp_date: string
  ads_device_id: null
  age: null
  apple: number
  country: null | TCountry
  country_id: null | number
  created_at: string
  deleted: number
  device_id: null | string
  email: string
  email_verified_at: string
  fcm_token: null | string
  gender: null | string
  id: number
  img: string
  isAdmin: number
  is_confirmed: number
  lang: string
  name: string
  orders: []
  phone: null | string
  points: TPoint
  referral_id: string
  referred: []
  status: number
  support_chat: number
  updated_at: string
  user_points_history_count: number
}

type TPoint = {
  answer_count: number
  const_points: string
  correct_answer_count: number
  created_at: string
  id: number
  inter_ad_count: number
  points: number
  reward_ad_count: number
  reward_ad_points: number
  updated_at: string
  user_id: number
}
