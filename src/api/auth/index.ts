import axios from "axios";
import { TChangePasswordPayload, TPayload, TResponse } from "./type";
import BASE_URL from "@/constants/domin";
import API_ROUTES from "@/constants/apiRoutes";
import { TGenericResponse } from "../type";
import apiInstance from "../apiInstance";

const login = async (payload: TPayload) => {
  const { data } = await axios.post<TGenericResponse<TResponse>>(
    `${BASE_URL.PUBLIC_BASE_URL}${API_ROUTES.AUTH.LOGIN}`,
    payload.body
  );

  return data;
};

const changePassword = async (payload: TChangePasswordPayload) => {
  // const { data } = await axios.post<TGenericResponse<TResponse>>(
  //   `${BASE_URL.PUBLIC_BASE_URL}${API_ROUTES.AUTH.LOGIN}`,
  //   payload.body
  // );

  return payload;
};

const logout = async () => {
  const { data } = await apiInstance.post<TGenericResponse<TResponse>>(
    `${API_ROUTES.AUTH.LOGOUT}`
  );
  return data.response.api_token;
};
const clearStatistics = async () => {
  const { data } = await apiInstance.post<TGenericResponse<TResponse>>(
    `${API_ROUTES.AUTH.CLEARSTATISTICS}`
  );
  return data.response.api_token;
};

export { login, changePassword, logout, clearStatistics };
