import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "@/app/hooks";
import { setToken } from "@/features/auth/authTokenSlice";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { login, changePassword, logout, clearStatistics } from ".";
import { TPayload } from "./type";
import { TChangePasswordInitialValues } from "@/components/auth/changePassword/type";

const useAuthMutation = () => {
  const dispatch = useAppDispatch();
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["auth-mutation"],
    mutationFn: (payload: TPayload) => login(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        localStorage.setItem("token", JSON.stringify(data.response.api_token));
        localStorage.setItem("userData", JSON.stringify(data.response.user));

        dispatch(setToken(data.response.api_token));
        queryClient.invalidateQueries(["get-permissions-tree-by-role"]);
        // navigate("/");
        window.location.replace("/");
      }
    },
  });
};

const useChangePasswordMutation = () => {
  return useMutation({
    mutationKey: ["changePassword-mutation"],
    mutationFn: (payload: TChangePasswordInitialValues) => {
      delete payload.confirmPassword;
      const data = {
        body: payload,
      };
      return changePassword(data);
    },
    onSuccess(data, variables, context) {},
  });
};

const useLogoutMutation = () => {
  const navigate = useNavigate();

  return useMutation({
    mutationKey: ["logout-mutation"],
    mutationFn: () => {
      return logout();
    },
    onSuccess(data, variables, context) {
      navigate("/login");
      console.log(data);
    },
  });
};
const useClearStatisticsMutation = (handleClose: () => void) => {
  return useMutation({
    mutationKey: ["clear-statistics-mutation"],
    mutationFn: () => {
      return clearStatistics();
    },
    onSuccess(data, variables, context) {
      console.log(data);
      handleClose();
    },
  });
};
export {
  useAuthMutation,
  useChangePasswordMutation,
  useLogoutMutation,
  useClearStatisticsMutation,
};
