import { TChangePasswordValues } from '@/components/auth/changePassword/type'
import { TInitialValues } from '@/pages/login/type'
import { TProductCategory } from '../products/prices/type'
import { TCountry } from '../countries/type'
import { TPoints } from '../profile/type'

export type TPayload = {
  body: TInitialValues
}
export type TChangePasswordPayload = {
  body: TChangePasswordValues
}
export type TResponse = {
  user: TUser
  api_token: string
}
export type TUser = {
  id: number
  isAdmin: number
  img: string
  name: string
  email: string
  email_verified_at: null
  created_at: Date
  updated_at: Date
  fcm_token: null
  is_confirmed: number
  activation_code: string
  activation_code_exp_date: string
  device_id: null
  referral_id: string
  country_id: null
  country?: TCountry
  status: number
  deleted: number
  apple: number
  lang: string
  phone: null
  age: null
  gender: null
  ads_device_id: null
  support_chat: number
  roles: TRole[]
  role_id: number
  orders?: TPrize[]
  referred_count?: number
  tasks_sum_point?: number
  points?: TPoints
}

export type TPrize = {
  card_id: string
  created_at: string
  details: string
  id: string
  price: TProductCategory
  price_id: number
  purchase_price: number
  special_offer_id: null
  status: number
  updated_at: string
  user_id: string
}
export type TRole = {
  id: string
  name: string
  guard_name: string
}

export type TPivot = {
  model_id: number
  role_id: number
  model_type: string
}
