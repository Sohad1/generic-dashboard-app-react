import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TBuildNumberResponse, TBuildNumbersResponse, TPayload } from "./type";

const getBuildNumbers = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TBuildNumbersResponse>
  >(API_ROUTES.BUILD_NUMBERS.GET_BUILD_NUMBERS, { params: payload.params });
  return data;
};
const getBuildNumber = async (buildNumberId: string) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TBuildNumberResponse>
  >(API_ROUTES.BUILD_NUMBERS.GET_BUILD_NUMBER, {
    params: { id: buildNumberId },
  });
  return data;
};
const addBuildNumber = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.BUILD_NUMBERS.ADD_BUILD_NUMBER}`,
    payload.body
  );
  console.log(data);
  return data;
};
const editBuildNumber = async (payload: TPayload) => {
  const { data } = await apiInstance.post(
    `${API_ROUTES.BUILD_NUMBERS.UPDATE_BUILD_NUMBER}`,
    payload.body,
    {
      params: payload.params,
    }
  );
  console.log(data);
  return data;
};
const deleteBuildNumber = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.BUILD_NUMBERS.DELETE_BUILD_NUMBER,
    {
      params: { id },
    }
  );
  return data;
};
export {
  getBuildNumbers,
  getBuildNumber,
  addBuildNumber,
  editBuildNumber,
  deleteBuildNumber,
};
