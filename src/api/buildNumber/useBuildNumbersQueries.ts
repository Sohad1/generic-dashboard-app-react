import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import {
  addBuildNumber,
  deleteBuildNumber,
  editBuildNumber,
  getBuildNumber,
  getBuildNumbers,
} from ".";
import { TPayload } from "./type";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";

const useGetBuildNumbersInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "buildNumbers-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getBuildNumbers({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useGetBuildNumberQuery = (buildNumberId: string | undefined) => {
  return useQuery({
    queryKey: ["get-buildNumber-query"],
    queryFn: () => getBuildNumber(buildNumberId!),
    cacheTime: 0,
    enabled: !!buildNumberId,
  });
};
const useAddBuildNumberMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["add-buildNumber-mutation"],
    mutationFn: (payload: TPayload) => addBuildNumber(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/build_numbers");
      }
    },
  });
};
const useEditBuildNumberMutation = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: ["edit-buildNumber-mutation"],
    mutationFn: (payload: TPayload) => editBuildNumber(payload),
    onSuccess(data, variables, context) {
      if (data.code === 200) {
        navigate("/build_numbers");
      }
    },
  });
};
const useDeleteBuildNumberMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["delete-build-number-mutation"],
    mutationFn: (id: number) => deleteBuildNumber(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};

export {
  useGetBuildNumbersInfiniteQuery,
  useGetBuildNumberQuery,
  useAddBuildNumberMutation,
  useEditBuildNumberMutation,
  useDeleteBuildNumberMutation,
};
