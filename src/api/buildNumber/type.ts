import { TParams } from '@/pages/buildNumbers/addEditBuildNumber/hooks/type'
import { TGenericPaginationResponse, TGenericRequestPayload } from '../type'

export type TBuildNumbersResponse = {
  build_numbers: TGenericPaginationResponse<TBuildNumber>
}
export type TBuildNumberResponse = {
  build_number: TBuildNumber
}
export type TBuildNumber = {
  id?: number
  number: number
  type: string
  force: number
}
export type TPayload = TGenericRequestPayload<TBuildNumber, TParams>
