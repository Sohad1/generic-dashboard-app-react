import { TGenericPaginationResponse } from "../type";

export type TArchiveCompetitionsResponse = {
  competitions: TGenericPaginationResponse<TArchiveCompetition>;
};
export type TArchiveCompetition = {
  id?: number;
  title: string;
  description: string;
  points: number;
  image: null;
  image_url: string;
  click: number;
  status: string;
};
