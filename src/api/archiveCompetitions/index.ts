import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";

import { TArchiveCompetitionsResponse } from "./type";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
const getArchiveCompetitions = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<
    TGenericResponse<TArchiveCompetitionsResponse>
  >(API_ROUTES.ARCHIVE_COMPETITIONS.GET_ARCHIVE_COMPETITIONS, {
    params: payload.params,
  });
  return data;
};
const un_archiveCompetition = async (id: number) => {
  const { data } = await apiInstance.get(
    API_ROUTES.ARCHIVE_COMPETITIONS.UN_ARCHIVE_COMPETITIONS,
    {
      params: { id },
    }
  );
  return data;
};
export { getArchiveCompetitions, un_archiveCompetition };
