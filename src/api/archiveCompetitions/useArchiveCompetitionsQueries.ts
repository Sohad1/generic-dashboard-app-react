import { useInfiniteQuery, useMutation, useQuery } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { getArchiveCompetitions, un_archiveCompetition } from ".";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
const useGetArchiveCompetitionsInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      "archive-competitions-infinite-query",
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],
    queryFn: ({ pageParam = 1 }) => {
      return getArchiveCompetitions({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
const useUnArchiveCompetitionMutation = (handleCloseModal: () => void) => {
  return useMutation({
    mutationKey: ["un-archive-competition-mutation"],
    mutationFn: (id: number) => un_archiveCompetition(id),
    onSuccess(data, variables, context) {
      handleCloseModal();
    },
  });
};
export {
  useGetArchiveCompetitionsInfiniteQuery,
  useUnArchiveCompetitionMutation,
};
