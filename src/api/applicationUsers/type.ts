import { TCountry } from "../countries/type";
import { TGenericPaginationResponse } from "../type";

export type TAppUsersResponse = {
  app_users: TGenericPaginationResponse<TAppUsers>;
};

export type TAppUsers = {
  name: string;
  email: string;
  phone: null | string;
  email_verified_at: string;
  country_id: null | number;
  country: null | TCountry;

  user_points_history_count: number;
  referral_id: string;
  referred: [];
  status: number;
  top10_referral_exception: Ttop10_referral_exception;
};
type Ttop10_referral_exception = {
  id: number;
  user_id: number;
  status: number;
  created_at: string | null;
  updated_at: string | null;
};
