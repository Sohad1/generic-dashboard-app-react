import API_ROUTES from "@/constants/apiRoutes";
import apiInstance from "../apiInstance";
import { TAppUsersResponse } from "./type";
import {
  TGenericPaginationParams,
  TGenericResponse,
  TGenericRequestPayload,
  TIdParam,
} from "../type";
import { TApplicationUsersPaginationParams } from "@/pages/applicationUsers/type";
const getApplicationUsers = async (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>
) => {
  const { data } = await apiInstance.get<TGenericResponse<TAppUsersResponse>>(
    API_ROUTES.APP_USERS.GET_APP_USERS,
    {
      params: payload.params,
    }
  );
  return data.response.app_users;
};
export { getApplicationUsers };
