import { TFilterInitialValues } from "@/pages/applicationUsers/type";
import { useInfiniteQuery, useQuery } from "@tanstack/react-query";
import { getApplicationUsers } from ".";
import {
  TGenericPaginationParams,
  TGenericRequestPayload,
  TGenericResponse,
  TIdParam,
} from "../type";
const useGetApplicationUsersInfiniteQuery = (
  payload: TGenericRequestPayload<null, TGenericPaginationParams>,
  options: TFilterInitialValues
) => {
  const { params } = payload;
  return useInfiniteQuery({
    queryKey: [
      `application-users-infinite-query`,
      ...(options ? [options] : []),
      params.search_input,
      params.order_by,
      params.type,
      params.limit,
    ],

    queryFn: ({ pageParam = 1 }) => {
      return getApplicationUsers({
        params: {
          pagination: 1,
          page: pageParam,
          ...params,
        },
        ...options,
      });
    },
    getNextPageParam: (lastPage, pages) => {
      return true;
    },
  });
};
export { useGetApplicationUsersInfiniteQuery };
