import { TGenericPaginationParams } from "./../type";
import { TNameAndId } from "@/type";
import { TGenericPaginationResponse } from "../type";
import { TRole } from "../roles/type";
import { getPermissions } from ".";

export type TResponse = {
  permissions: TGenericPaginationResponse<TPermission>;
};
export type TPermissionResponseByRole = {
  permissions: TGenericPaginationResponse<TPermissionByRole>;
};
export type TPermissionTreeResponseByRole = {
  permissions: TGenericPaginationResponse<TPermissionTreeByRole>;
};
export type TPermissionResponse = {
  permission: TPermissionWithRoles;
};
export type TPermissionParentResponse = { permissions: { data: TNameAndId[] } };

export type TPermission = {
  id?: number;
  name: string;
  guard_name: string;
};

export type TPermissionByRole = TPermission & {
  id?: number;
  name: string;
  guard_name: string;
  parent_id: string | null;
  is_parent: 0 | 1;
};
export type TPermissionTreeByRole = TPermissionByRole & {
  children: TPermissionByRole[];
};
export type TPermissionRequestData = TPermission & {
  parent_id: string | null;
  is_parent: 0 | 1;
  roles: string[];
};
export type TPermissionWithRoles = {
  roles: TRole[];
  is_parent: 0 | 1;
  parent: TNameAndId | null;
} & TPermission;
