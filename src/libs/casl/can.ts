import { createContextualCan } from "@casl/react";
import { createContext } from "react";

export const AbilityContext = createContext(undefined!);
export const Can = createContextualCan(AbilityContext.Consumer);
