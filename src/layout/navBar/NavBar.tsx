import React from 'react'
import {
  Avatar,
  Badge,
  Box,
  IconButton,
  Toolbar,
  Typography,
} from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import NotificationsIcon from '@mui/icons-material/Notifications'
import ChatIcon from '@mui/icons-material/Chat'
import { AppBar } from '../style'
import TProps from './type'
import LanguageMenu from '../components/languageMenu/LanguageMenu'
import SearchInput from '@/components/items/inputField/searchInput'
import AccountMenu from '../components/accountMenu/AccountMenu'
import { useNavigate } from 'react-router-dom'
const NavBar = ({ open, handleDrawerOpen }: TProps) => {
  const navigate = useNavigate()
  return (
    <AppBar position="fixed" open={open} sx={{ background: '#3a3657' }}>
      <Toolbar>
        <Box
          sx={{
            display: 'flex',
            gap: 3,
            alignItems: 'center',
            width: '100%',
          }}
        >
          {!open && <Avatar src="/images/logo.jpg" alt=" hadya logo" />}
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon fontSize="small" />
          </IconButton>
          <SearchInput />

          <Box
            sx={{
              flexGrow: 5,
              display: 'flex',
              gap: 2,
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <IconButton onClick={() => navigate('/chat')}>
              <Badge badgeContent={4} color="secondary">
                <ChatIcon sx={{ color: 'common.white' }} />
              </Badge>
            </IconButton>
            <IconButton>
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon sx={{ color: 'common.white' }} />
              </Badge>
            </IconButton>
            <LanguageMenu />
            <AccountMenu />
          </Box>
        </Box>
      </Toolbar>
    </AppBar>
  )
}

export default NavBar
