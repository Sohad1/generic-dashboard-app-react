import * as React from "react";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import SettingsIcon from "@mui/icons-material/Settings";
import Button from "@mui/material/Button";
import useMenuLogic from "./MenuListButtons";
import MenuItem from "@mui/material/MenuItem";
import { Divider, Typography, ListItemIcon } from "@mui/material";
import { t } from "i18next";
import LogoutModal from "@/components/auth/logout/LogoutModal";
import ClearStatisticsModal from "@/components/auth/clearStatistics/ClearStatisticsModal";
import ChangePasswordModal from "@/components/auth/changePassword/ChangePasswordModal";
const path = "layout.nav.accountSettings.";
export default function AccountMenu() {
  const {
    MENU_LIST_BUTTONS,
    openLogoutModal,
    handleCloseLogoutModal,
    openClearStatisticsModal,
    handleCloseClearStatisticsModal,
    openChangePasswordModal,
    handleCloseChangePasswordModal,
  } = useMenuLogic();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Button
          onClick={handleClick}
          aria-controls={open ? "account-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
          variant="contained"
          endIcon={<SettingsIcon />}
        >
          settings
        </Button>

        {/* <Tooltip title={t('layout.nav.accountSettings.name')}> */}

        {/* </Tooltip> */}
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: MENU_STYLES,
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            gap: "10px",
            padding: "10px 50px",
            width: "100%",
            marginBottom: "10px",
          }}
        >
          <Avatar
            alt=""
            src=""
            sx={{ width: "80px !important", height: "80px !important" }}
          />
          <Typography sx={{ fontWeight: 700, fontSize: "16px" }}>
            User Name
          </Typography>
        </Box>
        <Divider />
        {MENU_LIST_BUTTONS.map((button, index) => (
          <MenuItem
            key={index}
            onClick={() => {
              button.handleClick();
              handleClose();
            }}
          >
            <ListItemIcon>{button.icon}</ListItemIcon>
            {t(path + button.name)}
          </MenuItem>
        ))}
      </Menu>

      <LogoutModal
        {...{
          open: openLogoutModal,
          handleClose: handleCloseLogoutModal,
        }}
      />
      <ClearStatisticsModal
        {...{
          open: openClearStatisticsModal,
          handleClose: handleCloseClearStatisticsModal,
        }}
      />
      {/* <ChangePasswordModal */}
      <ChangePasswordModal
        open={openChangePasswordModal}
        handleClose={handleCloseChangePasswordModal}
        handleCloseModel={handleCloseChangePasswordModal}
      />
    </React.Fragment>
  );
}

const MENU_STYLES = {
  overflow: "visible",
  filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
  mt: 1.5,
  "& .MuiAvatar-root": {
    width: 32,
    height: 32,
    ml: -0.5,
    mr: 1,
  },
  "&:before": {
    content: '""',
    display: "block",
    position: "absolute",
    top: 0,
    right: 14,
    width: 10,
    height: 10,
    bgcolor: "background.paper",
    transform: "translateY(-50%) rotate(45deg)",
    zIndex: 0,
  },
};
