import PasswordIcon from "@mui/icons-material/Password";
import Logout from "@mui/icons-material/Logout";
import CardGiftcardIcon from "@mui/icons-material/CardGiftcard";
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

const useMenuLogic = () => {
  const navigate = useNavigate();

  const [openLogoutModal, setOpenLogoutModal] = useState(false);
  const handleLogoutProfile = () => {
    setOpenLogoutModal(true);
  };
  const handleCloseLogoutModal = () => setOpenLogoutModal(false);

  const [openClearStatisticsModal, setOpenClearStatisticsModal] =
    useState(false);
  const handleClearStatistics = () => {
    setOpenClearStatisticsModal(true);
  };
  const handleCloseClearStatisticsModal = () =>
    setOpenClearStatisticsModal(false);

  const [openChangePasswordModal, setOpenChangePasswordModal] = useState(false);
  const handleChangePasswordModal = () => {
    setOpenChangePasswordModal(true);
  };
  const handleCloseChangePasswordModal = () =>
    setOpenChangePasswordModal(false);

  const MENU_LIST_BUTTONS = [
    {
      name: "sendnotification",
      icon: <CircleNotificationsIcon fontSize="small" />,
      // path: ",
      handleClick: () => {
        navigate("/Notifications/AddNotifications");
      },
      path: "",
    },
    {
      name: "sendgift",
      icon: <CardGiftcardIcon fontSize="small" />,
      path: "/gifts/addGift",
      handleClick: () => {
        navigate("/gifts/addGift");
      },
    },
    {
      name: "clearStatistics",
      icon: <PasswordIcon fontSize="small" />,
      handleClick: () => {
        handleClearStatistics();
      },
      path: "",
    },
    {
      name: "changePassword",
      icon: <PasswordIcon fontSize="small" />,
      path: "",
      handleClick: () => {
        handleChangePasswordModal();
      },
    },
    {
      name: "logout",
      icon: <Logout fontSize="small" />,
      handleClick: () => {
        handleLogoutProfile();
      },
      path: "",
    },
  ];

  return {
    MENU_LIST_BUTTONS,
    openLogoutModal,
    handleCloseLogoutModal,
    openClearStatisticsModal,
    handleCloseClearStatisticsModal,
    openChangePasswordModal,
    handleCloseChangePasswordModal,
  };
};
export default useMenuLogic;
