import { IconButton, Menu, MenuItem } from "@mui/material";
import TranslateIcon from "@mui/icons-material/Translate";
import useAnchorEle from "@/hooks/useAnchorEle";
import { useTranslation } from "react-i18next";
import i18n from "@/localization/I18next";
import { useAppDispatch } from "@/app/hooks";
import { switchLang } from "@/features/localization/localizationSlice";
const LanguageMenu = () => {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const [open, anchorEl, handleClick, handleClose] = useAnchorEle();
  const handleChangeLanguage = (lang: "en" | "ar") => {
    i18n.changeLanguage(lang);
    dispatch(switchLang(lang));
    document.documentElement.lang = lang;
    localStorage.setItem("lang", lang);
    switch (lang) {
      case "ar":
        document.dir = "rtl";
        break;
      default:
        document.dir = "ltr";
    }
  };
  return (
    <>
      <IconButton
        id="language-menu-button"
        aria-controls={open ? "language-menu" : undefined}
        aria-haspopup={"true"}
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        <TranslateIcon sx={{ color: "common.white" }} />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "language-menu-button",
        }}
      >
        <MenuItem
          onClick={() => {
            handleChangeLanguage("en");
          }}
        >
          {t("languages.en")}
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleChangeLanguage("ar");
          }}
        >
          {t("languages.ar")}
        </MenuItem>
      </Menu>
    </>
  );
};

export default LanguageMenu;
