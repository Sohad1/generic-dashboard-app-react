import PersonIcon from "@mui/icons-material/Person";
import HomeIcon from "@mui/icons-material/Home";
import CategoryIcon from "@mui/icons-material/Category";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import PanoramaFishEyeIcon from "@mui/icons-material/PanoramaFishEye";
import SecurityIcon from "@mui/icons-material/Security";
import SettingsIcon from "@mui/icons-material/Settings";
import ArticleIcon from "@mui/icons-material/Article";
import CardGiftcardIcon from "@mui/icons-material/CardGiftcard";
import AssistantPhotoIcon from "@mui/icons-material/AssistantPhoto";
import ArtTrackRoundedIcon from "@mui/icons-material/ArtTrackRounded";
import { EUserTypes } from "@/api/users/type";
import MilitaryTechIcon from "@mui/icons-material/MilitaryTech";
import PublicIcon from "@mui/icons-material/Public";
import EditCalendarIcon from "@mui/icons-material/EditCalendar";
import ModeCommentIcon from "@mui/icons-material/ModeComment";
import { EReportsTypes } from "@/api/reports/type";
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import DeleteSweepIcon from "@mui/icons-material/DeleteSweep";
import { useContext } from "react";
import ArtTrackIcon from "@mui/icons-material/ArtTrack";
import { AbilityContext } from "@/libs/casl/can";
type THandleNameParams = {
  name: string;
  childName?: string;
  isChild?: boolean;
};
type THandleName = ({ name, childName, isChild }: THandleNameParams) => string;

const handleName: THandleName = ({ name, childName, isChild }) => {
  if (isChild) return `layout.drawer.${name}.children.${childName}`;
  else return `layout.drawer.${name}.name`;
};

const ICON_STYLES = {
  fontSize: 15,
};

const useNavLink = () => {
  const ability = useContext(AbilityContext);
  return [
    [
      ...(ability.can("index", "dashboard_api_reports")
        ? [
            {
              name: handleName({ name: "home", isChild: false }),
              href: "/",
              icon: <HomeIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_reports")
        ? [
            {
              name: handleName({ name: "reports", isChild: false }),
              href: "/reports",
              icon: <ArticleIcon />,
              children: [
                {
                  name: handleName({
                    name: "reports",
                    childName: "main",
                    isChild: true,
                  }),
                  href: `/reports?type=${EReportsTypes.main}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "reports",
                    childName: "play",
                    isChild: true,
                  }),
                  href: `/reports?type=${EReportsTypes.play}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "reports",
                    childName: "user-emails",
                    isChild: true,
                  }),
                  href: `/reports?type=${EReportsTypes.email}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),

      ...(ability.can("index", "dashboard_api_user-referral-exception") ||
      ability.can("index", "dashboard_api_app_users") ||
      ability.can("index", "dashboard_api_blocked_users")
        ? [
            {
              name: handleName({ name: "AppUsers", isChild: false }),
              href: "/AppUsers",
              icon: <PeopleAltIcon />,
              children: [
                {
                  name: handleName({
                    name: "AppUsers",
                    childName: "appusers",
                    isChild: true,
                  }),
                  href: "/app_users",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "AppUsers",
                    childName: "referralexception",
                    isChild: true,
                  }),
                  href: "/user-referral-exception",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "AppUsers",
                    childName: "blockedUsers",
                    isChild: true,
                  }),
                  href: "/blockedUsers",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),

      ...(ability.can("index", "dashboard_api_archive") ||
      ability.can("index", "dashboard_api_orders") ||
      ability.can("index", "dashboard_api_paused")
        ? [
            {
              name: handleName({ name: "orders", isChild: false }),
              href: "/orders",
              icon: <ContentPasteIcon />,
              children: [
                {
                  name: handleName({
                    name: "orders",
                    childName: "orders",
                    isChild: true,
                  }),
                  href: `/orders`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "orders",
                    childName: "paused",
                    isChild: true,
                  }),
                  href: `/orders/paused`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "orders",
                    childName: "archive",
                    isChild: true,
                  }),
                  href: `/orders/archive`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                // {
                //  name: handleName({
                //    name: "orders",
                //    childName: "reports",
                // isChild: true,
                // }),
                //  href: `/orders/reports`,
                //  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                // },
              ],
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_delete_orders")
        ? [
            {
              name: handleName({ name: "DeleteOrders", isChild: false }),
              href: "/deleteOrders",
              icon: <DeleteSweepIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_collections")
        ? [
            {
              name: handleName({ name: "collections", isChild: false }),
              href: "/collection",
              icon: <AssistantPhotoIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_countries")
        ? [
            {
              name: handleName({ name: "countries", isChild: false }),
              href: "/countries",
              icon: <PublicIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_prices") ||
      ability.can("index", "dashboard_api_categories") ||
      ability.can("index", "dashboard_api_companies") ||
      ability.can("index", "dashboard_api_groups") ||
      ability.can("index", "dashboard_api_cards")
        ? [
            {
              name: handleName({ name: "products", isChild: false }),
              href: "/products",
              icon: <CategoryIcon />,
              children: [
                {
                  name: handleName({
                    name: "products",
                    childName: "groups",
                    isChild: true,
                  }),
                  href: `/groups`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "products",
                    childName: "companies",
                    isChild: true,
                  }),
                  href: `/companies`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "products",
                    childName: "products",
                    isChild: true,
                  }),
                  href: `/products`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "products",
                    childName: "productsCategories",
                    isChild: true,
                  }),
                  href: `/productsCategories`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },

                {
                  name: handleName({
                    name: "products",
                    childName: "cards",
                    isChild: true,
                  }),
                  href: `/cards`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_tasks")
        ? [
            {
              name: handleName({ name: "tasks", isChild: false }),
              href: "/tasks",
              icon: <EditCalendarIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_ads") ||
      ability.can("index", "dashboard_api_ad_walls")
        ? [
            {
              name: handleName({ name: "Ads", isChild: false }),
              href: "/Ads",
              icon: <ArtTrackIcon />,
              children: [
                {
                  name: handleName({
                    name: "Ads",
                    childName: "Ad",
                    isChild: true,
                  }),
                  href: "/Ad",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "Ads",
                    childName: "ad walls",
                    isChild: true,
                  }),
                  href: "/ad walls",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),

      ...(ability.can("index", "dashboard_api_notifications") ||
      ability.can("index", "dashboard_api_gifts")
        ? [
            {
              name: handleName({ name: "Notification", isChild: false }),
              href: "/Notifications",
              icon: <CircleNotificationsIcon />,
              children: [
                {
                  name: handleName({
                    name: "Notification",
                    childName: "Notifications",
                    isChild: true,
                  }),
                  href: "/Notifications",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "Notification",
                    childName: "Gifts",
                    isChild: true,
                  }),
                  href: "/gifts",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),

      ...(ability.can("index", "dashboard_api_competitions") ||
      ability.can("index", "dashboard_api_archive_competitions")
        ? [
            {
              name: handleName({ name: "competition", isChild: false }),
              href: "/competition",
              icon: <MilitaryTechIcon />,
              children: [
                {
                  name: handleName({
                    name: "competition",
                    childName: "competitions",
                    isChild: true,
                  }),
                  href: "/competitions",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "competition",
                    childName: "archive_competitions",
                    isChild: true,
                  }),
                  href: "/archive_competitions",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),

      ...(ability.can("index", "dashboard_api_users")
        ? [
            {
              name: handleName({ name: "users", isChild: false }),
              href: "/users",
              icon: <PersonIcon />,
              children: [
                {
                  name: handleName({
                    name: "users",
                    childName: EUserTypes.user,
                    isChild: true,
                  }),
                  href: `/users?type=${EUserTypes.user}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "users",
                    childName: EUserTypes.admin,
                    isChild: true,
                  }),
                  href: `/users?type=${EUserTypes.admin}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "users",
                    childName: EUserTypes.superAdmin,
                    isChild: true,
                  }),
                  href: `/users?type=${EUserTypes.superAdmin}`,
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_roles") ||
      ability.can("index", "dashboard_api_permission")
        ? [
            {
              name: handleName({ name: "roleAndPermission", isChild: false }),
              href: "/role-and-permission",
              icon: <ManageAccountsIcon />,
              children: [
                {
                  name: handleName({
                    name: "roleAndPermission",
                    childName: "roles",
                    isChild: true,
                  }),
                  href: "/roles",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
                {
                  name: handleName({
                    name: "roleAndPermission",
                    childName: "permissions",
                    isChild: true,
                  }),
                  href: "/permissions",
                  icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
                },
              ],
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_message_templates")
        ? [
            {
              name: handleName({ name: "Message_template", isChild: false }),
              href: "/Message_template",
              icon: <ModeCommentIcon />,
            },
          ]
        : []),
      ...(ability.can("index", "dashboard_api_settings")
        ? [
            {
              name: handleName({ name: "generalSetting", isChild: false }),
              href: "/general-setting",
              icon: <SettingsIcon />,
              children: [
                {
                  name: handleName({
                    name: "generalSetting",
                    childName: "updateValue",
                    isChild: true,
                  }),
                  href: "/build_numbers",
                  icon: <PanoramaFishEyeIcon sx={{ fontSize: 15 }} />,
                },
                {
                  name: handleName({
                    name: "generalSetting",
                    childName: "settings",
                    isChild: true,
                  }),
                  href: "/settings",
                  icon: <PanoramaFishEyeIcon sx={{ fontSize: 15 }} />,
                },
              ],
            },
          ]
        : []),
    ],
  ].map((section, outerIndex) =>
    section.map((navLink, innerIndex) => ({
      id: `${outerIndex}${innerIndex}`,
      ...navLink,
    }))
  );
};

export default useNavLink;
