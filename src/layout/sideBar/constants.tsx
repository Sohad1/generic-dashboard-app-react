import PersonIcon from "@mui/icons-material/Person";
import HomeIcon from "@mui/icons-material/Home";
import CategoryIcon from "@mui/icons-material/Category";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import PanoramaFishEyeIcon from "@mui/icons-material/PanoramaFishEye";
import SecurityIcon from "@mui/icons-material/Security";
import SettingsIcon from "@mui/icons-material/Settings";
import ArticleIcon from "@mui/icons-material/Article";
import CardGiftcardIcon from "@mui/icons-material/CardGiftcard";
import QuizIcon from "@mui/icons-material/Quiz";
import ArchiveIcon from "@mui/icons-material/Archive";
import ArtTrackRoundedIcon from "@mui/icons-material/ArtTrackRounded";
import AssistantPhotoIcon from "@mui/icons-material/AssistantPhoto";
import { EUserTypes } from "@/api/users/type";
import PublicIcon from "@mui/icons-material/Public";
import CircleNotificationsIcon from "@mui/icons-material/CircleNotifications";
import { EReportsTypes } from "@/api/reports/type";

type THandleNameParams = {
  name: string;
  childName?: string;
  isChild?: boolean;
};
type THandleName = ({ name, childName, isChild }: THandleNameParams) => string;

const handleName: THandleName = ({ name, childName, isChild }) => {
  if (isChild) return `layout.drawer.${name}.children.${childName}`;
  else return `layout.drawer.${name}.name`;
};

const ICON_STYLES = {
  fontSize: 15,
};

const NAV_LINKS = [
  [
    {
      name: handleName({ name: "home", isChild: false }),
      href: "/",
      icon: <HomeIcon />,
    },
    {
      name: handleName({ name: "Notifications", isChild: false }),
      href: "/Notifications",
      icon: <CircleNotificationsIcon />,
    },
    {
      name: handleName({ name: "Message_template", isChild: false }),
      href: "/Message_template",
      icon: <ArticleIcon />,
    },
    {
      name: handleName({ name: "reports", isChild: false }),
      href: "/reports",
      icon: <ArticleIcon />,
      children: [
        {
          name: handleName({
            name: "reports",
            childName: "main",
            isChild: true,
          }),
          href: `/reports?type=${EReportsTypes.main}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "reports",
            childName: "play",
            isChild: true,
          }),
          href: `/reports?type=${EReportsTypes.play}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "reports",
            childName: "user-emails",
            isChild: true,
          }),
          href: `/reports?type=${EReportsTypes.email}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },
    {
      name: handleName({ name: "ad walls", isChild: false }),
      href: "/ad walls",
      icon: <ArtTrackRoundedIcon />,
    },

    {
      name: handleName({ name: "Ad", isChild: false }),
      href: "/Ad",
      icon: <ArtTrackRoundedIcon />,
    },
    {
      name: handleName({ name: "collections", isChild: false }),
      href: "/collection",
      icon: <AssistantPhotoIcon />,
    },

    {
      name: handleName({ name: "permissions", isChild: false }),
      href: "/permissions",

      icon: <SecurityIcon />,
    },
    {
      name: handleName({ name: "users", isChild: false }),
      href: "/users",
      icon: <PersonIcon />,
      children: [
        {
          name: handleName({
            name: "users",
            childName: EUserTypes.user,
            isChild: true,
          }),
          href: `/users?type=${EUserTypes.user}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "users",
            childName: EUserTypes.admin,
            isChild: true,
          }),
          href: `/users?type=${EUserTypes.admin}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "users",
            childName: EUserTypes.superAdmin,
            isChild: true,
          }),
          href: `/users?type=${EUserTypes.superAdmin}`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },
    {
      name: handleName({ name: "orders", isChild: false }),
      href: "/orders",
      icon: <PersonIcon />,
      children: [
        {
          name: handleName({
            name: "orders",
            childName: "orders",
            isChild: true,
          }),
          href: `/orders`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "orders",
            childName: "paused",
            isChild: true,
          }),
          href: `/orders/paused`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "orders",
            childName: "archive",
            isChild: true,
          }),
          href: `/orders/archive`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "orders",
            childName: "reports",
            isChild: true,
          }),
          href: `/orders/reports`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },
    {
      name: handleName({ name: "products", isChild: false }),
      href: "/products",
      icon: <PersonIcon />,
      children: [
        {
          name: handleName({
            name: "products",
            childName: "products",
            isChild: true,
          }),
          href: `/products`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "products",
            childName: "productsCategories",
            isChild: true,
          }),
          href: `/productsCategories`,
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },
    {
      name: handleName({ name: "countries", isChild: false }),
      href: "/countries",
      icon: <PublicIcon />,
    },

    {
      name: handleName({ name: "category", isChild: false }),
      href: "/category",
      icon: <CategoryIcon />,
      children: [
        {
          name: handleName({
            name: "category",
            childName: "categories",
            isChild: true,
          }),
          href: "/categories",
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "category",
            childName: "subCategory",
            isChild: true,
          }),
          href: "/sub-category",
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },

    {
      name: handleName({ name: "Gifts", isChild: false }),
      href: "/gifts",
      icon: <CardGiftcardIcon />,
    },
    {
      name: handleName({ name: "tasks", isChild: false }),
      href: "/tasks",
      icon: <PublicIcon />,
    },
    {
      name: handleName({ name: "roleAndPermission", isChild: false }),
      href: "/role-and-permission",
      icon: <ManageAccountsIcon />,
      children: [
        {
          name: handleName({
            name: "roleAndPermission",
            childName: "roles",
            isChild: true,
          }),
          href: "/roles",
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
        {
          name: handleName({
            name: "roleAndPermission",
            childName: "permissions",
            isChild: true,
          }),
          href: "/permissions",
          icon: <PanoramaFishEyeIcon sx={ICON_STYLES} />,
        },
      ],
    },
    {
      name: handleName({ name: "generalSetting", isChild: false }),
      href: "/general-setting",
      icon: <SettingsIcon />,
      children: [
        {
          name: handleName({
            name: "generalSetting",
            childName: "updateValue",
            isChild: true,
          }),
          href: "/build_numbers",
          icon: <PanoramaFishEyeIcon sx={{ fontSize: 15 }} />,
        },
        {
          name: handleName({
            name: "generalSetting",
            childName: "settings",
            isChild: true,
          }),
          href: "/settings",
          icon: <PanoramaFishEyeIcon sx={{ fontSize: 15 }} />,
        },
      ],
    },
  ],
].map((section, outerIndex) =>
  section.map((navLink, innerIndex) => ({
    id: `${outerIndex}${innerIndex}`,
    ...navLink,
  }))
);
export default NAV_LINKS;
