import { Fragment, useState } from 'react'
import { useLocation } from 'react-router-dom'
import {
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  IconButton,
  Collapse,
  Tooltip,
  Box,
  Avatar,
} from '@mui/material'

import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import { Drawer, DrawerHeader, LinkSx } from '../style'
import IProps from './type'
import OptionalLink from '@/components/items/Links/OptionalLink'
import RouterLink from '@/components/items/Links/RouterLink'
import { useTranslation } from 'react-i18next'
import useNavLink from './hook/useNavLink'

const SideBar = ({ open, handleDrawerClose }: IProps) => {
  const [openItem, setOpenItem] = useState<[boolean, string]>([true, ''])
  const { pathname } = useLocation()
  const { t } = useTranslation()
  const NAV_LINKS = useNavLink()
  return (
    <Drawer variant="permanent" open={open} sx={{ background: '#1e1740' }}>
      <DrawerHeader
        sx={{
          background: '#3a3657',
          color: '#eee',
          height: '65px',
          position: 'sticky',
          top: 0,
          zIndex: 9,
          justifyContent: 'space-between',
          display: 'flex',
        }}
      >
        <Avatar
          sx={{ mx: 'auto', width: 56, height: 56 }}
          sizes="large"
          src="/images/logo.jpg"
          alt=" hadya logo"
        />
        {/* <Box
          component={"img"}
          sx={{ objectFit: "contain", width: "100%", height: "200%" }}
          src="/images/logo.jpg"
          alt=" hadya logo"
        /> */}
        <IconButton onClick={handleDrawerClose}>
          <ChevronRightIcon sx={{ color: '#fff' }} />
        </IconButton>
      </DrawerHeader>
      <Box
        sx={{
          background: '#1e1740',
          height: 'calc(100vh - 65px)',
          overflowY: 'scroll',
          '&::-webkit-scrollbar': {
            width: '5px',
          },
          '&::-webkit-scrollbar-thumb': {
            background: '#3a3657',
          },
        }}
      >
        <List
          disablePadding
          sx={{
            background: '#1e1740',
            // height: 'calc(100vh - 65px)',
            // overflow: 'hidden',
          }}
        >
          {NAV_LINKS.map((section, index) => {
            return (
              <Fragment key={index}>
                <Divider />
                <List disablePadding>
                  {section.map((navLink) => (
                    <Fragment key={navLink.id}>
                      <OptionalLink
                        withLink={!navLink.children}
                        sx={{
                          textDecoration: 'none !important',
                          color: '#ddd',
                          '&:hover': { color: '#fff' },
                        }}
                        href={navLink.href}
                      >
                        <Tooltip
                          key={navLink.id}
                          title={open ? '' : t(navLink.name)}
                        >
                          <ListItem disablePadding sx={LinkSx}>
                            <ListItemButton
                              selected={
                                pathname === navLink.href && !navLink.children
                              }
                              sx={{
                                minHeight: 48,
                                justifyContent: open ? 'initial' : 'center',
                                px: 2.5,
                              }}
                              onClick={() =>
                                setOpenItem(([prevState, prevId]) => [
                                  prevId !== navLink.id ? true : !prevState,
                                  navLink.id,
                                ])
                              }
                            >
                              <ListItemIcon
                                sx={{
                                  minWidth: 0,
                                  mr: open ? 1 : 'auto',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                  color: '#ddd',
                                }}
                              >
                                {navLink.icon}
                              </ListItemIcon>
                              <ListItemText>
                                <Stack
                                  direction={'row'}
                                  justifyContent="space-between"
                                  sx={{
                                    opacity: open ? 1 : 0,
                                    ...(!navLink.href && {
                                      color: '#ddd',
                                      fontWeight: '550',
                                    }),
                                    color: '#ddd',
                                  }}
                                >
                                  {t(navLink.name)}
                                  {navLink.children &&
                                    (openItem[1] === navLink.id &&
                                    openItem[0] ? (
                                      <ExpandLess sx={{ color: '#ddd' }} />
                                    ) : (
                                      <ExpandMore sx={{ color: '#ddd' }} />
                                    ))}
                                </Stack>
                              </ListItemText>
                            </ListItemButton>
                          </ListItem>
                        </Tooltip>
                      </OptionalLink>
                      <Collapse in={openItem[0] && openItem[1] === navLink.id}>
                        <List
                          component="div"
                          disablePadding
                          sx={{ background: '#160f36' }}
                        >
                          {navLink.children?.map((navLinkChild, index) => (
                            <RouterLink
                              sx={{
                                textDecoration: 'none !important',
                                color: '#ddd',
                                '&:hover': { color: '#fff' },
                              }}
                              key={index}
                              href={navLinkChild.href}
                            >
                              <Tooltip
                                key={index}
                                title={open ? '' : t(navLinkChild.name)}
                              >
                                <ListItem disablePadding sx={LinkSx}>
                                  <ListItemButton
                                    selected={pathname === navLinkChild.href}
                                    sx={{
                                      minHeight: 48,
                                      justifyContent: open
                                        ? 'initial'
                                        : 'center',
                                      pl: open ? 4 : 2,
                                    }}
                                  >
                                    <ListItemIcon
                                      sx={{
                                        minWidth: 0,
                                        mr: open ? 1 : 'auto',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        color: '#ddd',
                                      }}
                                    >
                                      {navLinkChild.icon}
                                    </ListItemIcon>
                                    <ListItemText>
                                      <Stack
                                        direction={'row'}
                                        justifyContent="space-between"
                                        sx={{
                                          opacity: open ? 1 : 0,
                                          ...(!navLinkChild.href && {
                                            color: '#ddd',
                                            fontWeight: '550',
                                          }),
                                        }}
                                      >
                                        {t(navLinkChild.name)}
                                      </Stack>
                                    </ListItemText>
                                  </ListItemButton>
                                </ListItem>
                              </Tooltip>
                            </RouterLink>
                          ))}
                        </List>
                      </Collapse>
                    </Fragment>
                  ))}
                </List>
              </Fragment>
            )
          })}
        </List>
      </Box>
    </Drawer>
  )
}

export default SideBar
