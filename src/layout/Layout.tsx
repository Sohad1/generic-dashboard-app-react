import Box from "@mui/material/Box";
import NavBar from "./navBar/NavBar";
import useToggleEle from "@/hooks/useToggleEle";
import { DrawerHeader } from "./style";
import SideBar from "./sideBar/SideBar";
import { Outlet } from "react-router-dom";
export default function MiniDrawer() {
  const [open, handleDrawerOpen, handleDrawerClose] = useToggleEle(true);

  return (
    <Box sx={{ display: "flex" }}>
      <NavBar open={open} handleDrawerOpen={handleDrawerOpen} />
      <SideBar open={open} handleDrawerClose={handleDrawerClose} />
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        <Outlet />
      </Box>
    </Box>
  );
}
