export type TOrder = "asc" | "desc";
export type TRow = { [x: string]: string | number };
export interface HeadCell {
  disablePadding: boolean;
  id: string;
  label: string;
  numeric: boolean;
}
