export type TNameAndId = {
  id: string;
  name: string;
};
