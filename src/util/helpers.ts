export const createFormData = (data: object) => {
  let formData = new FormData()

  for (const [key, value] of Object.entries(data)) {
    if (value === undefined || value === null) continue

    if (Array.isArray(value)) {
      for (const item of value) {
        formData.append(key, item)
      }
    } else formData.append(key, value)
  }
  return formData
}

// type InputObject = {
//     [key: string]: string | number | Blob | InputObject | (string | number | Blob | InputObject)[];
//   };

//   function objectToFormData(obj: InputObject, formData: FormData = new FormData(), parentKey?: string): FormData {
//     for (const key in obj) {
//       if (obj.hasOwnProperty(key)) {
//         const value = obj[key];
//         const fullKey = parentKey ? ${parentKey}[${key}] : key;

//         if (Array.isArray(value)) {
//           for (let i = 0; i < value.length; i++) {
//             const arrayValue = value[i];
//             const arrayKey = ${fullKey}[${i}];

//             if (typeof arrayValue === 'object' && !(arrayValue instanceof Blob)) {
//               objectToFormData(arrayValue, formData, arrayKey);
//             } else {
//               formData.append(arrayKey, arrayValue);
//             }
//           }
//         } else if (typeof value === 'object' && !(value instanceof Blob)) {
//           objectToFormData(value, formData, fullKey);
//         } else {
//           formData.append(fullKey, value);
//         }
//       }
//     }

//     return formData;
//   }
